-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 24, 2017 at 01:23 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lovetravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `about_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `descr` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `about_id`, `lang_id`, `descr`) VALUES
(1, 1, 1, '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>The first bakery I ever worked in was called The Bread Garden in Berkeley, Ca. It was way ahead of its time, predating legendary bakeries like Acme Bread in Berkeley, Ca and Bread Alone in Boiceville, NY. These were the bakeries that started making Artisanal european style breads before anyone in the US knew what they were. To Americans, bread was white, squishy, pre-sliced mediocrity found in the bread aisle at the grocery store.</p>\r\n\r\n<p>David Morris, owner of The Bread Garden, died of cancer at the young age of 65 in April. He was one of the few certified Master Bakers in the US; a quirky, generous man with an interest in beekeeping, wine making and homebrewing beer. As a homebrewer myself, we shared an interest, and he shared his famous Malted Barley Bread recipe.</p>\r\n</body>\r\n</html>\r\n'),
(2, 1, 2, '<html>\n<head>\n	<title></title>\n</head>\n<body>\n<p>The first bakery I ever worked in was called The Bread Garden in Berkeley, Ca. It was way ahead of its time, predating legendary bakeries like Acme Bread in Berkeley, Ca and Bread Alone in Boiceville, NY. These were the bakeries that started making Artisanal european style breads before anyone in the US knew what they were. To Americans, bread was white, squishy, pre-sliced mediocrity found in the bread aisle at the grocery store.</p>\n\n<p>David Morris, owner of The Bread Garden, died of cancer at the young age of 65 in April. He was one of the few certified Master Bakers in the US; a quirky, generous man with an interest in beekeeping, wine making and homebrewing beer. As a homebrewer myself, we shared an interest, and he shared his famous Malted Barley Bread recipe.</p>\n</body>\n</html>\n'),
(3, 1, 3, '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>The first bakery I ever worked in was called The Bread Garden in Berkeley, Ca. It was way ahead of its time, predating legendary bakeries like Acme Bread in Berkeley, Ca and Bread Alone in Boiceville, NY. These were the bakeries that started making Artisanal european style breads before anyone in the US knew what they were. To Americans, bread was white, squishy, pre-sliced mediocrity found in the bread aisle at the grocery store.</p>\r\n\r\n<p>David Morris, owner of The Bread Garden, died of cancer at the young age of 65 in April. He was one of the few certified Master Bakers in the US; a quirky, generous man with an interest in beekeeping, wine making and homebrewing beer. As a homebrewer myself, we shared an interest, and he shared his famous Malted Barley Bread recipe.</p>\r\n</body>\r\n</html>\r\n'),
(7, 1, 4, '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>The first bakery I ever worked in was called The Bread Garden in Berkeley, Ca. It was way ahead of its time, predating legendary bakeries like Acme Bread in Berkeley, Ca and Bread Alone in Boiceville, NY. These were the bakeries that started making Artisanal european style breads before anyone in the US knew what they were. To Americans, bread was white, squishy, pre-sliced mediocrity found in the bread aisle at the grocery store.</p>\r\n\r\n<p>David Morris, owner of The Bread Garden, died of cancer at the young age of 65 in April. He was one of the few certified Master Bakers in the US; a quirky, generous man with an interest in beekeeping, wine making and homebrewing beer. As a homebrewer myself, we shared an interest, and he shared his famous Malted Barley Bread recipe.</p>\r\n</body>\r\n</html>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `admin_id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`admin_id`, `email`, `password`) VALUES
(1, 'admin@eurotrade.ge', '1');

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `blog_id` int(11) NOT NULL,
  `status_id` smallint(6) NOT NULL DEFAULT '0' COMMENT '0 - Inactive, 1 - Active, 2 - Deleted',
  `cat_id` int(11) NOT NULL,
  `photos_cnt` smallint(6) NOT NULL DEFAULT '0',
  `photo_ver` int(11) NOT NULL DEFAULT '0',
  `insert_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`blog_id`, `status_id`, `cat_id`, `photos_cnt`, `photo_ver`, `insert_date`) VALUES
(1, 2, 0, 1, 6, '2017-04-22 22:23:45'),
(2, 2, 0, 1, 3, '2017-04-22 22:52:29'),
(3, 2, 0, 1, 2, '2017-04-22 22:53:34'),
(4, 2, 0, 0, 1, '2017-04-23 17:25:35'),
(5, 2, 0, 0, 1, '2017-04-23 17:26:02'),
(6, 2, 0, 0, 1, '2017-04-23 17:50:10'),
(7, 2, 0, 0, 1, '2017-04-23 17:51:42'),
(8, 2, 0, 1, 5, '2017-05-02 13:41:30'),
(9, 1, 1, 1, 4, '2017-05-03 09:07:29'),
(10, 1, 2, 1, 6, '2017-05-03 09:08:29'),
(11, 1, 1, 1, 3, '2017-05-05 07:50:47');

-- --------------------------------------------------------

--
-- Table structure for table `blog_cats`
--

CREATE TABLE `blog_cats` (
  `cat_id` int(11) NOT NULL,
  `status_id` smallint(6) NOT NULL DEFAULT '0' COMMENT '0 - Inactive, 1 - Active, 2 - Deleted',
  `sort_order` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog_cats`
--

INSERT INTO `blog_cats` (`cat_id`, `status_id`, `sort_order`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `blog_cat_descr`
--

CREATE TABLE `blog_cat_descr` (
  `id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog_cat_descr`
--

INSERT INTO `blog_cat_descr` (`id`, `cat_id`, `lang_id`, `title`) VALUES
(1, 1, 1, 'უახლესი ბლოგები'),
(2, 1, 2, 'უახლესი ბლოგები'),
(3, 1, 3, 'უახლესი ბლოგები'),
(4, 2, 1, 'დასვენება ზღვაზე'),
(5, 2, 2, 'დასვენება ზღვაზე'),
(6, 2, 3, 'დასვენება ზღვაზე'),
(7, 3, 1, 'სამოგზაურო ხარჯების შემცირების კრეატიული გზები'),
(8, 3, 2, 'სამოგზაურო ხარჯების შემცირების კრეატიული გზები'),
(9, 3, 3, 'სამოგზაურო ხარჯების შემცირების კრეატიული გზები');

-- --------------------------------------------------------

--
-- Table structure for table `blog_descr`
--

CREATE TABLE `blog_descr` (
  `id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `short_descr` text NOT NULL,
  `descr` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blog_descr`
--

INSERT INTO `blog_descr` (`id`, `blog_id`, `lang_id`, `title`, `short_descr`, `descr`) VALUES
(1, 1, 1, 'First signs of gum disease', 'Gums disease is preceded by few very distinct symptoms. One of these is when you spit out blood after brushing your teeth-ka', '<html><head><title></title></head><body><p>Gums disease is preceded by few very distinct symptoms. One of these is when you spit out blood after brushing your teeth-ka</p></body></html>'),
(2, 1, 2, 'First signs of gum disease - en', 'Gums disease is preceded by few very distinct symptoms. One of these is when you spit out blood after brushing your teeth - en1', '<html><head><title></title></head><body><p>Gums disease is preceded by few very distinct symptoms. One of these is when you spit out blood after brushing your teeth-en1</p></body></html>'),
(3, 1, 3, 'First signs of gum disease - ru', 'Gums disease is preceded by few very distinct symptoms. One of these is when you spit out blood after brushing your teeth - ru1', '<html><head><title></title></head><body><p>Gums disease is preceded by few very distinct symptoms. One of these is when you spit out blood after brushing your teeth-ru1</p></body></html>'),
(16, 2, 1, 'Basic dental care 101', 'Americans oftentimes ignore some basic, daily routines of oral hygiene, that need to be upheld. Practicing healthy habits like these ones will', '<html><head><title></title></head><body><p>Americans oftentimes ignore some basic, daily routines of oral hygiene, that need to be upheld. Practicing healthy habits like these ones will</p></body></html>'),
(17, 2, 2, 'Basic dental care 101', 'Basic dental care 101', '<html><head><title></title></head><body><p>Basic dental care 101</p></body></html>'),
(18, 2, 3, 'Basic dental care 101', 'Basic dental care 101', '<html><head><title></title></head><body><p>Basic dental care 101</p></body></html>'),
(25, 3, 1, 'Avoiding bad breath', 'Americans oftentimes ignore some basic, daily routines of oral hygiene, that need to be upheld. Practicing healthy habits like these ones will', '<html><head><title></title></head><body><p>Americans oftentimes ignore some basic, daily routines of oral hygiene, that need to be upheld. Practicing healthy habits like these ones will</p></body></html>'),
(26, 3, 2, 'Avoiding bad breath', 'Avoiding bad breath', '<html><head><title></title></head><body><p>Avoiding bad breath</p></body></html>'),
(27, 3, 3, 'Avoiding bad breath', 'Avoiding bad breath', '<html><head><title></title></head><body><p>Avoiding bad breath</p></body></html>'),
(34, 4, 1, 'ასდფ', 'სადფ', '<html><head><title></title></head><body></body></html>'),
(35, 4, 2, 'სადფ', 'ასდფ', '<html><head><title></title></head><body><p>ასდფ</p></body></html>'),
(36, 4, 3, 'ასდფ', 'სადფ', '<html><head><title></title></head><body><p>ასდფ</p></body></html>'),
(37, 5, 1, 'ასდფ', 'სადფ', '<html><head><title></title></head><body><p>ასდფასდფ</p></body></html>'),
(38, 5, 2, 'ასდფ', 'სადფ', '<html><head><title></title></head><body><p>ასდფ</p></body></html>'),
(39, 5, 3, 'ასდფ', 'ასდფ', '<html><head><title></title></head><body><p>ასდფ</p></body></html>'),
(40, 6, 1, 'dsf', 'dfsda', '<html><head><title></title></head><body><p>sdfsadf</p></body></html>'),
(41, 6, 2, 'sadf', 'asdf', '<html><head><title></title></head><body><p>sadf</p></body></html>'),
(42, 6, 3, 'asdf', 'asdf', '<html><head><title></title></head><body><p>sadf</p></body></html>'),
(43, 7, 1, 'sadfsadf', 'asdf', '<html><head><title></title></head><body></body></html>'),
(44, 7, 2, 'sadfasdf', '', '<html><head><title></title></head><body></body></html>'),
(45, 7, 3, 'asdf', 'asdf', '<html><head><title></title></head><body><p>asdf</p></body></html>'),
(46, 8, 1, 'Milling and the first bread', 'Our mill has arrived and we''re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh...', '<html><head><title></title></head><body><p>The first bakery I ever worked in was called The Bread Garden in Berkeley, Ca. It was way ahead of its time, predating legendary bakeries like Acme Bread in Berkeley, Ca and Bread Alone in Boiceville, NY. These were the bakeries that started making Artisanal european style breads before anyone in the US knew what they were. To Americans, bread was white, squishy, pre-sliced mediocrity found in the bread aisle at the grocery store.</p><p>David Morris, owner of The Bread Garden, died of cancer at the young age of 65 in April, 2013. He was one of the few certified Master Bakers in the US; a quirky, generous man with an interest in beekeeping, wine making and homebrewing beer. As a homebrewer myself, we shared an interest, and he shared his famous Malted Barley Bread recipe.</p></body></html>'),
(47, 8, 2, 'Milling and the first bread!', 'Our mill has arrived and we''re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh...', '<html><head><title></title></head><body><p>The first bakery I ever worked in was called The Bread Garden in Berkeley, Ca. It was way ahead of its time, predating legendary bakeries like Acme Bread in Berkeley, Ca and Bread Alone in Boiceville, NY. These were the bakeries that started making Artisanal european style breads before anyone in the US knew what they were. To Americans, bread was white, squishy, pre-sliced mediocrity found in the bread aisle at the grocery store.</p><p>David Morris, owner of The Bread Garden, died of cancer at the young age of 65 in April, 2013. He was one of the few certified Master Bakers in the US; a quirky, generous man with an interest in beekeeping, wine making and homebrewing beer. As a homebrewer myself, we shared an interest, and he shared his famous Malted Barley Bread recipe.</p></body></html>'),
(48, 8, 3, 'Milling and the first bread!', 'Our mill has arrived and we''re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh...', '<html><head><title></title></head><body><p>The first bakery I ever worked in was called The Bread Garden in Berkeley, Ca. It was way ahead of its time, predating legendary bakeries like Acme Bread in Berkeley, Ca and Bread Alone in Boiceville, NY. These were the bakeries that started making Artisanal european style breads before anyone in the US knew what they were. To Americans, bread was white, squishy, pre-sliced mediocrity found in the bread aisle at the grocery store.</p><p>David Morris, owner of The Bread Garden, died of cancer at the young age of 65 in April, 2013. He was one of the few certified Master Bakers in the US; a quirky, generous man with an interest in beekeeping, wine making and homebrewing beer. As a homebrewer myself, we shared an interest, and he shared his famous Malted Barley Bread recipe.</p></body></html>'),
(55, 9, 1, 'Milling and the first bread!', 'Our mill has arrived and we''re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh...', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh...Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh...</p>\r\n</body>\r\n</html>\r\n'),
(56, 9, 2, 'Milling and the first bread!', 'Our mill has arrived and we''re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh...', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh...Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh...Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh...</p>\r\n</body>\r\n</html>\r\n'),
(57, 9, 3, 'Milling and the first bread!', 'Our mill has arrived and we''re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh...', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh...Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh...</p>\r\n</body>\r\n</html>\r\n'),
(61, 10, 1, 'Milling and the first bread!', 'Our mill has arrived and we''re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh..Our mill has arrived and we''re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh..', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh..Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh..Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh..</p>\r\n</body>\r\n</html>\r\n'),
(62, 10, 2, 'Milling and the first bread!', 'Our mill has arrived and we''re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh..', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh..Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh..Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh..</p>\r\n</body>\r\n</html>\r\n'),
(63, 10, 3, 'Milling and the first bread!', 'Our mill has arrived and we''re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh..', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh..Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh..Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh..</p>\r\n</body>\r\n</html>\r\n'),
(73, 11, 1, 'erwfgregreg', 'ergergreg', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n'),
(74, 11, 2, 'erger', 'ergerg', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>ergerg</p>\r\n</body>\r\n</html>\r\n'),
(75, 11, 3, 'regerg', 'regergerg', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>ergergergerg</p>\r\n</body>\r\n</html>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `cats`
--

CREATE TABLE `cats` (
  `cat_id` int(11) NOT NULL,
  `parent_cat_id` int(11) NOT NULL DEFAULT '0',
  `status_id` tinyint(1) NOT NULL COMMENT '0 - Inactive, 1 - Active',
  `sub_cats` varchar(255) NOT NULL,
  `has_photo` tinyint(1) NOT NULL DEFAULT '0',
  `photo_ver` smallint(6) NOT NULL DEFAULT '0',
  `sort_order` smallint(6) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cats`
--

INSERT INTO `cats` (`cat_id`, `parent_cat_id`, `status_id`, `sub_cats`, `has_photo`, `photo_ver`, `sort_order`) VALUES
(1, 0, 1, '1', 0, 0, 0),
(2, 0, 1, '2', 0, 0, 0),
(3, 0, 1, '3', 0, 0, 0),
(4, 0, 1, '4', 0, 0, 0),
(5, 0, 1, '5', 0, 0, 0),
(6, 0, 1, '6', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cat_descr`
--

CREATE TABLE `cat_descr` (
  `id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cat_descr`
--

INSERT INTO `cat_descr` (`id`, `cat_id`, `lang_id`, `title`) VALUES
(1, 1, 1, 'ეგვიპტე'),
(2, 1, 2, 'ეგვიპტე'),
(3, 1, 3, 'ეგვიპტე'),
(4, 2, 1, 'დუბაი'),
(5, 2, 2, 'დუბაი'),
(6, 2, 3, 'დუბაი'),
(7, 3, 1, 'თურქეთი'),
(8, 3, 2, 'თურქეთი'),
(9, 3, 3, 'თურქეთი'),
(10, 4, 1, 'ისრაელი'),
(11, 4, 2, 'ისრაელი'),
(12, 4, 3, 'ისრაელი'),
(13, 5, 1, 'საბერძნეთი'),
(14, 5, 2, 'საბერძნეთი'),
(15, 5, 3, 'საბერძნეთი'),
(16, 6, 1, 'აზია'),
(17, 6, 2, 'აზია'),
(18, 6, 3, 'აზია');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `contact_id` int(11) NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `noreply_email` varchar(100) NOT NULL,
  `social_media` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`contact_id`, `mobile`, `phone`, `email`, `noreply_email`, `social_media`) VALUES
(1, '5986532124', '032 2 123 654', 'info@lovetravel.ge', 'no-reply@lovetravel.ge', '{"1":"Facebook 1","2":"Twitter 1","3":"Youtube 1","4":"Instagram 1"}');

-- --------------------------------------------------------

--
-- Table structure for table `contact_descr`
--

CREATE TABLE `contact_descr` (
  `id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `address` varchar(100) NOT NULL,
  `short_descr` text NOT NULL,
  `open_hours` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contact_descr`
--

INSERT INTO `contact_descr` (`id`, `contact_id`, `lang_id`, `address`, `short_descr`, `open_hours`) VALUES
(1, 1, 1, 'N17 ი.აბაშიძის ქ.თბილისი II სართული . R&L თრაველი', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>კომპანია &bdquo;Love Travel&rdquo; 2012 წლიდან მნიშვნელოვან ადგილს იკავებს საქართველოს ტურისტულ სექტორში. გარდა ამისა, კომპანია აქტიურად თანამშრომლობს მსოფლიოში ცნობილ სასტუმროს ქსელებთან, საერთაშორისო დონის ტურისტულ კომპანიებთან და ავიაკომპანიებთან.</p>\r\n\r\n<p>აქ მოგემსახურებიან კვალიფიციური თანამშროლები, რომლებიც უზრუნველყოფენ თქვენთვის როგორც სრული ტურის, ისე ცალკეული სამოგზაურო დეტალების დაგეგმვას მიმზიდველ ფასად.</p>\r\n</body>\r\n</html>\r\n', 'ორშ-პარ: 9:00 - 18:00<br>შაბ: 10:00 - 16:00'),
(2, 1, 2, 'N17 ი.აბაშიძის ქ.თბილისი II სართული . R&L თრაველი', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>კომპანია &bdquo;Love Travel&rdquo; 2012 წლიდან მნიშვნელოვან ადგილს იკავებს საქართველოს ტურისტულ სექტორში. გარდა ამისა, კომპანია აქტიურად თანამშრომლობს მსოფლიოში ცნობილ სასტუმროს ქსელებთან, საერთაშორისო დონის ტურისტულ კომპანიებთან და ავიაკომპანიებთან.</p>\r\n\r\n<p>აქ მოგემსახურებიან კვალიფიციური თანამშროლები, რომლებიც უზრუნველყოფენ თქვენთვის როგორც სრული ტურის, ისე ცალკეული სამოგზაურო დეტალების დაგეგმვას მიმზიდველ ფასად.</p>\r\n</body>\r\n</html>\r\n', 'ორშ-პარ: 9:00 - 18:00<br>შაბ: 10:00 - 16:00'),
(3, 1, 3, 'N17 ი.აბაშიძის ქ.თბილისი II სართული . R&L თრაველი', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>კომპანია &bdquo;Love Travel&rdquo; 2012 წლიდან მნიშვნელოვან ადგილს იკავებს საქართველოს ტურისტულ სექტორში. გარდა ამისა, კომპანია აქტიურად თანამშრომლობს მსოფლიოში ცნობილ სასტუმროს ქსელებთან, საერთაშორისო დონის ტურისტულ კომპანიებთან და ავიაკომპანიებთან.</p>\r\n\r\n<p>აქ მოგემსახურებიან კვალიფიციური თანამშროლები, რომლებიც უზრუნველყოფენ თქვენთვის როგორც სრული ტურის, ისე ცალკეული სამოგზაურო დეტალების დაგეგმვას მიმზიდველ ფასად.</p>\r\n</body>\r\n</html>\r\n', 'ორშ-პარ: 9:00 - 18:00<br>შაბ: 10:00 - 16:00'),
(7, 1, 4, 'N17 ი.აბაშიძის ქ.თბილისი II სართული . R&L თრაველი', '<p>\n                        კომპანია „Love Travel” 2012 წლიდან მნიშვნელოვან ადგილს იკავებს საქართველოს ტურისტულ სექტორში. გარდა ამისა, კომპანია აქტიურად თანამშრომლობს მსოფლიოში ცნობილ სასტუმროს ქსელებთან, საერთაშორისო დონის ტურისტულ კომპანიებთან და ავიაკომპანიებთან.\n                    </p>\n                    <p>\n                        აქ მოგემსახურებიან კვალიფიციური თანამშროლები, რომლებიც უზრუნველყოფენ თქვენთვის როგორც სრული ტურის, ისე ცალკეული სამოგზაურო დეტალების დაგეგმვას მიმზიდველ ფასად.\n                    </p>\n                    <p>კომპანიის სერვისები მოიცავს:</p>\n                    <ul>\n                        <li>ავიაბილეთები</li>\n                        <li>დასასვენებელი ტურები ევროპა/თურქეთი/ეგვიპტე/დუბაი/ისრაელი/მალდივები/შრი ლანკა და სხვ.</li>\n                        <li>სამოგზაურო დაზღვევა</li>\n                        <li>სავიზო მომსახურება</li>\n                        <li>სატრანსფერო მომსახურება</li>\n                        <li>მატარებლის/ავტობუსის ბილეთები ევროპის ყველა მიმართულებით</li>\n                        <li>კულტურულ/სპორტულ ღონისძიებაზე დასასწრები ბილეთები საექსკურსიო/სამუზეუმო ტურები მსოფლიოს სხვადასხვა ქალაქებში </li>\n                    </ul>\n                    <P>მოგზაურობა თავისთავად ბედნიერებაა - პროფესიონალურად დაგეგმილი მოგზაურობა კი ნამდვილი კომფორტი თქვენთვის.</P>\n                    <P>იმოგზაურეთ „Love Travel” - თან ერთად, იმოგზაურეთ სიყვარულით!</P>', 'ორშ-პარ: 9:00 - 18:00 შაბ: 10:00 - 16:00');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `country_id` int(11) NOT NULL,
  `status_id` smallint(6) NOT NULL DEFAULT '0' COMMENT '0 - Inactive, 1 - Active, 2 - Deleted',
  `sort_order` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`country_id`, `status_id`, `sort_order`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `country_descr`
--

CREATE TABLE `country_descr` (
  `id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `country_descr`
--

INSERT INTO `country_descr` (`id`, `country_id`, `lang_id`, `title`) VALUES
(1, 1, 1, 'საქართველო'),
(2, 1, 2, 'Georgia'),
(3, 1, 3, 'საქართველო'),
(4, 2, 1, 'თურქეთი'),
(5, 2, 2, 'თურქეთი'),
(6, 2, 3, 'თურქეთი'),
(7, 3, 1, 'რუსეთი'),
(8, 3, 2, 'რუსეთი'),
(9, 3, 3, 'რუსეთი'),
(10, 4, 1, 'სომხეთი'),
(11, 4, 2, 'სომხეთი'),
(12, 4, 3, 'სომხეთი'),
(13, 5, 1, 'აზერბაიჯანი'),
(14, 5, 2, 'აზერბაიჯანი'),
(15, 5, 3, 'აზერბაიჯანი');

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `currency_id` int(11) NOT NULL,
  `title` varchar(4) NOT NULL,
  `symbol` varchar(20) NOT NULL,
  `sort_order` smallint(6) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`currency_id`, `title`, `symbol`, `sort_order`) VALUES
(1, 'GEL', '₾', 1),
(2, 'USD', '$', 2),
(3, 'EURO', '€', 3);

-- --------------------------------------------------------

--
-- Table structure for table `db_error_log`
--

CREATE TABLE `db_error_log` (
  `db_error_log_id` int(11) NOT NULL,
  `log_dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `db_error` varchar(4000) NOT NULL,
  `url` varchar(4000) NOT NULL,
  `debug_data` text,
  `http_referer` text,
  `http_user_agent` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `db_error_log`
--

INSERT INTO `db_error_log` (`db_error_log_id`, `log_dt`, `db_error`, `url`, `debug_data`, `http_referer`, `http_user_agent`) VALUES
(1, '2017-05-21 20:49:51', 'DataBase: Unknown column ''d.descr'' in ''field list''. Full query: [SELECT n.service_id, n.status_id,\r\n												   d.title, d.descr\r\n											  FROM services n\r\n									     LEFT JOIN service_descr d ON d.service_id = n.service_id AND d.lang_id = 1\r\n										     WHERE n.status_id != 2 \r\n										  ORDER BY n.service_id DESC\r\n										     LIMIT 0, 10]. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 178, thrown', '/rltravel/ka/admin/services/', NULL, 'http://localhost/rltravel/ka/admin/contact/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(2, '2017-05-21 21:03:20', 'DataBase: Table ''rltravel.services'' doesn''t exist. Full query: [SELECT n.service_id, n.status_id,\r\n												   d.title, d.descr\r\n											  FROM services n\r\n									     LEFT JOIN service_descr d ON d.service_id = n.service_id AND d.lang_id = 1\r\n										     WHERE n.status_id != 2 \r\n										  ORDER BY n.service_id DESC\r\n										     LIMIT 0, 10]. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 178, thrown', '/rltravel/ka/admin/services/', NULL, 'http://localhost/rltravel/ka/admin/contact/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(3, '2017-05-21 21:03:21', 'DataBase: Table ''rltravel.services'' doesn''t exist. Full query: [SELECT n.service_id, n.status_id,\r\n												   d.title, d.descr\r\n											  FROM services n\r\n									     LEFT JOIN service_descr d ON d.service_id = n.service_id AND d.lang_id = 1\r\n										     WHERE n.status_id != 2 \r\n										  ORDER BY n.service_id DESC\r\n										     LIMIT 0, 10]. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 178, thrown', '/rltravel/ka/admin/services/', NULL, 'http://localhost/rltravel/ka/admin/contact/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(4, '2017-05-21 21:04:01', 'DataBase: Table ''rltravel.services'' doesn''t exist. Full query: [SELECT n.service_id, n.status_id,\r\n												   d.title, d.descr\r\n											  FROM services n\r\n									     LEFT JOIN service_descr d ON d.service_id = n.service_id AND d.lang_id = 1\r\n										     WHERE n.status_id != 2 \r\n										  ORDER BY n.service_id DESC\r\n										     LIMIT 0, 10]. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 178, thrown', '/rltravel/ka/admin/edittour/12/', NULL, 'http://localhost/rltravel/ka/admin/tours/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(5, '2017-05-21 21:04:43', 'DataBase: Table ''rltravel.services'' doesn''t exist. Full query: [SELECT n.service_id, n.status_id,\r\n												   d.title, d.descr\r\n											  FROM services n\r\n									     LEFT JOIN service_descr d ON d.service_id = n.service_id AND d.lang_id = 1\r\n										     WHERE n.status_id != 2 \r\n										  ORDER BY n.service_id DESC\r\n										     LIMIT 0, 10]. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 178, thrown', '/rltravel/ka/admin/edittour/12/', NULL, 'http://localhost/rltravel/ka/admin/tours/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(6, '2017-05-21 21:04:43', 'DataBase: Table ''rltravel.services'' doesn''t exist. Full query: [SELECT n.service_id, n.status_id,\r\n												   d.title, d.descr\r\n											  FROM services n\r\n									     LEFT JOIN service_descr d ON d.service_id = n.service_id AND d.lang_id = 1\r\n										     WHERE n.status_id != 2 \r\n										  ORDER BY n.service_id DESC\r\n										     LIMIT 0, 10]. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 178, thrown', '/rltravel/ka/admin/edittour/12/', NULL, 'http://localhost/rltravel/ka/admin/tours/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(7, '2017-05-21 21:04:43', 'DataBase: Table ''rltravel.services'' doesn''t exist. Full query: [SELECT n.service_id, n.status_id,\r\n												   d.title, d.descr\r\n											  FROM services n\r\n									     LEFT JOIN service_descr d ON d.service_id = n.service_id AND d.lang_id = 1\r\n										     WHERE n.status_id != 2 \r\n										  ORDER BY n.service_id DESC\r\n										     LIMIT 0, 10]. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 178, thrown', '/rltravel/ka/admin/edittour/12/', NULL, 'http://localhost/rltravel/ka/admin/tours/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(8, '2017-05-21 21:04:44', 'DataBase: Table ''rltravel.services'' doesn''t exist. Full query: [SELECT n.service_id, n.status_id,\r\n												   d.title, d.descr\r\n											  FROM services n\r\n									     LEFT JOIN service_descr d ON d.service_id = n.service_id AND d.lang_id = 1\r\n										     WHERE n.status_id != 2 \r\n										  ORDER BY n.service_id DESC\r\n										     LIMIT 0, 10]. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 178, thrown', '/rltravel/ka/admin/edittour/12/', NULL, 'http://localhost/rltravel/ka/admin/tours/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(9, '2017-05-21 21:04:44', 'DataBase: Table ''rltravel.services'' doesn''t exist. Full query: [SELECT n.service_id, n.status_id,\r\n												   d.title, d.descr\r\n											  FROM services n\r\n									     LEFT JOIN service_descr d ON d.service_id = n.service_id AND d.lang_id = 1\r\n										     WHERE n.status_id != 2 \r\n										  ORDER BY n.service_id DESC\r\n										     LIMIT 0, 10]. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 178, thrown', '/rltravel/ka/admin/edittour/12/', NULL, 'http://localhost/rltravel/ka/admin/tours/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(10, '2017-05-21 21:05:04', 'DataBase: Table ''rltravel.services'' doesn''t exist. Full query: [SELECT n.service_id, n.status_id,\r\n												   d.title, d.descr\r\n											  FROM services n\r\n									     LEFT JOIN service_descr d ON d.service_id = n.service_id AND d.lang_id = 1\r\n										     WHERE n.status_id != 2 \r\n										  ORDER BY n.service_id DESC\r\n										     LIMIT 0, 10]. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 178, thrown', '/rltravel/ka/admin/services/', NULL, 'http://localhost/rltravel/ka/admin/edittour/12/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(11, '2017-05-21 21:07:33', 'DataBase: Integer (?i) placeholder expects numeric value, boolean given. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 66, thrown', '/rltravel/ka/admin/editnewspost/', NULL, 'http://localhost/rltravel/ka/admin/editservice/11/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(12, '2017-05-21 21:07:37', 'DataBase: Integer (?i) placeholder expects numeric value, boolean given. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 66, thrown', '/rltravel/ka/admin/editnewspost/', NULL, 'http://localhost/rltravel/ka/admin/editservice/11/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(13, '2017-05-21 21:08:55', 'DataBase: Integer (?i) placeholder expects numeric value, boolean given. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 147, thrown', '/rltravel/ka/admin/editservicepost/', NULL, 'http://localhost/rltravel/ka/admin/editservice/11/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(14, '2017-05-22 12:06:37', 'DataBase: Unknown column ''s.type_id'' in ''field list''. Full query: [SELECT s.service_id, s.status_id, s.type_id, d.title\r\n				                                  FROM tour_services s\r\n				                             LEFT JOIN tour_service_descr d ON d.service_id = s.service_id AND d.lang_id = 1\r\n				                                 WHERE s.status_id IN (''1'')\r\n				                              ORDER BY s.type_id, s.sort_order]. Error initiated in C:\\xampp\\htdocs\\rltravel\\core\\Model.php on line 93, thrown', '/rltravel/ka/admin/addtour/', NULL, 'http://localhost/rltravel/ka/admin/tours/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(15, '2017-05-22 12:06:57', 'DataBase: Unknown column ''s.type_id'' in ''order clause''. Full query: [SELECT s.service_id, s.status_id, d.title\r\n				                                  FROM tour_services s\r\n				                             LEFT JOIN tour_service_descr d ON d.service_id = s.service_id AND d.lang_id = 1\r\n				                                 WHERE s.status_id IN (''1'')\r\n				                              ORDER BY s.type_id, s.sort_order]. Error initiated in C:\\xampp\\htdocs\\rltravel\\core\\Model.php on line 93, thrown', '/rltravel/ka/admin/addtour/', NULL, 'http://localhost/rltravel/ka/admin/tours/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(16, '2017-05-22 12:06:58', 'DataBase: Unknown column ''s.type_id'' in ''order clause''. Full query: [SELECT s.service_id, s.status_id, d.title\r\n				                                  FROM tour_services s\r\n				                             LEFT JOIN tour_service_descr d ON d.service_id = s.service_id AND d.lang_id = 1\r\n				                                 WHERE s.status_id IN (''1'')\r\n				                              ORDER BY s.type_id, s.sort_order]. Error initiated in C:\\xampp\\htdocs\\rltravel\\core\\Model.php on line 93, thrown', '/rltravel/ka/admin/addtour/', NULL, 'http://localhost/rltravel/ka/admin/tours/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(17, '2017-05-22 12:06:59', 'DataBase: Unknown column ''s.type_id'' in ''order clause''. Full query: [SELECT s.service_id, s.status_id, d.title\r\n				                                  FROM tour_services s\r\n				                             LEFT JOIN tour_service_descr d ON d.service_id = s.service_id AND d.lang_id = 1\r\n				                                 WHERE s.status_id IN (''1'')\r\n				                              ORDER BY s.type_id, s.sort_order]. Error initiated in C:\\xampp\\htdocs\\rltravel\\core\\Model.php on line 93, thrown', '/rltravel/ka/admin/addtour/', NULL, 'http://localhost/rltravel/ka/admin/tours/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(18, '2017-05-22 19:18:03', 'DataBase: Table ''rltravel.products'' doesn''t exist. Full query: [SELECT n.product_id, n.photos_cnt, n.photo_ver, n.insert_date, d.title, d.short_descr\r\n										      FROM products n\r\n									     LEFT JOIN product_descr d ON d.product_id = n.product_id AND d.lang_id = 1\r\n										     WHERE n.status_id = 1 \r\n										  ORDER BY n.product_id DESC\r\n										     LIMIT 0, 10]. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\ToursModel.php on line 30, thrown', '/rltravel/ka/tours/?CatID=1', NULL, 'http://localhost/rltravel/ka/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(19, '2017-05-22 19:18:04', 'DataBase: Table ''rltravel.products'' doesn''t exist. Full query: [SELECT n.product_id, n.photos_cnt, n.photo_ver, n.insert_date, d.title, d.short_descr\r\n										      FROM products n\r\n									     LEFT JOIN product_descr d ON d.product_id = n.product_id AND d.lang_id = 1\r\n										     WHERE n.status_id = 1 \r\n										  ORDER BY n.product_id DESC\r\n										     LIMIT 0, 10]. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\ToursModel.php on line 30, thrown', '/rltravel/ka/tours/?CatID=1', NULL, 'http://localhost/rltravel/ka/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(20, '2017-05-22 19:20:23', 'DataBase: Table ''rltravel.products'' doesn''t exist. Full query: [SELECT n.product_id, n.photos_cnt, n.photo_ver, n.insert_date, d.title, d.short_descr\r\n										      FROM products n\r\n									     LEFT JOIN product_descr d ON d.product_id = n.product_id AND d.lang_id = 1\r\n										     WHERE n.status_id = 1 \r\n										  ORDER BY n.product_id DESC\r\n										     LIMIT 0, 10]. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\ToursModel.php on line 30, thrown', '/rltravel/ka/tours/?CatID=1', NULL, 'http://localhost/rltravel/ka/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(21, '2017-05-22 19:22:30', 'DataBase: Unknown column ''t.tour_id'' in ''field list''. Full query: [SELECT t.tour_id, t.photo_ver, t.price_from, d.title, d.descr\r\n										      FROM tours n\r\n									     LEFT JOIN tour_descr d ON d.tour_id = t.tour_id AND d.lang_id = 1\r\n										     WHERE t.status_id = 1 AND t.photos_cnt > 0  AND t.cat_id IN (''1'',''3'',''16'',''17'',''4'',''5'',''6'',''7'',''8'',''9'',''10'',''11'',''12'',''13'',''14'')\r\n										  ORDER BY t.tour_id DESC\r\n										     LIMIT 0, 10]. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\ToursModel.php on line 30, thrown', '/rltravel/ka/tours/?CatID=1', NULL, 'http://localhost/rltravel/ka/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(22, '2017-05-22 19:22:30', 'DataBase: Unknown column ''t.tour_id'' in ''field list''. Full query: [SELECT t.tour_id, t.photo_ver, t.price_from, d.title, d.descr\r\n										      FROM tours n\r\n									     LEFT JOIN tour_descr d ON d.tour_id = t.tour_id AND d.lang_id = 1\r\n										     WHERE t.status_id = 1 AND t.photos_cnt > 0  AND t.cat_id IN (''1'',''3'',''16'',''17'',''4'',''5'',''6'',''7'',''8'',''9'',''10'',''11'',''12'',''13'',''14'')\r\n										  ORDER BY t.tour_id DESC\r\n										     LIMIT 0, 10]. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\ToursModel.php on line 30, thrown', '/rltravel/ka/tours/?CatID=1', NULL, 'http://localhost/rltravel/ka/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(23, '2017-05-22 19:22:31', 'DataBase: Unknown column ''t.tour_id'' in ''field list''. Full query: [SELECT t.tour_id, t.photo_ver, t.price_from, d.title, d.descr\r\n										      FROM tours n\r\n									     LEFT JOIN tour_descr d ON d.tour_id = t.tour_id AND d.lang_id = 1\r\n										     WHERE t.status_id = 1 AND t.photos_cnt > 0  AND t.cat_id IN (''1'',''3'',''16'',''17'',''4'',''5'',''6'',''7'',''8'',''9'',''10'',''11'',''12'',''13'',''14'')\r\n										  ORDER BY t.tour_id DESC\r\n										     LIMIT 0, 10]. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\ToursModel.php on line 30, thrown', '/rltravel/ka/tours/?CatID=1', NULL, 'http://localhost/rltravel/ka/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(24, '2017-05-22 19:22:31', 'DataBase: Unknown column ''t.tour_id'' in ''field list''. Full query: [SELECT t.tour_id, t.photo_ver, t.price_from, d.title, d.descr\r\n										      FROM tours n\r\n									     LEFT JOIN tour_descr d ON d.tour_id = t.tour_id AND d.lang_id = 1\r\n										     WHERE t.status_id = 1 AND t.photos_cnt > 0  AND t.cat_id IN (''1'',''3'',''16'',''17'',''4'',''5'',''6'',''7'',''8'',''9'',''10'',''11'',''12'',''13'',''14'')\r\n										  ORDER BY t.tour_id DESC\r\n										     LIMIT 0, 10]. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\ToursModel.php on line 30, thrown', '/rltravel/ka/tours/?CatID=1', NULL, 'http://localhost/rltravel/ka/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(25, '2017-05-22 19:22:31', 'DataBase: Unknown column ''t.tour_id'' in ''field list''. Full query: [SELECT t.tour_id, t.photo_ver, t.price_from, d.title, d.descr\r\n										      FROM tours n\r\n									     LEFT JOIN tour_descr d ON d.tour_id = t.tour_id AND d.lang_id = 1\r\n										     WHERE t.status_id = 1 AND t.photos_cnt > 0  AND t.cat_id IN (''1'',''3'',''16'',''17'',''4'',''5'',''6'',''7'',''8'',''9'',''10'',''11'',''12'',''13'',''14'')\r\n										  ORDER BY t.tour_id DESC\r\n										     LIMIT 0, 10]. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\ToursModel.php on line 30, thrown', '/rltravel/ka/tours/?CatID=1', NULL, 'http://localhost/rltravel/ka/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(26, '2017-05-22 19:22:31', 'DataBase: Unknown column ''t.tour_id'' in ''field list''. Full query: [SELECT t.tour_id, t.photo_ver, t.price_from, d.title, d.descr\r\n										      FROM tours n\r\n									     LEFT JOIN tour_descr d ON d.tour_id = t.tour_id AND d.lang_id = 1\r\n										     WHERE t.status_id = 1 AND t.photos_cnt > 0  AND t.cat_id IN (''1'',''3'',''16'',''17'',''4'',''5'',''6'',''7'',''8'',''9'',''10'',''11'',''12'',''13'',''14'')\r\n										  ORDER BY t.tour_id DESC\r\n										     LIMIT 0, 10]. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\ToursModel.php on line 30, thrown', '/rltravel/ka/tours/?CatID=1', NULL, 'http://localhost/rltravel/ka/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(27, '2017-05-22 19:22:31', 'DataBase: Unknown column ''t.tour_id'' in ''field list''. Full query: [SELECT t.tour_id, t.photo_ver, t.price_from, d.title, d.descr\r\n										      FROM tours n\r\n									     LEFT JOIN tour_descr d ON d.tour_id = t.tour_id AND d.lang_id = 1\r\n										     WHERE t.status_id = 1 AND t.photos_cnt > 0  AND t.cat_id IN (''1'',''3'',''16'',''17'',''4'',''5'',''6'',''7'',''8'',''9'',''10'',''11'',''12'',''13'',''14'')\r\n										  ORDER BY t.tour_id DESC\r\n										     LIMIT 0, 10]. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\ToursModel.php on line 30, thrown', '/rltravel/ka/tours/?CatID=1', NULL, 'http://localhost/rltravel/ka/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(28, '2017-05-22 19:22:43', 'DataBase: Unknown column ''t.status_id'' in ''where clause''. Full query: [SELECT COUNT(0)\r\n											 FROM tours n\r\n										LEFT JOIN tour_descr d ON d.tour_id = t.tour_id AND d.lang_id = 1\r\n											WHERE t.status_id = 1 AND t.photos_cnt > 0]. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\ToursModel.php on line 36, thrown', '/rltravel/ka/tours/?CatID=1', NULL, 'http://localhost/rltravel/ka/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(29, '2017-05-22 19:22:44', 'DataBase: Unknown column ''t.status_id'' in ''where clause''. Full query: [SELECT COUNT(0)\r\n											 FROM tours n\r\n										LEFT JOIN tour_descr d ON d.tour_id = t.tour_id AND d.lang_id = 1\r\n											WHERE t.status_id = 1 AND t.photos_cnt > 0]. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\ToursModel.php on line 36, thrown', '/rltravel/ka/tours/?CatID=1', NULL, 'http://localhost/rltravel/ka/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(30, '2017-05-22 19:22:44', 'DataBase: Unknown column ''t.status_id'' in ''where clause''. Full query: [SELECT COUNT(0)\r\n											 FROM tours n\r\n										LEFT JOIN tour_descr d ON d.tour_id = t.tour_id AND d.lang_id = 1\r\n											WHERE t.status_id = 1 AND t.photos_cnt > 0]. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\ToursModel.php on line 36, thrown', '/rltravel/ka/tours/?CatID=1', NULL, 'http://localhost/rltravel/ka/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(31, '2017-05-22 19:22:44', 'DataBase: Unknown column ''t.status_id'' in ''where clause''. Full query: [SELECT COUNT(0)\r\n											 FROM tours n\r\n										LEFT JOIN tour_descr d ON d.tour_id = t.tour_id AND d.lang_id = 1\r\n											WHERE t.status_id = 1 AND t.photos_cnt > 0]. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\ToursModel.php on line 36, thrown', '/rltravel/ka/tours/?CatID=1', NULL, 'http://localhost/rltravel/ka/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(32, '2017-05-22 19:22:44', 'DataBase: Unknown column ''t.status_id'' in ''where clause''. Full query: [SELECT COUNT(0)\r\n											 FROM tours n\r\n										LEFT JOIN tour_descr d ON d.tour_id = t.tour_id AND d.lang_id = 1\r\n											WHERE t.status_id = 1 AND t.photos_cnt > 0]. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\ToursModel.php on line 36, thrown', '/rltravel/ka/tours/?CatID=1', NULL, 'http://localhost/rltravel/ka/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(33, '2017-05-22 19:22:44', 'DataBase: Unknown column ''t.status_id'' in ''where clause''. Full query: [SELECT COUNT(0)\r\n											 FROM tours n\r\n										LEFT JOIN tour_descr d ON d.tour_id = t.tour_id AND d.lang_id = 1\r\n											WHERE t.status_id = 1 AND t.photos_cnt > 0]. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\ToursModel.php on line 36, thrown', '/rltravel/ka/tours/?CatID=1', NULL, 'http://localhost/rltravel/ka/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(34, '2017-05-22 19:22:45', 'DataBase: Unknown column ''t.status_id'' in ''where clause''. Full query: [SELECT COUNT(0)\r\n											 FROM tours n\r\n										LEFT JOIN tour_descr d ON d.tour_id = t.tour_id AND d.lang_id = 1\r\n											WHERE t.status_id = 1 AND t.photos_cnt > 0]. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\ToursModel.php on line 36, thrown', '/rltravel/ka/tours/?CatID=1', NULL, 'http://localhost/rltravel/ka/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(35, '2017-05-24 15:20:02', 'DataBase: Empty value for identifier (?n) placeholder. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 495, thrown', '/rltravel/ka/admin/addlistitem/', NULL, 'http://localhost/rltravel/ka/admin/cats/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(36, '2017-05-24 16:22:55', 'DataBase: Empty value for identifier (?n) placeholder. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 580, thrown', '/rltravel/ka/admin/deletelistitem/', NULL, 'http://localhost/rltravel/ka/admin/types/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(37, '2017-05-24 16:23:26', 'DataBase: Empty value for identifier (?n) placeholder. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 580, thrown', '/rltravel/ka/admin/deletelistitem/', NULL, 'http://localhost/rltravel/ka/admin/types/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(38, '2017-05-24 16:34:52', 'DataBase: Number of args (8) doesn''t match number of placeholders (9) in [SELECT ?n, (SELECT GROUP_CONCAT(?n) FROM ?n WHERE ?n = ?i AND status_id = ?i) AS sub_items\r\n										     FROM ?n\r\n										    WHERE ?n = ?i]. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 542, thrown', '/rltravel/ka/admin/changelistitemstatus/', NULL, 'http://localhost/rltravel/ka/admin/cats/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(39, '2017-05-24 16:36:59', 'DataBase: Number of args (8) doesn''t match number of placeholders (9) in [SELECT ?n, (SELECT GROUP_CONCAT(?n) FROM ?n WHERE ?n = ?i AND status_id = ?i) AS sub_items\r\n										     FROM ?n\r\n										    WHERE ?n = ?i]. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 542, thrown', '/rltravel/ka/admin/changelistitemstatus/', NULL, 'http://localhost/rltravel/ka/admin/cats/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(40, '2017-05-24 17:31:24', 'DataBase: Empty value for identifier (?n) placeholder. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 506, thrown', '/rltravel/ka/admin/changelistitemsortorder/', NULL, 'http://localhost/rltravel/ka/admin/cats/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(41, '2017-05-31 19:48:58', 'DataBase: Table ''rltravel.tour_notes_descr'' doesn''t exist. Full query: [INSERT INTO `tour_notes_descr`\r\n									  SET `note_id` = 1, lang_id = 1, title = ''ფასები მოცემულია ერთ ზრდასრულ ადამიანზე ორადგილიან ნომერში'']. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 481, thrown', '/rltravel/en/admin/addlistitem/', NULL, 'http://localhost/rltravel/en/admin/notes/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(42, '2017-05-31 19:49:03', 'DataBase: Table ''rltravel.tour_notes_descr'' doesn''t exist. Full query: [INSERT INTO `tour_notes_descr`\r\n									  SET `note_id` = 2, lang_id = 1, title = ''ფასები მოცემულია ერთ ზრდასრულ ადამიანზე ორადგილიან ნომერში'']. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 481, thrown', '/rltravel/en/admin/addlistitem/', NULL, 'http://localhost/rltravel/en/admin/notes/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(43, '2017-05-31 19:50:05', 'DataBase: Integer (?i) placeholder expects numeric value, boolean given. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 546, thrown', '/rltravel/en/admin/changelistitemstatus/', NULL, 'http://localhost/rltravel/en/admin/notes/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(44, '2017-05-31 20:08:49', 'DataBase: Integer (?i) placeholder expects numeric value, boolean given. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 264, thrown', '/rltravel/ka/admin/addtourpost/?CatID=25&TypeID=3&Services%5B1%5D=1&Services%5B2%5D=1&Services%5B3%5D=1&Services%5B4%5D=0&Services%5B5%5D=0&Services%5B6%5D=0&Services%5B7%5D=1&Services%5B8%5D=2&Services%5B9%5D=2&Services%5B10%5D=0&Services%5B11%5D=2&Notes%5B%5D=1&Notes%5B%5D=3&Hotels=regergerg&PriceFrom=12&PriceTo=46&CurrencyID=2&PeriodFrom=1234&PeriodTo=2342342343&Title-ka=2323&ShortDescr-ka=4234&Descr-ka=2ergsergsg&Title-en=sdfgsdf&ShortDescr-en=gsdfgsdfg&Descr-en=sdfgsdfg&Title-ru=dsfgsdf&ShortDescr-ru=gsdfgsdfg&Descr-ru=sdfgsdfgsdfg&Title-de=sdfgsdfgdfg&ShortDescr-de=sdfgsdfgsdfg&Descr-de=sdfgsdfgfdg', NULL, 'http://localhost/rltravel/ka/admin/addtour/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(45, '2017-05-31 20:09:24', 'DataBase: Integer (?i) placeholder expects numeric value, boolean given. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 264, thrown', '/rltravel/ka/admin/addtourpost/?CatID=25&TypeID=3&Services%5B1%5D=1&Services%5B2%5D=1&Services%5B3%5D=1&Services%5B4%5D=0&Services%5B5%5D=0&Services%5B6%5D=0&Services%5B7%5D=1&Services%5B8%5D=2&Services%5B9%5D=2&Services%5B10%5D=0&Services%5B11%5D=2&Notes%5B%5D=1&Notes%5B%5D=3&Hotels=regergerg&PriceFrom=12&PriceTo=46&CurrencyID=2&PeriodFrom=1234&PeriodTo=2342342343&Title-ka=2323&ShortDescr-ka=4234&Descr-ka=2ergsergsg&Title-en=sdfgsdf&ShortDescr-en=gsdfgsdfg&Descr-en=sdfgsdfg&Title-ru=dsfgsdf&ShortDescr-ru=gsdfgsdfg&Descr-ru=sdfgsdfgsdfg&Title-de=sdfgsdfgdfg&ShortDescr-de=sdfgsdfgsdfg&Descr-de=sdfgsdfgfdg', NULL, 'http://localhost/rltravel/ka/admin/addtour/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(46, '2017-06-01 16:39:00', 'DataBase: Number of args (4) doesn''t match number of placeholders (3) in [INSERT INTO team_descr\r\n									   	  SET team_id = ?i, lang_id = ?i, fullname = ?s]. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 395, thrown', '/rltravel/ka/admin/addteampost/', NULL, 'http://localhost/rltravel/ka/admin/addteam/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(47, '2017-06-02 15:50:36', 'DataBase: Integer (?i) placeholder expects numeric value, boolean given. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\ToursModel.php on line 92, thrown', '/rltravel/ka/tours/photos/?Photo=15&PhotoNum=4&PhotoType=.jpg%3Fv%3D3&PhotosCnt=5', NULL, 'http://localhost/rltravel/ka/tours/15/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(48, '2017-06-02 16:09:23', 'DataBase: Integer (?i) placeholder expects numeric value, boolean given. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\ToursModel.php on line 92, thrown', '/rltravel/ka/tours/photos/?Photo=15&PhotoNum=1&PhotoType=.jpg%3Fv%3D3&PhotosCnt=5', NULL, 'http://localhost/rltravel/ka/tours/15/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(49, '2017-06-03 15:53:39', 'DataBase: Empty value for identifier (?n) placeholder. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 686, thrown', '/rltravel/ka/admin/viewlistitem/?post_table=langs&id=1&popup_title=%E1%83%AA%E1%83%95%E1%83%9A%E1%83%90%E1%83%93%E1%83%98%E1%83%A1+%E1%83%A0%E1%83%94%E1%83%93%E1%83%90%E1%83%A5%E1%83%A2%E1%83%98%E1%83%A0%E1%83%94%E1%83%91%E1%83%90', NULL, 'http://localhost/rltravel/ka/admin/langs/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(50, '2017-06-03 16:01:06', 'DataBase: Table ''rltravel.lange_descr'' doesn''t exist. Full query: [SELECT s.lang_var_id, s.lang_var_id AS id, d.title\r\n				                               	   FROM langs s\r\n				                              LEFT JOIN lange_descr d ON d.lang_var_id = s.lang_var_id AND d.lang_id = 1\r\n				                               ORDER BY s.lang_var_id]. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 634, thrown', '/rltravel/ka/admin/langs/', NULL, 'http://localhost/rltravel/ka/admin/notes/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(51, '2017-06-03 16:01:07', 'DataBase: Table ''rltravel.lange_descr'' doesn''t exist. Full query: [SELECT s.lang_var_id, s.lang_var_id AS id, d.title\r\n				                               	   FROM langs s\r\n				                              LEFT JOIN lange_descr d ON d.lang_var_id = s.lang_var_id AND d.lang_id = 1\r\n				                               ORDER BY s.lang_var_id]. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 634, thrown', '/rltravel/ka/admin/langs/', NULL, 'http://localhost/rltravel/ka/admin/notes/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(52, '2017-06-03 16:01:07', 'DataBase: Table ''rltravel.lange_descr'' doesn''t exist. Full query: [SELECT s.lang_var_id, s.lang_var_id AS id, d.title\r\n				                               	   FROM langs s\r\n				                              LEFT JOIN lange_descr d ON d.lang_var_id = s.lang_var_id AND d.lang_id = 1\r\n				                               ORDER BY s.lang_var_id]. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 634, thrown', '/rltravel/ka/admin/langs/', NULL, 'http://localhost/rltravel/ka/admin/notes/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(53, '2017-06-03 23:12:32', 'DataBase: Integer (?i) placeholder expects numeric value, array given. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 520, thrown', '/rltravel/ka/admin/addlistitem/', NULL, 'http://localhost/rltravel/ka/admin/langs/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(54, '2017-06-03 23:13:58', 'DataBase: Integer (?i) placeholder expects numeric value, array given. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 520, thrown', '/rltravel/ka/admin/addlistitem/', NULL, 'http://localhost/rltravel/ka/admin/langs/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(55, '2017-06-03 23:14:32', 'DataBase: Integer (?i) placeholder expects numeric value, array given. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 520, thrown', '/rltravel/ka/admin/addlistitem/', NULL, 'http://localhost/rltravel/ka/admin/langs/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(56, '2017-06-03 23:14:43', 'DataBase: Integer (?i) placeholder expects numeric value, array given. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 520, thrown', '/rltravel/ka/admin/addlistitem/', NULL, 'http://localhost/rltravel/ka/admin/langs/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(57, '2017-06-03 23:15:22', 'DataBase: Integer (?i) placeholder expects numeric value, array given. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 520, thrown', '/rltravel/ka/admin/addlistitem/', NULL, 'http://localhost/rltravel/ka/admin/langs/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(58, '2017-06-03 23:15:35', 'DataBase: Integer (?i) placeholder expects numeric value, array given. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 520, thrown', '/rltravel/ka/admin/addlistitem/', NULL, 'http://localhost/rltravel/ka/admin/langs/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(59, '2017-06-03 23:16:03', 'DataBase: Integer (?i) placeholder expects numeric value, array given. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 520, thrown', '/rltravel/ka/admin/addlistitem/', NULL, 'http://localhost/rltravel/ka/admin/langs/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(60, '2017-06-03 23:16:34', 'DataBase: Integer (?i) placeholder expects numeric value, array given. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 520, thrown', '/rltravel/ka/admin/addlistitem/', NULL, 'http://localhost/rltravel/ka/admin/langs/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(61, '2017-06-03 23:17:47', 'DataBase: Integer (?i) placeholder expects numeric value, array given. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 520, thrown', '/rltravel/ka/admin/addlistitem/', NULL, 'http://localhost/rltravel/ka/admin/langs/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(62, '2017-06-03 23:18:11', 'DataBase: Integer (?i) placeholder expects numeric value, array given. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 520, thrown', '/rltravel/ka/admin/addlistitem/', NULL, 'http://localhost/rltravel/ka/admin/langs/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(63, '2017-06-03 23:25:43', 'DataBase: You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near '''' at line 1. Full query: [INSERT INTO `cats` SET ]. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 547, thrown', '/rltravel/ka/admin/addlistitem/', NULL, 'http://localhost/rltravel/ka/admin/cats/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(64, '2017-06-03 23:28:28', 'DataBase: Unknown column ''file_ver'' in ''field list''. Full query: [INSERT INTO `cats` SET `status_id` = '''', `parent_cat_id` = '''', `sub_cats` = '''', `has_photo` = '''', `file_ver` = '''']. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 545, thrown', '/rltravel/ka/admin/addlistitem/', NULL, 'http://localhost/rltravel/ka/admin/cats/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(65, '2017-06-03 23:29:09', 'DataBase: Unknown column ''Array'' in ''field list''. Full query: [UPDATE `cats`\r\n								 SET `Array` = ''27''\r\n							   WHERE `cat_id` = 27]. Error initiated in C:\\xampp\\htdocs\\rltravel\\models\\AdminModel.php on line 517, thrown', '/rltravel/ka/admin/addlistitem/', NULL, 'http://localhost/rltravel/ka/admin/cats/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'),
(66, '2017-07-16 18:14:34', 'DataBase: Unknown column ''t.price_from'' in ''field list''. Full query: [SELECT t.tour_id, t.photo_ver, t.cat_id, t.price_from, t.currency_id, d.title\r\n								    FROM tours t\r\n							   LEFT JOIN tour_descr d ON d.tour_id = t.tour_id AND d.lang_id = 1\r\n								   WHERE t.status_id = 1 AND t.photos_cnt > 0 AND t.tour_id != 15 AND t.cat_id = 26\r\n								ORDER BY RAND()\r\n								   LIMIT 8]. Error initiated in C:\\xampp\\htdocs\\lovetravel\\models\\ToursModel.php on line 104, thrown', '/lovetravel/ka/tours/15/', NULL, 'http://localhost/lovetravel/ka/admin/tours/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'),
(67, '2017-07-16 18:14:43', 'DataBase: Unknown column ''t.price_from'' in ''field list''. Full query: [SELECT t.tour_id, t.photos_cnt, t.photo_ver, t.cat_id, t.price_from, t.currency_id, d.title, d.short_descr\r\n								    FROM tours t\r\n							   LEFT JOIN tour_descr d ON d.tour_id = t.tour_id AND d.lang_id = 1\r\n								   WHERE t.status_id = 1 AND t.is_main = 1 AND t.photos_cnt > 0\r\n								ORDER BY RAND()]. Error initiated in C:\\xampp\\htdocs\\lovetravel\\models\\MainModel.php on line 20, thrown', '/lovetravel/ka/', NULL, 'http://localhost/lovetravel/ka/admin/login/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'),
(68, '2017-07-16 18:15:52', 'DataBase: Unknown column ''t.price_from'' in ''field list''. Full query: [SELECT t.tour_id, t.photo_ver, t.price_from, t.currency_id, t.services, d.title, d.short_descr\r\n										      FROM tours t\r\n									     LEFT JOIN tour_descr d ON d.tour_id = t.tour_id AND d.lang_id = 1\r\n										     WHERE t.status_id = 1 AND t.photos_cnt > 0 \r\n										  ORDER BY t.tour_id DESC\r\n										     LIMIT 0, 9]. Error initiated in C:\\xampp\\htdocs\\lovetravel\\models\\ToursModel.php on line 61, thrown', '/lovetravel/ka/tours/?Keyword=&CatID=0&PeriodFrom=&PeriodTo=&PriceFrom=&PriceTo=', NULL, 'http://localhost/lovetravel/ka/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'),
(69, '2017-07-16 20:25:13', 'DataBase: Integer (?i) placeholder expects numeric value, string given. Error initiated in C:\\xampp\\htdocs\\lovetravel\\models\\MainModel.php on line 62, thrown', '/lovetravel/ka/', NULL, 'http://localhost/lovetravel/ka/tours/?Keyword=&CatID=0&PeriodFrom=&PeriodTo=&PriceFrom=&PriceTo=', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'),
(70, '2017-07-16 20:25:13', 'DataBase: Integer (?i) placeholder expects numeric value, string given. Error initiated in C:\\xampp\\htdocs\\lovetravel\\models\\MainModel.php on line 62, thrown', '/lovetravel/ka/', NULL, 'http://localhost/lovetravel/ka/tours/?Keyword=&CatID=0&PeriodFrom=&PeriodTo=&PriceFrom=&PriceTo=', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'),
(71, '2017-07-19 17:17:10', 'DataBase: Table ''lovetravel.blog'' doesn''t exist. Full query: [SELECT n.blog_id, n.status_id, n.photos_cnt, n.photo_ver,\r\n												   d.title, d.descr\r\n											  FROM blog n\r\n									     LEFT JOIN blog_descr d ON d.blog_id = n.blog_id AND d.lang_id = 2\r\n										     WHERE n.status_id != 2 \r\n										  ORDER BY n.blog_id DESC\r\n										     LIMIT 0, 9]. Error initiated in C:\\xampp\\htdocs\\lovetravel\\models\\AdminModel.php on line 300, thrown', '/lovetravel/en/admin/blog/', NULL, 'http://localhost/lovetravel/en/admin/tours/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'),
(72, '2017-07-19 17:40:02', 'DataBase: Integer (?i) placeholder expects numeric value, boolean given. Error initiated in C:\\xampp\\htdocs\\lovetravel\\models\\AdminModel.php on line 176, thrown', '/lovetravel/en/admin/editblogpost/', NULL, 'http://localhost/lovetravel/en/admin/editblog/10/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'),
(73, '2017-07-19 17:40:21', 'DataBase: Integer (?i) placeholder expects numeric value, boolean given. Error initiated in C:\\xampp\\htdocs\\lovetravel\\models\\AdminModel.php on line 176, thrown', '/lovetravel/en/admin/editblogpost/', NULL, 'http://localhost/lovetravel/en/admin/editblog/10/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'),
(74, '2017-07-19 17:40:28', 'DataBase: Integer (?i) placeholder expects numeric value, boolean given. Error initiated in C:\\xampp\\htdocs\\lovetravel\\models\\AdminModel.php on line 176, thrown', '/lovetravel/en/admin/editblogpost/', NULL, 'http://localhost/lovetravel/en/admin/editblog/10/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'),
(75, '2017-07-19 17:41:22', 'DataBase: Unknown column ''has_photo'' in ''field list''. Full query: [UPDATE `blog`\r\n									SET `has_photo` = 1, `photo_ver` = `photo_ver` + 1\r\n								  WHERE `blog_id` = 10]. Error initiated in C:\\xampp\\htdocs\\lovetravel\\models\\AdminModel.php on line 1023, thrown', '/lovetravel/en/admin/editblogpost/', NULL, 'http://localhost/lovetravel/en/admin/editblog/10/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'),
(76, '2017-07-19 18:10:27', 'DataBase: Integer (?i) placeholder expects numeric value, string given. Error initiated in C:\\xampp\\htdocs\\lovetravel\\models\\BlogModel.php on line 29, thrown', '/lovetravel/ka/blog/', NULL, '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'),
(77, '2017-07-19 18:10:28', 'DataBase: Integer (?i) placeholder expects numeric value, string given. Error initiated in C:\\xampp\\htdocs\\lovetravel\\models\\BlogModel.php on line 29, thrown', '/lovetravel/ka/blog/', NULL, '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'),
(78, '2017-07-19 18:10:29', 'DataBase: Integer (?i) placeholder expects numeric value, string given. Error initiated in C:\\xampp\\htdocs\\lovetravel\\models\\BlogModel.php on line 29, thrown', '/lovetravel/ka/blog/', NULL, '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'),
(79, '2017-07-19 18:29:37', 'DataBase: Unknown column ''s.blog_cat_id'' in ''field list''. Full query: [SELECT s.blog_cat_id, s.blog_cat_id AS id, s.status_id, d.title\r\n				                               	   FROM blog_cats s\r\n				                              LEFT JOIN blog_cat_descr d ON d.blog_cat_id = s.blog_cat_id AND d.lang_id = 1\r\n				                                  WHERE s.status_id IN (''1'')\r\n				                               ORDER BY s.sort_order]. Error initiated in C:\\xampp\\htdocs\\lovetravel\\core\\Model.php on line 166, thrown', '/lovetravel/ka/', NULL, 'http://localhost/lovetravel/ka/blog', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'),
(80, '2017-07-19 18:29:38', 'DataBase: Unknown column ''s.blog_cat_id'' in ''field list''. Full query: [SELECT s.blog_cat_id, s.blog_cat_id AS id, s.status_id, d.title\r\n				                               	   FROM blog_cats s\r\n				                              LEFT JOIN blog_cat_descr d ON d.blog_cat_id = s.blog_cat_id AND d.lang_id = 1\r\n				                                  WHERE s.status_id IN (''1'')\r\n				                               ORDER BY s.sort_order]. Error initiated in C:\\xampp\\htdocs\\lovetravel\\core\\Model.php on line 166, thrown', '/lovetravel/ka/', NULL, 'http://localhost/lovetravel/ka/blog', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'),
(81, '2017-07-19 18:38:51', 'DataBase: You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near ''SELECT COUNT(0) FROM blog b WHERE b.cat_id = s.cat_id AND b.status_id = 1)\r\n				'' at line 1. Full query: [SELECT s.cat_id, s.cat_id AS id, s.status_id, d.title (SELECT COUNT(0) FROM blog b WHERE b.cat_id = s.cat_id AND b.status_id = 1)\r\n				                              FROM blog_cats s\r\n				                         LEFT JOIN blog_cat_descr d ON d.cat_id = s.cat_id AND d.lang_id = 1\r\n				                             WHERE s.status_id IN (''1'')\r\n				                          ORDER BY s.sort_order]. Error initiated in C:\\xampp\\htdocs\\lovetravel\\core\\Model.php on line 168, thrown', '/lovetravel/ka/blog/?CatID=1', NULL, 'http://localhost/lovetravel/ka/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'),
(82, '2017-07-19 18:38:51', 'DataBase: You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near ''SELECT COUNT(0) FROM blog b WHERE b.cat_id = s.cat_id AND b.status_id = 1)\r\n				'' at line 1. Full query: [SELECT s.cat_id, s.cat_id AS id, s.status_id, d.title (SELECT COUNT(0) FROM blog b WHERE b.cat_id = s.cat_id AND b.status_id = 1)\r\n				                              FROM blog_cats s\r\n				                         LEFT JOIN blog_cat_descr d ON d.cat_id = s.cat_id AND d.lang_id = 1\r\n				                             WHERE s.status_id IN (''1'')\r\n				                          ORDER BY s.sort_order]. Error initiated in C:\\xampp\\htdocs\\lovetravel\\core\\Model.php on line 168, thrown', '/lovetravel/ka/blog/?CatID=1', NULL, 'http://localhost/lovetravel/ka/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'),
(83, '2017-07-19 18:38:52', 'DataBase: You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near ''SELECT COUNT(0) FROM blog b WHERE b.cat_id = s.cat_id AND b.status_id = 1)\r\n				'' at line 1. Full query: [SELECT s.cat_id, s.cat_id AS id, s.status_id, d.title (SELECT COUNT(0) FROM blog b WHERE b.cat_id = s.cat_id AND b.status_id = 1)\r\n				                              FROM blog_cats s\r\n				                         LEFT JOIN blog_cat_descr d ON d.cat_id = s.cat_id AND d.lang_id = 1\r\n				                             WHERE s.status_id IN (''1'')\r\n				                          ORDER BY s.sort_order]. Error initiated in C:\\xampp\\htdocs\\lovetravel\\core\\Model.php on line 168, thrown', '/lovetravel/ka/blog/?CatID=1', NULL, 'http://localhost/lovetravel/ka/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36');
INSERT INTO `db_error_log` (`db_error_log_id`, `log_dt`, `db_error`, `url`, `debug_data`, `http_referer`, `http_user_agent`) VALUES
(84, '2017-07-19 18:39:09', 'DataBase: You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near ''SELECT COUNT(0) FROM blog b WHERE b.cat_id = s.cat_id AND b.status_id = 1) AS cn'' at line 1. Full query: [SELECT s.cat_id, s.cat_id AS id, s.status_id, d.title (SELECT COUNT(0) FROM blog b WHERE b.cat_id = s.cat_id AND b.status_id = 1) AS cnt\r\n				                              FROM blog_cats s\r\n				                         LEFT JOIN blog_cat_descr d ON d.cat_id = s.cat_id AND d.lang_id = 1\r\n				                             WHERE s.status_id IN (''1'')\r\n				                          ORDER BY s.sort_order]. Error initiated in C:\\xampp\\htdocs\\lovetravel\\core\\Model.php on line 168, thrown', '/lovetravel/ka/blog/?CatID=1', NULL, 'http://localhost/lovetravel/ka/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'),
(85, '2017-07-19 18:39:09', 'DataBase: You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near ''SELECT COUNT(0) FROM blog b WHERE b.cat_id = s.cat_id AND b.status_id = 1) AS cn'' at line 1. Full query: [SELECT s.cat_id, s.cat_id AS id, s.status_id, d.title (SELECT COUNT(0) FROM blog b WHERE b.cat_id = s.cat_id AND b.status_id = 1) AS cnt\r\n				                              FROM blog_cats s\r\n				                         LEFT JOIN blog_cat_descr d ON d.cat_id = s.cat_id AND d.lang_id = 1\r\n				                             WHERE s.status_id IN (''1'')\r\n				                          ORDER BY s.sort_order]. Error initiated in C:\\xampp\\htdocs\\lovetravel\\core\\Model.php on line 168, thrown', '/lovetravel/ka/blog/?CatID=1', NULL, 'http://localhost/lovetravel/ka/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'),
(86, '2017-07-19 18:39:10', 'DataBase: You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near ''SELECT COUNT(0) FROM blog b WHERE b.cat_id = s.cat_id AND b.status_id = 1) AS cn'' at line 1. Full query: [SELECT s.cat_id, s.cat_id AS id, s.status_id, d.title (SELECT COUNT(0) FROM blog b WHERE b.cat_id = s.cat_id AND b.status_id = 1) AS cnt\r\n				                              FROM blog_cats s\r\n				                         LEFT JOIN blog_cat_descr d ON d.cat_id = s.cat_id AND d.lang_id = 1\r\n				                             WHERE s.status_id IN (''1'')\r\n				                          ORDER BY s.sort_order]. Error initiated in C:\\xampp\\htdocs\\lovetravel\\core\\Model.php on line 168, thrown', '/lovetravel/ka/blog/?CatID=1', NULL, 'http://localhost/lovetravel/ka/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'),
(87, '2017-07-19 18:57:46', 'DataBase: Integer (?i) placeholder expects numeric value, string given. Error initiated in C:\\xampp\\htdocs\\lovetravel\\models\\BlogModel.php on line 29, thrown', '/lovetravel/ka/blog/?CatID=rgrg', NULL, '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'),
(88, '2017-07-19 18:57:49', 'DataBase: Integer (?i) placeholder expects numeric value, boolean given. Error initiated in C:\\xampp\\htdocs\\lovetravel\\models\\BlogModel.php on line 29, thrown', '/lovetravel/ka/blog/', NULL, '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'),
(89, '2017-07-19 18:57:50', 'DataBase: Integer (?i) placeholder expects numeric value, string given. Error initiated in C:\\xampp\\htdocs\\lovetravel\\models\\BlogModel.php on line 29, thrown', '/lovetravel/ka/blog/?CatID=rgrg', NULL, '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'),
(90, '2017-07-20 19:52:48', 'DataBase: You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near ''.icon, s.photos_cnt, s.photo_ver, d.short_descr, d.descr\r\n                      '' at line 1. Full query: [SELECT s.service_id, d.title s.icon, s.photos_cnt, s.photo_ver, d.short_descr, d.descr\r\n                                    FROM services s\r\n                               LEFT JOIN service_descr d ON d.service_id = s.service_id AND d.lang_id = 1\r\n                                   WHERE s.status_id = 1\r\n                                ORDER BY s.service_id]. Error initiated in C:\\xampp\\htdocs\\lovetravel\\core\\Model.php on line 114, thrown', '/lovetravel/ka/services/', NULL, 'http://localhost/lovetravel/ka/services/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'),
(91, '2017-07-22 14:17:16', 'DataBase: Table ''lovetravel.constants'' doesn''t exist. Full query: [SELECT noreply_email FROM constants WHERE contact_id = 1]. Error initiated in C:\\xampp\\htdocs\\lovetravel\\models\\UserModel.php on line 158, thrown', '/lovetravel/ka/user/add/', NULL, 'http://localhost/lovetravel/ka/user/register/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'),
(92, '2017-07-22 14:17:56', 'DataBase: Table ''lovetravel.contacts'' doesn''t exist. Full query: [SELECT noreply_email FROM contacts WHERE contact_id = 1]. Error initiated in C:\\xampp\\htdocs\\lovetravel\\models\\UserModel.php on line 158, thrown', '/lovetravel/ka/user/add/', NULL, 'http://localhost/lovetravel/ka/user/register/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'),
(93, '2017-07-22 14:18:18', 'DataBase: Column ''hash'' cannot be null. Full query: [INSERT INTO email_hashes SET user_id = 0, type_id = 0, hash = NULL]. Error initiated in C:\\xampp\\htdocs\\lovetravel\\models\\UserModel.php on line 177, thrown', '/lovetravel/ka/user/add/', NULL, 'http://localhost/lovetravel/ka/user/register/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'),
(94, '2017-07-22 14:20:10', 'DataBase: Column ''mail_reply_to'' cannot be null. Full query: [INSERT INTO emails SET `mail_from`=''no-reply@lovetravel.ge'',`mail_to`=''levanpankisi@mail.ru'',`mail_reply_to`=NULL,`mail_subject`=''პროფილის აქტივაცია'',`mail_message`=''თქვენი პროფილის გასააქტიურებლად დააკლიკეთ შემდეგ ლინკს: <a href=\\"http://localhost/lovetravel/ka/user/emailactivation/?Hash=5c788c7fdfba75d99baa5f57a6c4dc72\\" target=\\"_blank\\">http://localhost/lovetravel/ka/user/emailactivation/?Hash=5c788c7fdfba75d99baa5f57a6c4dc72</a>'',`mail_priority`=''0'']. Error initiated in C:\\xampp\\htdocs\\lovetravel\\core\\Model.php on line 197, thrown', '/lovetravel/ka/user/add/', NULL, 'http://localhost/lovetravel/ka/user/register/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'),
(95, '2017-07-23 13:05:49', 'DataBase: Integer (?i) placeholder expects numeric value, boolean given. Error initiated in C:\\xampp\\htdocs\\lovetravel\\models\\UserModel.php on line 297, thrown', '/lovetravel/ka/user/addtofavorites/', NULL, 'http://localhost/lovetravel/ka/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'),
(96, '2017-07-23 13:16:02', 'DataBase: Integer (?i) placeholder expects numeric value, boolean given. Error initiated in C:\\xampp\\htdocs\\lovetravel\\models\\UserModel.php on line 297, thrown', '/lovetravel/ka/user/addtofavorites/', NULL, 'http://localhost/lovetravel/ka/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'),
(97, '2017-07-23 13:35:26', 'DataBase: Integer (?i) placeholder expects numeric value, boolean given. Error initiated in C:\\xampp\\htdocs\\lovetravel\\models\\UserModel.php on line 297, thrown', '/lovetravel/ka/user/addtofavorites/', NULL, 'http://localhost/lovetravel/ka/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'),
(98, '2017-07-23 13:35:38', 'DataBase: Integer (?i) placeholder expects numeric value, boolean given. Error initiated in C:\\xampp\\htdocs\\lovetravel\\models\\UserModel.php on line 297, thrown', '/lovetravel/ka/user/addtofavorites/', NULL, 'http://localhost/lovetravel/ka/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'),
(99, '2017-07-23 13:39:28', 'DataBase: Integer (?i) placeholder expects numeric value, boolean given. Error initiated in C:\\xampp\\htdocs\\lovetravel\\models\\UserModel.php on line 297, thrown', '/lovetravel/ka/user/addtofavorites/', NULL, 'http://localhost/lovetravel/ka/tours/11/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36');

-- --------------------------------------------------------

--
-- Table structure for table `emails`
--

CREATE TABLE `emails` (
  `mail_id` bigint(20) NOT NULL,
  `mail_subject` varchar(200) DEFAULT NULL,
  `mail_from` varchar(100) DEFAULT NULL,
  `mail_to` varchar(100) NOT NULL,
  `mail_reply_to` varchar(100) DEFAULT NULL,
  `mail_message` text,
  `mail_priority` smallint(6) NOT NULL DEFAULT '0',
  `attempt` smallint(6) NOT NULL DEFAULT '0',
  `insert_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `emails`
--

INSERT INTO `emails` (`mail_id`, `mail_subject`, `mail_from`, `mail_to`, `mail_reply_to`, `mail_message`, `mail_priority`, `attempt`, `insert_date`) VALUES
(1, 'Contact', 'wefwefe@mail.ru', 'info@eurotrade.ge', 'wefwefe@mail.ru', 'Phone: ewfwefe<br><br>wefwefewf', 0, 0, '2017-05-02 15:09:36'),
(2, 'Contact', 'levanpankisi@mail.ru', 'info@rltravel.ge', 'levanpankisi@mail.ru', 'Phone: wefwefew<br><br>wefwefwefe\r\n', 0, 0, '2017-05-21 20:31:54'),
(3, 'პროფილის აქტივაცია', 'no-reply@lovetravel.ge', 'levanpankisi@mail.ru', NULL, 'თქვენი პროფილის გასააქტიურებლად დააკლიკეთ შემდეგ ლინკს: <a href="http://localhost/lovetravel/ka/user/emailactivation/?Hash=6b731f3fee03299cadd59a0a65f4435f" target="_blank">http://localhost/lovetravel/ka/user/emailactivation/?Hash=6b731f3fee03299cadd59a0a65f4435f</a>', 0, 0, '2017-07-22 14:21:42'),
(4, 'პროფილის აქტივაცია', 'no-reply@lovetravel.ge', 'levanpankisi@mail.ru', NULL, 'თქვენი პროფილის გასააქტიურებლად დააკლიკეთ შემდეგ ლინკს: <a href="http://localhost/lovetravel/ka/user/emailactivation/?Hash=4af5718a1ca4b9244a067cadf36ca12f" target="_blank">http://localhost/lovetravel/ka/user/emailactivation/?Hash=4af5718a1ca4b9244a067cadf36ca12f</a>', 0, 0, '2017-07-22 14:23:02'),
(5, 'პროფილის აქტივაცია', 'no-reply@lovetravel.ge', 'levanpankisi@mail.ru', NULL, 'თქვენი პროფილის გასააქტიურებლად დააკლიკეთ შემდეგ ლინკს: <a href="http://localhost/lovetravel/ka/user/emailactivation/?Hash=23261d6523b0f4adb402ba7ac5263f7b" target="_blank">http://localhost/lovetravel/ka/user/emailactivation/?Hash=23261d6523b0f4adb402ba7ac5263f7b</a>', 0, 0, '2017-07-22 14:24:34'),
(6, 'პროფილის აქტივაცია', 'no-reply@lovetravel.ge', 'levanpankisi@mail.ru', NULL, 'თქვენი პროფილის გასააქტიურებლად დააკლიკეთ შემდეგ ლინკს: <a href="http://localhost/lovetravel/ka/user/emailactivation/?Hash=58c9ea089d1484b585c0a45afab5579f" target="_blank">http://localhost/lovetravel/ka/user/emailactivation/?Hash=58c9ea089d1484b585c0a45afab5579f</a>', 0, 0, '2017-07-22 14:25:27'),
(7, 'პროფილის აქტივაცია', 'no-reply@lovetravel.ge', '', NULL, 'თქვენი პროფილის გასააქტიურებლად დააკლიკეთ შემდეგ ლინკს: <a href="http://localhost/lovetravel/ka/user/emailactivation/?Hash=6554301152247efd92aebea5cc757d4f" target="_blank">http://localhost/lovetravel/ka/user/emailactivation/?Hash=6554301152247efd92aebea5cc757d4f</a>', 0, 0, '2017-07-22 14:26:05'),
(8, 'პროფილის აქტივაცია', 'no-reply@lovetravel.ge', 'wefwef@mail.ru', NULL, 'თქვენი პროფილის გასააქტიურებლად დააკლიკეთ შემდეგ ლინკს: <a href="http://localhost/lovetravel/ka/user/emailactivation/?Hash=7ee138a73ba277cb6925cc75d4715849" target="_blank">http://localhost/lovetravel/ka/user/emailactivation/?Hash=7ee138a73ba277cb6925cc75d4715849</a>', 0, 0, '2017-07-22 14:28:01'),
(9, 'პროფილის აქტივაცია', 'no-reply@lovetravel.ge', 'levanpankisi@mail.ru', NULL, 'თქვენი პროფილის გასააქტიურებლად დააკლიკეთ შემდეგ ლინკს: <a href="http://localhost/lovetravel/ka/user/emailactivation/?Hash=9cd23b6014f61f97d78c7ab514a8d58e" target="_blank">http://localhost/lovetravel/ka/user/emailactivation/?Hash=9cd23b6014f61f97d78c7ab514a8d58e</a>', 0, 0, '2017-07-22 14:29:54'),
(10, 'პროფილის აქტივაცია', 'no-reply@lovetravel.ge', 'levanpankisi@mail.ru', NULL, 'თქვენი პროფილის გასააქტიურებლად დააკლიკეთ შემდეგ ლინკს: <a href="http://localhost/lovetravel/ka/user/emailactivation/?Hash=dc7bed46912cd2d1599b56cc8b351e5c" target="_blank">http://localhost/lovetravel/ka/user/emailactivation/?Hash=dc7bed46912cd2d1599b56cc8b351e5c</a>', 0, 0, '2017-07-22 15:58:54'),
(11, 'პროფილის აქტივაცია', 'no-reply@lovetravel.ge', 'levanpankisi@mail.ru', NULL, 'თქვენი პროფილის გასააქტიურებლად დააკლიკეთ შემდეგ ლინკს: <a href="http://localhost/lovetravel/ka/user/emailactivation/?Hash=ad5175090367cebae6227791bb1eb8c6" target="_blank">http://localhost/lovetravel/ka/user/emailactivation/?Hash=ad5175090367cebae6227791bb1eb8c6</a>', 0, 0, '2017-07-22 16:03:13'),
(12, 'პროფილის აქტივაცია', 'no-reply@lovetravel.ge', 'levanpankisi@mail.ru', NULL, 'თქვენი პროფილის გასააქტიურებლად დააკლიკეთ შემდეგ ლინკს: <a href="http://localhost/lovetravel/ka/user/emailactivation/?Hash=3abb9878ecef9823258560717b4f10fd" target="_blank">http://localhost/lovetravel/ka/user/emailactivation/?Hash=3abb9878ecef9823258560717b4f10fd</a>', 0, 0, '2017-07-22 16:03:40'),
(13, 'პაროლის აღდგენა', 'no-reply@lovetravel.ge', 'levanpankisi@mail.ru', NULL, 'levanpankisi@mail.ru', 0, 0, '2017-07-22 17:29:30'),
(14, 'პაროლის აღდგენა', 'no-reply@lovetravel.ge', 'levanpankisi@mail.ru', NULL, 'levanpankisi@mail.ru', 0, 0, '2017-07-22 17:29:45'),
(15, 'პაროლის აღდგენა', 'no-reply@lovetravel.ge', 'levanpankisi@mail.ru', NULL, 'levanpankisi@mail.ru', 0, 0, '2017-07-22 19:41:14'),
(16, 'პაროლის აღდგენა', 'no-reply@lovetravel.ge', 'levanpankisi@mail.ru', NULL, 'levanpankisi@mail.ru', 0, 0, '2017-07-22 20:51:21'),
(17, 'პაროლის აღდგენა', 'no-reply@lovetravel.ge', 'levanpankisi@mail.ru', NULL, 'მომხმარებლის levanpankisi@mail.ru პაროლის შესაცვლელად დააკლიკეთ შემდეგ ლინკს: <a href="http://localhost/lovetravel/ka/user/newpassword/?Hash=c915175efe5895c6bb54862b00341efe" target="_blank">http://localhost/lovetravel/ka/user/newpassword/?Hash=c915175efe5895c6bb54862b00341efe</a>', 0, 0, '2017-07-22 20:52:43'),
(18, 'პაროლის აღდგენა', 'no-reply@lovetravel.ge', 'levanpankisi@mail.ru', NULL, 'მომხმარებლის levanpankisi@mail.ru პაროლის შესაცვლელად დააკლიკეთ შემდეგ ლინკს: <a href="http://localhost/lovetravel/ka/user/newpassword/?Hash=bac6822d0886633b2d09ad97b78578f2" target="_blank">http://localhost/lovetravel/ka/user/newpassword/?Hash=bac6822d0886633b2d09ad97b78578f2</a>', 0, 0, '2017-07-22 22:47:22'),
(19, 'პაროლის აღდგენა', 'no-reply@lovetravel.ge', 'levanpankisi@mail.ru', NULL, 'მომხმარებლის levanpankisi@mail.ru პაროლის შესაცვლელად დააკლიკეთ შემდეგ ლინკს: <a href="http://localhost/lovetravel/ka/user/newpassword/?Hash=8c737db6dbd8afc9a7221eed35d2c38b" target="_blank">http://localhost/lovetravel/ka/user/newpassword/?Hash=8c737db6dbd8afc9a7221eed35d2c38b</a>', 0, 0, '2017-07-23 17:39:31'),
(20, 'Contact', 'levanpankisi@mail.ru', 'info@lovetravel.ge', 'levanpankisi@mail.ru', 'Name: ლევან<br><br>Phone: 598640814<br><br>test message', 0, 0, '2017-07-23 18:03:47'),
(21, 'Contact', 'levanpankisi@mail.ru', 'info@lovetravel.ge', 'levanpankisi@mail.ru', 'Name: fdgerger<br><br>Phone: ergergreg<br><br>wefwefew\r\n', 1, 0, '2017-07-23 18:04:56');

-- --------------------------------------------------------

--
-- Table structure for table `emails_arc`
--

CREATE TABLE `emails_arc` (
  `mail_id` int(11) NOT NULL,
  `status_id` tinyint(1) NOT NULL,
  `mail_subject` varchar(200) NOT NULL,
  `mail_from` varchar(100) NOT NULL,
  `mail_to` varchar(100) NOT NULL,
  `mail_reply_to` varchar(100) NOT NULL,
  `mail_message` text NOT NULL,
  `mail_priority` smallint(6) NOT NULL DEFAULT '0',
  `attempt` smallint(6) NOT NULL,
  `insert_date` datetime NOT NULL,
  `send_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `email_hashes`
--

CREATE TABLE `email_hashes` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `type_id` smallint(6) NOT NULL DEFAULT '0' COMMENT '0 - Email activation, 1 - Password recovery',
  `hash` varchar(32) NOT NULL,
  `insert_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `email_hashes`
--

INSERT INTO `email_hashes` (`id`, `user_id`, `type_id`, `hash`, `insert_date`) VALUES
(3, 0, 0, '5c788c7fdfba75d99baa5f57a6c4dc72', '2017-07-22 14:20:10'),
(4, 0, 0, '6b731f3fee03299cadd59a0a65f4435f', '2017-07-22 14:21:42'),
(5, 0, 0, '4af5718a1ca4b9244a067cadf36ca12f', '2017-07-22 14:23:02'),
(6, 0, 0, '23261d6523b0f4adb402ba7ac5263f7b', '2017-07-22 14:24:34'),
(7, 0, 0, '58c9ea089d1484b585c0a45afab5579f', '2017-07-22 14:25:27'),
(8, 0, 0, '6554301152247efd92aebea5cc757d4f', '2017-07-22 14:26:04'),
(9, 0, 0, '7ee138a73ba277cb6925cc75d4715849', '2017-07-22 14:28:01'),
(17, 14, 1, '5ff93e687f8c0f4fc3cf43ac232ce409', '2017-07-22 20:51:21'),
(19, 14, 1, 'bac6822d0886633b2d09ad97b78578f2', '2017-07-22 22:47:22'),
(20, 14, 1, '8c737db6dbd8afc9a7221eed35d2c38b', '2017-07-23 17:39:31');

-- --------------------------------------------------------

--
-- Table structure for table `favorites`
--

CREATE TABLE `favorites` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `tour_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `favorites`
--

INSERT INTO `favorites` (`id`, `user_id`, `tour_id`) VALUES
(47, 14, 10),
(50, 14, 11),
(58, 14, 13),
(54, 14, 14),
(48, 14, 18);

-- --------------------------------------------------------

--
-- Table structure for table `langs`
--

CREATE TABLE `langs` (
  `lang_var_id` int(11) NOT NULL,
  `lang_var` varchar(255) NOT NULL,
  `lang_var_js` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `langs`
--

INSERT INTO `langs` (`lang_var_id`, `lang_var`, `lang_var_js`) VALUES
(1, 'Close', 1),
(2, 'Message', 1),
(3, 'Tel', 1),
(4, 'Address', 1),
(5, 'Home', 1),
(6, 'About', 1),
(7, 'Partners', 1),
(8, 'Products', 1),
(9, 'News', 1),
(10, 'Blog', 1),
(11, 'Contact', 1),
(12, 'OurTeam', 0),
(13, 'ReadMore', 0),
(14, 'AllNews', 0),
(15, 'url', 0),
(16, 'WriteYourEmail', 0),
(17, 'WriteYourPhone', 0),
(18, 'YourPhone', 0),
(19, 'MessageText', 0),
(20, 'WriteMessageText', 0),
(21, 'WriteCaptcha', 0),
(22, 'Code', 0),
(23, 'YourEmail', 0),
(24, 'Send', 0),
(25, 'MessageSendError', 0),
(26, 'MessageSendSuccess', 0),
(27, 'IncorrectCaptcha', 0),
(28, 'AboutCompany', 1),
(29, 'OurSpecialOffers', 1),
(30, 'HealthTourismAcademy1', 1),
(31, 'HealthTourismAcademy2', 1),
(33, 'HealthTourismAcademy3', 1),
(34, 'Phones', 0),
(35, 'Email', 0),
(36, 'OpenHours', 0),
(37, 'Documentation', 0),
(38, 'OurProducts', 0),
(39, 'Services', 0),
(40, 'AllOutTours', 0),
(41, 'TravelWithUs', 0),
(42, 'ReadMoreAboutUs', 0),
(44, 'Social', 0),
(45, 'ContactUs', 0),
(46, 'ContactPhones', 0),
(47, 'AllRightsReserved', 0),
(48, 'Direction', 0),
(49, 'Category', 0),
(50, 'Period', 0),
(51, 'From', 0),
(52, 'To', 0),
(53, 'Budget', 0),
(54, 'Search', 0),
(55, 'Select', 0),
(56, 'Published', 0),
(57, 'Views', 0),
(58, 'ShowLess', 0),
(59, 'Hotels', 0),
(60, 'TourServices', 0),
(61, 'TourAddServices', 0),
(62, 'Note', 0),
(63, 'SimilarTours', 0),
(64, 'ThematicTours', 0),
(65, 'ToursGallery', 0),
(75, 'test123', 0),
(76, 'LogIn', 0),
(77, 'Register', 0),
(78, 'AirlineTickets', 0),
(79, 'Theme', 0),
(80, 'Tours', 0),
(81, 'SubscribeNews', 0),
(82, 'SearchKeyword', 0),
(83, 'Begin', 0),
(84, 'End', 0),
(85, 'Bestsellers', 0),
(86, 'Favorite', 0),
(87, 'Day', 0),
(88, 'Minute', 0),
(89, 'Hour', 0),
(90, 'AddServices', 0),
(91, 'TourBuyNow', 0),
(92, 'Tour', 0),
(93, 'Duration', 0),
(94, 'SelectBlogTheme', 0),
(95, 'LatestBlogs', 0),
(96, 'Read', 0),
(97, 'Name', 0),
(98, 'SurName', 0),
(99, 'BirthDate', 0),
(100, 'Gender', 0),
(101, 'Male', 0),
(102, 'Female', 0),
(103, 'ContactPhone', 0),
(104, 'Country', 0),
(105, 'Password', 0),
(106, 'RetypePassword', 0),
(107, 'WriteName', 0),
(108, 'WriteSurName', 0),
(109, 'SelectCountry', 0),
(110, 'SelectBirthDate', 0),
(111, 'WriteAddress', 0),
(112, 'WriteEmail', 0),
(113, 'WriteContactPhone', 0),
(114, 'WritePassword', 0),
(115, 'PasswordsDontMatch', 0),
(116, 'Authorize', 0),
(117, 'Continue', 0),
(118, 'RecoverPassword', 0),
(119, 'MyPage', 0),
(120, 'PersonalData', 0),
(121, 'EditAccount', 0),
(122, 'ChangePassword', 0),
(123, 'MyFavorites', 0),
(124, 'UserID', 0),
(125, 'LogOut', 0),
(126, 'SuccessMessage', 1),
(127, 'ErrorMessage', 1),
(128, 'UserNotFound', 0),
(129, 'IncorrectPassword', 0),
(130, 'CurrentPassword', 0),
(131, 'WriteCurrentPassword', 0),
(132, 'NewPassword', 0),
(133, 'RetypeNewPassword', 0),
(134, 'Save', 0),
(135, 'IncorrectCurrentPassword', 0),
(136, 'UserExists', 0),
(137, 'ActEmailSent', 0),
(138, 'ActEmailMessage', 0),
(139, 'EmailActivation', 0),
(140, 'RegisterSuccess', 0),
(141, 'InactEmailMessage', 0),
(142, 'ResendLink', 1),
(143, 'ForgotPassword', 0),
(144, 'PasswordRecoverEmailSent', 0),
(145, 'PasswordRecoverMessage', 0),
(146, 'WriteNewPassword', 0),
(147, 'NewPasswordError', 0),
(148, 'EmailActivationSuccess', 0),
(149, 'EmailActivationError', 0),
(150, 'PleaseLogIn', 1),
(151, 'UpdateCaptcha', 0),
(152, 'Directions', 0),
(153, 'TourTypes', 0);

-- --------------------------------------------------------

--
-- Table structure for table `lang_descr`
--

CREATE TABLE `lang_descr` (
  `id` int(11) NOT NULL,
  `lang_var_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lang_descr`
--

INSERT INTO `lang_descr` (`id`, `lang_var_id`, `lang_id`, `title`) VALUES
(1, 1, 1, 'დახურვა'),
(2, 2, 1, 'შეტყობინება'),
(3, 3, 1, 'ტელ.:'),
(4, 4, 1, 'მისამართი'),
(5, 5, 1, 'მთავარი'),
(6, 6, 1, 'კომპანიის შესახებ'),
(7, 7, 1, 'პარტნიორები'),
(8, 8, 1, 'პროდუქცია'),
(9, 9, 1, 'სიახლეები'),
(10, 10, 1, 'ბლოგი'),
(11, 11, 1, 'კონტაქტი'),
(12, 12, 1, 'ჩვენი გუნდი'),
(13, 13, 1, 'ვრცლად ნახვა'),
(14, 14, 1, 'ყველა სიახლე'),
(15, 15, 1, 'ვებ გვერდი'),
(16, 16, 1, 'მიუთითეთ თქვენი ელ-ფოსტა'),
(17, 17, 1, 'მიუთითეთ თქვენი ტელეფონის ნომერი'),
(18, 18, 1, 'თქვენი ტელეფონის ნომერი'),
(19, 19, 1, 'წერილის ტექსტი'),
(20, 20, 1, 'მიუთითეთ წერილის ტექსტი'),
(21, 21, 1, 'მიუთითეთ უსაფრთხოების კოდი'),
(22, 22, 1, 'კოდი'),
(23, 23, 1, 'თქვენი ელ-ფოსტა'),
(24, 24, 1, 'გაგზავნა'),
(25, 25, 1, 'წერილის გაგზზავნის დროს მოხდა შეცდომა. სცადეთ თავიდან.'),
(26, 26, 1, 'წერილი წარმატებით გაიგზავნა'),
(27, 27, 1, 'უსაფრთხოების კოდი არასწორია'),
(28, 28, 1, 'კომპანიის შესახებ'),
(29, 29, 1, 'ჩვენი შემოთავაზებები'),
(30, 30, 1, 'ტრენინგები სამკურნალო-განმაჯანსაღებელი ცენტრების პროფესიონალთათვის'),
(31, 31, 1, 'ტრენინგები სასტუმრო-რეზორტების და განმაჯანსაღებელი ცენტრებისთვის'),
(32, 32, 1, ''),
(33, 33, 1, 'ტრენინგები სამედიცინო ტურიზმის პროფესიონალთათვის'),
(34, 34, 1, 'ტელეფონები'),
(35, 35, 1, 'ელ. ფოსტა'),
(36, 36, 1, 'სამუშაო საათები'),
(37, 37, 1, 'დოკუმენტაცია'),
(38, 38, 1, 'ჩვენ გთავაზობთ'),
(39, 39, 1, 'სერვისები'),
(40, 40, 1, 'ყველა ტური საზღვარგარეთ'),
(41, 41, 1, 'იმოგზაურეთ ჩვენთან ერთად'),
(42, 42, 1, 'ვრცლად ჩვენს შესახებ'),
(43, 43, 1, ''),
(44, 44, 1, 'ჩვენი სოციალური ბმულები'),
(45, 45, 1, 'დაგვიკავშირდით'),
(46, 46, 1, 'საკონტაქტო ტელეფონები'),
(47, 47, 1, 'R&L თრაველი 2017 ყველა უფლება დაცულია'),
(48, 48, 1, 'მიმართულება'),
(49, 49, 1, 'კატეგორია'),
(50, 50, 1, 'პერიოდი'),
(51, 51, 1, 'დან'),
(52, 52, 1, 'მდე'),
(53, 53, 1, 'ბიუჯეტი'),
(54, 54, 1, 'ძებნა'),
(55, 55, 1, 'აირჩიეთ'),
(56, 56, 1, 'გამოქვეყნებულია'),
(57, 57, 1, 'ნახვები'),
(58, 58, 1, 'აჩვენე ნაკლები'),
(59, 59, 1, 'სასტუმროები'),
(60, 60, 1, 'მომსახურება, რომელიც ფასში შედის'),
(61, 61, 1, 'დამატებითი მომსახურება'),
(62, 62, 1, 'შენიშვნა'),
(63, 63, 1, 'მსგავსი ტურები კატეგორიიდან'),
(64, 64, 1, 'თემატური ტურები'),
(65, 65, 1, 'ტურების გალერეა'),
(66, 1, 2, 'Close'),
(67, 2, 2, 'Message'),
(68, 3, 2, 'Tel.:'),
(69, 4, 2, 'Address'),
(70, 5, 2, 'Home'),
(71, 6, 2, 'About Company'),
(72, 7, 2, 'Partners'),
(73, 8, 2, 'Products'),
(74, 9, 2, 'News'),
(75, 10, 2, 'Blog'),
(76, 11, 2, 'Contact'),
(77, 12, 2, 'Our Team'),
(78, 13, 2, 'Read More'),
(79, 14, 2, 'All news'),
(80, 15, 2, 'url'),
(81, 16, 2, 'მიუთითეთ თქვენი ელ-ფოსტა'),
(82, 17, 2, 'მიუთითეთ თქვენი ტელეფონის ნომერი'),
(83, 18, 2, 'თქვენი ტელეფონის ნომერი'),
(84, 19, 2, 'წერილის ტექსტი'),
(85, 20, 2, 'მიუთითეთ წერილის ტექსტი'),
(86, 21, 2, 'მიუთითეთ უსაფრთხოების კოდი'),
(87, 22, 2, 'Code'),
(88, 23, 2, 'თქვენი ელ-ფოსტა1'),
(89, 24, 2, 'Send'),
(90, 25, 2, 'წერილის გაგზზავნის დროს მოხდა შეცდომა. სცადეთ თავიდან.'),
(91, 26, 2, 'წერილი წარმატებით გაიგზავნა1'),
(92, 27, 2, 'უსაფრთხოების კოდი არასწორია'),
(93, 28, 2, 'About Company'),
(94, 29, 2, 'Our Special Offers'),
(95, 30, 2, 'Training for Wellness centres’ Professionals'),
(96, 31, 2, 'Training for Hotels, Resorts and Wellness Centres'),
(97, 32, 2, ''),
(98, 33, 2, 'Trainings for Health Tourism Professionals'),
(99, 34, 2, 'Phones'),
(100, 35, 2, 'Email'),
(101, 36, 2, 'We Are Open'),
(102, 37, 2, 'Documentation'),
(103, 38, 2, 'We are offer'),
(104, 39, 2, 'Services'),
(105, 40, 2, 'ყველა ტური საზღვარგარეთ'),
(106, 41, 2, 'Travel with us'),
(107, 42, 2, 'Read more about us'),
(108, 43, 2, ''),
(109, 44, 2, 'ჩვენი სოციალური ბმულები'),
(110, 45, 2, 'დაგვიკავშირდით'),
(111, 46, 2, 'საკონტაქტო ტელეფონები'),
(112, 47, 2, 'R&L თრაველი 2017 ყველა უფლება დაცულია'),
(113, 48, 2, 'Direction'),
(114, 49, 2, 'Category'),
(115, 50, 2, 'Period'),
(116, 51, 2, 'From'),
(117, 52, 2, 'To'),
(118, 53, 2, 'Budget'),
(119, 54, 2, 'Search'),
(120, 55, 2, 'Select'),
(121, 56, 2, 'Published'),
(122, 57, 2, 'Views'),
(123, 58, 2, 'აჩვენე ნაკლები'),
(124, 59, 2, 'Hotels'),
(125, 60, 2, 'მომსახურება, რომელიც ფასში შედის'),
(126, 61, 2, 'დამატებითი მომსახურება'),
(127, 62, 2, 'Note'),
(128, 63, 2, 'მსგავსი ტურები კატეგორიიდან'),
(129, 64, 2, 'Thematic tours'),
(130, 65, 2, 'ტურების გალერეა'),
(131, 1, 3, 'Закрыть'),
(132, 2, 3, 'Сообщение'),
(133, 3, 3, 'Тел .:'),
(134, 4, 3, 'Aдрес'),
(135, 5, 3, 'Главная'),
(136, 6, 3, 'О компании'),
(137, 7, 3, 'Партнеры'),
(138, 8, 3, 'პროდუქცია'),
(139, 9, 3, 'Новости'),
(140, 10, 3, 'Блог'),
(141, 11, 3, 'Контакты'),
(142, 12, 3, 'ჩვენი გუნდი'),
(143, 13, 3, 'ვრცლად ნახვა'),
(144, 14, 3, 'Новости'),
(145, 15, 3, 'ვებ გვერდი'),
(146, 16, 3, 'მიუთითეთ თქვენი ელ-ფოსტა'),
(147, 17, 3, 'მიუთითეთ თქვენი ტელეფონის ნომერი'),
(148, 18, 3, 'თქვენი ტელეფონის ნომერი'),
(149, 19, 3, 'წერილის ტექსტი'),
(150, 20, 3, 'მიუთითეთ წერილის ტექსტი'),
(151, 21, 3, 'მიუთითეთ უსაფრთხოების კოდი'),
(152, 22, 3, 'კოდი'),
(153, 23, 3, 'თქვენი ელ-ფოსტა'),
(154, 24, 3, 'გაგზავნა'),
(155, 25, 3, 'წერილის გაგზზავნის დროს მოხდა შეცდომა. სცადეთ თავიდან.'),
(156, 26, 3, 'წერილი წარმატებით გაიგზავნა'),
(157, 27, 3, 'უსაფრთხოების კოდი არასწორია'),
(158, 28, 3, 'О компании'),
(159, 29, 3, 'Спецпредложения'),
(160, 30, 3, 'Тренинги для профессионалов лечебно-оздоровительных центров'),
(161, 31, 3, 'Тренинги для гостиниц, курортов и оздоровительных центров'),
(162, 32, 3, ''),
(163, 33, 3, 'Тренинги для профессионалов лечебного туризма'),
(164, 34, 3, 'ტელეფონები'),
(165, 35, 3, 'ელ. ფოსტა'),
(166, 36, 3, 'სამუშაო საათები'),
(167, 37, 3, 'დოკუმენტაცია'),
(168, 38, 3, 'Мы предлагаем'),
(169, 39, 3, 'სერვისები'),
(170, 40, 3, 'ყველა ტური საზღვარგარეთ'),
(171, 41, 3, 'იმოგზაურეთ ჩვენთან ერთად'),
(172, 42, 3, 'ვრცლად ჩვენს შესახებ'),
(173, 43, 3, ''),
(174, 44, 3, 'ჩვენი სოციალური ბმულები'),
(175, 45, 3, 'დაგვიკავშირდით'),
(176, 46, 3, 'საკონტაქტო ტელეფონები'),
(177, 47, 3, 'R&L თრაველი 2017 ყველა უფლება დაცულია'),
(178, 48, 3, 'მიმართულება'),
(179, 49, 3, 'კატეგორია'),
(180, 50, 3, 'პერიოდი'),
(181, 51, 3, 'დან'),
(182, 52, 3, 'მდე'),
(183, 53, 3, 'ბიუჯეტი'),
(184, 54, 3, 'ძებნა'),
(185, 55, 3, 'აირჩიეთ'),
(186, 56, 3, 'გამოქვეყნებულია'),
(187, 57, 3, 'ნახვები'),
(188, 58, 3, 'აჩვენე ნაკლები'),
(189, 59, 3, 'სასტუმროები'),
(190, 60, 3, 'მომსახურება, რომელიც ფასში შედის'),
(191, 61, 3, 'დამატებითი მომსახურება'),
(192, 62, 3, 'Note'),
(193, 63, 3, 'მსგავსი ტურები კატეგორიიდან'),
(194, 64, 3, 'თემატური ტურები'),
(195, 65, 3, 'ტურების გალერეა'),
(196, 1, 4, 'Close'),
(197, 2, 4, 'შეტყობინება'),
(198, 3, 4, 'ტელ.:'),
(199, 4, 4, 'მისამართი'),
(200, 5, 4, 'მთავარი'),
(201, 6, 4, 'კომპანიის შესახებ'),
(202, 7, 4, 'პარტნიორები'),
(203, 8, 4, 'პროდუქცია'),
(204, 9, 4, 'სიახლეები'),
(205, 10, 4, 'ბლოგი'),
(206, 11, 4, 'კონტაქტი'),
(207, 12, 4, 'ჩვენი გუნდი'),
(208, 13, 4, 'ვრცლად ნახვა'),
(209, 14, 4, 'ყველა სიახლე'),
(210, 15, 4, 'ვებ გვერდი'),
(211, 16, 4, 'მიუთითეთ თქვენი ელ-ფოსტა'),
(212, 17, 4, 'მიუთითეთ თქვენი ტელეფონის ნომერი'),
(213, 18, 4, 'თქვენი ტელეფონის ნომერი'),
(214, 19, 4, 'წერილის ტექსტი'),
(215, 20, 4, 'მიუთითეთ წერილის ტექსტი'),
(216, 21, 4, 'მიუთითეთ სურათზე მითითებული კოდი'),
(217, 22, 4, 'კოდი'),
(218, 23, 4, 'თქვენი ელ-ფოსტა'),
(219, 24, 4, 'გაგზავნა'),
(220, 25, 4, 'წერილის გაგზზავნის დროს მოხდა შეცდომა. სცადეთ თავიდან.'),
(221, 26, 4, 'წერილი წარმატებით გაიგზავნა'),
(222, 27, 4, 'არასწორია სურათზე მითითებული კოდი'),
(223, 28, 4, 'კომპანიის შესახებ'),
(224, 29, 4, 'ჩვენი შემოთავაზებები'),
(225, 30, 4, 'ტრენინგები სამკურნალო-განმაჯანსაღებელი ცენტრების პროფესიონალთათვის'),
(226, 31, 4, 'ტრენინგები სასტუმრო-რეზორტების და განმაჯანსაღებელი ცენტრებისთვის'),
(227, 32, 4, ''),
(228, 33, 4, 'ტრენინგები სამედიცინო ტურიზმის პროფესიონალთათვის'),
(229, 34, 4, 'ტელეფონები'),
(230, 35, 4, 'ელ. ფოსტა'),
(231, 36, 4, 'სამუშაო საათები'),
(232, 37, 4, 'დოკუმენტაცია'),
(233, 38, 4, 'ჩვენ გთავაზობთ'),
(234, 39, 4, 'სერვისები'),
(235, 40, 4, 'ყველა ტური საზღვარგარეთ'),
(236, 41, 4, 'იმოგზაურეთ ჩვენთან ერთად'),
(237, 42, 4, 'ვრცლად ჩვენს შესახებ'),
(238, 43, 4, ''),
(239, 44, 4, 'სოციალური ბმულები'),
(240, 45, 4, 'დაგვიკავშირდით'),
(241, 46, 4, 'საკონტაქტო ტელეფონები'),
(242, 47, 4, 'R&L თრაველი 2017 ყველა უფლება დაცულია'),
(243, 48, 4, 'მიმართულება'),
(244, 49, 4, 'კატეგორია'),
(245, 50, 4, 'პერიოდი'),
(246, 51, 4, 'დან'),
(247, 52, 4, 'მდე'),
(248, 53, 4, 'ბიუჯეტი'),
(249, 54, 4, 'ძებნა'),
(250, 55, 4, 'აირჩიეთ'),
(251, 56, 4, 'გამოქვეყნებულია'),
(252, 57, 4, 'ნახვები'),
(253, 58, 4, 'აჩვენე ნაკლები'),
(254, 59, 4, 'სასტუმროები'),
(255, 60, 4, 'მომსახურება, რომელიც ფასში შედის'),
(256, 61, 4, 'დამატებითი მომსახურება'),
(257, 62, 4, 'Note'),
(258, 63, 4, 'მსგავსი ტურები'),
(259, 64, 4, 'თემატური ტურები'),
(260, 65, 4, 'ტურების გალერეა'),
(373, 75, 1, 'ewfwefwefwef1231'),
(374, 75, 2, 'wefwefwe'),
(375, 75, 3, 'fwefwefewf'),
(376, 75, 4, 'wefwefewf'),
(383, 76, 1, 'შესვლა'),
(384, 76, 2, 'შესვლა'),
(385, 76, 3, 'შესვლა'),
(386, 77, 1, 'რეგისტრაცია'),
(387, 77, 2, 'რეგისტრაცია'),
(388, 77, 3, 'რეგისტრაცია'),
(389, 78, 1, 'ავიაბილეთები'),
(390, 78, 2, 'Airline tickets'),
(391, 78, 3, 'ავიაბილეთები'),
(392, 79, 1, 'თემა'),
(393, 79, 2, 'Theme'),
(394, 79, 3, 'თემა'),
(395, 80, 1, 'ტურები'),
(396, 80, 2, 'Tours'),
(397, 80, 3, 'ტურები'),
(398, 81, 1, 'გამოიწერეთ ჩვენი სიახლეები'),
(399, 81, 2, 'გამოიწერეთ ჩვენი სიახლეები'),
(400, 81, 3, 'გამოიწერეთ ჩვენი სიახლეები'),
(401, 82, 1, 'საძიებო სიტყვა'),
(402, 82, 2, 'საძიებო სიტყვა'),
(403, 82, 3, 'საძიებო სიტყვა'),
(404, 83, 1, 'დასაწყისი'),
(405, 83, 2, 'დასაწყისი'),
(406, 83, 3, 'დასაწყისი'),
(407, 84, 1, 'დასასრული'),
(408, 84, 2, 'დასასრული'),
(409, 84, 3, 'დასასრული'),
(410, 85, 1, 'ბესტსელერები'),
(411, 85, 2, 'Bestsellers'),
(412, 85, 3, 'ბესტსელერები'),
(413, 86, 1, 'ფავორიტი'),
(414, 86, 2, 'Favorite'),
(415, 86, 3, 'ფავორიტი'),
(416, 87, 1, 'დღე'),
(417, 87, 2, 'Days'),
(418, 87, 3, 'დღე'),
(419, 88, 1, 'წუთი'),
(420, 88, 2, 'Minute'),
(421, 88, 3, 'წუთი'),
(422, 89, 1, 'საათი'),
(423, 89, 2, 'Hour'),
(424, 89, 3, 'საათი'),
(425, 90, 1, 'დამატებითი სერვისები'),
(426, 90, 2, 'Additional services'),
(427, 90, 3, 'დამატებითი სერვისები'),
(428, 91, 1, 'შეიძინეთ ახლა - მიიღეთ დაბალი ფასი'),
(429, 91, 2, 'შეიძინეთ ახლა - მიიღეთ დაბალი ფასი'),
(430, 91, 3, 'შეიძინეთ ახლა - მიიღეთ დაბალი ფასი'),
(431, 92, 1, 'ტური'),
(432, 92, 2, 'Tour'),
(433, 92, 3, 'ტური'),
(434, 93, 1, 'ხანგრძლივობა'),
(435, 93, 2, 'Duration'),
(436, 93, 3, 'ხანგრძლივობა'),
(443, 94, 1, 'აირჩიეთ ბლოგის თემა'),
(444, 94, 2, 'აირჩიეთ ბლოგის თემა'),
(445, 94, 3, 'აირჩიეთ ბლოგის თემა'),
(446, 95, 1, 'უახლესი ბლოგები'),
(447, 95, 2, 'უახლესი ბლოგები'),
(448, 95, 3, 'უახლესი ბლოგები'),
(449, 96, 1, 'წაკითხვა'),
(450, 96, 2, 'წაკითხვა'),
(451, 96, 3, 'წაკითხვა'),
(452, 97, 1, 'სახელი'),
(453, 97, 2, 'Name'),
(454, 97, 3, 'სახელი'),
(455, 98, 1, 'გვარი'),
(456, 98, 2, 'გვარი'),
(457, 98, 3, 'გვარი'),
(458, 99, 1, 'დაბადების თარიღი'),
(459, 99, 2, 'დაბადების თარიღი'),
(460, 99, 3, 'დაბადების თარიღი'),
(461, 100, 1, 'სქესი'),
(462, 100, 2, 'სქესი'),
(463, 100, 3, 'სქესი'),
(464, 101, 1, 'მამრ.'),
(465, 101, 2, 'მამრ.'),
(466, 101, 3, 'მამრ.'),
(467, 102, 1, 'მდედრ.'),
(468, 102, 2, 'მდედრ.'),
(469, 102, 3, 'მდედრ.'),
(470, 103, 1, 'საკონტაქტო ტელეფონი'),
(471, 103, 2, 'საკონტაქტო ტელეფონი'),
(472, 103, 3, 'საკონტაქტო ტელეფონი'),
(473, 104, 1, 'ქვეყანა'),
(474, 104, 2, 'ქვეყანა'),
(475, 104, 3, 'ქვეყანა'),
(476, 105, 1, 'პაროლი'),
(477, 105, 2, 'პაროლი'),
(478, 105, 3, 'პაროლი'),
(479, 106, 1, 'გაიმეორეთ პაროლი'),
(480, 106, 2, 'გაიმეორეთ პაროლი'),
(481, 106, 3, 'გაიმეორეთ პაროლი'),
(482, 107, 1, 'მიუთითეთ სახელი'),
(483, 107, 2, 'მიუთითეთ სახელი'),
(484, 107, 3, 'მიუთითეთ სახელი'),
(485, 108, 1, 'მიუთითეთ გვარი'),
(486, 108, 2, 'მიუთითეთ გვარი'),
(487, 108, 3, 'მიუთითეთ გვარი'),
(488, 109, 1, 'მიუთითეთ ქვეყანა'),
(489, 109, 2, 'მიუთითეთ ქვეყანა'),
(490, 109, 3, 'მიუთითეთ ქვეყანა'),
(491, 110, 1, 'მიუთითეთ დაბადების თარიღი'),
(492, 110, 2, 'მიუთითეთ დაბადების თარიღი'),
(493, 110, 3, 'მიუთითეთ დაბადების თარიღი'),
(494, 111, 1, 'მიუთითეთ მისამართი'),
(495, 111, 2, 'მიუთითეთ მისამართი'),
(496, 111, 3, 'მიუთითეთ მისამართი'),
(497, 112, 1, 'მიუთითეთ ელ.ფოსტა'),
(498, 112, 2, 'მიუთითეთ ელ.ფოსტა'),
(499, 112, 3, 'მიუთითეთ ელ.ფოსტა'),
(500, 113, 1, 'მიუთითეთ საკონტაქტო ტელეფონი'),
(501, 113, 2, 'მიუთითეთ საკონტაქტო ტელეფონი'),
(502, 113, 3, 'მიუთითეთ საკონტაქტო ტელეფონი'),
(503, 114, 1, 'მიუთითეთ პაროლი'),
(504, 114, 2, 'მიუთითეთ პაროლი'),
(505, 114, 3, 'მიუთითეთ პაროლი'),
(506, 115, 1, 'პაროლები არ ემთხვევა'),
(507, 115, 2, 'Passwords do not match'),
(508, 115, 3, 'Пароли не совпадают'),
(512, 116, 1, 'ავტორიზაცია'),
(513, 116, 2, 'ავტორიზაცია'),
(514, 116, 3, 'ავტორიზაცია'),
(515, 117, 1, 'გაგრძელება'),
(516, 117, 2, 'Continue'),
(517, 117, 3, 'გაგრძელება'),
(518, 118, 1, 'პაროლის აღდგენა'),
(519, 118, 2, 'პაროლის აღდგენა'),
(520, 118, 3, 'პაროლის აღდგენა'),
(521, 119, 1, 'ჩემი გვერდი'),
(522, 119, 2, 'ჩემი გვერდი'),
(523, 119, 3, 'ჩემი გვერდი'),
(524, 120, 1, 'პირადი მონაცემები'),
(525, 120, 2, 'პირადი მონაცემები'),
(526, 120, 3, 'პირადი მონაცემები'),
(527, 121, 1, 'ანგარიშის რედაქტირება'),
(528, 121, 2, 'ანგარიშის რედაქტირება'),
(529, 121, 3, 'ანგარიშის რედაქტირება'),
(530, 122, 1, 'პაროლის შეცვლა'),
(531, 122, 2, 'პაროლის შეცვლა'),
(532, 122, 3, 'პაროლის შეცვლა'),
(533, 123, 1, 'ჩემი რჩეულები'),
(534, 123, 2, 'ჩემი რჩეულები'),
(535, 123, 3, 'ჩემი რჩეულები'),
(536, 124, 1, 'მომხმარებლის ID'),
(537, 124, 2, 'User ID'),
(538, 124, 3, 'მომხმარებლის ID'),
(539, 125, 1, 'გასვლა'),
(540, 125, 2, 'გასვლა'),
(541, 125, 3, 'გასვლა'),
(542, 126, 1, 'ოპერაცია წარმატებით დასრულდა'),
(543, 126, 2, 'Operation completed successfully'),
(544, 126, 3, 'Операция была успешно завершена'),
(545, 127, 1, 'ოპერაციის შესრულების დროს მოხდა შეცდომა. სცადეთ თავიდან.'),
(546, 127, 2, 'ოპერაციის შესრულების დროს მოხდა შეცდომა. სცადეთ თავიდან.'),
(547, 127, 3, 'ოპერაციის შესრულების დროს მოხდა შეცდომა. სცადეთ თავიდან.'),
(548, 128, 1, 'მომხმარებელი არ მოიძებნა'),
(549, 128, 2, 'მომხმარებელი არ მოიძებნა'),
(550, 128, 3, 'მომხმარებელი არ მოიძებნა'),
(551, 129, 1, 'პაროლი არასწორია'),
(552, 129, 2, 'პაროლი არასწორია'),
(553, 129, 3, 'პაროლი არასწორია'),
(554, 130, 1, 'არსებული პაროლი'),
(555, 130, 2, 'Current password'),
(556, 130, 3, 'Существующий период '),
(557, 131, 1, 'მიუთითეთ არსებული პაროლი'),
(558, 131, 2, 'Write the current password'),
(559, 131, 3, 'Укажите существующий пароль'),
(560, 132, 1, 'ახალი პაროლი'),
(561, 132, 2, 'New password'),
(562, 132, 3, 'Новый пароль '),
(563, 133, 1, 'გაიმეორეთ ახალი პაროლი'),
(564, 133, 2, 'Retype new password'),
(565, 133, 3, 'Повторите новый пароль '),
(566, 134, 1, 'შენახვა'),
(567, 134, 2, 'Save'),
(568, 134, 3, 'შენახვა'),
(569, 135, 1, 'არსებული პაროლი არასწორია'),
(570, 135, 2, 'არსებული პაროლი არასწორია'),
(571, 135, 3, 'არსებული პაროლი არასწორია'),
(572, 136, 1, 'ასეთი ელ-ფოსტით უკვე რეგისტრირებულია სხვა მომხმარებელი, ჩაწერეთ სხვა ელ-ფოსტა.'),
(573, 136, 2, 'ასეთი ელ-ფოსტით უკვე რეგისტრირებულია სხვა მომხმარებელი, ჩაწერეთ სხვა ელ-ფოსტა.'),
(574, 136, 3, 'ასეთი ელ-ფოსტით უკვე რეგისტრირებულია სხვა მომხმარებელი, ჩაწერეთ სხვა ელ-ფოსტა.'),
(575, 137, 1, 'პროფილის აქტივაციის ლინკი გაიგზავნა ელ.ფოსტაზე: {EMAIL}'),
(576, 137, 2, 'Profile activation link was sent to the e-mail: {EMAIL}'),
(577, 137, 3, 'Ссылка активации была отправлена на эл. почту: {EMAIL}'),
(578, 138, 1, 'თქვენი პროფილის გასააქტიურებლად დააკლიკეთ შემდეგ ლინკს: <a href="{LINK}" target="_blank">{LINK}</a>'),
(579, 138, 2, 'თქვენი პროფილის გასააქტიურებლად დააკლიკეთ შემდეგ ლინკს: <a href="{LINK}" target="_blank">{LINK}</a>'),
(580, 138, 3, 'თქვენი პროფილის გასააქტიურებლად დააკლიკეთ შემდეგ ლინკს: <a href="{LINK}" target="_blank">{LINK}</a>'),
(581, 139, 1, 'პროფილის აქტივაცია'),
(582, 139, 2, 'პროფილის აქტივაცია'),
(583, 139, 3, 'პროფილის აქტივაცია'),
(587, 140, 1, 'რეგისტრაცია წარატებით დასრულდა'),
(588, 140, 2, 'რეგისტრაცია წარატებით დასრულდა'),
(589, 140, 3, 'რეგისტრაცია წარატებით დასრულდა'),
(590, 141, 1, 'თქვენი პროფილი არააქტიურია.<br>გააქტიურებისათვის საჭიროა ელ. ფოსტის აქტივაცია. დადასტურების ლინკი გამოგზავნილია ელ. ფოსტაზე {EMAIL}<br><br>მითითებულ ელ.ფოსტაზე აქტივაციის ლინკი თუ არ მოგივიდათ, შეგიძლიათ აქტივაციის ლინკი გააგზავნოთ ხელახლა.'),
(591, 141, 2, 'თქვენი პროფილი არააქტიურია.<br>გააქტიურებისათვის საჭიროა ელ. ფოსტის აქტივაცია. დადასტურების ლინკი გამოგზავნილია ელ. ფოსტაზე {EMAIL}<br><br>მითითებულ ელ.ფოსტაზე აქტივაციის ლინკი თუ არ მოგივიდათ, შეგიძლიათ აქტივაციის ლინკი გააგზავნოთ ხელახლა.'),
(592, 141, 3, 'თქვენი პროფილი არააქტიურია.<br>გააქტიურებისათვის საჭიროა ელ. ფოსტის აქტივაცია. დადასტურების ლინკი გამოგზავნილია ელ. ფოსტაზე {EMAIL}<br><br>მითითებულ ელ.ფოსტაზე აქტივაციის ლინკი თუ არ მოგივიდათ, შეგიძლიათ აქტივაციის ლინკი გააგზავნოთ ხელახლა.'),
(593, 142, 1, 'ლინკის ხელახლა გაგზავნა'),
(594, 142, 2, 'Resend link'),
(595, 142, 3, 'Повторная отправка ссылки'),
(596, 143, 1, 'დაგავიწყდათ პაროლი?'),
(597, 143, 2, 'Forgot password?'),
(598, 143, 3, 'დაგავიწყდათ პაროლი?'),
(599, 144, 1, 'პაროლის აღდგენის ინსტრუქცია გაიგზავნა ელ.ფოსტაზე: {EMAIL}'),
(600, 144, 2, 'პაროლის აღდგენის ინსტრუქცია გაიგზავნა ელ.ფოსტაზე: {EMAIL}'),
(601, 144, 3, 'პაროლის აღდგენის ინსტრუქცია გაიგზავნა ელ.ფოსტაზე: {EMAIL}'),
(602, 145, 1, 'მომხმარებლის {EMAIL} პაროლის შესაცვლელად დააკლიკეთ შემდეგ ლინკს: <a href="{LINK}" target="_blank">{LINK}</a>'),
(603, 145, 2, 'მომხმარებლის {EMAIL} პაროლის შესაცვლელად დააკლიკეთ შემდეგ ლინკს: <a href="{LINK}" target="_blank">{LINK}</a>'),
(604, 145, 3, 'მომხმარებლის {EMAIL} პაროლის შესაცვლელად დააკლიკეთ შემდეგ ლინკს: <a href="{LINK}" target="_blank">{LINK}</a>'),
(605, 146, 1, 'მიუთითეთ ახალი პაროლი'),
(606, 146, 2, 'მიუთითეთ ახალი პაროლი'),
(607, 146, 3, 'მიუთითეთ ახალი პაროლი'),
(608, 147, 1, 'პაროლის აღდგენის დროს მოხდა შეცდომა, პარამეტრები ან ბმული არასწორია. სცადეთ პაროლის აღდგენა თავიდან.'),
(609, 147, 2, 'პაროლის აღდგენის დროს მოხდა შეცდომა, პარამეტრები ან ბმული არასწორია. სცადეთ პაროლის აღდგენა თავიდან.'),
(610, 147, 3, 'პაროლის აღდგენის დროს მოხდა შეცდომა, პარამეტრები ან ბმული არასწორია. სცადეთ პაროლის აღდგენა თავიდან.'),
(611, 148, 1, 'პროფილის აქტივაცია წარმატებით დასრულდა'),
(612, 148, 2, 'პროფილის აქტივაცია წარმატებით დასრულდა'),
(613, 148, 3, 'პროფილის აქტივაცია წარმატებით დასრულდა'),
(614, 149, 1, 'პროფილის აქტივაციის დროს მოხდა შეცდომა, პარამეტრები ან ბმული არასწორია. სცადეთ თავიდან.'),
(615, 149, 2, 'პროფილის აქტივაციის დროს მოხდა შეცდომა, პარამეტრები ან ბმული არასწორია. სცადეთ თავიდან.'),
(616, 149, 3, 'პროფილის აქტივაციის დროს მოხდა შეცდომა, პარამეტრები ან ბმული არასწორია. სცადეთ თავიდან.'),
(617, 150, 1, 'გთხოვთ გაიარეთ ავტორიზაცია'),
(618, 150, 2, 'გთხოვთ გაიარეთ ავტორიზაცია'),
(619, 150, 3, 'გთხოვთ გაიარეთ ავტორიზაცია'),
(626, 151, 1, 'კოდის განახლება'),
(627, 151, 2, 'Update code'),
(628, 151, 3, 'კოდის განახლება'),
(629, 152, 1, 'მიმართულებები'),
(630, 152, 2, 'Directions'),
(631, 152, 3, 'მიმართულებები'),
(632, 153, 1, 'ტურის კატეგორიები'),
(633, 153, 2, 'ტურის კატეგორიები'),
(634, 153, 3, 'ტურის კატეგორიები');

-- --------------------------------------------------------

--
-- Table structure for table `locs`
--

CREATE TABLE `locs` (
  `loc_id` int(11) NOT NULL,
  `parent_loc_id` int(11) NOT NULL DEFAULT '0',
  `status_id` tinyint(1) NOT NULL COMMENT '0 - Inactive, 1 - Active',
  `sub_locs` varchar(255) NOT NULL,
  `sort_order` smallint(6) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `loc_descr`
--

CREATE TABLE `loc_descr` (
  `id` int(11) NOT NULL,
  `loc_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `news_id` int(11) NOT NULL,
  `status_id` smallint(6) NOT NULL DEFAULT '0' COMMENT '0 - Inactive, 1 - Active, 2 - Deleted',
  `photos_cnt` smallint(6) NOT NULL DEFAULT '0',
  `photo_ver` int(11) NOT NULL DEFAULT '0',
  `insert_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`news_id`, `status_id`, `photos_cnt`, `photo_ver`, `insert_date`) VALUES
(1, 2, 1, 6, '2017-04-22 22:23:45'),
(2, 2, 1, 3, '2017-04-22 22:52:29'),
(3, 2, 1, 2, '2017-04-22 22:53:34'),
(4, 2, 0, 1, '2017-04-23 17:25:35'),
(5, 2, 0, 1, '2017-04-23 17:26:02'),
(6, 2, 0, 1, '2017-04-23 17:50:10'),
(7, 2, 0, 1, '2017-04-23 17:51:42'),
(8, 2, 1, 5, '2017-05-02 13:41:30'),
(9, 1, 1, 2, '2017-05-03 09:07:29'),
(10, 1, 1, 3, '2017-05-03 09:08:29'),
(11, 0, 0, 1, '2017-05-05 07:50:47');

-- --------------------------------------------------------

--
-- Table structure for table `news_descr`
--

CREATE TABLE `news_descr` (
  `id` int(11) NOT NULL,
  `news_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `short_descr` text NOT NULL,
  `descr` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `news_descr`
--

INSERT INTO `news_descr` (`id`, `news_id`, `lang_id`, `title`, `short_descr`, `descr`) VALUES
(1, 1, 1, 'First signs of gum disease', 'Gums disease is preceded by few very distinct symptoms. One of these is when you spit out blood after brushing your teeth-ka', '<html><head><title></title></head><body><p>Gums disease is preceded by few very distinct symptoms. One of these is when you spit out blood after brushing your teeth-ka</p></body></html>'),
(2, 1, 2, 'First signs of gum disease - en', 'Gums disease is preceded by few very distinct symptoms. One of these is when you spit out blood after brushing your teeth - en1', '<html><head><title></title></head><body><p>Gums disease is preceded by few very distinct symptoms. One of these is when you spit out blood after brushing your teeth-en1</p></body></html>'),
(3, 1, 3, 'First signs of gum disease - ru', 'Gums disease is preceded by few very distinct symptoms. One of these is when you spit out blood after brushing your teeth - ru1', '<html><head><title></title></head><body><p>Gums disease is preceded by few very distinct symptoms. One of these is when you spit out blood after brushing your teeth-ru1</p></body></html>'),
(16, 2, 1, 'Basic dental care 101', 'Americans oftentimes ignore some basic, daily routines of oral hygiene, that need to be upheld. Practicing healthy habits like these ones will', '<html><head><title></title></head><body><p>Americans oftentimes ignore some basic, daily routines of oral hygiene, that need to be upheld. Practicing healthy habits like these ones will</p></body></html>'),
(17, 2, 2, 'Basic dental care 101', 'Basic dental care 101', '<html><head><title></title></head><body><p>Basic dental care 101</p></body></html>'),
(18, 2, 3, 'Basic dental care 101', 'Basic dental care 101', '<html><head><title></title></head><body><p>Basic dental care 101</p></body></html>'),
(25, 3, 1, 'Avoiding bad breath', 'Americans oftentimes ignore some basic, daily routines of oral hygiene, that need to be upheld. Practicing healthy habits like these ones will', '<html><head><title></title></head><body><p>Americans oftentimes ignore some basic, daily routines of oral hygiene, that need to be upheld. Practicing healthy habits like these ones will</p></body></html>'),
(26, 3, 2, 'Avoiding bad breath', 'Avoiding bad breath', '<html><head><title></title></head><body><p>Avoiding bad breath</p></body></html>'),
(27, 3, 3, 'Avoiding bad breath', 'Avoiding bad breath', '<html><head><title></title></head><body><p>Avoiding bad breath</p></body></html>'),
(34, 4, 1, 'ასდფ', 'სადფ', '<html><head><title></title></head><body></body></html>'),
(35, 4, 2, 'სადფ', 'ასდფ', '<html><head><title></title></head><body><p>ასდფ</p></body></html>'),
(36, 4, 3, 'ასდფ', 'სადფ', '<html><head><title></title></head><body><p>ასდფ</p></body></html>'),
(37, 5, 1, 'ასდფ', 'სადფ', '<html><head><title></title></head><body><p>ასდფასდფ</p></body></html>'),
(38, 5, 2, 'ასდფ', 'სადფ', '<html><head><title></title></head><body><p>ასდფ</p></body></html>'),
(39, 5, 3, 'ასდფ', 'ასდფ', '<html><head><title></title></head><body><p>ასდფ</p></body></html>'),
(40, 6, 1, 'dsf', 'dfsda', '<html><head><title></title></head><body><p>sdfsadf</p></body></html>'),
(41, 6, 2, 'sadf', 'asdf', '<html><head><title></title></head><body><p>sadf</p></body></html>'),
(42, 6, 3, 'asdf', 'asdf', '<html><head><title></title></head><body><p>sadf</p></body></html>'),
(43, 7, 1, 'sadfsadf', 'asdf', '<html><head><title></title></head><body></body></html>'),
(44, 7, 2, 'sadfasdf', '', '<html><head><title></title></head><body></body></html>'),
(45, 7, 3, 'asdf', 'asdf', '<html><head><title></title></head><body><p>asdf</p></body></html>'),
(46, 8, 1, 'Milling and the first bread', 'Our mill has arrived and we''re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh...', '<html><head><title></title></head><body><p>The first bakery I ever worked in was called The Bread Garden in Berkeley, Ca. It was way ahead of its time, predating legendary bakeries like Acme Bread in Berkeley, Ca and Bread Alone in Boiceville, NY. These were the bakeries that started making Artisanal european style breads before anyone in the US knew what they were. To Americans, bread was white, squishy, pre-sliced mediocrity found in the bread aisle at the grocery store.</p><p>David Morris, owner of The Bread Garden, died of cancer at the young age of 65 in April, 2013. He was one of the few certified Master Bakers in the US; a quirky, generous man with an interest in beekeeping, wine making and homebrewing beer. As a homebrewer myself, we shared an interest, and he shared his famous Malted Barley Bread recipe.</p></body></html>'),
(47, 8, 2, 'Milling and the first bread!', 'Our mill has arrived and we''re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh...', '<html><head><title></title></head><body><p>The first bakery I ever worked in was called The Bread Garden in Berkeley, Ca. It was way ahead of its time, predating legendary bakeries like Acme Bread in Berkeley, Ca and Bread Alone in Boiceville, NY. These were the bakeries that started making Artisanal european style breads before anyone in the US knew what they were. To Americans, bread was white, squishy, pre-sliced mediocrity found in the bread aisle at the grocery store.</p><p>David Morris, owner of The Bread Garden, died of cancer at the young age of 65 in April, 2013. He was one of the few certified Master Bakers in the US; a quirky, generous man with an interest in beekeeping, wine making and homebrewing beer. As a homebrewer myself, we shared an interest, and he shared his famous Malted Barley Bread recipe.</p></body></html>'),
(48, 8, 3, 'Milling and the first bread!', 'Our mill has arrived and we''re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh...', '<html><head><title></title></head><body><p>The first bakery I ever worked in was called The Bread Garden in Berkeley, Ca. It was way ahead of its time, predating legendary bakeries like Acme Bread in Berkeley, Ca and Bread Alone in Boiceville, NY. These were the bakeries that started making Artisanal european style breads before anyone in the US knew what they were. To Americans, bread was white, squishy, pre-sliced mediocrity found in the bread aisle at the grocery store.</p><p>David Morris, owner of The Bread Garden, died of cancer at the young age of 65 in April, 2013. He was one of the few certified Master Bakers in the US; a quirky, generous man with an interest in beekeeping, wine making and homebrewing beer. As a homebrewer myself, we shared an interest, and he shared his famous Malted Barley Bread recipe.</p></body></html>'),
(55, 9, 1, 'Milling and the first bread!', 'Our mill has arrived and we''re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh...', '<html><head><title></title></head><body><p>Our mill has arrived and we''re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh...Our mill has arrived and we''re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh...</p></body></html>'),
(56, 9, 2, 'Milling and the first bread!', 'Our mill has arrived and we''re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh...', '<html><head><title></title></head><body><p>Our mill has arrived and we''re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh...Our mill has arrived and we''re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh...Our mill has arrived and we''re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh...</p></body></html>'),
(57, 9, 3, 'Milling and the first bread!', 'Our mill has arrived and we''re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh...', '<html><head><title></title></head><body><p>Our mill has arrived and we''re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh...Our mill has arrived and we''re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh...</p></body></html>'),
(61, 10, 1, 'Milling and the first bread!', 'Our mill has arrived and we''re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh..', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh..Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh..Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh..</p>\r\n</body>\r\n</html>\r\n'),
(62, 10, 2, 'Milling and the first bread!', 'Our mill has arrived and we''re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh..', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh..Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh..Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh..</p>\r\n</body>\r\n</html>\r\n'),
(63, 10, 3, 'Milling and the first bread!', 'Our mill has arrived and we''re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh..', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh..Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh..Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh..</p>\r\n</body>\r\n</html>\r\n'),
(73, 11, 1, '', '', ''),
(74, 11, 2, '', '', ''),
(75, 11, 3, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `service_id` int(11) NOT NULL,
  `status_id` smallint(6) NOT NULL DEFAULT '0' COMMENT '0 - Inactive, 1 - Active, 2 - Deleted',
  `icon` varchar(100) NOT NULL,
  `photos_cnt` smallint(6) NOT NULL DEFAULT '0',
  `photo_ver` int(11) NOT NULL DEFAULT '0',
  `insert_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`service_id`, `status_id`, `icon`, `photos_cnt`, `photo_ver`, `insert_date`) VALUES
(1, 1, 'ic-servsightseeingpass', 1, 5, '2017-07-20 18:54:46'),
(2, 1, 'ic-servplainpass', 1, 1, '2017-07-20 19:07:56'),
(3, 1, 'ic-servtransportpass', 1, 1, '2017-07-20 19:09:17'),
(4, 1, 'ic-servhotelpass', 1, 1, '2017-07-20 19:10:22'),
(5, 1, 'ic-servinsurancepass', 1, 1, '2017-07-20 19:11:27');

-- --------------------------------------------------------

--
-- Table structure for table `service_descr`
--

CREATE TABLE `service_descr` (
  `id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `short_descr` varchar(255) NOT NULL,
  `descr` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `service_descr`
--

INSERT INTO `service_descr` (`id`, `service_id`, `lang_id`, `title`, `short_descr`, `descr`) VALUES
(1, 1, 1, 'სავიზო მომსახურება', 'ლავ თრაველი გთავაზობთ სავიზო მომსახურებას', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>საგზური აქტიურია ორი თვის განმავლობაში. ყველას ვურჩევთ ბოდრუმის ნახვას, იგი არის საპორტო ქალაქი განთავსებული ბოდრუმის ნახევარკუნძულზე. ქალაქი მდებარეობ ეგეოსის ზღვის სანაპიროზე თურქეთის სამხრეთ დასავლთით, მუღლას პროვინციაში.</p>\r\n\r\n<p>მისი თავდაპირველი სახელწოდებაა &quot;ჰელიკარნასი&quot; და დაარსებულია ძველ ბერძენთა მიერ. ტურიზმი აქ მაღალ დონეზე დგას. ქალაქი პოპულარობით სარგებლობს ტურისტებს შორის, როგორც ევროპიდან (გერმანია, დიდი ბრიტანეთი) ასევე რუსეთიდან და აზიის მრავალი ქვეყნიდან.</p>\r\n</body>\r\n</html>\r\n'),
(2, 1, 2, 'სავიზო მომსახურება', 'სავიზო მომსახურება', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>სავიზო მომსახურება</p>\r\n</body>\r\n</html>\r\n'),
(3, 1, 3, 'სავიზო მომსახურება', 'სავიზო მომსახურება', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>სავიზო მომსახურება</p>\r\n</body>\r\n</html>\r\n'),
(16, 2, 1, 'ავიაბილეთები', 'ლავ თრაველი გთავაზობთ სავიზო მომსახურებას', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>საგზური აქტიურია ორი თვის განმავლობაში. ყველას ვურჩევთ ბოდრუმის ნახვას, იგი არის საპორტო ქალაქი განთავსებული ბოდრუმის ნახევარკუნძულზე. ქალაქი მდებარეობ ეგეოსის ზღვის სანაპიროზე თურქეთის სამხრეთ დასავლთით, მუღლას პროვინციაში.</p>\r\n\r\n<p>მისი თავდაპირველი სახელწოდებაა &quot;ჰელიკარნასი&quot; და დაარსებულია ძველ ბერძენთა მიერ. ტურიზმი აქ მაღალ დონეზე დგას. ქალაქი პოპულარობით სარგებლობს ტურისტებს შორის, როგორც ევროპიდან (გერმანია, დიდი ბრიტანეთი) ასევე რუსეთიდან და აზიის მრავალი ქვეყნიდან.</p>\r\n</body>\r\n</html>\r\n'),
(17, 2, 2, 'ავიაბილეთები', 'ავიაბილეთები', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>ავიაბილეთები</p>\r\n</body>\r\n</html>\r\n'),
(18, 2, 3, 'ავიაბილეთები', 'ავიაბილეთები', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>ავიაბილეთები</p>\r\n</body>\r\n</html>\r\n'),
(19, 3, 1, 'ტრანსპორტით უზრუნველყოფა', 'ტრანსპორტით უზრუნველყოფა', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>საგზური აქტიურია ორი თვის განმავლობაში. ყველას ვურჩევთ ბოდრუმის ნახვას, იგი არის საპორტო ქალაქი განთავსებული ბოდრუმის ნახევარკუნძულზე. ქალაქი მდებარეობ ეგეოსის ზღვის სანაპიროზე თურქეთის სამხრეთ დასავლთით, მუღლას პროვინციაში.</p>\r\n\r\n<p>მისი თავდაპირველი სახელწოდებაა &quot;ჰელიკარნასი&quot; და დაარსებულია ძველ ბერძენთა მიერ. ტურიზმი აქ მაღალ დონეზე დგას. ქალაქი პოპულარობით სარგებლობს ტურისტებს შორის, როგორც ევროპიდან (გერმანია, დიდი ბრიტანეთი) ასევე რუსეთიდან და აზიის მრავალი ქვეყნიდან.</p>\r\n</body>\r\n</html>\r\n'),
(20, 3, 2, 'ტრანსპორტით უზრუნველყოფა', 'ტრანსპორტით უზრუნველყოფა', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>ტრანსპორტით უზრუნველყოფა</p>\r\n</body>\r\n</html>\r\n'),
(21, 3, 3, 'ტრანსპორტით უზრუნველყოფა', 'ტრანსპორტით უზრუნველყოფა', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>ტრანსპორტით უზრუნველყოფა</p>\r\n</body>\r\n</html>\r\n'),
(22, 4, 1, 'სასტუმრო', 'გთავაზობტ სასტუმროს დაჯავშნას', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n'),
(23, 4, 2, 'სასტუმრო', 'სასტუმრო', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>სასტუმრო</p>\r\n</body>\r\n</html>\r\n'),
(24, 4, 3, 'სასტუმრო', 'სასტუმრო', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>სასტუმრო</p>\r\n</body>\r\n</html>\r\n'),
(25, 5, 1, 'სამოგზაურო დაზღვევა', 'გთავაზობთ დაზღვევას', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>საგზური აქტიურია ორი თვის განმავლობაში. ყველას ვურჩევთ ბოდრუმის ნახვას, იგი არის საპორტო ქალაქი განთავსებული ბოდრუმის ნახევარკუნძულზე. ქალაქი მდებარეობ ეგეოსის ზღვის სანაპიროზე თურქეთის სამხრეთ დასავლთით, მუღლას პროვინციაში.</p>\r\n\r\n<p>მისი თავდაპირველი სახელწოდებაა &quot;ჰელიკარნასი&quot; და დაარსებულია ძველ ბერძენთა მიერ. ტურიზმი აქ მაღალ დონეზე დგას. ქალაქი პოპულარობით სარგებლობს ტურისტებს შორის, როგორც ევროპიდან (გერმანია, დიდი ბრიტანეთი) ასევე რუსეთიდან და აზიის მრავალი ქვეყნიდან.</p>\r\n</body>\r\n</html>\r\n'),
(26, 5, 2, 'სამოგზაურო დაზღვევა', 'სამოგზაურო დაზღვევა', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>სამოგზაურო დაზღვევა</p>\r\n</body>\r\n</html>\r\n'),
(27, 5, 3, 'სამოგზაურო დაზღვევა', 'სამოგზაურო დაზღვევა', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>სამოგზაურო დაზღვევა</p>\r\n</body>\r\n</html>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `slider_id` int(11) NOT NULL,
  `sort_order` smallint(6) NOT NULL DEFAULT '0',
  `status_id` smallint(6) NOT NULL DEFAULT '0' COMMENT '0 - Inactive, 1 - Active, 2 - Deleted',
  `has_photo` smallint(6) NOT NULL DEFAULT '0',
  `photo_ver` int(11) NOT NULL DEFAULT '0',
  `insert_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`slider_id`, `sort_order`, `status_id`, `has_photo`, `photo_ver`, `insert_date`) VALUES
(1, 2, 1, 1, 7, '2017-05-05 07:56:02'),
(2, 1, 1, 1, 6, '2017-05-05 08:07:54'),
(3, 3, 2, 1, 2, '2017-05-05 08:08:40'),
(4, 0, 2, 1, 1, '2017-05-21 13:55:58'),
(5, 1, 2, 1, 2, '2017-05-21 13:56:46');

-- --------------------------------------------------------

--
-- Table structure for table `slider_descr`
--

CREATE TABLE `slider_descr` (
  `id` int(11) NOT NULL,
  `slider_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title_1` varchar(255) NOT NULL,
  `title_2` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `slider_descr`
--

INSERT INTO `slider_descr` (`id`, `slider_id`, `lang_id`, `title_1`, `title_2`) VALUES
(1, 5, 1, 'wefwef', 'ergerg'),
(2, 5, 2, 'wefwefwefwef', 'regerg'),
(3, 5, 3, 'wefwefwef', 'ergerg'),
(4, 5, 4, 'wefwefwefewf', 'ergerg'),
(5, 1, 1, 'იმოგზაურეთ ჩვენთან ერთად', 'საზღვარგარეთ და საქართველოში'),
(6, 1, 2, 'იმოგზაურეთ ჩვენთან ერთად', 'საზღვარგარეთ და საქართველოში'),
(7, 1, 3, 'იმოგზაურეთ ჩვენთან ერთად', 'საზღვარგარეთ და საქართველოში'),
(8, 1, 4, 'იმოგზაურეთ<br> ჩვენთან ერთად', 'საზღვარგარეთ და საქართველოში'),
(13, 2, 1, 'საზღვარგარეთ და საქართველოში', 'საზღვარგარეთ და საქართველოში'),
(14, 2, 2, 'საზღვარგარეთ და საქართველოში', 'საზღვარგარეთ და საქართველოში'),
(15, 2, 3, 'საზღვარგარეთ და საქართველოში', 'საზღვარგარეთ და საქართველოში'),
(16, 2, 4, 'იმოგზაურეთ<br> ჩვენთან ერთად', 'საზღვარგარეთ და საქართველოში');

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE `team` (
  `team_id` int(11) NOT NULL,
  `status_id` smallint(11) NOT NULL DEFAULT '0' COMMENT '0 - Inactive, 1 - Active, 2 - Deleted',
  `is_main` tinyint(1) NOT NULL DEFAULT '0',
  `has_photo` tinyint(1) NOT NULL,
  `photo_ver` smallint(6) NOT NULL,
  `insert_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `team`
--

INSERT INTO `team` (`team_id`, `status_id`, `is_main`, `has_photo`, `photo_ver`, `insert_date`) VALUES
(1, 1, 1, 1, 9, '2017-04-22 19:04:10'),
(2, 1, 1, 1, 3, '2017-04-22 19:30:18'),
(3, 1, 1, 1, 2, '2017-04-22 19:54:58'),
(4, 1, 1, 1, 2, '2017-04-22 19:55:12'),
(5, 1, 1, 1, 3, '2017-04-22 19:55:34'),
(6, 1, 1, 1, 7, '2017-04-22 20:56:51'),
(7, 0, 0, 1, 1, '2017-06-01 16:39:00'),
(8, 0, 0, 1, 1, '2017-06-01 16:39:49');

-- --------------------------------------------------------

--
-- Table structure for table `team_descr`
--

CREATE TABLE `team_descr` (
  `id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `position` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `team_descr`
--

INSERT INTO `team_descr` (`id`, `team_id`, `lang_id`, `fullname`, `position`) VALUES
(1, 1, 1, 'Kimberly Barker', 'Patient Services Manager'),
(2, 1, 2, 'Kimberly Barker', 'Patient Services Manager'),
(3, 1, 3, 'Kimberly Barker', 'Patient Services Manager'),
(19, 2, 1, 'wefewf', 'wef'),
(20, 2, 2, 'wefwef', 'wefwef'),
(21, 2, 3, 'wefwef', 'wef'),
(28, 3, 1, 'Kimberly Barker', 'Patient Services Manager'),
(29, 3, 2, 'Kimberly Barker', 'Patient Services Manager'),
(30, 3, 3, 'Kimberly Barker', 'Patient Services Manager'),
(31, 4, 1, 'Kimberly Barker', 'Patient Services Manager'),
(32, 4, 2, 'Kimberly Barker', 'Patient Services Manager'),
(33, 4, 3, 'Kimberly Barker', 'Patient Services Manager'),
(34, 5, 1, 'Kimberly Barker', 'Patient Services Manager'),
(35, 5, 2, 'Kimberly Barker', 'Patient Services Manager'),
(36, 5, 3, 'Kimberly Barker', 'Patient Services Manager'),
(37, 6, 1, 'rgerg', 'ergergre'),
(38, 6, 2, 'ergergreg', 'ergergreg'),
(39, 6, 3, 'ergergreg', 'ergergerg'),
(43, 6, 4, 'agasdgafg', 'adfgergerg'),
(59, 5, 4, 'wqdfwefewf', 'wefwefewf'),
(71, 4, 4, 'Kimberly Barker', 'Patient Services Manager'),
(75, 3, 4, 'wefwef', 'wefwefewf'),
(79, 2, 4, 'wefwef', 'wefwefew'),
(83, 1, 4, 'ewfwef', 'wefwefwefew'),
(88, 8, 1, 'რეგერგრეგ', 'ერგერგრეგ'),
(89, 8, 2, 'ერგერგერგ', 'ერგაერგაერგ'),
(90, 8, 3, 'არეგაერგაერგ', 'არეგაერგერგ'),
(91, 8, 4, 'აერგაერგაერგ', 'არგაერგარგ'),
(96, 7, 1, 'wefwef', 'wefwefew'),
(97, 7, 2, 'wefwefewf', 'wefwefew'),
(98, 7, 3, 'wefwefewf', 'wefwefwef'),
(99, 7, 4, 'wefwefwef', 'wefwefewf');

-- --------------------------------------------------------

--
-- Table structure for table `tours`
--

CREATE TABLE `tours` (
  `tour_id` int(11) NOT NULL,
  `status_id` smallint(6) NOT NULL DEFAULT '0' COMMENT '0 - Inactive, 1 - Active, 2 - Deleted',
  `is_main` tinyint(1) NOT NULL DEFAULT '0',
  `cat_id` int(11) NOT NULL DEFAULT '0',
  `type_id` int(11) NOT NULL DEFAULT '0',
  `old_price` decimal(6,2) NOT NULL DEFAULT '0.00',
  `price` decimal(6,2) NOT NULL DEFAULT '0.00',
  `currency_id` smallint(6) NOT NULL DEFAULT '1',
  `period_from` date NOT NULL,
  `period_to` date NOT NULL,
  `services` varchar(255) NOT NULL,
  `notes` varchar(255) NOT NULL,
  `hotels` text NOT NULL,
  `photos_cnt` smallint(6) NOT NULL DEFAULT '0',
  `photo_ver` int(11) NOT NULL DEFAULT '0',
  `map_coords` varchar(50) NOT NULL,
  `views` int(11) NOT NULL DEFAULT '0',
  `insert_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tours`
--

INSERT INTO `tours` (`tour_id`, `status_id`, `is_main`, `cat_id`, `type_id`, `old_price`, `price`, `currency_id`, `period_from`, `period_to`, `services`, `notes`, `hotels`, `photos_cnt`, `photo_ver`, `map_coords`, `views`, `insert_date`) VALUES
(1, 2, 0, 0, 0, '0.00', '0.00', 1, '0000-00-00', '0000-00-00', '', '', '', 1, 6, '', 0, '2017-04-22 22:23:45'),
(2, 2, 0, 0, 0, '0.00', '0.00', 1, '0000-00-00', '0000-00-00', '', '', '', 1, 3, '', 0, '2017-04-22 22:52:29'),
(3, 2, 0, 0, 0, '0.00', '0.00', 1, '0000-00-00', '0000-00-00', '', '', '', 1, 2, '', 0, '2017-04-22 22:53:34'),
(4, 2, 0, 0, 0, '0.00', '0.00', 1, '0000-00-00', '0000-00-00', '', '', '', 0, 1, '', 0, '2017-04-23 17:25:35'),
(5, 2, 0, 0, 0, '0.00', '0.00', 1, '0000-00-00', '0000-00-00', '', '', '', 0, 1, '', 0, '2017-04-23 17:26:02'),
(6, 2, 0, 0, 0, '0.00', '0.00', 1, '0000-00-00', '0000-00-00', '', '', '', 0, 1, '', 0, '2017-04-23 17:50:10'),
(7, 2, 0, 0, 0, '0.00', '0.00', 1, '0000-00-00', '0000-00-00', '', '', '', 0, 1, '', 0, '2017-04-23 17:51:42'),
(8, 1, 0, 5, 1, '100.00', '150.00', 2, '1970-01-01', '1970-01-01', '{"1":"1","3":"2","4":"1","5":"1","7":"2","6":"1","8":"2","9":"2"}', 'false', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', 5, 10, '', 13, '2017-05-02 13:41:30'),
(9, 1, 0, 2, 3, '100.00', '120.00', 2, '1970-01-01', '1970-01-01', '{"1":"1","2":"1","3":"1","7":"1","9":"1","11":"1"}', 'false', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', 4, 6, '', 8, '2017-05-03 09:07:29'),
(10, 1, 0, 2, 1, '0.00', '500.00', 2, '2017-07-17', '2017-07-21', '{"1":"2","3":"2","8":"1","9":"1","10":"2","11":"1"}', 'false', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', 3, 8, '', 30, '2017-05-03 09:08:29'),
(11, 1, 1, 6, 3, '10.00', '50.00', 1, '2017-05-30', '2017-05-31', '{"1":"1","3":"2","4":"1","5":"1","7":"2","6":"1","8":"2","9":"2"}', '["1","2"]', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', 3, 10, '', 16, '2017-05-05 07:50:47'),
(12, 1, 1, 5, 3, '100.00', '150.00', 1, '2017-05-22', '2017-05-30', '{"1":"1","3":"2","4":"1","5":"1","7":"2","6":"1","8":"2","9":"2"}', 'false', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<ul>\r\n	<li>Ayaz Aqua Hotel 4* (AI) - 269&euro;</li>\r\n	<li>Riva Bodrum Resort 4* (AI) - 279&euro;</li>\r\n	<li>Costa Bitezhan Hotel 4* (AI) - 299&euro;</li>\r\n	<li>Bodrum Park Resort 5* (UA) - 377&euro;</li>\r\n	<li>WOW Bodrum Resort 5* (UA) - 381&euro;</li>\r\n	<li>La Blanche Island ex.Amara Island 5* (UA) - 409&euro;</li>\r\n</ul>\r\n</body>\r\n</html>\r\n', 5, 14, '', 37, '2017-05-21 12:20:22'),
(13, 1, 1, 4, 3, '213.00', '9999.99', 1, '2017-05-17', '2017-05-31', '{"1":"1","3":"2","4":"1","5":"1","7":"2","6":"1","8":"2","9":"2"}', 'false', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', 5, 8, '', 30, '2017-05-22 12:36:12'),
(14, 1, 1, 3, 2, '500.00', '700.00', 2, '2017-05-30', '2017-07-31', '{"1":"1","2":"1","3":"1","4":"1","7":"2","6":"1","8":"1","9":"2"}', 'false', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', 12, 10, '41.69133321257897,44.840733110904694', 184, '2017-05-23 15:57:16'),
(15, 1, 1, 3, 1, '400.00', '600.00', 3, '2017-05-29', '2017-06-06', '{"1":"1","3":"1","4":"1","5":"1","7":"2","8":"2"}', '["1","2","3"]', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<h3>his Okurcalar resort is on the beach, 0.7 mi (1.2 km) from Alara Bazaar, and within 9 mi (15 km) of Alara Han Castle and Sealanya. Konakli Mosque and Konakli Clock Tower Square are also within 12 mi (20 km).</h3>\r\n\r\n<p>Location</p>\r\n\r\n<h3>Resort Features</h3>\r\n\r\n<p>At Justiniano Club Park Conti &ndash; All Inclusive, spend the day on the private sand beach where you can enjoy parasailing. Then return for a meal at one of the resort&#39;s 7 restaurants.</p>\r\n\r\n<h3>Room Amenities</h3>\r\n\r\n<p>All 659 rooms offer furnished balconies, minibars, and LED TVs with satellite channels. Free bottled water, hair dryers, and safes are among the other amenities that guests will find.</p>\r\n</body>\r\n</html>\r\n', 5, 5, '', 55, '2017-05-23 16:20:25'),
(16, 1, 1, 2, 3, '123.00', '3333.00', 2, '2017-06-01', '2017-06-14', '{"1":"1","2":"1","5":"2","7":"1","8":"1","9":"2","10":"1","11":"2"}', '["1","3"]', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<ul>\r\n	<li>სასტუმრო 1</li>\r\n	<li>სასტუმრო 2</li>\r\n	<li>სასტუმრო 3</li>\r\n	<li>სასტუმრო 4</li>\r\n	<li>სასტუმრო 5</li>\r\n</ul>\r\n</body>\r\n</html>\r\n', 12, 9, '', 59, '2017-05-31 20:12:11'),
(17, 1, 1, 1, 3, '200.00', '500.00', 2, '2017-06-09', '2017-06-22', '{"1":"1","2":"1","7":"2","8":"2","11":"1"}', '["1","3"]', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', 6, 4, '', 8, '2017-06-02 10:16:07'),
(18, 1, 0, 5, 7, '0.00', '150.00', 1, '2017-07-13', '2017-07-22', '{"1":"1","3":"1","4":"1","7":"1","8":"2","9":"2","10":"2","11":"2"}', '["2","7","3"]', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>regregreg</p>\r\n</body>\r\n</html>\r\n', 3, 2, '', 42, '2017-07-16 21:24:44');

-- --------------------------------------------------------

--
-- Table structure for table `tour_checked_services`
--

CREATE TABLE `tour_checked_services` (
  `id` int(11) NOT NULL,
  `tour_id` int(11) NOT NULL DEFAULT '0' COMMENT '0 - Inactive, 1 - Active, 2 - Deleted',
  `service_id` int(11) NOT NULL DEFAULT '0',
  `type_id` smallint(6) NOT NULL DEFAULT '1' COMMENT '1 - Service 2 - Additional service'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tour_checked_services`
--

INSERT INTO `tour_checked_services` (`id`, `tour_id`, `service_id`, `type_id`) VALUES
(361, 12, 1, 1),
(362, 12, 3, 2),
(363, 12, 4, 1),
(364, 12, 5, 1),
(365, 12, 7, 2),
(366, 12, 6, 1),
(367, 12, 8, 2),
(368, 12, 9, 2),
(369, 11, 1, 1),
(370, 11, 3, 2),
(371, 11, 4, 1),
(372, 11, 5, 1),
(373, 11, 7, 2),
(374, 11, 6, 1),
(375, 11, 8, 2),
(376, 11, 9, 2),
(432, 17, 1, 1),
(433, 17, 2, 1),
(434, 17, 7, 2),
(435, 17, 8, 2),
(436, 17, 11, 1),
(437, 16, 1, 1),
(438, 16, 2, 1),
(439, 16, 5, 2),
(440, 16, 7, 1),
(441, 16, 8, 1),
(442, 16, 9, 2),
(443, 16, 10, 1),
(444, 16, 11, 2),
(445, 15, 1, 1),
(446, 15, 3, 1),
(447, 15, 4, 1),
(448, 15, 5, 1),
(449, 15, 7, 2),
(450, 15, 8, 2),
(459, 13, 1, 1),
(460, 13, 3, 2),
(461, 13, 4, 1),
(462, 13, 5, 1),
(463, 13, 7, 2),
(464, 13, 6, 1),
(465, 13, 8, 2),
(466, 13, 9, 2),
(467, 14, 1, 1),
(468, 14, 2, 1),
(469, 14, 3, 1),
(470, 14, 4, 1),
(471, 14, 7, 2),
(472, 14, 6, 1),
(473, 14, 8, 1),
(474, 14, 9, 2),
(475, 18, 1, 1),
(476, 18, 3, 1),
(477, 18, 4, 1),
(478, 18, 7, 1),
(479, 18, 8, 2),
(480, 18, 9, 2),
(481, 18, 10, 2),
(482, 18, 11, 2),
(483, 10, 1, 2),
(484, 10, 3, 2),
(485, 10, 8, 1),
(486, 10, 9, 1),
(487, 10, 10, 2),
(488, 10, 11, 1),
(489, 9, 1, 1),
(490, 9, 2, 1),
(491, 9, 3, 1),
(492, 9, 7, 1),
(493, 9, 9, 1),
(494, 9, 11, 1),
(495, 8, 1, 1),
(496, 8, 3, 2),
(497, 8, 4, 1),
(498, 8, 5, 1),
(499, 8, 7, 2),
(500, 8, 6, 1),
(501, 8, 8, 2),
(502, 8, 9, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tour_descr`
--

CREATE TABLE `tour_descr` (
  `id` int(11) NOT NULL,
  `tour_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `short_descr` text NOT NULL,
  `descr` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tour_descr`
--

INSERT INTO `tour_descr` (`id`, `tour_id`, `lang_id`, `title`, `short_descr`, `descr`) VALUES
(1, 1, 1, 'First signs of gum disease', '', '<html><head><title></title></head><body><p>Gums disease is preceded by few very distinct symptoms. One of these is when you spit out blood after brushing your teeth-ka</p></body></html>'),
(2, 1, 2, 'First signs of gum disease - en', '', '<html><head><title></title></head><body><p>Gums disease is preceded by few very distinct symptoms. One of these is when you spit out blood after brushing your teeth-en1</p></body></html>'),
(3, 1, 3, 'First signs of gum disease - ru', '', '<html><head><title></title></head><body><p>Gums disease is preceded by few very distinct symptoms. One of these is when you spit out blood after brushing your teeth-ru1</p></body></html>'),
(16, 2, 1, 'Basic dental care 101', '', '<html><head><title></title></head><body><p>Americans oftentimes ignore some basic, daily routines of oral hygiene, that need to be upheld. Practicing healthy habits like these ones will</p></body></html>'),
(17, 2, 2, 'Basic dental care 101', '', '<html><head><title></title></head><body><p>Basic dental care 101</p></body></html>'),
(18, 2, 3, 'Basic dental care 101', '', '<html><head><title></title></head><body><p>Basic dental care 101</p></body></html>'),
(25, 3, 1, 'Avoiding bad breath', '', '<html><head><title></title></head><body><p>Americans oftentimes ignore some basic, daily routines of oral hygiene, that need to be upheld. Practicing healthy habits like these ones will</p></body></html>'),
(26, 3, 2, 'Avoiding bad breath', '', '<html><head><title></title></head><body><p>Avoiding bad breath</p></body></html>'),
(27, 3, 3, 'Avoiding bad breath', '', '<html><head><title></title></head><body><p>Avoiding bad breath</p></body></html>'),
(34, 4, 1, 'ასდფ', '', '<html><head><title></title></head><body></body></html>'),
(35, 4, 2, 'სადფ', '', '<html><head><title></title></head><body><p>ასდფ</p></body></html>'),
(36, 4, 3, 'ასდფ', '', '<html><head><title></title></head><body><p>ასდფ</p></body></html>'),
(37, 5, 1, 'ასდფ', '', '<html><head><title></title></head><body><p>ასდფასდფ</p></body></html>'),
(38, 5, 2, 'ასდფ', '', '<html><head><title></title></head><body><p>ასდფ</p></body></html>'),
(39, 5, 3, 'ასდფ', '', '<html><head><title></title></head><body><p>ასდფ</p></body></html>'),
(40, 6, 1, 'dsf', '', '<html><head><title></title></head><body><p>sdfsadf</p></body></html>'),
(41, 6, 2, 'sadf', '', '<html><head><title></title></head><body><p>sadf</p></body></html>'),
(42, 6, 3, 'asdf', '', '<html><head><title></title></head><body><p>sadf</p></body></html>'),
(43, 7, 1, 'sadfsadf', '', '<html><head><title></title></head><body></body></html>'),
(44, 7, 2, 'sadfasdf', '', '<html><head><title></title></head><body></body></html>'),
(45, 7, 3, 'asdf', '', '<html><head><title></title></head><body><p>asdf</p></body></html>'),
(46, 8, 1, 'Milling and the first bread', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>The first bakery I ever worked in was called The Bread Garden in Berkeley, Ca. It was way ahead of its time, predating legendary bakeries like Acme Bread in Berkeley, Ca and Bread Alone in Boiceville, NY. These were the bakeries that started making Artisanal european style breads before anyone in the US knew what they were. To Americans, bread was white, squishy, pre-sliced mediocrity found in the bread aisle at the grocery store.</p>\r\n\r\n<p>David Morris, owner of The Bread Garden, died of cancer at the young age of 65 in April, 2013. He was one of the few certified Master Bakers in the US; a quirky, generous man with an interest in beekeeping, wine making and homebrewing beer. As a homebrewer myself, we shared an interest, and he shared his famous Malted Barley Bread recipe.</p>\r\n</body>\r\n</html>\r\n'),
(47, 8, 2, 'Milling and the first bread!', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>The first bakery I ever worked in was called The Bread Garden in Berkeley, Ca. It was way ahead of its time, predating legendary bakeries like Acme Bread in Berkeley, Ca and Bread Alone in Boiceville, NY. These were the bakeries that started making Artisanal european style breads before anyone in the US knew what they were. To Americans, bread was white, squishy, pre-sliced mediocrity found in the bread aisle at the grocery store.</p>\r\n\r\n<p>David Morris, owner of The Bread Garden, died of cancer at the young age of 65 in April, 2013. He was one of the few certified Master Bakers in the US; a quirky, generous man with an interest in beekeeping, wine making and homebrewing beer. As a homebrewer myself, we shared an interest, and he shared his famous Malted Barley Bread recipe.</p>\r\n</body>\r\n</html>\r\n'),
(48, 8, 3, 'Milling and the first bread!', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>The first bakery I ever worked in was called The Bread Garden in Berkeley, Ca. It was way ahead of its time, predating legendary bakeries like Acme Bread in Berkeley, Ca and Bread Alone in Boiceville, NY. These were the bakeries that started making Artisanal european style breads before anyone in the US knew what they were. To Americans, bread was white, squishy, pre-sliced mediocrity found in the bread aisle at the grocery store.</p>\r\n\r\n<p>David Morris, owner of The Bread Garden, died of cancer at the young age of 65 in April, 2013. He was one of the few certified Master Bakers in the US; a quirky, generous man with an interest in beekeeping, wine making and homebrewing beer. As a homebrewer myself, we shared an interest, and he shared his famous Malted Barley Bread recipe.</p>\r\n</body>\r\n</html>\r\n'),
(55, 9, 1, 'Milling and the first bread!', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh...Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh...</p>\r\n</body>\r\n</html>\r\n'),
(56, 9, 2, 'Milling and the first bread!', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh...Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh...Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh...</p>\r\n</body>\r\n</html>\r\n'),
(57, 9, 3, 'Milling and the first bread!', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh...Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh...</p>\r\n</body>\r\n</html>\r\n'),
(61, 10, 1, 'Milling and the first bread!', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh..Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh..Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh..</p>\r\n</body>\r\n</html>\r\n', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh..Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh..Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh..</p>\r\n</body>\r\n</html>\r\n'),
(62, 10, 2, 'Milling and the first bread!', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh..Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh..Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh..</p>\r\n</body>\r\n</html>\r\n'),
(63, 10, 3, 'Milling and the first bread!', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh..Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh..Our mill has arrived and we&#39;re so excited. This marks a new chapter in our Bakery and how we treat our products. Starting this week, we have begun incorporating some of the fresh..</p>\r\n</body>\r\n</html>\r\n'),
(73, 11, 1, 'მოზგაურობა კახეთში', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n'),
(74, 11, 2, 'მოზგაურობა კახეთში', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n'),
(75, 11, 3, 'მოზგაურობა კახეთში', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n'),
(76, 12, 1, 'ტური მესხეთში', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>საგზური აქტიურია ორი თვის განმავლობაში. ყველას ვურჩევთ ბოდრუმის ნახვას, იგი არის საპორტო ქალაქი განთავსებული ბოდრუმის ნახევარკუნძულზე. ქალაქი მდებარეობ ეგეოსის ზღვის სანაპიროზე თურქეთის სამხრეთ დასავლთით, მუღლას პროვინციაში. მისი თავდაპირველი სახელწოდებაა &quot;ჰელიკარნასი&quot; და დაარსებულია ძველ ბერძენთა მიერ. ტურიზმი აქ მაღალ დონეზე დგას. ქალაქი პოპულარობით სარგებლობს ტურისტებს შორის, როგორც ევროპიდან (გერმანია, დიდი ბრიტანეთი) ასევე რუსეთიდან და აზიის მრავალი ქვეყნიდან.</p>\r\n\r\n<p>ღირსშესანიშნაობები</p>\r\n\r\n<p>საუკეთესო ოქროს პლაჟებისა და თანამედროვე ტურისტული ინფრასტრუქტურის გარდა ქალაქი სტუმრებს იზიდავს კიდევ მისი ლამაზი სანაპიროებით, ბოდრუმის ისტორიული ციხესიმაგრით, მის ქვემოთ არსებული წყალქვეშა არქეოლოგიის მუეუმით, მსოფლოში პირველი მავზოლეუმის ნანგრევებით, რომელიც ძველი სამყაროს შვიდ საოცრებათა შორის იყო და მრავალი სხვა ირსშესანიშნაობებით.</p>\r\n\r\n<p>კლიმატი</p>\r\n\r\n<p>აქ ცხელი ხმელთაშუა კლიმატია - ზამთარში შედარებით თბილი და ტენიანი ჰავაა, ტემპერატურა - დაახლოებით 15 &deg;C, ზაფხულში კი საშუალო ტემპერატურაა 34 &deg;C, უმეტესად მშრალი.</p>\r\n\r\n<p>ისტორია</p>\r\n\r\n<p>ჰელიკარნასი დორიელ ბერძენთა მიერ დაარსდა და წარმოადგენდა დორიული ჰექსაპოლისის ნაწილს. იქ აღმოჩენილ მონეტებზე ასახული გორგონა მედუზას თავი, სიბრძნის ქალღმერთი ათინა და პოსეიდონი მიუთითებს, რომ აქაური დორიელები წარმოშობით ტროზედან და არგოსიდან იყვნენ. 1402 წელს წმ. იოანეს ორდენის ჯვაროსანმა რაინდებმა მავზოლეუმის ნანგრევებზე ოტომან სულთან მეჰმედ პირველის ნებართვით ააშენეს წმ. პეტრეს ციხესიმაგრე , რომელიც ჯვაროსნული ეპოქის ერთ-ერთი კარგად შემონახული ძეგლია მცირეაზიაში. მისი სახელწოდებიდან - &quot;პეტრონიუმი&quot; წარმოიშვა ქალაქის ახალი სახელი - ბოდრუმი.</p>\r\n\r\n<p>ადგილობრივი ტრანსპორტი</p>\r\n\r\n<p>ქალაქიდან უახლოესი აეროპორტი არის 36 კილომეტრში. ქალაქის ცენტრში არის ავტოსადგური, რომელიც აკავშირებს თურქეთის სხვადასხვა რეგიონს. პორტში კი ფუნქციონირებს ბორანი, ახლომდებარე თურქულ-ბერძნული ქალაქების მიმართულებით.</p>\r\n</body>\r\n</html>\r\n'),
(77, 12, 2, 'ტური მესხეთში', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>საგზური აქტიურია ორი თვის განმავლობაში. ყველას ვურჩევთ ბოდრუმის ნახვას, იგი არის საპორტო ქალაქი განთავსებული ბოდრუმის ნახევარკუნძულზე. ქალაქი მდებარეობ ეგეოსის ზღვის სანაპიროზე თურქეთის სამხრეთ დასავლთით, მუღლას პროვინციაში. მისი თავდაპირველი სახელწოდებაა &quot;ჰელიკარნასი&quot; და დაარსებულია ძველ ბერძენთა მიერ. ტურიზმი აქ მაღალ დონეზე დგას. ქალაქი პოპულარობით სარგებლობს ტურისტებს შორის, როგორც ევროპიდან (გერმანია, დიდი ბრიტანეთი) ასევე რუსეთიდან და აზიის მრავალი ქვეყნიდან.</p>\r\n\r\n<p>ღირსშესანიშნაობები</p>\r\n\r\n<p>საუკეთესო ოქროს პლაჟებისა და თანამედროვე ტურისტული ინფრასტრუქტურის გარდა ქალაქი სტუმრებს იზიდავს კიდევ მისი ლამაზი სანაპიროებით, ბოდრუმის ისტორიული ციხესიმაგრით, მის ქვემოთ არსებული წყალქვეშა არქეოლოგიის მუეუმით, მსოფლოში პირველი მავზოლეუმის ნანგრევებით, რომელიც ძველი სამყაროს შვიდ საოცრებათა შორის იყო და მრავალი სხვა ირსშესანიშნაობებით.</p>\r\n\r\n<p>კლიმატი</p>\r\n\r\n<p>აქ ცხელი ხმელთაშუა კლიმატია - ზამთარში შედარებით თბილი და ტენიანი ჰავაა, ტემპერატურა - დაახლოებით 15 &deg;C, ზაფხულში კი საშუალო ტემპერატურაა 34 &deg;C, უმეტესად მშრალი.</p>\r\n\r\n<p>ისტორია</p>\r\n\r\n<p>ჰელიკარნასი დორიელ ბერძენთა მიერ დაარსდა და წარმოადგენდა დორიული ჰექსაპოლისის ნაწილს. იქ აღმოჩენილ მონეტებზე ასახული გორგონა მედუზას თავი, სიბრძნის ქალღმერთი ათინა და პოსეიდონი მიუთითებს, რომ აქაური დორიელები წარმოშობით ტროზედან და არგოსიდან იყვნენ. 1402 წელს წმ. იოანეს ორდენის ჯვაროსანმა რაინდებმა მავზოლეუმის ნანგრევებზე ოტომან სულთან მეჰმედ პირველის ნებართვით ააშენეს წმ. პეტრეს ციხესიმაგრე , რომელიც ჯვაროსნული ეპოქის ერთ-ერთი კარგად შემონახული ძეგლია მცირეაზიაში. მისი სახელწოდებიდან - &quot;პეტრონიუმი&quot; წარმოიშვა ქალაქის ახალი სახელი - ბოდრუმი.</p>\r\n\r\n<p>ადგილობრივი ტრანსპორტი</p>\r\n\r\n<p>ქალაქიდან უახლოესი აეროპორტი არის 36 კილომეტრში. ქალაქის ცენტრში არის ავტოსადგური, რომელიც აკავშირებს თურქეთის სხვადასხვა რეგიონს. პორტში კი ფუნქციონირებს ბორანი, ახლომდებარე თურქულ-ბერძნული ქალაქების მიმართულებით.</p>\r\n</body>\r\n</html>\r\n'),
(78, 12, 3, 'ტური მესხეთში', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>საგზური აქტიურია ორი თვის განმავლობაში. ყველას ვურჩევთ ბოდრუმის ნახვას, იგი არის საპორტო ქალაქი განთავსებული ბოდრუმის ნახევარკუნძულზე. ქალაქი მდებარეობ ეგეოსის ზღვის სანაპიროზე თურქეთის სამხრეთ დასავლთით, მუღლას პროვინციაში. მისი თავდაპირველი სახელწოდებაა &quot;ჰელიკარნასი&quot; და დაარსებულია ძველ ბერძენთა მიერ. ტურიზმი აქ მაღალ დონეზე დგას. ქალაქი პოპულარობით სარგებლობს ტურისტებს შორის, როგორც ევროპიდან (გერმანია, დიდი ბრიტანეთი) ასევე რუსეთიდან და აზიის მრავალი ქვეყნიდან.</p>\r\n\r\n<p>ღირსშესანიშნაობები</p>\r\n\r\n<p>საუკეთესო ოქროს პლაჟებისა და თანამედროვე ტურისტული ინფრასტრუქტურის გარდა ქალაქი სტუმრებს იზიდავს კიდევ მისი ლამაზი სანაპიროებით, ბოდრუმის ისტორიული ციხესიმაგრით, მის ქვემოთ არსებული წყალქვეშა არქეოლოგიის მუეუმით, მსოფლოში პირველი მავზოლეუმის ნანგრევებით, რომელიც ძველი სამყაროს შვიდ საოცრებათა შორის იყო და მრავალი სხვა ირსშესანიშნაობებით.</p>\r\n\r\n<p>კლიმატი</p>\r\n\r\n<p>აქ ცხელი ხმელთაშუა კლიმატია - ზამთარში შედარებით თბილი და ტენიანი ჰავაა, ტემპერატურა - დაახლოებით 15 &deg;C, ზაფხულში კი საშუალო ტემპერატურაა 34 &deg;C, უმეტესად მშრალი.</p>\r\n\r\n<p>ისტორია</p>\r\n\r\n<p>ჰელიკარნასი დორიელ ბერძენთა მიერ დაარსდა და წარმოადგენდა დორიული ჰექსაპოლისის ნაწილს. იქ აღმოჩენილ მონეტებზე ასახული გორგონა მედუზას თავი, სიბრძნის ქალღმერთი ათინა და პოსეიდონი მიუთითებს, რომ აქაური დორიელები წარმოშობით ტროზედან და არგოსიდან იყვნენ. 1402 წელს წმ. იოანეს ორდენის ჯვაროსანმა რაინდებმა მავზოლეუმის ნანგრევებზე ოტომან სულთან მეჰმედ პირველის ნებართვით ააშენეს წმ. პეტრეს ციხესიმაგრე , რომელიც ჯვაროსნული ეპოქის ერთ-ერთი კარგად შემონახული ძეგლია მცირეაზიაში. მისი სახელწოდებიდან - &quot;პეტრონიუმი&quot; წარმოიშვა ქალაქის ახალი სახელი - ბოდრუმი.</p>\r\n\r\n<p>ადგილობრივი ტრანსპორტი</p>\r\n\r\n<p>ქალაქიდან უახლოესი აეროპორტი არის 36 კილომეტრში. ქალაქის ცენტრში არის ავტოსადგური, რომელიც აკავშირებს თურქეთის სხვადასხვა რეგიონს. პორტში კი ფუნქციონირებს ბორანი, ახლომდებარე თურქულ-ბერძნული ქალაქების მიმართულებით.</p>\r\n</body>\r\n</html>\r\n'),
(79, 12, 4, 'ტური მესხეთში', '', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>საგზური აქტიურია ორი თვის განმავლობაში. ყველას ვურჩევთ ბოდრუმის ნახვას, იგი არის საპორტო ქალაქი განთავსებული ბოდრუმის ნახევარკუნძულზე. ქალაქი მდებარეობ ეგეოსის ზღვის სანაპიროზე თურქეთის სამხრეთ დასავლთით, მუღლას პროვინციაში. მისი თავდაპირველი სახელწოდებაა &quot;ჰელიკარნასი&quot; და დაარსებულია ძველ ბერძენთა მიერ. ტურიზმი აქ მაღალ დონეზე დგას. ქალაქი პოპულარობით სარგებლობს ტურისტებს შორის, როგორც ევროპიდან (გერმანია, დიდი ბრიტანეთი) ასევე რუსეთიდან და აზიის მრავალი ქვეყნიდან.</p>\r\n\r\n<p>ღირსშესანიშნაობები</p>\r\n\r\n<p>საუკეთესო ოქროს პლაჟებისა და თანამედროვე ტურისტული ინფრასტრუქტურის გარდა ქალაქი სტუმრებს იზიდავს კიდევ მისი ლამაზი სანაპიროებით, ბოდრუმის ისტორიული ციხესიმაგრით, მის ქვემოთ არსებული წყალქვეშა არქეოლოგიის მუეუმით, მსოფლოში პირველი მავზოლეუმის ნანგრევებით, რომელიც ძველი სამყაროს შვიდ საოცრებათა შორის იყო და მრავალი სხვა ირსშესანიშნაობებით.</p>\r\n\r\n<p>კლიმატი</p>\r\n\r\n<p>აქ ცხელი ხმელთაშუა კლიმატია - ზამთარში შედარებით თბილი და ტენიანი ჰავაა, ტემპერატურა - დაახლოებით 15 &deg;C, ზაფხულში კი საშუალო ტემპერატურაა 34 &deg;C, უმეტესად მშრალი.</p>\r\n\r\n<p>ისტორია</p>\r\n\r\n<p>ჰელიკარნასი დორიელ ბერძენთა მიერ დაარსდა და წარმოადგენდა დორიული ჰექსაპოლისის ნაწილს. იქ აღმოჩენილ მონეტებზე ასახული გორგონა მედუზას თავი, სიბრძნის ქალღმერთი ათინა და პოსეიდონი მიუთითებს, რომ აქაური დორიელები წარმოშობით ტროზედან და არგოსიდან იყვნენ. 1402 წელს წმ. იოანეს ორდენის ჯვაროსანმა რაინდებმა მავზოლეუმის ნანგრევებზე ოტომან სულთან მეჰმედ პირველის ნებართვით ააშენეს წმ. პეტრეს ციხესიმაგრე , რომელიც ჯვაროსნული ეპოქის ერთ-ერთი კარგად შემონახული ძეგლია მცირეაზიაში. მისი სახელწოდებიდან - &quot;პეტრონიუმი&quot; წარმოიშვა ქალაქის ახალი სახელი - ბოდრუმი.</p>\r\n\r\n<p>ადგილობრივი ტრანსპორტი</p>\r\n\r\n<p>ქალაქიდან უახლოესი აეროპორტი არის 36 კილომეტრში. ქალაქის ცენტრში არის ავტოსადგური, რომელიც აკავშირებს თურქეთის სხვადასხვა რეგიონს. პორტში კი ფუნქციონირებს ბორანი, ახლომდებარე თურქულ-ბერძნული ქალაქების მიმართულებით.</p>\r\n</body>\r\n</html>\r\n'),
(103, 11, 4, 'მოზგაურობა კახეთში', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>საგზური აქტიურია ორი თვის განმავლობაში. ყველას ვურჩევთ ბოდრუმის ნახვას, იგი არის საპორტო ქალაქი განთავსებული ბოდრუმის ნახევარკუნძულზე. ქალაქი მდებარეობ ეგეოსის ზღვის სანაპიროზე თურქეთის სამხრეთ დასავლთით, მუღლას პროვინციაში. მისი თავდაპირველი სახელწოდებაა &quot;ჰელიკარნასი&quot; და დაარსებულია ძველ ბერძენთა მიერ. ტურიზმი აქ მაღალ დონეზე დგას. ქალაქი პოპულარობით სარგებლობს ტურისტებს შორის, როგორც ევროპიდან (გერმანია, დიდი ბრიტანეთი) ასევე რუსეთიდან და აზიის მრავალი ქვეყნიდან.</p>\r\n\r\n<p>ღირსშესანიშნაობები</p>\r\n\r\n<p>საუკეთესო ოქროს პლაჟებისა და თანამედროვე ტურისტული ინფრასტრუქტურის გარდა ქალაქი სტუმრებს იზიდავს კიდევ მისი ლამაზი სანაპიროებით, ბოდრუმის ისტორიული ციხესიმაგრით, მის ქვემოთ არსებული წყალქვეშა არქეოლოგიის მუეუმით, მსოფლოში პირველი მავზოლეუმის ნანგრევებით, რომელიც ძველი სამყაროს შვიდ საოცრებათა შორის იყო და მრავალი სხვა ირსშესანიშნაობებით.</p>\r\n\r\n<p>კლიმატი</p>\r\n\r\n<p>აქ ცხელი ხმელთაშუა კლიმატია - ზამთარში შედარებით თბილი და ტენიანი ჰავაა, ტემპერატურა - დაახლოებით 15 &deg;C, ზაფხულში კი საშუალო ტემპერატურაა 34 &deg;C, უმეტესად მშრალი.</p>\r\n\r\n<p>ისტორია</p>\r\n\r\n<p>ჰელიკარნასი დორიელ ბერძენთა მიერ დაარსდა და წარმოადგენდა დორიული ჰექსაპოლისის ნაწილს. იქ აღმოჩენილ მონეტებზე ასახული გორგონა მედუზას თავი, სიბრძნის ქალღმერთი ათინა და პოსეიდონი მიუთითებს, რომ აქაური დორიელები წარმოშობით ტროზედან და არგოსიდან იყვნენ. 1402 წელს წმ. იოანეს ორდენის ჯვაროსანმა რაინდებმა მავზოლეუმის ნანგრევებზე ოტომან სულთან მეჰმედ პირველის ნებართვით ააშენეს წმ. პეტრეს ციხესიმაგრე , რომელიც ჯვაროსნული ეპოქის ერთ-ერთი კარგად შემონახული ძეგლია მცირეაზიაში. მისი სახელწოდებიდან - &quot;პეტრონიუმი&quot; წარმოიშვა ქალაქის ახალი სახელი - ბოდრუმი.</p>\r\n\r\n<p>ადგილობრივი ტრანსპორტი</p>\r\n\r\n<p>ქალაქიდან უახლოესი აეროპორტი არის 36 კილომეტრში. ქალაქის ცენტრში არის ავტოსადგური, რომელიც აკავშირებს თურქეთის სხვადასხვა რეგიონს. პორტში კი ფუნქციონირებს ბორანი, ახლომდებარე თურქულ-ბერძნული ქალაქების მიმართულებით.</p>\r\n</body>\r\n</html>\r\n'),
(123, 8, 4, 'Milling and the first bread!', '', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n'),
(124, 13, 1, 'ისრაელი', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n'),
(125, 13, 2, '23r23r23r', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n'),
(126, 13, 3, '23r23r32r', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n'),
(127, 13, 4, '23r23r23r32', '', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n'),
(159, 9, 4, 'titld de', '', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n'),
(167, 10, 4, 'Egvipte tour', '', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n'),
(172, 14, 1, 'GRUND SUNLIFE', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>Deniz ve havuz manzaralı odalarda tatil keyfini m&uuml;kemmel bir konfor ile s&uuml;receksiniz. Hotelimizde her biri 25 m2 b&uuml;y&uuml;kl&uuml;ğe sahip standard odalar bulunmaktadir.</p>\r\n\r\n<p>Grand Sunlife Hotel&rsquo;de konuklar denizin, g&uuml;neşin, keyfin, lezzetin, eğlencenin ve tatilin doyasıya tadina varmakta; Akdeniz&rsquo;in eşsiz sularinin yanısıra, y&uuml;zme havuzu, &ccedil;ocuk havuzu ve kaydıraklı havuzdan da diledikleri gibi yararlanabilmektedirler ve a&ccedil;ık restauranttan alişveriş merkezine, T&uuml;rk hamamından saunaya, konferans salonundan diskoya(animasyon alanında), masa tenisinden y&uuml;r&uuml;y&uuml;ş parkuruna kadar pek &ccedil;ok alternatifi tek bir tesis i&ccedil;erisinde bulmanın keyfini yaşamaktadırlar.</p>\r\n\r\n<p>Deneyimli ve sertifikalı SPA terapistleri ile T&uuml;rk misafirperverliğini ve T&uuml;rk k&uuml;lt&uuml;r&uuml;ne ait geleneksel motifleri siz değerli ziyaret&ccedil;ilerimize sunmaktadir.</p>\r\n</body>\r\n</html>\r\n'),
(173, 14, 2, 'GRUND SUNLIFE', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>Deniz ve havuz manzaralı odalarda tatil keyfini m&uuml;kemmel bir konfor ile s&uuml;receksiniz. Hotelimizde her biri 25 m2 b&uuml;y&uuml;kl&uuml;ğe sahip standard odalar bulunmaktadir.</p>\r\n\r\n<p>Grand Sunlife Hotel&rsquo;de konuklar denizin, g&uuml;neşin, keyfin, lezzetin, eğlencenin ve tatilin doyasıya tadina varmakta; Akdeniz&rsquo;in eşsiz sularinin yanısıra, y&uuml;zme havuzu, &ccedil;ocuk havuzu ve kaydıraklı havuzdan da diledikleri gibi yararlanabilmektedirler ve a&ccedil;ık restauranttan alişveriş merkezine, T&uuml;rk hamamından saunaya, konferans salonundan diskoya(animasyon alanında), masa tenisinden y&uuml;r&uuml;y&uuml;ş parkuruna kadar pek &ccedil;ok alternatifi tek bir tesis i&ccedil;erisinde bulmanın keyfini yaşamaktadırlar.</p>\r\n\r\n<p>Deneyimli ve sertifikalı SPA terapistleri ile T&uuml;rk misafirperverliğini ve T&uuml;rk k&uuml;lt&uuml;r&uuml;ne ait geleneksel motifleri siz değerli ziyaret&ccedil;ilerimize sunmaktadir.</p>\r\n\r\n<p>4 - 12 yaş arası &ccedil;ocuklar i&ccedil;in Oyun Alanı, Oyun Odası, Mini Disco, Mini Playback, &Ccedil;ocuk Bakıcısı (&Uuml;cretli) mevcuttur.&nbsp;</p>\r\n\r\n<p>Grand Sunlife Hotel has been welcoming Booking.com guests since Apr 7, 2015</p>\r\n\r\n<p>We speak your language!</p>\r\n</body>\r\n</html>\r\n'),
(174, 14, 3, 'GRUND SUNLIFE', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>Deniz ve havuz manzaralı odalarda tatil keyfini m&uuml;kemmel bir konfor ile s&uuml;receksiniz. Hotelimizde her biri 25 m2 b&uuml;y&uuml;kl&uuml;ğe sahip standard odalar bulunmaktadir.</p>\r\n\r\n<p>Grand Sunlife Hotel&rsquo;de konuklar denizin, g&uuml;neşin, keyfin, lezzetin, eğlencenin ve tatilin doyasıya tadina varmakta; Akdeniz&rsquo;in eşsiz sularinin yanısıra, y&uuml;zme havuzu, &ccedil;ocuk havuzu ve kaydıraklı havuzdan da diledikleri gibi yararlanabilmektedirler ve a&ccedil;ık restauranttan alişveriş merkezine, T&uuml;rk hamamından saunaya, konferans salonundan diskoya(animasyon alanında), masa tenisinden y&uuml;r&uuml;y&uuml;ş parkuruna kadar pek &ccedil;ok alternatifi tek bir tesis i&ccedil;erisinde bulmanın keyfini yaşamaktadırlar.</p>\r\n\r\n<p>Deneyimli ve sertifikalı SPA terapistleri ile T&uuml;rk misafirperverliğini ve T&uuml;rk k&uuml;lt&uuml;r&uuml;ne ait geleneksel motifleri siz değerli ziyaret&ccedil;ilerimize sunmaktadir.</p>\r\n\r\n<p>4 - 12 yaş arası &ccedil;ocuklar i&ccedil;in Oyun Alanı, Oyun Odası, Mini Disco, Mini Playback, &Ccedil;ocuk Bakıcısı (&Uuml;cretli) mevcuttur.&nbsp;</p>\r\n\r\n<p>Grand Sunlife Hotel has been welcoming Booking.com guests since Apr 7, 2015</p>\r\n\r\n<p>We speak your language!</p>\r\n</body>\r\n</html>\r\n'),
(175, 14, 4, 'GRUND SUNLIFE', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>Deniz ve havuz manzaralı odalarda tatil keyfini m&uuml;kemmel bir konfor ile s&uuml;receksiniz. Hotelimizde her biri 25 m2 b&uuml;y&uuml;kl&uuml;ğe sahip standard odalar bulunmaktadir.</p>\r\n\r\n<p>Grand Sunlife Hotel&rsquo;de konuklar denizin, g&uuml;neşin, keyfin, lezzetin, eğlencenin ve tatilin doyasıya tadina varmakta; Akdeniz&rsquo;in eşsiz sularinin yanısıra, y&uuml;zme havuzu, &ccedil;ocuk havuzu ve kaydıraklı havuzdan da diledikleri gibi yararlanabilmektedirler ve a&ccedil;ık restauranttan alişveriş merkezine, T&uuml;rk hamamından saunaya, konferans salonundan diskoya(animasyon alanında), masa tenisinden y&uuml;r&uuml;y&uuml;ş parkuruna kadar pek &ccedil;ok alternatifi tek bir tesis i&ccedil;erisinde bulmanın keyfini yaşamaktadırlar.</p>\r\n\r\n<p>Deneyimli ve sertifikalı SPA terapistleri ile T&uuml;rk misafirperverliğini ve T&uuml;rk k&uuml;lt&uuml;r&uuml;ne ait geleneksel motifleri siz değerli ziyaret&ccedil;ilerimize sunmaktadir.</p>\r\n\r\n<p>4 - 12 yaş arası &ccedil;ocuklar i&ccedil;in Oyun Alanı, Oyun Odası, Mini Disco, Mini Playback, &Ccedil;ocuk Bakıcısı (&Uuml;cretli) mevcuttur.&nbsp;</p>\r\n\r\n<p>Grand Sunlife Hotel has been welcoming Booking.com guests since Apr 7, 2015</p>\r\n\r\n<p>We speak your language!</p>\r\n</body>\r\n</html>\r\n'),
(212, 15, 1, 'სამდღიანი ტური საქართველოში', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>დევისის თასის ევრო-აფრიკული ზონის მეორე ჯგუფის დასკვნითი მესამე დღის პირველი შეხვედრა გაიმართა, სადაც ერთმანეთს საქართველოსა და ლიტვის პირველი ნომრები დაუპირისპირდნენ.</p>\r\n\r\n<p>შეხვედრა სრულად ქართველი ჩოგბურთელის უპირატესობით წარიმართა. ნიკოლოზ ბასილაშვილმა ლაურინას გრიგელისს პირველ სეტში მარტივად 6:0 სძლია. მეორე სეტში საქართველოს პირველმა ჩოგანმა მხოლოდ ერთი გეიმი დათმო და 6:1 იმარჯვა.</p>\r\n</body>\r\n</html>\r\n', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<h3>his Okurcalar resort is on the beach, 0.7 mi (1.2 km) from Alara Bazaar, and within 9 mi (15 km) of Alara Han Castle and Sealanya. Konakli Mosque and Konakli Clock Tower Square are also within 12 mi (20 km).</h3>\r\n\r\n<p>Location</p>\r\n\r\n<h3>Resort Features</h3>\r\n\r\n<p>At Justiniano Club Park Conti &ndash; All Inclusive, spend the day on the private sand beach where you can enjoy parasailing. Then return for a meal at one of the resort&#39;s 7 restaurants.</p>\r\n\r\n<h3>Room Amenities</h3>\r\n\r\n<p>All 659 rooms offer furnished balconies, minibars, and LED TVs with satellite channels. Free bottled water, hair dryers, and safes are among the other amenities that guests will find.</p>\r\n</body>\r\n</html>\r\n'),
(213, 15, 2, 'JUSTINIANO CLUB PARK', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<h3>his Okurcalar resort is on the beach, 0.7 mi (1.2 km) from Alara Bazaar, and within 9 mi (15 km) of Alara Han Castle and Sealanya. Konakli Mosque and Konakli Clock Tower Square are also within 12 mi (20 km).</h3>\r\n\r\n<p>Location</p>\r\n\r\n<h3>Resort Features</h3>\r\n\r\n<p>At Justiniano Club Park Conti &ndash; All Inclusive, spend the day on the private sand beach where you can enjoy parasailing. Then return for a meal at one of the resort&#39;s 7 restaurants.</p>\r\n\r\n<h3>Room Amenities</h3>\r\n\r\n<p>All 659 rooms offer furnished balconies, minibars, and LED TVs with satellite channels. Free bottled water, hair dryers, and safes are among the other amenities that guests will find.</p>\r\n</body>\r\n</html>\r\n'),
(214, 15, 3, 'JUSTINIANO CLUB PARK', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<h3>his Okurcalar resort is on the beach, 0.7 mi (1.2 km) from Alara Bazaar, and within 9 mi (15 km) of Alara Han Castle and Sealanya. Konakli Mosque and Konakli Clock Tower Square are also within 12 mi (20 km).</h3>\r\n\r\n<p>Location</p>\r\n\r\n<h3>Resort Features</h3>\r\n\r\n<p>At Justiniano Club Park Conti &ndash; All Inclusive, spend the day on the private sand beach where you can enjoy parasailing. Then return for a meal at one of the resort&#39;s 7 restaurants.</p>\r\n\r\n<h3>Room Amenities</h3>\r\n\r\n<p>All 659 rooms offer furnished balconies, minibars, and LED TVs with satellite channels. Free bottled water, hair dryers, and safes are among the other amenities that guests will find.</p>\r\n</body>\r\n</html>\r\n'),
(215, 15, 4, 'JUSTINIANO CLUB PARK', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n'),
(216, 16, 1, 'დასახელება', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>wefwefwefewf</p>\r\n</body>\r\n</html>\r\n', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>wefwefwaefwe</p>\r\n</body>\r\n</html>\r\n'),
(217, 16, 2, 'ewfwefwef', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body>\r\n<p>ergreg</p>\r\n</body>\r\n</html>\r\n', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n'),
(218, 16, 3, '<html><head>	<title></title></head><body></body></html>', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n'),
(219, 16, 4, '<html><head>	<title></title></head><body></body></html>', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n'),
(252, 17, 1, 'ერთ კვირიანი ტური საქართველოში', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n'),
(253, 17, 2, 'ერთ კვირიანი ტური საქართველოში', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n'),
(254, 17, 3, 'ერთ კვირიანი ტური საქართველოში', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n'),
(255, 17, 4, 'ერთ კვირიანი ტური საქართველოში', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n'),
(289, 18, 1, 'საზამთრი ტური დუშეთში', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n'),
(290, 18, 2, 'საზამთრი ტური დუშეთში', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n'),
(291, 18, 3, 'საზამთრი ტური დუშეთში', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n', '<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body></body>\r\n</html>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `tour_notes`
--

CREATE TABLE `tour_notes` (
  `note_id` int(11) NOT NULL,
  `status_id` smallint(6) NOT NULL DEFAULT '0' COMMENT '0 - Inactive, 1 - Active, 2 - Deleted',
  `sort_order` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tour_notes`
--

INSERT INTO `tour_notes` (`note_id`, `status_id`, `sort_order`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 7),
(4, 2, 4),
(5, 2, 5),
(6, 2, 6),
(7, 1, 3),
(8, 1, 8);

-- --------------------------------------------------------

--
-- Table structure for table `tour_note_descr`
--

CREATE TABLE `tour_note_descr` (
  `id` int(11) NOT NULL,
  `note_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tour_note_descr`
--

INSERT INTO `tour_note_descr` (`id`, `note_id`, `lang_id`, `title`) VALUES
(1, 1, 1, 'ფასები მოცემულია ერთ ზრდასრულ ადამიანზე ორადგილიან ნომერში'),
(2, 1, 2, 'ფასები მოცემულია ერთ ზრდასრულ ადამიანზე ორადგილიან ნომერში'),
(3, 1, 3, 'ფასები მოცემულია ერთ ზრდასრულ ადამიანზე ორადგილიან ნომერში'),
(4, 1, 4, 'ფასები მოცემულია ერთ ზრდასრულ ადამიანზე ორადგილიან ნომერში'),
(5, 2, 1, 'ანგარიშსწორება წარმოებს ეროვნულ ვალუტაში'),
(6, 2, 2, 'ანგარიშსწორება წარმოებს ეროვნულ ვალუტაში'),
(7, 2, 3, 'ანგარიშსწორება წარმოებს ეროვნულ ვალუტაში'),
(8, 2, 4, 'ანგარიშსწორება წარმოებს ეროვნულ ვალუტაში'),
(9, 3, 1, 'ფასები განახლებადია ფრენის, სასტუმროსა და ა.შ. ფასთა ცვალებადობის გამო'),
(10, 3, 2, 'ფასები განახლებადია ფრენის, სასტუმროსა და ა.შ. ფასთა ცვალებადობის გამო'),
(11, 3, 3, 'ფასები განახლებადია ფრენის, სასტუმროსა და ა.შ. ფასთა ცვალებადობის გამო'),
(12, 3, 4, 'ფასები განახლებადია ფრენის, სასტუმროსა და ა.შ. ფასთა ცვალებადობის გამო'),
(53, 4, 1, 'test'),
(54, 4, 2, 'test'),
(55, 4, 3, 'test'),
(56, 4, 4, 'test'),
(57, 5, 1, 'test'),
(58, 5, 2, 'test'),
(59, 5, 3, 'test'),
(60, 5, 4, 'test'),
(61, 6, 1, 'test'),
(62, 6, 2, 'test'),
(63, 6, 3, 'test'),
(64, 6, 4, 'test'),
(65, 7, 1, 'ewfwefwfregergreg'),
(66, 7, 2, 'wefwefwe'),
(67, 7, 3, 'wefwefewf'),
(68, 7, 4, 'wefwef'),
(77, 8, 1, 'trhrthrthrth'),
(78, 8, 2, 'fhdfghdrth'),
(79, 8, 3, 'rthrthwrthrth'),
(80, 8, 4, 'rthrthtrth');

-- --------------------------------------------------------

--
-- Table structure for table `tour_services`
--

CREATE TABLE `tour_services` (
  `service_id` int(11) NOT NULL,
  `status_id` smallint(6) NOT NULL DEFAULT '0' COMMENT '0 - Inactive, 1 - Active, 2 - Deleted',
  `icon` varchar(100) NOT NULL,
  `sort_order` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tour_services`
--

INSERT INTO `tour_services` (`service_id`, `status_id`, `icon`, `sort_order`) VALUES
(1, 1, 'ic-servplainpass', 1),
(2, 1, 'ic-servhotelpass', 2),
(3, 1, 'ic-servrestaurantpass', 3),
(4, 1, 'ic-servinsurancepass', 4),
(5, 1, 'ic-servtransportpass', 5),
(6, 1, 'ic-servluggagepass', 7),
(7, 1, 'ic-servsightseeingpass', 6),
(8, 1, 'ic-servguidpass', 8),
(9, 1, 'ic-servinterpreterpass', 9),
(10, 1, 'ic-servdriverpass', 10),
(11, 1, 'ic-suitcase', 11);

-- --------------------------------------------------------

--
-- Table structure for table `tour_service_descr`
--

CREATE TABLE `tour_service_descr` (
  `id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tour_service_descr`
--

INSERT INTO `tour_service_descr` (`id`, `service_id`, `lang_id`, `title`) VALUES
(1, 1, 1, 'ორმხრივი ავიაბილეთი'),
(2, 2, 1, 'სასტუმრო'),
(3, 3, 1, 'კვება'),
(4, 4, 1, 'სამოგზაურო დაზღვევა'),
(5, 5, 1, 'ტრანსფერი'),
(6, 6, 1, 'ზელბარგი (8კგ. ზრდასრულზე)'),
(7, 7, 1, 'ღირსშესანიშნაობები'),
(8, 8, 1, 'გიდი'),
(9, 9, 1, 'თარჯიმანი'),
(10, 10, 1, 'მძღოლი'),
(11, 11, 1, 'ძიძა'),
(12, 1, 2, 'ორმხრივი ავიაბილეთი'),
(13, 2, 2, 'სასტუმრო'),
(14, 3, 2, 'კვება'),
(15, 4, 2, 'სამოგზაურო დაზღვევა'),
(16, 5, 2, 'ტრანსფერი'),
(17, 6, 2, 'ზელბარგი (8კგ. ზრდასრულზე)'),
(18, 7, 2, 'ღირსშესანიშნაობები'),
(19, 8, 2, 'გიდი'),
(20, 9, 2, 'თარჯიმანი'),
(21, 10, 2, 'მძღოლი'),
(22, 11, 2, 'ძიძა'),
(23, 1, 3, 'ორმხრივი ავიაბილეთი'),
(24, 2, 3, 'სასტუმრო'),
(25, 3, 3, 'კვება'),
(26, 4, 3, 'სამოგზაურო დაზღვევა'),
(27, 5, 3, 'ტრანსფერი'),
(28, 6, 3, 'ზელბარგი (8კგ. ზრდასრულზე)'),
(29, 7, 3, 'ღირსშესანიშნაობები'),
(30, 8, 3, 'გიდი'),
(31, 9, 3, 'თარჯიმანი'),
(32, 10, 3, 'მძღოლი'),
(33, 11, 3, 'ძიძა'),
(34, 1, 4, 'ორმხრივი ავიაბილეთი'),
(35, 2, 4, 'სასტუმრო'),
(36, 3, 4, 'კვება'),
(37, 4, 4, 'სამოგზაურო დაზღვევა'),
(38, 5, 4, 'ტრანსფერი'),
(39, 6, 4, 'ზელბარგი (8კგ. ზრდასრულზე)'),
(40, 7, 4, 'ღირსშესანიშნაობები'),
(41, 8, 4, 'გიდი'),
(42, 9, 4, 'თარჯიმანი'),
(43, 10, 4, 'მძღოლი'),
(44, 11, 4, 'ძიძა');

-- --------------------------------------------------------

--
-- Table structure for table `tour_types`
--

CREATE TABLE `tour_types` (
  `type_id` int(11) NOT NULL,
  `status_id` smallint(6) NOT NULL DEFAULT '0' COMMENT '0 - Inactive, 1 - Active, 2 - Deleted',
  `sort_order` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tour_types`
--

INSERT INTO `tour_types` (`type_id`, `status_id`, `sort_order`) VALUES
(1, 1, 2),
(2, 1, 3),
(3, 1, 4),
(4, 1, 5),
(5, 1, 7),
(6, 2, 6),
(7, 1, 1),
(8, 2, 8),
(9, 2, 9),
(10, 2, 10),
(11, 2, 11),
(12, 2, 12);

-- --------------------------------------------------------

--
-- Table structure for table `tour_type_descr`
--

CREATE TABLE `tour_type_descr` (
  `id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tour_type_descr`
--

INSERT INTO `tour_type_descr` (`id`, `type_id`, `lang_id`, `title`) VALUES
(1, 1, 1, 'საზღვაო ტურები'),
(2, 2, 1, 'სასტუმრო'),
(3, 3, 1, 'კრუიზი'),
(4, 4, 1, 'მთის კურორტი'),
(5, 5, 1, 'სხვა'),
(12, 1, 2, 'საზღვაო ტურები'),
(13, 2, 2, 'სასტუმრო'),
(14, 3, 2, 'კრუიზი'),
(15, 4, 2, 'მთის კურორტი'),
(16, 5, 2, 'სხვა'),
(23, 1, 3, 'საზღვაო ტურები'),
(24, 2, 3, 'სასტუმრო'),
(25, 3, 3, 'კრუიზი'),
(26, 4, 3, 'მთის კურორტი'),
(27, 5, 3, 'სხვა'),
(34, 1, 4, 'საზღვაო ტური'),
(35, 2, 4, 'სასტუმრო'),
(36, 3, 4, 'კრუიზი'),
(37, 4, 4, 'მთის კურორტი'),
(38, 5, 4, 'სხვა'),
(47, 6, 1, 'test ka'),
(48, 6, 2, 'test en'),
(49, 6, 3, 'test ru'),
(50, 6, 4, 'test de'),
(71, 7, 1, 'საზამთრო ტურები'),
(72, 7, 2, 'საზამთრო ტურები'),
(73, 7, 3, 'საზამთრო ტურები'),
(74, 7, 4, 'ალი 4'),
(75, 8, 1, 'regerg'),
(76, 8, 2, 'regergre'),
(77, 8, 3, 'ergergreg'),
(78, 8, 4, 'ergerg'),
(79, 9, 1, 'regerg'),
(80, 9, 2, 'regergre'),
(81, 9, 3, 'ergergreg'),
(82, 9, 4, 'ergerg'),
(83, 10, 1, 'fwefewfwe fwe f'),
(84, 10, 2, 'wefwefewf'),
(85, 10, 3, 'wefwefewf'),
(86, 10, 4, 'wefwef'),
(91, 11, 1, 'test 1'),
(92, 11, 2, 'test 1'),
(93, 11, 3, 'test 1'),
(94, 11, 4, 'test 1'),
(107, 12, 1, 'ეწფწეფაწეფაწეფ'),
(108, 12, 2, 'ასეფაწეფასდფ'),
(109, 12, 3, 'ასდფასდფადფ'),
(110, 12, 4, 'ასდფასდფასდფ');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `status_id` smallint(6) NOT NULL,
  `active_email` tinyint(1) NOT NULL DEFAULT '0',
  `email` varchar(100) NOT NULL,
  `password` varchar(32) NOT NULL,
  `name` varchar(50) NOT NULL,
  `surname` varchar(80) NOT NULL,
  `gender_id` smallint(6) NOT NULL DEFAULT '1' COMMENT '1 - Male, 2 - Female',
  `birth_date` date NOT NULL,
  `country_id` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `status_id`, `active_email`, `email`, `password`, `name`, `surname`, `gender_id`, `birth_date`, `country_id`, `address`, `phone`) VALUES
(1, 0, 0, 'admin@lovetravel.ge', '123456', 'ადმინ', 'ლოვეთრაველ', 1, '1990-01-01', 1, 'თბილისი, ვერა, გოგებაშვილის #20', '+995598123456'),
(14, 0, 1, 'levanpankisi@mail.ru', '7a9e83b458753daa14cafa69f2fa1c14', 'ლევან', 'ბორჩაშვილი', 1, '1970-01-01', 1, 'ჯოყოლო', '598640814');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `team_id` (`about_id`,`lang_id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`blog_id`),
  ADD KEY `status_id` (`status_id`,`cat_id`);

--
-- Indexes for table `blog_cats`
--
ALTER TABLE `blog_cats`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `blog_cat_descr`
--
ALTER TABLE `blog_cat_descr`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_id` (`cat_id`,`lang_id`);

--
-- Indexes for table `blog_descr`
--
ALTER TABLE `blog_descr`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `news_id` (`blog_id`,`lang_id`);

--
-- Indexes for table `cats`
--
ALTER TABLE `cats`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `cat_descr`
--
ALTER TABLE `cat_descr`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `team_id` (`cat_id`,`lang_id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `contact_descr`
--
ALTER TABLE `contact_descr`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `team_id` (`contact_id`,`lang_id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `country_descr`
--
ALTER TABLE `country_descr`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_id` (`country_id`,`lang_id`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`currency_id`);

--
-- Indexes for table `db_error_log`
--
ALTER TABLE `db_error_log`
  ADD PRIMARY KEY (`db_error_log_id`);

--
-- Indexes for table `emails`
--
ALTER TABLE `emails`
  ADD PRIMARY KEY (`mail_id`),
  ADD KEY `from_` (`mail_from`),
  ADD KEY `to_` (`mail_to`),
  ADD KEY `replay_to` (`mail_reply_to`);

--
-- Indexes for table `emails_arc`
--
ALTER TABLE `emails_arc`
  ADD KEY `email_id` (`mail_id`),
  ADD KEY `from_` (`mail_from`),
  ADD KEY `to_` (`mail_to`),
  ADD KEY `replay_to` (`mail_reply_to`);

--
-- Indexes for table `email_hashes`
--
ALTER TABLE `email_hashes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hash` (`hash`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `type_id` (`type_id`,`hash`);

--
-- Indexes for table `favorites`
--
ALTER TABLE `favorites`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`,`tour_id`);

--
-- Indexes for table `langs`
--
ALTER TABLE `langs`
  ADD PRIMARY KEY (`lang_var_id`),
  ADD UNIQUE KEY `lang_var` (`lang_var`),
  ADD KEY `lang_js` (`lang_var_js`);

--
-- Indexes for table `lang_descr`
--
ALTER TABLE `lang_descr`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_id` (`lang_var_id`,`lang_id`);

--
-- Indexes for table `locs`
--
ALTER TABLE `locs`
  ADD PRIMARY KEY (`loc_id`);

--
-- Indexes for table `loc_descr`
--
ALTER TABLE `loc_descr`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `team_id` (`loc_id`,`lang_id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`news_id`);

--
-- Indexes for table `news_descr`
--
ALTER TABLE `news_descr`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `news_id` (`news_id`,`lang_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `service_descr`
--
ALTER TABLE `service_descr`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `news_id` (`service_id`,`lang_id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`slider_id`);

--
-- Indexes for table `slider_descr`
--
ALTER TABLE `slider_descr`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slider_id` (`slider_id`,`lang_id`);

--
-- Indexes for table `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`team_id`);

--
-- Indexes for table `team_descr`
--
ALTER TABLE `team_descr`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `team_id` (`team_id`,`lang_id`);

--
-- Indexes for table `tours`
--
ALTER TABLE `tours`
  ADD PRIMARY KEY (`tour_id`);

--
-- Indexes for table `tour_checked_services`
--
ALTER TABLE `tour_checked_services`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tour_id` (`tour_id`,`service_id`);

--
-- Indexes for table `tour_descr`
--
ALTER TABLE `tour_descr`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `news_id` (`tour_id`,`lang_id`);

--
-- Indexes for table `tour_notes`
--
ALTER TABLE `tour_notes`
  ADD PRIMARY KEY (`note_id`);

--
-- Indexes for table `tour_note_descr`
--
ALTER TABLE `tour_note_descr`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_id` (`note_id`,`lang_id`);

--
-- Indexes for table `tour_services`
--
ALTER TABLE `tour_services`
  ADD PRIMARY KEY (`service_id`),
  ADD UNIQUE KEY `icon` (`icon`);

--
-- Indexes for table `tour_service_descr`
--
ALTER TABLE `tour_service_descr`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `service_id` (`service_id`,`lang_id`);

--
-- Indexes for table `tour_types`
--
ALTER TABLE `tour_types`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `tour_type_descr`
--
ALTER TABLE `tour_type_descr`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_id` (`type_id`,`lang_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `blog_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `blog_cats`
--
ALTER TABLE `blog_cats`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `blog_cat_descr`
--
ALTER TABLE `blog_cat_descr`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `blog_descr`
--
ALTER TABLE `blog_descr`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;
--
-- AUTO_INCREMENT for table `cats`
--
ALTER TABLE `cats`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `cat_descr`
--
ALTER TABLE `cat_descr`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `contact_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contact_descr`
--
ALTER TABLE `contact_descr`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `country_descr`
--
ALTER TABLE `country_descr`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `currency_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `db_error_log`
--
ALTER TABLE `db_error_log`
  MODIFY `db_error_log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;
--
-- AUTO_INCREMENT for table `emails`
--
ALTER TABLE `emails`
  MODIFY `mail_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `email_hashes`
--
ALTER TABLE `email_hashes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `favorites`
--
ALTER TABLE `favorites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT for table `langs`
--
ALTER TABLE `langs`
  MODIFY `lang_var_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=154;
--
-- AUTO_INCREMENT for table `lang_descr`
--
ALTER TABLE `lang_descr`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=635;
--
-- AUTO_INCREMENT for table `locs`
--
ALTER TABLE `locs`
  MODIFY `loc_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `loc_descr`
--
ALTER TABLE `loc_descr`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `news_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `news_descr`
--
ALTER TABLE `news_descr`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `service_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `service_descr`
--
ALTER TABLE `service_descr`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `slider_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `slider_descr`
--
ALTER TABLE `slider_descr`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `team`
--
ALTER TABLE `team`
  MODIFY `team_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `team_descr`
--
ALTER TABLE `team_descr`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;
--
-- AUTO_INCREMENT for table `tours`
--
ALTER TABLE `tours`
  MODIFY `tour_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tour_checked_services`
--
ALTER TABLE `tour_checked_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=503;
--
-- AUTO_INCREMENT for table `tour_descr`
--
ALTER TABLE `tour_descr`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=331;
--
-- AUTO_INCREMENT for table `tour_notes`
--
ALTER TABLE `tour_notes`
  MODIFY `note_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tour_note_descr`
--
ALTER TABLE `tour_note_descr`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;
--
-- AUTO_INCREMENT for table `tour_services`
--
ALTER TABLE `tour_services`
  MODIFY `service_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `tour_service_descr`
--
ALTER TABLE `tour_service_descr`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `tour_types`
--
ALTER TABLE `tour_types`
  MODIFY `type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tour_type_descr`
--
ALTER TABLE `tour_type_descr`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=117;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
