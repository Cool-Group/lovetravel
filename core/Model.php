<?php

class Model 
{
    /**
     * @var DataBase SafeMysql Object
     */
	public $DB;
	public $MC;
	public $Resp;

	function __construct()
	{
		$this->DB 	= new DataBase();
		$this->MC 	= new MCache($this->DB);
		$this->Resp = array('StatusCode' => 0, 'StatusMessage' => Lang::Get('ErrorMessage'));
	}

	public function Response()
	{
		if ($this->Resp['StatusCode'] == 1) {
			$this->Resp['StatusMessage'] = Lang::Get('SuccessMessage');
		}
		return $this->Resp;
	}

    public function GetStaticContent($ContentID = 0)
	{
		return $this->DB->GetOne('SELECT content
			                        FROM contents 
			                       WHERE content_id = ?i', $ContentID);
	}

	public function GetStaticAbout($AboutID = 0)
	{
		return $this->DB->GetRow('SELECT a.has_photo, a.photo_ver, d.h1, d.h2, d.descr
								    FROM about a
							   LEFT JOIN about_descr d ON d.about_id = a.about_id AND d.lang_id = ?i
								   WHERE a.about_id = ?i', Lang::GetLangID(), $AboutID);
	}
	
	public function GetContactInfo() {
		return $this->DB->GetRow('SELECT * 
									FROM contact c 
							  INNER JOIN contact_descr cd ON c.contact_id = cd.contact_id 
							  		 AND cd.lang_id = ?i WHERE c.contact_id = ?i', Lang::GetLangID(), 1);
	}

	public function GetSlider()
    {
        return $this->DB->GetAll('SELECT s.slider_id, s.photo_ver, d.title_1, d.title_2
                                    FROM slider s
                               LEFT JOIN slider_descr d ON d.slider_id = s.slider_id AND d.lang_id = ?i 
                                   WHERE s.status_id = ?i AND s.has_photo = ?i
                                ORDER BY sort_order, slider_id DESC', Lang::GetLangID(), ACTIVE_STATUS_ID, ACTIVE_STATUS_ID);
    }

	public function GetCats($StatusID = ACTIVE_STATUS_ID)
    {
        $Data 			= [];
        $Data['List'] 	= $this->DB->GetInd('cat_id', 'SELECT c.cat_id, c.cat_id AS id, c.status_id, d.title, c.parent_cat_id, c.has_photo, c.photo_ver
                                     		   			 FROM cats c
                               			           INNER JOIN cat_descr d ON d.cat_id = c.cat_id AND d.lang_id = ?i
                                    		            WHERE c.status_id IN (?a)
                                 		  			 ORDER BY c.sort_order', Lang::GetLangID(), $StatusID == ACTIVE_STATUS_ID ? [ACTIVE_STATUS_ID] : [INACTIVE_STATUS_ID, ACTIVE_STATUS_ID]);
        $Data['Tree'] 	= $this->CatsTree($Data['List'], 0);

        return $Data;
    }
    
    private function CatsTree($Cats, $Current, $ParentCatID = 0)
	{
	    $Branch = [];

	    foreach ($Cats as $Cat) {
	        if ($Cat['parent_cat_id'] == $ParentCatID) {
	            $Children = $this->CatsTree($Cats, $Current, $Cat['cat_id']);
	            if ($Children) {
	                $Cat['children'] = $Children;
	            }

	            $Branch[$Cat['cat_id']] = $Cat;
	            
	            unset($Cat);
	        }
	    }
	    return $Branch;
	}

	public function GetTourServices($StatusID = ACTIVE_STATUS_ID)
    {
        return $this->DB->GetInd('service_id', 'SELECT s.service_id, s.service_id AS id, s.status_id, s.icon, d.title, s.icon AS title_1
				                                  FROM tour_services s
				                             LEFT JOIN tour_service_descr d ON d.service_id = s.service_id AND d.lang_id = ?i
				                                 WHERE s.status_id IN (?a)
				                              ORDER BY s.sort_order', Lang::GetLangID(), $StatusID == ACTIVE_STATUS_ID ? [ACTIVE_STATUS_ID] : [INACTIVE_STATUS_ID, ACTIVE_STATUS_ID]);
    }

    public function GetNotes($StatusID = ACTIVE_STATUS_ID)
    {
        return $this->DB->GetInd('note_id', 'SELECT s.note_id, s.note_id AS id, s.status_id, d.title
				                               FROM tour_notes s
				                          LEFT JOIN tour_note_descr d ON d.note_id = s.note_id AND d.lang_id = ?i
				                              WHERE s.status_id IN (?a)
				                           ORDER BY s.sort_order', Lang::GetLangID(), $StatusID == ACTIVE_STATUS_ID ? [ACTIVE_STATUS_ID] : [INACTIVE_STATUS_ID, ACTIVE_STATUS_ID]);
    }

    public function GetServices($Descr = false)
    {
        return $this->DB->GetAll('SELECT s.service_id, d.title ?p
                                    FROM services s
                               LEFT JOIN service_descr d ON d.service_id = s.service_id AND d.lang_id = ?i
                                   WHERE s.status_id = ?i
                                ORDER BY s.insert_date', $Descr ? ', s.icon, s.photos_cnt, s.photo_ver, d.short_descr, d.descr' : '', Lang::GetLangID(), ACTIVE_STATUS_ID);
    }

    public function GetTypes($StatusID = ACTIVE_STATUS_ID)
    {
        return $this->DB->GetInd('type_id', 'SELECT s.type_id, s.type_id AS id, s.status_id, d.title
				                               FROM tour_types s
				                          LEFT JOIN tour_type_descr d ON d.type_id = s.type_id AND d.lang_id = ?i
				                              WHERE s.status_id IN (?a)
				                           ORDER BY s.sort_order', Lang::GetLangID(), $StatusID == ACTIVE_STATUS_ID ? [ACTIVE_STATUS_ID] : [INACTIVE_STATUS_ID, ACTIVE_STATUS_ID]);
    }

    public function GetCountries($StatusID = ACTIVE_STATUS_ID)
    {
        return $this->DB->GetInd('country_id', 'SELECT s.country_id, s.country_id AS id, s.status_id, d.title
				                               	  FROM countries s
				                          	 LEFT JOIN country_descr d ON d.country_id = s.country_id AND d.lang_id = ?i
				                              	 WHERE s.status_id IN (?a)
				                           	  ORDER BY s.sort_order', Lang::GetLangID(), $StatusID == ACTIVE_STATUS_ID ? [ACTIVE_STATUS_ID] : [INACTIVE_STATUS_ID, ACTIVE_STATUS_ID]);
    }

    public function GetCurrencies()
    {
    	return $this->DB->GetInd('currency_id', 'SELECT currency_id, title FROM currencies ORDER BY sort_order');
    }

    public function GetContacts()
    {
        return $this->DB->GetRow('SELECT c.*, d.*
				                    FROM contact c
  							   LEFT JOIN contact_descr d ON d.contact_id = c.contact_id AND d.lang_id = ?i
				                   WHERE c.contact_id = 1', Lang::GetLangID());
    }

    public function GetSubCats($CatID = TOURS_IN_CAT_ID)
    {
    	return explode(',', $this->DB->GetOne('SELECT sub_cats FROM cats WHERE cat_id = ?s', $CatID));
    }

    public function MySqlDate($Date)
	{
		return date('Y-m-d', strtotime($Date));
	}

	public function GetGalleryTours()
	{
		return $this->DB->GetAll('SELECT t.tour_id, t.photo_ver, d.title
								    FROM tours t
							   LEFT JOIN tour_descr d ON d.tour_id = t.tour_id AND d.lang_id = ?i
								   WHERE t.status_id = ?i AND t.photos_cnt > 0
								ORDER BY RAND()
								   LIMIT ?i',
										 Lang::GetLangID(), ACTIVE_STATUS_ID, GALLERY_TOURS_NUM);
	}

	public function GetBlogCats($StatusID = ACTIVE_STATUS_ID, $Cnt = false)
    {
        return $this->DB->GetInd('cat_id', 'SELECT s.cat_id, s.cat_id AS id, s.status_id, d.title ?p
				                              FROM blog_cats s
				                         LEFT JOIN blog_cat_descr d ON d.cat_id = s.cat_id AND d.lang_id = ?i
				                             WHERE s.status_id IN (?a)
				                          ORDER BY s.sort_order',
				                          		   $Cnt ? $this->DB->Parse(', (SELECT COUNT(0) FROM blog b WHERE b.cat_id = s.cat_id AND b.status_id = ?i) AS cnt', ACTIVE_STATUS_ID) : '',
				                          		   Lang::GetLangID(), $StatusID == ACTIVE_STATUS_ID ? [ACTIVE_STATUS_ID] : [INACTIVE_STATUS_ID, ACTIVE_STATUS_ID]);
    }

    public function Email($Data = null)
	{
		$DataSQL = array(
			'mail_from'        	=> null,
			'mail_to'          	=> null,
			'mail_reply_to'    	=> null,
			'mail_subject'     	=> null,
			'mail_message'     	=> null,
			'mail_priority' 	=> 0
		);

		foreach ($DataSQL as $Key => $Val) {
			if (isset($Data[$Key])) {
				$DataSQL[$Key] = $Data[$Key];
			}
		}

		return $this->DB->Query('INSERT INTO emails SET ?u', $DataSQL);
	}

	public function GetFavorites()
    {
    	return Session::Get('UserID') ? $this->DB->GetCol('SELECT tour_id FROM favorites WHERE user_id = ?i', Session::Get('UserID')) : [1, 2];
    }
}

?>