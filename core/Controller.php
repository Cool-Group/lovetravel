<?php

class Controller
{
	public $View;
	public $Model;

	function __construct()
	{
		//
	}
	
	public function SetModel($Name, $ModelPath)
	{	
		$File = $ModelPath . $Name . 'Model.php';
		if (file_exists($File)) {
			include $File;
			$ModelName   = $Name . 'Model';
			$this->Model = new $ModelName();
		}
	}

	public function SetView($Lang, $Controller, $Method)
	{
		$this->View = new View($Lang, $Controller, $Method);
	}

	public function GetURI()
	{
		return URL . (defined('LANGS') ? Lang::GetLang() . '/' : '');
	}

	public function Error()
	{
		header('Location: ' . URL . Lang::GetLang() . '/error/');
		exit;
	}

	public function GetCaptcha()
	{
		$this->View->RenderJSON(1, Captcha::Get());
	}
}