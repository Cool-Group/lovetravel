<?php

class View
{
	public 	$Method;
    /**
     *
     * @var Twig_Environment 
     */
	private $Twig;
	public  $Page;
	public  $Action;
	private $Lang;
	private $AdditionalData = null;
        
	function __construct($Lang, $Page, $Action, $AdditionalData = NULL)
	{
		$this->Lang 			= $Lang;
		$this->Page 			= $Page;
		$this->Action 			= $Action;
		$this->AdditionalData   = $AdditionalData;
	}
	
	public function Render($File, $Replace = array(), $Return = false)
	{
		$this->SetTwigOptions();

		if ($Return == false) {
			echo $this->Twig->render($File, $Replace);
		} else {
			return $this->Twig->render($File, $Replace);
		}
	}

	private function SetTwigOptions()
	{
		$Loader 	= new Twig_Loader_Filesystem(TWIG_TPL);
		$Options 	= array('debug' => BRANCH != 'PRODUCTION', 'auto_reload' => true);
		
		if (TWIG_CACHE) {
			$Options['cache'] = ROOT . TWIG_CACHE_PATH;
		}
		
		$this->Twig = new Twig_Environment($Loader, $Options);
		$this->Twig->addExtension(new Twig_Extension_Debug());
		
		$Serialize = new Twig_SimpleFunction('serialize', 'serialize');
		$this->Twig->addFunction($Serialize);

		$Unserialize = new Twig_SimpleFunction('unserialize', 'unserialize');
		$this->Twig->addFunction($Unserialize);

		$JsonDecode = new Twig_SimpleFunction('json_decode', 'json_decode');
		$this->Twig->addFunction($JsonDecode);

		$YuotubeEmbed = new Twig_SimpleFunction('YuotubeEmbed', array('Functions', 'YuotubeEmbed'));
		$this->Twig->addFunction($YuotubeEmbed);

		$PhoneFormat = new Twig_SimpleFunction('PhoneFormat', array('Functions', 'PhoneFormat'));
		$this->Twig->addFunction($PhoneFormat);

		$FullAddress = new Twig_SimpleFunction('FullAddress', array('Functions', 'FullAddress'));
		$this->Twig->addFunction($FullAddress);

		$Price = new Twig_SimpleFunction('Price', array('Functions', 'Price'));
		$this->Twig->addFunction($Price);

		$Shuffle = new Twig_SimpleFunction('Shuffle', array('Functions', 'ShuffleArray'));
		$this->Twig->addFunction($Shuffle);

		$Period = new Twig_SimpleFunction('Period', array('Functions', 'Period'));
		$this->Twig->addFunction($Period);

		$ShippingDate = new Twig_SimpleFunction('ShippingDate', array('Functions', 'ShippingDate'));
		$this->Twig->addFunction($ShippingDate);

		$ThumbPhoto = new Twig_SimpleFunction('ThumbPhoto', array('Functions', 'ThumbPhoto'));
		$this->Twig->addFunction($ThumbPhoto);

		$PopulateGet = new Twig_SimpleFunction('PopulateGet', array('Functions', 'PopulateGet'));
		$this->Twig->addFunction($PopulateGet);

		$Date = new Twig_SimpleFunction('Date', array('Functions', 'Date'));
		$this->Twig->addFunction($Date);
        
		$FileExists = new Twig_SimpleFunction('file_exists', 'file_exists');
		$this->Twig->addFunction($FileExists);
        
		$ArrayColumn = new Twig_SimpleFunction('array_column', 'array_column');
		$this->Twig->addFunction($ArrayColumn);
        
		$ArraySum = new Twig_SimpleFunction('array_sum', 'array_sum');
		$this->Twig->addFunction($ArraySum);
        
        /*$ToArray = new Twig_SimpleFunction('to_array', function($a){return (array) $a;});
		$this->Twig->addFunction($ToArray);*/

		$Message = new Twig_SimpleFunction('Message', array('Functions', 'Message'));
		$this->Twig->addFunction($Message);
        
		$Url = new Twig_SimpleFunction('Url', array('Functions', 'URL'));
		$this->Twig->addFunction($Url);

		$GetBrowser = new Twig_SimpleFunction('GetBrowser', array('Functions', 'GetBrowser'));
		$this->Twig->addFunction($GetBrowser);

		if ($this->Lang !== false) {
			$Globals = array(
				'Lang' 	=> array(
					'ID' 	=> Lang::GetLangID(),
					'Code' 	=> Lang::GetLang(),
				),
				'URI' => URL . $this->Lang . '/',
			);
		} else {
			$Globals = array(
				'URI' => URL
			);
		}
		$Server = array(
			'URI' 		=> $Globals['URI'],
			'Page' 		=> strtolower($this->Page),
			'Action' 	=> strtolower($this->Action)
		);
		$Constants = get_defined_constants(true);
		
		$this->Twig->addGlobal('constants', $Constants['user']);
		$this->Twig->addGlobal('session', $_SESSION);
		$this->Twig->addGlobal('cookie', $_COOKIE);
		$this->Twig->addGlobal('post', $_POST);
		$this->Twig->addGlobal('get', $_GET);
		$this->Twig->addGlobal('request', $_REQUEST);
		$this->Twig->addGlobal('langs', Lang::Get());
		$this->Twig->addGlobal('globals', $Globals);
		$this->Twig->addGlobal('server', $Server);
		if(!is_null($this->AdditionalData)) {
			$this->Twig->addGlobal('CustomData', $this->AdditionalData);
		}
	}

	public function RenderJSON($StatusCode = 0, $Data = null, $StatusMessage = null)
	{
		$Response = Array('StatusCode' => $StatusCode);

		if (is_array($StatusCode)) {
			if (isset($StatusCode['StatusCode'])) {
				$Response['StatusCode'] = $StatusCode['StatusCode'];
				unset($StatusCode['StatusCode']);
			}

			if (isset($StatusCode['StatusMessage'])) {
				$Response['StatusMessage'] = $StatusCode['StatusMessage'];
				unset($StatusCode['StatusMessage']);
			}

			$Response['Data'] = $StatusCode;
		}

		$Response['StatusCode'] = $Response['StatusCode'] === true ? 1 : $Response['StatusCode'];
		$Response['StatusCode'] = $Response['StatusCode'] === false || ! is_int($Response['StatusCode']) ? 0 : $Response['StatusCode'];

		if (! is_null($StatusMessage)) {
			$Response['StatusMessage'] = $StatusMessage;
		} elseif ($Response['StatusCode'] == 1 && ! isset($Response['StatusMessage'])) {
			$Response['StatusMessage'] = Lang::Get('SuccessMessage');
		}
		
		if (! is_null($Data)) {
			$Response['Data'] = isset($Response['Data']) ? array_merge($Response['Data'], $Data) : $Data;
		}

		echo json_encode($Response);
	}
}