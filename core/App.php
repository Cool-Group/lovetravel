<?php

class App
{
	private $URL 			= null;
	private $Controller 	= null;
	
	private $ControllerPath = 'controllers/';
	private $ModelPath 		= 'models/';
	//private $_ViewPath 	= 'views/';
	
	private $DefaultFile 	= 'Main';
	private $ErrorFile 		= 'Error';

	private $DefaultMethod 	= 'Index';

	private $UrlIndex 		= 0;
	private $Lang 			= false;
	
	public function Init()
	{
		Session::Init();
		
		$this->SetURL();
		$this->LoadController();
		$this->CallControllerMethod();
	}
	
	private function SetURL()
	{
		$this->URLStr	= Request::Get('url');
		$this->URLStr	= rtrim($this->URLStr, '/');
		$this->URLStr  	= filter_var($this->URLStr, FILTER_SANITIZE_URL);
		$this->URL		= explode('/', $this->URLStr);
		if (defined('LANGS')) {
			$this->Langs 	= unserialize(LANGS);
			$this->Lang 	= $this->URL[0];
			if (! in_array($this->Lang, array_keys($this->Langs))) {
				$this->Lang = Cookie::Get('Lang');
				if ($this->Lang == false || ! in_array($this->Lang, array_keys($this->Langs))) {
					$this->Lang = DEFAULT_LANG;
				}
				header('Location: ' . URL . $this->Lang . '/' . $this->URLStr);
				exit;
			}
			Lang::SetLang($this->Lang, $this->Langs[$this->Lang]);
			$this->UrlIndex = 1;
		}
	}

	private function LoadController() {
		if (! isset($this->URL[$this->UrlIndex]) || empty($this->URL[$this->UrlIndex])) {
			$this->URL[$this->UrlIndex] = $this->DefaultFile;
		}
		if (! file_exists($this->ControllerPath . ucfirst($this->URL[$this->UrlIndex]) . 'Controller.php')) {
			$this->URL[$this->UrlIndex] = $this->ErrorFile;
		}
		$this->URL[$this->UrlIndex] = ucfirst(strtolower($this->URL[$this->UrlIndex]));
		include $this->ControllerPath . $this->URL[$this->UrlIndex] . 'Controller.php';
		if (! class_exists($this->URL[$this->UrlIndex])) {
			exit('Controller ' . $this->URL[$this->UrlIndex] . ' Not Found');
		}
		$this->Controller = new $this->URL[$this->UrlIndex];
		$this->Controller->SetModel($this->URL[$this->UrlIndex], $this->ModelPath);
	}

	private function CallControllerMethod()
	{
		if (isset($this->URL[$this->UrlIndex + 1]) && ! is_numeric($this->URL[$this->UrlIndex + 1])) {
			$this->Controller->Method 	= ucfirst($this->URL[$this->UrlIndex + 1]);
			$this->Args 				= array_slice($this->URL, $this->UrlIndex + 2);
		} else {
			$this->Controller->Method 	= $this->DefaultMethod;
			$this->Args 				= array_slice($this->URL, $this->UrlIndex + 1);
		}
		if (! method_exists($this->Controller, $this->Controller->Method)) {
			exit('Method ' . $this->Controller->Method . ' Not Found');
		}
		$this->Controller->SetView($this->Lang, $this->URL[$this->UrlIndex], $this->Controller->Method);
		call_user_func_array(array($this->Controller, $this->Controller->Method), $this->Args);
	}
	
	public function SetControllerPath($Path)
	{
		$this->ControllerPath = trim($Path, '/') . '/';
	}
	
	public function SetModelPath($Path)
	{
		$this->ModelPath = trim($Path, '/') . '/';
	}
	
	public function SetDefaultFile($File)
	{
		$this->DefaultFile = $File;
	}
	
	public function SetErrorFile($File)
	{
		$this->ErrorFile = $File;
	}
}