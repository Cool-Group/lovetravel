<?php

/**
* Blog Model
*/
class BlogModel extends Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function GetBlog($ID = 0)
	{
		if (! empty($ID)) {
			return $this->GetBlogData($ID);
		}

		$Resp  = array();
		$Page  = Functions::GetPage(ADMIN_NEWS_NUM);

		$Resp['Data'] =  $this->DB->GetAll('SELECT b.blog_id, b.photos_cnt, b.photo_ver, b.insert_date, d.title, d.short_descr
										      FROM blog b
									     LEFT JOIN blog_descr d ON d.blog_id = b.blog_id AND d.lang_id = ?i
										     WHERE b.status_id = ?i ?p
										  ORDER BY b.blog_id DESC
										     LIMIT ?i, ?i',
										           Lang::GetLangID(), ACTIVE_STATUS_ID,
										           Request::Get('CatID') ? $this->DB->Parse(' AND b.cat_id = ?i', Request::Get('CatID')) : '', $Page['StartRow'], ADMIN_NEWS_NUM);

		$Resp['Page'] = $Page['Page'];
		$Resp['Cnt']  = $this->DB->GetOne('SELECT COUNT(0)
											 FROM blog b
										LEFT JOIN blog_descr d ON d.blog_id = b.blog_id AND d.lang_id = ?i
											WHERE b.status_id = ?i ?p',
												  Lang::GetLangID(), ACTIVE_STATUS_ID,
												  Request::Get('CatID') ? $this->DB->Parse(' AND b.cat_id = ?i', Request::Get('CatID')) : '');

		return $Resp;
	}

	private function GetBlogData($ID)
	{
		return $this->DB->GetRow('SELECT b.blog_id, b.photos_cnt, b.photo_ver, b.insert_date, d.title, d.short_descr, d.descr
								    FROM blog b
							   LEFT JOIN blog_descr d ON d.blog_id = b.blog_id AND d.lang_id = ?i
								   WHERE b.status_id = ?i AND b.blog_id = ?s',
										 Lang::GetLangID(), ACTIVE_STATUS_ID, $ID);
	}
}