<?php

/**
* User Model
*/
class UserModel extends Model
{
	public $PostTables;

	function __construct()
	{
		parent::__construct();

		$this->PostTables = [
			'news' 			=> ['id' => 'news_id', 'descr_table' => 'news_descr'],
			'tours' 		=> ['id' => 'tour_id', 'descr_table' => 'tour_descr'],
			'slider' 		=> [
				'id' 			=> 'slider_id',
				'descr_table' 	=> 'slider_descr',
				'upl_dir' 		=> UPL_SLIDER,
				'file_type' 	=> IMG_TYPE,
				'fields' 			=> [
					'status_id' 	=> ['name' => 'status_id'],
					'sort_order' 	=> ['name' => 'sort_order'],
					'has_file' 		=> ['name' => 'has_photo'],
					'file_ver' 		=> ['name' => 'photo_ver'],
				],
			],
			'team' 			=> [
				'id' 			=> 'team_id',
				'descr_table' 	=> 'team_descr',
				'upl_dir' 		=> UPL_TEAM,
				'file_type' 	=> IMG_TYPE,
				'fields' 			=> [
					'status_id' 	=> ['name' => 'status_id'],
					'sort_order' 	=> ['name' => 'sort_order'],
					'has_file' 		=> ['name' => 'has_photo'],
					'file_ver' 		=> ['name' => 'photo_ver'],
				],
			],
			'contact' 		=> ['id' => 'contact_id', 'descr_table' => 'contact_descr'],
			'services' 		=> ['id' => 'service_id', 'descr_table' => 'service_descr'],
			'cats' 			=> [
				'id' 				=> 'cat_id',
				'descr_table' 		=> 'cat_descr',
				'sub_items' 		=> 'sub_cats',
				'upl_dir' 			=> UPL_CATS,
				'file_type' 		=> '.png',
				'unable_to_delete'	=> ['ids' => [TOURS_IN_CAT_ID, TOURS_OUT_CAT_ID], 'status_message' => 'მთავარი კატეგორიის წაშლა შეუძლებელია (შეზღუდულია)'],
				'fields' 			=> [
					'status_id' => ['name' => 'status_id'],
					'parent_id' => ['name' => 'parent_cat_id'],
					'sub_items' => ['name' => 'sub_cats'],
					'has_file' 	=> ['name' => 'has_photo'],
					'file_ver' 	=> ['name' => 'photo_ver'],
				],
				'options' 			=> ['status', 'sort', 'edit', 'delete']
			],
			'tour_types' 	=> [
				'id'			=> 'type_id',
				'descr_table' 	=> 'tour_type_descr',
				'fields' 		=> [
					'status_id' 	=> ['name' => 'status_id'],
					'sort_order' 	=> ['name' => 'sort_order']
				],
				'options' 		=> ['status', 'sort', 'edit', 'delete']
			],
			'tour_notes' 	=> [
				'id' 			=> 'note_id',
				'descr_table'	=> 'tour_note_descr',
				'fields' 		=> [
					'status_id' 	=> ['name' => 'status_id'],
					'sort_order' 	=> ['name' => 'sort_order']
				],
				'options' 		=> ['status', 'sort', 'edit', 'delete']
			],
			'tour_services' 	=> [
				'id' 			=> 'service_id',
				'descr_table'	=> 'tour_service_descr',
				'fields' 		=> [
					'status_id' 	=> ['name' => 'status_id'],
					'icon' 			=> ['name' => 'icon', 'input' => 'text', 'label' => 'იკონა', 'required' => true, 'unique' => true, 'status_message' => 'ასეთი იკონა უკვე დამატებულია ბაზაში'],
					'sort_order' 	=> ['name' => 'sort_order']
				],
				'options' 		=> ['status', 'sort', 'edit', 'delete']
			],
			'langs' 		=> [
				'id' 			=> 'lang_var_id',
				'descr_table' 	=> 'lang_descr',
				'fields' => [
					'lang_var' 		=> ['name' => 'lang_var', 'input' => 'text', 'label' => 'ცვლადი', 'required' => true, 'unique' => true, 'status_message' => 'ასეთი ცვლადი უკვე დამატებულია ბაზაში'],
					'lang_var_js' 	=> ['name' => 'lang_var_js', 'input' => 'checkbox', 'label' => 'javascript (js) ცვლადი'],
				],
				'options' 		=> ['edit', 'delete'],
				'callback'		=> 'UpdateLangs',
			],
		];
	}

	public function Authorize()
	{
		$UserData = $this->DB->GetRow('SELECT user_id, active_email, email, password
									     FROM users
								        WHERE email = ?s', Request::Post('Email'));

		if (! empty($UserData)) {
			if ($UserData['password'] != $this->HashPassword(Request::Post('Password'))) {
				return array('StatusCode' => 3, 'StatusMessage' => Lang::Get('IncorrectPassword'));
			}
			if ($UserData['active_email'] != 1) {
				return array('StatusCode' => 4, 'StatusMessage' => str_replace('{EMAIL}', '<b>' . Request::Post('Email') . '</b>', Lang::Get('InactEmailMessage')));
			}
			Session::Set('UserID', $UserData['user_id']);
			Session::Set('Email', $UserData['email']);
			return true;
		}
		return array('StatusCode' => 2, 'StatusMessage' => Lang::Get('UserNotFound'));
	}

	private function HashPassword($Password)
	{
		return md5('1a4y5y9kwl' . $Password . '84i67hoyis');
	}

	public function Add()
	{
		if (! Functions::IsEmail(Request::Post('Email')) || strlen(trim(Request::Post('Password'))) < 6) {
			$this->Response();
		}

		if (! empty($this->CheckEmail(Request::Post('Email')))) {
			return array('StatusCode' => 2, 'StatusMessage' => Lang::Get('UserExists'));
		}

		$InsertData = array(
			'email' 		=> Request::Post('Email'),
			'password' 		=> $this->HashPassword(Request::Post('Password')),
			'name' 			=> Request::Post('Name'),
			'surname' 		=> Request::Post('SurName'),
			'birth_date' 	=> $this->MySqlDate(Request::Post('BirthDate')),
			'gender_id'     => Request::Post('GenderID'),
			'country_id' 	=> Request::Post('CountryID'),
			'address' 		=> Request::Post('Address'),
			'phone' 		=> Request::Post('Phone')
		);

		if ($this->DB->Query('INSERT INTO users SET ?u', $InsertData)) {
			return $this->SendActEmail($this->DB->InsertID(), Request::Post('Email'), Lang::Get('RegisterSuccess') . '<br><br>');
		}

		return false;
	}

	public function Edit()
	{
		if (! $this->CheckCurrentPassword()) {
			return array('StatusCode' => 2, 'StatusMessage' => Lang::Get('IncorrectCurrentPassword'));
		}

		$UpdateData = array(
			'name' 			=> Request::Post('Name'),
			'surname' 		=> Request::Post('SurName'),
			'birth_date' 	=> $this->MySqlDate(Request::Post('BirthDate')),
			'gender_id'     => Request::Post('GenderID'),
			'country_id' 	=> Request::Post('CountryID'),
			'address' 		=> Request::Post('Address'),
			'phone' 		=> Request::Post('Phone')
		);

		if ($this->DB->Query('UPDATE users SET ?u WHERE user_id = ?i', $UpdateData, Session::Get('UserID'))) {
			return true;
		}

		return false;
	}

	private function CheckEmail($Email)
	{
		return $this->DB->GetOne('SELECT user_id FROM users WHERE email = ?s', $Email);
	}

	public function ResendActEmail()
	{
		$UserID = $this->CheckEmail(Request::Post('Email'));

		if (empty($UserID)) {
			return array('StatusCode' => 2, 'StatusMessage' => Lang::Get('UserNotFound'));
		}

		return $this->SendActEmail($UserID, Request::Post('Email'));
	}

	private function SendActEmail($UserID, $Email, $Message = '')
	{
		$this->Email(array(
			'mail_from' 	=> $this->GetNoreplayEmail(),
			'mail_to' 		=> $Email,
			'mail_subject' 	=> Lang::Get('EmailActivation'),
			'mail_message' 	=> str_replace('{LINK}', URL . Lang::GetLang() . '/user/emailactivation/?Hash=' . $this->EmailHash($UserID, 0), Lang::Get('ActEmailMessage'))
		));
		return array('StatusCode' => 1, 'StatusMessage' => $Message . str_replace('{EMAIL}', '<b>' . $Email . '<b>', Lang::Get('ActEmailSent')));
	}

	private function GetNoreplayEmail()
	{
		return $this->DB->GetOne('SELECT noreply_email FROM contact WHERE contact_id = 1');
	}

	public function RecoverPassword()
	{
		$UserID = $this->CheckEmail(Request::Post('Email'));

		if (empty($UserID)) {
			return array('StatusCode' => 2, 'StatusMessage' => Lang::Get('UserNotFound'));
		}

		$this->Email(array(
			'mail_from' 	=> $this->GetNoreplayEmail(),
			'mail_to' 		=> Request::Post('Email'),
			'mail_subject' 	=> Lang::Get('RecoverPassword'),
			'mail_message' 	=> str_replace(
				['{LINK}', '{EMAIL}'],
				[URL . Lang::GetLang() . '/user/newpassword/?Hash=' . $this->EmailHash($UserID, 1), Request::Post('Email')],
				Lang::Get('PasswordRecoverMessage')
			)
		));

		return array('StatusCode' => 1, 'StatusMessage' => str_replace('{EMAIL}', '<b>' . Request::Post('Email') . '<b>', Lang::Get('PasswordRecoverEmailSent')));
	}

	public function ChangePass($NewPassword = false)
	{
		if (strlen(trim(Request::Post('Password'))) < 6) {
			$this->Response();
		}

		if ($NewPassword) {
			$UserID = $this->CheckEmailHash(1);
			if (empty($UserID)) {
				return array('StatusCode' => 0, 'StatusMessage' => Lang::Get('NewPasswordError'));
			}
			if ($this->DB->Query('UPDATE users SET active_email = 1, password = ?s WHERE user_id = ?i', $this->HashPassword(Request::Post('Password')), $UserID)) {
				$this->DeleteEmailHash();
				return true;
			}
			return false;
		} elseif ($this->CheckCurrentPassword()) {
			return $this->DB->Query('UPDATE users SET password = ?s WHERE user_id = ?i', $this->HashPassword(Request::Post('Password')), Session::Get('UserID'));
		}
		return array('StatusCode' => 2, 'StatusMessage' => Lang::Get('IncorrectCurrentPassword'));
	}

	private function CheckCurrentPassword()
	{
		return $this->DB->GetOne('SELECT COUNT(0) FROM users WHERE user_id = ?i AND password = ?s', Session::Get('UserID'), $this->HashPassword(Request::Post('CurrentPassword')));
	}

	public function EmailActivation()
	{
		$UserID = $this->CheckEmailHash(0);

		if (empty($UserID)) {
			return false;
		}

		if ($this->DB->Query('UPDATE users SET active_email = 1 WHERE user_id = ?i', $UserID)) {
			$this->DeleteEmailHash();
			return true;
		}
		
		return false;
	}

	private function EmailHash($UserID, $TypeID)
	{
		$Hash = md5(Functions::GeneratePassword(9, 8) . '-' . $UserID . '-' . $TypeID);

		if ($this->DB->Query('INSERT INTO email_hashes SET user_id = ?i, type_id = ?i, hash = ?s', $UserID, $TypeID, $Hash)) {
			return $Hash;
		} else {
			return false;
		}
	}

	public function CheckEmailHash($TypeID)
	{
		return $this->DB->GetOne('SELECT user_id FROM email_hashes WHERE type_id = ?i AND hash = ?s', $TypeID, Request::Req('Hash'));
	}

	public function DeleteEmailHash()
	{
		return $this->DB->Query('DELETE FROM email_hashes WHERE hash = ?s', Request::Req('Hash'));
	}

	public function AddToFavorites()
	{
		if ($this->DB->GetOne('SELECT COUNT(0) FROM favorites WHERE user_id = ?i AND tour_id = ?i', Session::Get('UserID'), Request::Post('id'))) {
			if ($this->DB->Query('DELETE FROM favorites WHERE user_id = ?i AND tour_id = ?i', Session::Get('UserID'), Request::Post('id'))) {
				return array('StatusCode' => 2);
			}
		} else {
			if ($this->DB->Query('INSERT INTO favorites SET user_id = ?i, tour_id = ?i', Session::Get('UserID'), Request::Post('id'))) {
				return array('StatusCode' => 1);
			}
		}
		return false;
	}

	public function GetFavorites()
	{
		return $this->DB->GetAll('SELECT t.tour_id, t.photos_cnt, t.photo_ver, t.cat_id, t.old_price, t.price, t.currency_id, t.services, t.period_from, t.period_to,
							 		     d.title, d.short_descr
							        FROM tours t
						      INNER JOIN tour_descr d ON d.tour_id = t.tour_id AND d.lang_id = ?i
						      INNER JOIN favorites f ON f.tour_id = t.tour_id AND f.user_id = ?i
							       WHERE t.status_id = ?i AND t.photos_cnt > 0
							    ORDER BY f.id DESC
							       LIMIT ?i',
							             Lang::GetLangID(), Session::Get('UserID'), ACTIVE_STATUS_ID, 50);
	}

	/* News start */

	public function AddNewsPost()
	{
		
		if ($this->DB->Query('INSERT INTO news SET status_id = 0')) {
			$NewsID = $this->DB->InsertID();
			foreach (unserialize(LANGS) as $Lang => $LangID) {
				$this->DB->Query('INSERT INTO news_descr
									   	  SET news_id = ?i, lang_id = ?i, title = ?s, short_descr = ?s, descr = ?s',
									   	      $NewsID, $LangID, Request::Post('Title-' . $Lang), Request::Post('ShortDescr-' . $Lang), Request::Post('Descr-' . $Lang));
			}
			$this->UpdateNewsPhotos($NewsID);
			$this->Resp['StatusCode'] = 1;
		}
		return $this->Response();
	}

	public function EditNewsPost()
	{
		$NewsID = Request::Post('NewsID');
		foreach (unserialize(LANGS) as $Lang => $LangID) {
			$this->DB->Query('INSERT INTO news_descr
								   	  SET news_id = ?i, lang_id = ?i, title = ?s, short_descr = ?s, descr = ?s
								   	   ON DUPLICATE KEY
								   UPDATE title = ?s, short_descr = ?s, descr = ?s',
								   	      $NewsID, $LangID, Request::Post('Title-' . $Lang), Request::Post('ShortDescr-' . $Lang), Request::Post('Descr-' . $Lang),
								   	      Request::Post('Title-' . $Lang), Request::Post('ShortDescr-' . $Lang), Request::Post('Descr-' . $Lang));
		}
		$this->UpdateNewsPhotos($NewsID);
		$this->Resp['StatusCode'] = 1;

		return $this->Response();
	}

	public function GetNewsData($ID)
	{
		$Data = [];

		$Data['News'] 	= $this->DB->GetRow('SELECT * FROM news WHERE news_id = ?i', $ID);
		$Data['Descr'] 	= $this->DB->GetInd('lang_id', 'SELECT * FROM news_descr WHERE news_id = ?i', $ID);

		return $Data;
	}

	public function GetNews()
	{
		$Resp  = array();
		$Where = '';
		$Page  = Functions::GetPage(ADMIN_NEWS_NUM);

		$SearchFields = array(
			'Keyword' => ' AND (d.title LIKE "%?p%" OR d.descr LIKE "%?p%")'
		);

		foreach($SearchFields as $Key => $Val) {
			if(Request::Get($Key)) {
		    	$Where .= $this->DB->Parse($Val, Request::Get($Key), Request::Get($Key));
		    }
		}

		$Resp['Data'] =  $this->DB->GetAll('SELECT n.news_id, n.status_id, n.photos_cnt, n.photo_ver,
												   d.title, d.descr
											  FROM news n
									     LEFT JOIN news_descr d ON d.news_id = n.news_id AND d.lang_id = ?i
										     WHERE n.status_id != ?i ?p
										  ORDER BY n.news_id DESC
										     LIMIT ?i, ?i',
										           Lang::GetLangID(), DELETED_STATUS_ID,
										           $Where, $Page['StartRow'], ADMIN_NEWS_NUM);

		$Resp['Page'] = $Page['Page'];
		$Resp['Cnt']  = $this->DB->GetOne('SELECT COUNT(0)
											 FROM news n
										LEFT JOIN news_descr d ON d.news_id = n.news_id AND d.lang_id = ?i
											WHERE n.status_id != ?i ?p', Lang::GetLangID(), DELETED_STATUS_ID, $Where);

		return $Resp;
	}

	/* News end */

	/* Services start */

	public function AddServicePost()
	{
		
		if ($this->DB->Query('INSERT INTO services SET status_id = 0')) {
			$ID = $this->DB->InsertID();
			foreach (unserialize(LANGS) as $Lang => $LangID) {
				$this->DB->Query('INSERT INTO service_descr
									   	  SET service_id = ?i, lang_id = ?i, title = ?s, descr = ?s',
									   	      $ID, $LangID, Request::Post('Title-' . $Lang), Request::Post('Descr-' . $Lang));
			}
			$this->Resp['StatusCode'] = 1;
		}
		return $this->Response();
	}

	public function EditServicePost()
	{
		$ID = Request::Post('ServiceID');
		foreach (unserialize(LANGS) as $Lang => $LangID) {
			$this->DB->Query('INSERT INTO service_descr
								   	  SET service_id = ?i, lang_id = ?i, title = ?s, descr = ?s
								   	   ON DUPLICATE KEY
								   UPDATE title = ?s, descr = ?s',
								   	      $ID, $LangID, Request::Post('Title-' . $Lang), Request::Post('Descr-' . $Lang),
								   	      Request::Post('Title-' . $Lang), Request::Post('Descr-' . $Lang));
		}
		$this->Resp['StatusCode'] = 1;

		return $this->Response();
	}

	/*public function GetServices()
	{
		$Resp  = array();
		$Where = '';
		$Page  = Functions::GetPage(ADMIN_NEWS_NUM);

		$SearchFields = array(
			'Keyword' => ' AND (d.title LIKE "%?p%" OR d.descr LIKE "%?p%")'
		);

		foreach($SearchFields as $Key => $Val) {
			if(Request::Get($Key)) {
		    	$Where .= $this->DB->Parse($Val, Request::Get($Key), Request::Get($Key));
		    }
		}

		$Resp['Data'] =  $this->DB->GetAll('SELECT n.service_id, n.status_id,
												   d.title, d.descr
											  FROM services n
									     LEFT JOIN service_descr d ON d.service_id = n.service_id AND d.lang_id = ?i
										     WHERE n.status_id != ?i ?p
										  ORDER BY n.service_id DESC
										     LIMIT ?i, ?i',
										           Lang::GetLangID(), DELETED_STATUS_ID,
										           $Where, $Page['StartRow'], ADMIN_NEWS_NUM);

		$Resp['Page'] = $Page['Page'];
		$Resp['Cnt']  = $this->DB->GetOne('SELECT COUNT(0)
											 FROM services n
										LEFT JOIN service_descr d ON d.service_id = n.service_id AND d.lang_id = ?i
											WHERE n.status_id != ?i ?p', Lang::GetLangID(), DELETED_STATUS_ID, $Where);

		return $Resp;
	}*/

	/* Services end */

	/* Slider start */

	public function AddSliderPost()
	{
		if ($this->DB->Query('INSERT INTO slider SET sort_order = ?i', Request::Post('SortOrder'))) {
			$ID = $this->DB->InsertID();
			foreach (unserialize(LANGS) as $Lang => $LangID) {
				$this->DB->Query('INSERT INTO slider_descr
									   	  SET slider_id = ?i, lang_id = ?i, title_1 = ?s, title_2 = ?s',
									   	      $ID, $LangID, Request::Post('Title-1-' . $Lang), Request::Post('Title-2-' . $Lang));
			}
			$this->UpdateFile('slider', $this->DB->InsertID());
			$this->Resp['StatusCode'] = 1;
		}
		return $this->Response();
	}

	public function EditSliderPost()
	{
		$ID = Request::Post('SliderID');
		$this->DB->Query('UPDATE slider SET sort_order = ?i WHERE slider_id = ?i', Request::Post('SortOrder'), $ID);
		foreach (unserialize(LANGS) as $Lang => $LangID) {
			$this->DB->Query('INSERT INTO slider_descr
								   	  SET slider_id = ?i, lang_id = ?i, title_1 = ?s, title_2 = ?s
								   	   ON DUPLICATE KEY
								   UPDATE title_1 = ?s, title_2 = ?s',
								   	      $ID, $LangID, Request::Post('Title-1-' . $Lang), Request::Post('Title-2-' . $Lang),
								   	      Request::Post('Title-1-' . $Lang), Request::Post('Title-2-' . $Lang));
		}
		$this->UpdateFile('slider', $ID);
		$this->Resp['StatusCode'] = 1;

		return $this->Response();
	}

	public function GetSlider()
	{
		$Resp  = array();
		$Where = '';
		$Page  = Functions::GetPage(ADMIN_NEWS_NUM);

		$Resp['Data'] =  $this->DB->GetAll('SELECT s.slider_id, s.status_id, s.sort_order, s.has_photo, s.photo_ver, d.title_1, d.title_2
											  FROM slider s
										 LEFT JOIN slider_descr d ON d.slider_id = s.slider_id AND d.lang_id = ?i
										     WHERE s.status_id != ?i
										  ORDER BY s.sort_order, s.slider_id DESC
										     LIMIT ?i, ?i',
										           Lang::GetLangID(), DELETED_STATUS_ID,
										           $Page['StartRow'], ADMIN_NEWS_NUM);

		$Resp['Page'] = $Page['Page'];
		$Resp['Cnt']  = $this->DB->GetOne('SELECT COUNT(0) FROM slider WHERE status_id != ?i', DELETED_STATUS_ID);

		return $Resp;
	}

	/* Slider end */

	/* Tours start */

	public function AddTourPost()
	{
		$this->SetTourServices();

		$Insert = $this->DB->Query('INSERT INTO tours
										    SET cat_id = ?i, type_id = ?i, price_from = ?s, price_to = ?s, currency_id = ?i,
										    	period_from = ?s, period_to = ?s, services = ?s, notes = ?s, hotels = ?s, map_coords = ?s',
										        Request::Post('CatID'), Request::Post('TypeID'), Request::Post('PriceFrom'), Request::Post('PriceTo'),
										        Request::Post('CurrencyID'), $this->MySqlDate(Request::Post('PeriodFrom')), $this->MySqlDate(Request::Post('PeriodTo')),
										        json_encode(Request::Post('Services')), json_encode(Request::Post('Notes')), Request::Post('Hotels'), Request::Post('MapCoords'));
		if ($Insert) {
			$ID = $this->DB->InsertID();
			foreach (unserialize(LANGS) as $Lang => $LangID) {
				$this->DB->Query('INSERT INTO tour_descr
									   	  SET tour_id = ?i, lang_id = ?i, title = ?s, short_descr = ?s, descr = ?s',
									   	      $ID, $LangID, Request::Post('Title-' . $Lang), Request::Post('ShortDescr-' . $Lang), Request::Post('Descr-' . $Lang));
			}

			$this->EditTourServices($ID, false);
			$this->UpdateTourPhotos($ID);
			$this->Resp['StatusCode'] = 1;
		}
		return $this->Response();
	}

	public function EditTourPost()
	{
		$this->SetTourServices();

		$ID = Request::Post('TourID');

		$Update = $this->DB->Query('UPDATE tours
									   SET cat_id = ?i, type_id = ?i, price_from = ?s, price_to = ?s, currency_id = ?i,
									   	   period_from = ?s, period_to = ?s, services = ?s, notes = ?s, hotels = ?s, map_coords = ?s
									 WHERE tour_id = ?i',
										   Request::Post('CatID'), Request::Post('TypeID'), Request::Post('PriceFrom'), Request::Post('PriceTo'),
										   Request::Post('CurrencyID'), $this->MySqlDate(Request::Post('PeriodFrom')), $this->MySqlDate(Request::Post('PeriodTo')),
										   json_encode(Request::Post('Services')), json_encode(Request::Post('Notes')), Request::Post('Hotels'), Request::Post('MapCoords'), $ID);

		foreach (unserialize(LANGS) as $Lang => $LangID) {
			$this->DB->Query('INSERT INTO tour_descr
								   	  SET tour_id = ?i, lang_id = ?i, title = ?s, short_descr = ?s, descr = ?s
								   	   ON DUPLICATE KEY
								   UPDATE title = ?s, short_descr = ?s, descr = ?s',
								   	      $ID, $LangID, Request::Post('Title-' . $Lang), Request::Post('ShortDescr-' . $Lang), Request::Post('Descr-' . $Lang),
								   	      Request::Post('Title-' . $Lang), Request::Post('ShortDescr-' . $Lang), Request::Post('Descr-' . $Lang));
		}

		$this->EditTourServices($ID);
		$this->UpdateTourPhotos($ID);
		$this->Resp['StatusCode'] = 1;

		return $this->Response();
	}

	private function SetTourServices()
	{
		$Values = [];

		foreach (Request::Post('Services') as $Key => $Val) {
			if ($Val != 0) {
				$Values[$Key] = $Val;
			}
		}

		Request::SetPost('Services', $Values);
	}

	private function EditTourServices($ID, $Edit = true)
	{		
		if ($Edit) {
			$this->DB->Query('DELETE FROM tour_checked_services WHERE tour_id = ?i', $ID);
		}
		if (is_array(Request::Post('Services'))) {
			$Values = [];
			foreach (Request::Post('Services') as $Key => $Val) {
				$Values[] = $this->DB->Parse('(?i, ?i, ?i)', $ID, $Key, $Val);
			}
			if (count($Values)) {
				$this->DB->Query('INSERT INTO tour_checked_services (tour_id, service_id, type_id) VALUES ?p', implode(',', $Values));
			}
		}
	}

	public function GetTours()
	{
		$Resp  = array();
		$Where = '';
		$Page  = Functions::GetPage(ADMIN_NEWS_NUM);

		$SearchFields = array(
			'Keyword' => ' AND (d.title LIKE "%?p%" OR d.descr LIKE "%?p%")'
		);

		foreach($SearchFields as $Key => $Val) {
			if(Request::Get($Key)) {
		    	$Where .= $this->DB->Parse($Val, Request::Get($Key), Request::Get($Key));
		    }
		}

		$Resp['Data'] =  $this->DB->GetAll('SELECT n.tour_id, n.status_id, n.is_main, n.photos_cnt, n.photo_ver,
												   d.title, d.descr
											  FROM tours n
									     LEFT JOIN tour_descr d ON d.tour_id = n.tour_id AND d.lang_id = ?i
										     WHERE n.status_id != ?i ?p
										  ORDER BY n.is_main DESC, n.tour_id DESC
										     LIMIT ?i, ?i',
										           Lang::GetLangID(), DELETED_STATUS_ID,
										           $Where, $Page['StartRow'], ADMIN_NEWS_NUM);

		$Resp['Page'] = $Page['Page'];
		$Resp['Cnt']  = $this->DB->GetOne('SELECT COUNT(0)
											 FROM tours n
										LEFT JOIN tour_descr d ON d.tour_id = n.tour_id AND d.lang_id = ?i
											WHERE n.status_id != ?i ?p', Lang::GetLangID(), DELETED_STATUS_ID, $Where);

		return $Resp;
	}

	/* Tours end */

	/* Team start */

	public function AddTeamPost()
	{
		if ($this->DB->Query('INSERT INTO team SET status_id = 0')) {
			$ID = $this->DB->InsertID();
			foreach (unserialize(LANGS) as $Lang => $LangID) {
				$this->DB->Query('INSERT INTO team_descr
									   	  SET team_id = ?i, lang_id = ?i, fullname = ?s, position = ?s',
									   	      $ID, $LangID, Request::Post('Fullname-' . $Lang), Request::Post('Position-' . $Lang));
			}
			$this->UpdateFile('team', $ID);
			$this->Resp['StatusCode'] = 1;
		}
		return $this->Response();
	}

	public function EditTeamPost()
	{
		$ID = Request::Post('TeamID');
		foreach (unserialize(LANGS) as $Lang => $LangID) {
			$this->DB->Query('INSERT INTO team_descr
								   	  SET team_id = ?i, lang_id = ?i, fullname = ?s, position = ?s
								   	   ON DUPLICATE KEY
								   UPDATE fullname = ?s, position = ?s',
								   	      $ID, $LangID, Request::Post('Fullname-' . $Lang), Request::Post('Position-' . $Lang),
								   	      Request::Post('Fullname-' . $Lang), Request::Post('Position-' . $Lang));
		}
		$this->UpdateFile('team', $ID);
		$this->Resp['StatusCode'] = 1;

		return $this->Response();
	}

	public function GetTeam()
	{
		$Resp  = array();
		$Where = '';
		$Page  = Functions::GetPage(ADMIN_NEWS_NUM);

		$SearchFields = array(
			'Keyword' => ' AND (d.fullname LIKE "%?p%" OR d.position LIKE "%?p%")'
		);

		foreach($SearchFields as $Key => $Val) {
			if(Request::Get($Key)) {
		    	$Where .= $this->DB->Parse($Val, Request::Get($Key), Request::Get($Key));
		    }
		}

		$Resp['Data'] =  $this->DB->GetAll('SELECT t.team_id, t.status_id, t.is_main, t.has_photo, t.photo_ver,
												   d.fullname, d.position
											  FROM team t
									     LEFT JOIN team_descr d ON d.team_id = t.team_id AND d.lang_id = ?i
										     WHERE t.status_id != ?i ?p
										  ORDER BY t.is_main DESC, t.team_id DESC
										     LIMIT ?i, ?i',
										           Lang::GetLangID(), DELETED_STATUS_ID,
										           $Where, $Page['StartRow'], ADMIN_NEWS_NUM);

		$Resp['Page'] = $Page['Page'];
		$Resp['Cnt']  = $this->DB->GetOne('SELECT COUNT(0)
											 FROM team t
										LEFT JOIN team_descr d ON d.team_id = t.team_id AND d.lang_id = ?i
											WHERE status_id != ?i ?p', Lang::GetLangID(), DELETED_STATUS_ID, $Where);

		return $Resp;
	}

	/* Team end */

	/*List start*/

	public function AddListItem()
	{
		$ID = $this->InsertListItem();

		if (is_array($ID)) {
			return $ID;
		}

		if (isset($this->PostTables[Request::Post('PostTable')]['fields']['sub_items'])) {
			$this->DB->Query('UPDATE ?n
								 SET ?n = ?s
							   WHERE ?n = ?i',
							         Request::Post('PostTable'),
							         $this->PostTables[Request::Post('PostTable')]['fields']['sub_items']['name'], $ID,
							         $this->PostTables[Request::Post('PostTable')]['id'], $ID);
		}

		foreach (unserialize(LANGS) as $Key => $Val) {
			$this->DB->Query('INSERT INTO ?n
									  SET ?n = ?i, lang_id = ?i, title = ?s',
									      $this->PostTables[Request::Post('PostTable')]['descr_table'],
									      $this->PostTables[Request::Post('PostTable')]['id'], $ID, $Val, Request::Post('title-' . $Key));
		}
		
		if (isset($this->PostTables[Request::Post('PostTable')]['upl_dir']) && ! empty(Request::Post('File'))) {
			$this->UpdateFile(Request::Post('PostTable'), $ID);
		}

		$this->ListItemCallback();
		
		return array(
			'StatusCode' 	=> 1,
			'parent_id' 	=> isset($this->PostTables[Request::Post('PostTable')]['fields']['parent_id']) ? Request::Post($this->PostTables[Request::Post('PostTable')]['fields']['parent_id']['name']) : 0,
			'ID' 			=> $ID,
			'title' 		=> Request::Post('title-' . Lang::GetLang()));
	}

	private function InsertListItem()
	{
		$Query = [];

		foreach ($this->PostTables[Request::Post('PostTable')]['fields'] as $Key => $Val) {
			if (isset($Val['unique']) && $this->DB->GetOne('SELECT COUNT(0) FROM ?n WHERE ?n = ?s LIMIT 1', Request::Post('PostTable'), $Val['name'], Request::Post($Val['name']))) {
				return array('StatusCode' => 0, 'StatusMessage' => $Val['status_message']);
			}
			$Query[] = $this->DB->Parse('?n = ?s', $Val['name'], $Val['name'] == 'sort_order' ? $this->GetNewListItemSortOrder() : Request::Post($Val['name']));
		}

		$this->DB->Query('INSERT INTO ?n SET ?p', Request::Post('PostTable'), implode(', ', $Query));

		return $this->DB->InsertID();
	}

	private function GetNewListItemSortOrder()
	{
		return (int)$this->DB->GetOne('SELECT MAX(sort_order)
										 FROM ?n ?p',
										      Request::Post('PostTable'),
										      isset($this->PostTables[Request::Post('PostTable')]['fields']['parent_id']) ?
										      $this->DB->Parse('WHERE ?n = ?i', $this->PostTables[Request::Post('PostTable')]['fields']['parent_id']['name'],
										      Request::Post($this->PostTables[Request::Post('PostTable')]['fields']['parent_id']['name'])) : '') + 1;
	}

	public function ChangeListItemSortOrder()
	{
		$ItemData 		= $this->DB->GetRow('SELECT sort_order ?p
											   FROM ?n
											  WHERE ?n = ?i',
											   		isset($this->PostTables[Request::Post('PostTable')]['fields']['parent_id']) ?
											  		$this->DB->Parse(', ?n', $this->PostTables[Request::Post('PostTable')]['fields']['parent_id']['name']) : '',
											  		Request::Post('PostTable'),
											        $this->PostTables[Request::Post('PostTable')]['id'], Request::Post('ID'));
		$SecondItemData = $this->DB->GetRow('SELECT sort_order, ?n
											   FROM ?n
											  WHERE sort_order ?p ?i AND status_id < ?i
											  		?p
										   ORDER BY sort_order ?p
											  LIMIT 1',
											  		$this->PostTables[Request::Post('PostTable')]['id'],
											  		Request::Post('PostTable'),
											        Request::Post('SortID') ? '>' : '<', $ItemData['sort_order'], DELETED_STATUS_ID,
											        isset($this->PostTables[Request::Post('PostTable')]['fields']['parent_id']) ?
											        $this->DB->Parse(' AND ?n = ?i', $this->PostTables[Request::Post('PostTable')]['fields']['parent_id']['name'],
											        								 $ItemData[$this->PostTables[Request::Post('PostTable')]['fields']['parent_id']['name']]) : '',
											        Request::Post('SortID') ? '' : 'DESC');

		$this->DB->Query('UPDATE ?n
							 SET sort_order = ?i
						   WHERE ?n = ?i',
						         Request::Post('PostTable'),
						         $SecondItemData['sort_order'],
						         $this->PostTables[Request::Post('PostTable')]['id'], Request::Post('ID'));
		$this->DB->Query('UPDATE ?n
							 SET sort_order = ?i
						   WHERE ?n = ?i',
						     	 Request::Post('PostTable'),
						     	 $ItemData['sort_order'],
						     	 $this->PostTables[Request::Post('PostTable')]['id'], $SecondItemData[$this->PostTables[Request::Post('PostTable')]['id']]);

		return true;
	}

	public function ChangeListItemStatus()
	{
		$ItemData = $this->DB->GetRow('SELECT status_id ?p
										 FROM ?n
										WHERE ?n = ?i',
											  isset($this->PostTables[Request::Post('PostTable')]['fields']['parent_id']) ?
											  $this->DB->Parse(', ?n', $this->PostTables[Request::Post('PostTable')]['fields']['parent_id']['name']) : '',
										      Request::Post('PostTable'),
										      $this->PostTables[Request::Post('PostTable')]['id'], Request::Post('ID'));
		$ItemData['status_id'] = $ItemData['status_id'] ? INACTIVE_STATUS_ID : ACTIVE_STATUS_ID;
		
		if ($this->DB->Query('UPDATE ?n SET status_id = ?i WHERE ?n = ?i', Request::Post('PostTable'), $ItemData['status_id'], $this->PostTables[Request::Post('PostTable')]['id'], Request::Post('ID'))) {
			if (isset($this->PostTables[Request::Post('PostTable')]['fields']['parent_id'])) {
				$this->UpdateSubListItems($ItemData[$this->PostTables[Request::Post('PostTable')]['fields']['parent_id']['name']]);
			}
			return array('StatusCode' => 1, 'ItemStatusID' => $ItemData['status_id']);
		}

		return false;
	}

	private function UpdateSubListItems($ParentID)
	{
		if ($ParentID) {
			$ItemData = $this->DB->GetRow('SELECT ?n, (SELECT GROUP_CONCAT(?n) FROM ?n WHERE ?n = ?i AND status_id = ?i) AS sub_items
										     FROM ?n
										    WHERE ?n = ?i',
										    	  $this->PostTables[Request::Post('PostTable')]['fields']['parent_id']['name'], $this->PostTables[Request::Post('PostTable')]['fields']['sub_items']['name'],
										    	  Request::Post('PostTable'), $this->PostTables[Request::Post('PostTable')]['fields']['parent_id']['name'], $ParentID, ACTIVE_STATUS_ID,
										    	  Request::Post('PostTable'), $this->PostTables[Request::Post('PostTable')]['id'], $ParentID);
			
			$ItemData['sub_items'] = rtrim($ItemData['sub_items'], ',');

			$this->DB->Query('UPDATE ?n
								 SET ?n = ?s
							   WHERE cat_id = ?i',
							   		 Request::Post('PostTable'),
							   		 $this->PostTables[Request::Post('PostTable')]['fields']['sub_items']['name'], $ParentID . ( ! empty($ItemData['sub_items']) ? ',' . $ItemData['sub_items'] : ''),
							   		 $ParentID);

			$this->UpdateSubListItems($ItemData[$this->PostTables[Request::Post('PostTable')]['fields']['parent_id']['name']]);
		}
	}

	public function EditListItem()
	{
		foreach (unserialize(LANGS) as $Lang => $LangID) {
			$this->DB->Query('INSERT INTO ?n
									  SET ?n = ?i, lang_id = ?i, title = ?s
									   ON DUPLICATE KEY
								   UPDATE title = ?s',
							  			  $this->PostTables[Request::Post('PostTable')]['descr_table'],
							  			  $this->PostTables[Request::Post('PostTable')]['id'], Request::Post('ID'), $LangID, Request::Post('title-' . $Lang),
							  			  Request::Post('title-' . $Lang));
		}

		if (isset($this->PostTables[Request::Post('PostTable')]['upl_dir']) && ! empty(Request::Post('File'))) {
			$this->UpdateFile(Request::Post('PostTable'), Request::Post('ID'));
		}

		$this->ListItemCallback();

		return array('StatusCode' => 1, 'ID' => Request::Post('ID'), 'title' => Request::Post('title-' . Lang::GetLang()));
	}

	public function DeleteListItem()
	{
		if (isset($this->PostTables[Request::Post('PostTable')]['unable_to_delete']) && in_array(Request::Post('ID'), $this->PostTables[Request::Post('PostTable')]['unable_to_delete']['ids'])) {
			return array('StatusCode' => 0, 'StatusMessage' => $this->PostTables[Request::Post('PostTable')]['unable_to_delete']['status_message']);
		}

		if (! isset($this->PostTables[Request::Post('PostTable')]['fields']['status_id'])) {
			$this->DB->Query('DELETE FROM ?n WHERE ?n = ?i', Request::Post('PostTable'), $this->PostTables[Request::Post('PostTable')]['id'], Request::Post('ID'));
			$this->DB->Query('DELETE FROM ?n WHERE ?n = ?i', $this->PostTables[Request::Post('PostTable')]['descr_table'], $this->PostTables[Request::Post('PostTable')]['id'], Request::Post('ID'));

			$this->ListItemCallback();

			return true;
		}

		if ($this->DB->Query('UPDATE ?n SET status_id = ?i WHERE ?n = ?i', Request::Post('PostTable'), DELETED_STATUS_ID, $this->PostTables[Request::Post('PostTable')]['id'], Request::Post('ID'))) {
			if (isset($this->PostTables[Request::Post('PostTable')]['fields']['parent_id'])) {
				$ParentID = $this->DB->GetOne('SELECT ?n
												 FROM ?n
												WHERE ?n = ?i',
												      $this->PostTables[Request::Post('PostTable')]['fields']['parent_id']['name'],
												      Request::Post('PostTable'),
												      $this->PostTables[Request::Post('PostTable')]['id'], Request::Post('ID'));
				$this->UpdateSubListItems($ParentID);
			}

			$this->ListItemCallback();

			return true;
		}

		return false;
	}

	private function ListItemCallback()
	{
		if (isset($this->PostTables[Request::Post('PostTable')]['callback'])) {
			$this->{$this->PostTables[Request::Post('PostTable')]['callback']}();
		}
	}

	/*List end*/

	/*Langs start*/

	public function GetLangs()
    {
        return $this->DB->GetInd('lang_var_id', 'SELECT s.lang_var_id, s.lang_var_id AS id, d.title, s.lang_var AS title_1, IF(s.lang_var_js, "js", "") AS title_2
				                               	   FROM langs s
				                              LEFT JOIN lang_descr d ON d.lang_var_id = s.lang_var_id AND d.lang_id = ?i
				                               ORDER BY s.lang_var_id', Lang::GetLangID());
    }

    public function UpdateLangs()
	{
		new LangsGenerator($this->DB);
	}

	/*Langs end*/

	/*About start*/

	public function EditAbout()
	{
		foreach (unserialize(LANGS) as $Lang => $LangID) {
			$this->DB->Query('INSERT INTO about
								   	  SET about_id = 1, lang_id = ?i, descr = ?s
								   	   ON DUPLICATE KEY
								   UPDATE descr = ?s',
								   	      $LangID, Request::Post('Descr-' . $Lang), Request::Post('Descr-' . $Lang));
		}
		$this->Resp['StatusCode'] = 1;

		return $this->Response();
	}

	public function GetAbout()
	{
		return $this->DB->GetInd('lang_id', 'SELECT * FROM about WHERE about_id = 1');
	}

	/*About end*/

	/*Contact start*/

	public function EditContact ()
	{
		$Values = [];

		foreach (Request::Post('SocialMedia') as $Key => $Val) {
			if (! empty($Val)) {
				$Values[$Key] = $Val;
			}
		}

		$this->DB->Query('UPDATE contact
							 SET mobile = ?s, phone = ?s, email = ?s, social_media = ?s
						   WHERE contact_id = 1',
						         Request::Post('Mobile'), Request::Post('Phone'), Request::Post('Email'), json_encode($Values));
		foreach (unserialize(LANGS) as $Lang => $LangID) {
			$this->DB->Query('INSERT INTO contact_descr
									  SET contact_id = 1, lang_id = ?i, address = ?s, short_descr = ?s, open_hours = ?s
									   ON DUPLICATE KEY
								   UPDATE address = ?s, short_descr = ?s, open_hours = ?s',
							 			  $LangID, Request::Post('Address-' . $Lang), Request::Post('ShortDescr-' . $Lang), Request::Post('OpenHours-' . $Lang),
							 			  Request::Post('Address-' . $Lang), Request::Post('ShortDescr-' . $Lang), Request::Post('OpenHours-' . $Lang));
		}
		$this->Resp['StatusCode'] = 1;

		return $this->Response();
	}

	/*Contact end*/

	/*Post start*/

	public function GetPostData($PostTable, $PostID)
	{
		$Data = [];

		$Data['Data']	= $this->DB->GetRow('SELECT * FROM ?n WHERE ?n = ?i', $PostTable, $this->PostTables[$PostTable]['id'], $PostID);
		$Data['Descr'] 	= $this->DB->GetInd('lang_id', 'SELECT * FROM ?n WHERE ?n = ?i', $this->PostTables[$PostTable]['descr_table'], $this->PostTables[$PostTable]['id'], $PostID);

		//$Data['Data']['id'] = $this->PostTables[$PostTable]['id'];

		if (isset($this->PostTables[$PostTable]['upl_dir'])) {
			$Data['Data']['upl_dir'] = $this->PostTables[$PostTable]['upl_dir'];
		}

		return $Data;
	}

	public function ChangePostStatus($PostTable, $PostField, $PostID)
	{
		$this->DB->Query('UPDATE ?n
			                 SET ?n = IF(?n = ?i, ?i, ?i)
			               WHERE ?n = ?i', $PostTable, $PostField, $PostField, INACTIVE_STATUS_ID, ACTIVE_STATUS_ID, INACTIVE_STATUS_ID, $this->PostTables[$PostTable]['id'], $PostID);
		$this->Resp['StatusCode'] 	= 1;
		$this->Resp['StatusID'] 	= $this->DB->GetOne('SELECT ?n FROM ?n WHERE ?n = ?i', $PostField, $PostTable, $this->PostTables[$PostTable]['id'], $PostID);
		
		return $this->Response();
	}

	public function DeletePost($PostTable, $PostID)
	{
		return $this->DB->Query('UPDATE ?n SET status_id = ?i WHERE ?n = ?i', $PostTable, DELETED_STATUS_ID, $this->PostTables[$PostTable]['id'], $PostID);
	}

	/*Post end*/

	/*Files start*/

	private function UpdateNewsPhotos($ID)
	{
		$i = 0;
		$j = 0;
		if (Request::Post('Photos') != '') {
			$Photos 	= explode(',', Request::Post('Photos'));
			$PhotosDir 	= str_replace(URL, '', UPL_NEWS);
			foreach ($Photos as $Val) {
				$Val = str_replace(URL, '', $Val);
				if (rename($Val, $PhotosDir . 'thumbs/' . $ID . '_' . ($i + 1) . '_' . IMG_TYPE) &&
					rename(str_replace('thumbs', 'large', $Val), $PhotosDir . 'large/' . $ID . '_' . ($i + 1) . '_' . IMG_TYPE)) {
					$i++;
				}
			}
			if ($i) {
				for ($k = 1; $k <= $i; $k++) {
					if (rename($PhotosDir . 'thumbs/' . $ID . '_' . $k . '_' . IMG_TYPE, $PhotosDir . 'thumbs/' . $ID . '_' . ($j + 1) . IMG_TYPE) &&
						rename($PhotosDir . 'large/' . $ID . '_' . $k . '_' . IMG_TYPE, $PhotosDir . 'large/' . $ID . '_' . ($j + 1) . IMG_TYPE)) {
						$j++;
					}
				}
			}
		}
		return $this->DB->Query('UPDATE news SET photos_cnt = ?i, photo_ver = photo_ver + 1 WHERE news_id = ?i', $j, $ID);
	}

	private function UpdateTourPhotos($ID)
	{
		$i = 0;
		$j = 0;
		if (Request::Post('Photos') != '') {
			$Photos 	= explode(',', Request::Post('Photos'));
			$PhotosDir 	= str_replace(URL, '', UPL_TOURS);
			foreach ($Photos as $Val) {
				$Val = str_replace(URL, '', $Val);
				if (rename($Val, $PhotosDir . 'thumbs/' . $ID . '_' . ($i + 1) . '_' . IMG_TYPE) &&
					rename(str_replace('thumbs', 'large', $Val), $PhotosDir . 'large/' . $ID . '_' . ($i + 1) . '_' . IMG_TYPE)) {
					$i++;
				}
			}
			if ($i) {
				for ($k = 1; $k <= $i; $k++) {
					if (rename($PhotosDir . 'thumbs/' . $ID . '_' . $k . '_' . IMG_TYPE, $PhotosDir . 'thumbs/' . $ID . '_' . ($j + 1) . IMG_TYPE) &&
						rename($PhotosDir . 'large/' . $ID . '_' . $k . '_' . IMG_TYPE, $PhotosDir . 'large/' . $ID . '_' . ($j + 1) . IMG_TYPE)) {
						$j++;
					}
				}
			}
		}
		return $this->DB->Query('UPDATE tours SET photos_cnt = ?i, photo_ver = photo_ver + 1 WHERE tour_id = ?i', $j, $ID);
	}

	private function UpdateProductFile($ID)
	{
		$HasFile 	= 0;
		$File 		= str_replace(URL, '', Request::Post('File'));

		if ($File != '' && file_exists($File)) {
			$HasFile = 1;
			if (strpos($File, 'tmp/') !== false && ! rename($File, str_replace(URL, '', UPL_PRODUCT_DOCS) . $ID . '.pdf')) {
				$HasFile = 0;
			}
		}
		return $this->DB->Query('UPDATE products SET has_file = ?i WHERE tour_id = ?i', $HasFile, $ID);
	}

	private function UpdateFile($PostTable, $ID)
	{
		$HasFile 	= 0;
		$File 		= str_replace(URL, '', Request::Post('File'));

		if ($File != '' && file_exists($File)) {
			$HasFile = 1;
			if (strpos($File, 'tmp/') !== false && ! rename($File, str_replace(URL, '', $this->PostTables[$PostTable]['upl_dir']) . $ID . $this->PostTables[$PostTable]['file_type'])) {
				$HasFile = 0;
			}
		}
		return $this->DB->Query('UPDATE ?n
									SET ?n = ?i, ?n = ?n + 1
								  WHERE ?n = ?i',
								  		$PostTable, $this->PostTables[$PostTable]['fields']['has_file']['name'], $HasFile,
								  		$this->PostTables[$PostTable]['fields']['file_ver']['name'], $this->PostTables[$PostTable]['fields']['file_ver']['name'],
								  		$this->PostTables[$PostTable]['id'], $ID);
	}

	/*Files end*/

	public function GetUserData()
	{
		return $this->DB->GetRow('SELECT * FROM users WHERE user_id = ?i', Session::Get('UserID'));
	}
}