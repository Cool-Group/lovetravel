<?php

/**
* Main Model
*/
class MainModel extends Model
{
	function __construct()
	{
		parent::__construct();
	}

	public function GetMainTours()
	{
		return $this->DB->GetAll('SELECT t.tour_id, t.photos_cnt, t.photo_ver, t.cat_id, t.old_price, t.price, t.currency_id, t.services, t.period_from, t.period_to,
										 d.title
								    FROM tours t
							   LEFT JOIN tour_descr d ON d.tour_id = t.tour_id AND d.lang_id = ?i
								   WHERE t.status_id = ?i AND t.is_main = ?i AND t.photos_cnt > 0
								ORDER BY RAND()
								   LIMIT ?i',
										 Lang::GetLangID(), ACTIVE_STATUS_ID, ACTIVE_STATUS_ID, 6);
	}

	public function GetNews()
	{
		return $this->DB->GetAll('SELECT n.news_id, n.photos_cnt, n.photo_ver, n.insert_date, d.title, d.short_descr
								    FROM news n
							   LEFT JOIN news_descr d ON d.news_id = n.news_id AND d.lang_id = ?i
								   WHERE n.status_id = ?i
								ORDER BY n.news_id DESC
								   LIMIT 3',
										 Lang::GetLangID(), ACTIVE_STATUS_ID);
	}

	public function GetAbout()
	{
		return $this->DB->GetOne('SELECT descr
								    FROM about
							   	   WHERE about_id = 1 AND lang_id = ?i', Lang::GetLangID());
	}

	public function GetTeam()
	{
		return $this->DB->GetAll('SELECT t.team_id, t.photo_ver, d.fullname, d.position
								    FROM team t
							   LEFT JOIN team_descr d ON d.team_id = t.team_id AND d.lang_id = ?i
							   	   WHERE t.status_id = ?i AND t.has_photo = ?i AND t.is_main = ?i
							   	ORDER BY RAND()
							   	   LIMIT ?i', Lang::GetLangID(), ACTIVE_STATUS_ID, ACTIVE_STATUS_ID, ACTIVE_STATUS_ID, 3);
	}

	public function GetTypedTours($TypeID)
	{
		return $this->DB->GetAll('SELECT t.tour_id, t.photos_cnt, t.photo_ver, t.cat_id, t.old_price, t.price, t.currency_id, t.services, t.period_from, t.period_to,
										 d.title, d.short_descr
								    FROM tours t
							   LEFT JOIN tour_descr d ON d.tour_id = t.tour_id AND d.lang_id = ?i
								   WHERE t.status_id = ?i AND t.is_main = ?i AND t.type_id = ?i AND t.photos_cnt > 0
								ORDER BY RAND()
								   LIMIT ?i',
										 Lang::GetLangID(), ACTIVE_STATUS_ID, INACTIVE_STATUS_ID, $TypeID, 3);
	}
}