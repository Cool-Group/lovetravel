<?php

/**
* Contact Model
*/
class ContactModel extends Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function Send()
	{
		$Data = array(
			'mail_to' 		=> $this->GetEmail(),
			'mail_from' 	=> Request::Post('Email'),
			'mail_reply_to' => Request::Post('Email'),
			'mail_priority'	=> 1,
			'mail_subject'  => 'Contact',
			'mail_message'  => 'Name: ' . Request::Post('Name') . '<br><br>' . 
			                   'Phone: ' . Request::Post('Phone') . '<br><br>' . Request::Post('Message')
		);

		return $this->Email($Data);
	}

	private function GetEmail()
	{
		return $this->DB->GetOne('SELECT email FROM contact WHERE contact_id = 1');
	}
}