<?php

class CronModel extends Model
{
	public function __construct()
	{		
		parent::__construct();
	}

	public function UpdateLangs()
	{
		new LangsGenerator($this->DB, true);
	}

	public function SendEmails()
	{
		$AttemptMailIDs = array();
		$DeleteMailIDs  = array();

		$Emails = $this->DB->GetAll('SELECT mail_id, mail_subject, mail_to, mail_from, mail_message, mail_priority, attempt, insert_date
									   FROM emails
								   ORDER BY mail_priority
									  LIMIT ?i', SEND_EMAILS_LIMIT);

		foreach ($Emails as $Key => $Val) {
			$MailSent = false;

			if ($Val['attempt'] >= SEND_EMAIL_MAX_ATTEMPT || ($MailSent = Email::Send($Val['mail_to'], $Val['mail_from'], $Val['mail_subject'], $Val['mail_message']))) {
				$this->DB->Query('INSERT INTO emails_arc SET ?u', array(
					'mail_id'      	=> $Val['mail_id'],
					'status_id'		=> $MailSent ? 1 : 0,	
					'mail_to'      	=> $Val['mail_to'],
					'mail_from'    	=> $Val['mail_from'],
					'mail_subject' 	=> $Val['mail_subject'],
					'mail_message' 	=> $Val['mail_message'],
					'mail_priority' => $Val['mail_priority'],
					'attempt' 		=> $Val['attempt'],
					'insert_date'   => $Val['insert_date']
				));
				$DeleteMailIDs[]  = $Val['mail_id'];
			} else  {
				$AttemptMailIDs[] = $Val['mail_id'];
			}
		}

		if (count($DeleteMailIDs)) {
			$this->DB->Query('DELETE FROM emails WHERE mail_id IN (?a)', $DeleteMailIDs);
		}

		if (count($AttemptMailIDs)) {
			$this->DB->Query('UPDATE emails SET attempt = attempt + 1 WHERE mail_id IN (?a)', $AttemptMailIDs);
		}
	}

	public function SendSMS()
	{
		$AttemptSMS_IDs = array();
		$DeleteSMS_IDs  = array();

		$Emails = $this->DB->GetRows('SELECT sms_id, phone, message, attempt, insert_date FROM sms LIMIT ' . SEND_SMS_LIMIT);

		foreach ($Emails as $Key => $Val) 
		{
			$SMS_Sent = false;

			if($Val['attempt'] == SEND_SMS_MAX_ATTEMPT || ($SMS_Sent = SMS::Send($Val['phone'], $Val['message'])))
			{
				$this->DB->Query('INSERT INTO sms_arc SET ?u', array(
					'sms_id'      => $Val['sms_id'],
					'status_id'   => $SMS_Sent ? 1 : 0,
					'phone'       => $Val['phone'],
					'message'     => $Val['message'],
					'attempt'     => $Val['attempt'],
					'insert_date' => $Val['insert_date']
				));
				$DeleteSMS_IDs[]  = $Val['sms_id'];
			}
			else 
			{
				$AttemptSMS_IDs[] = $Val['sms_id'];
			}
		}

		if(count($DeleteSMS_IDs)){
			$this->DB->Query('DELETE FROM sms WHERE sms_id IN ?a', $DeleteSMS_IDs);
		}

		if(count($AttemptSMS_IDs)){
			$this->DB->Query('UPDATE sms SET attempt = attempt + 1 WHERE sms_id IN ?a', $AttemptSMS_IDs);
		}
	}

	private function GetTableColumns($Table)
	{
		$Resp = array();

		foreach($this->DB->GetAll('SHOW COLUMNS FROM ?n', $Table) as $Val)
		{
			$Resp[] = $Val['Field'];
		}

		return $Resp;
	}
}