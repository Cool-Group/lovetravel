<?php
/**
* About Model
*/
class AboutModel extends Model
{
	function __construct()
	{
		parent::__construct();
	}

	public function GetAbout()
	{
		return $this->DB->GetOne('SELECT descr
								    FROM about
							   	   WHERE about_id = 1 AND lang_id = ?i', Lang::GetLangID());
	}

	public function GetTeam()
	{
		return $this->DB->GetAll('SELECT t.team_id, t.photo_ver, d.fullname, d.position
								    FROM team t
							   LEFT JOIN team_descr d ON d.team_id = t.team_id AND d.lang_id = ?i
							   	   WHERE t.status_id = ?i AND t.has_photo = ?i
							   	ORDER BY t.is_main DESC, t.team_id DESC', Lang::GetLangID(), ACTIVE_STATUS_ID, ACTIVE_STATUS_ID);
	}
}