<?php

/**
* News Model
*/
class NewsModel extends Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function GetNews($NewsID = 0)
	{
		if (! empty($NewsID)) {
			return $this->GetNewsData($NewsID);
		}

		$Resp  = array();
		$Page  = Functions::GetPage(ADMIN_NEWS_NUM);

		$Resp['Data'] =  $this->DB->GetAll('SELECT n.news_id, n.photos_cnt, n.photo_ver, n.insert_date, d.title, d.short_descr
										      FROM news n
									     LEFT JOIN news_descr d ON d.news_id = n.news_id AND d.lang_id = ?i
										     WHERE n.status_id = ?i
										  ORDER BY n.news_id DESC
										     LIMIT ?i, ?i',
										           Lang::GetLangID(), ACTIVE_STATUS_ID, $Page['StartRow'], ADMIN_NEWS_NUM);

		$Resp['Page'] = $Page['Page'];
		$Resp['Cnt']  = $this->DB->GetOne('SELECT COUNT(0)
											 FROM news n
										LEFT JOIN news_descr d ON d.news_id = n.news_id AND d.lang_id = ?i
											WHERE n.status_id = ?i', Lang::GetLangID(), ACTIVE_STATUS_ID);

		return $Resp;
	}

	private function GetNewsData($NewsID)
	{
		return $this->DB->GetRow('SELECT n.news_id, n.photos_cnt, n.photo_ver, n.insert_date, d.title, d.short_descr, d.descr
								    FROM news n
							   LEFT JOIN news_descr d ON d.news_id = n.news_id AND d.lang_id = ?i
								   WHERE n.status_id = ?i AND n.news_id = ?s',
										 Lang::GetLangID(), ACTIVE_STATUS_ID, $NewsID);
	}
}