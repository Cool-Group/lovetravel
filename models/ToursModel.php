<?php

/**
* Tours Model
*/
class ToursModel extends Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function GetTours()
	{
		$Resp  = array();
		$Page  = Functions::GetPage(ADMIN_NEWS_NUM);
		$Where = '';

		if (intval(Request::Get('CatID'))) {
			$Where = $this->DB->Parse(' AND t.cat_id IN (?a)', $this->GetSubCats(Request::Get('CatID')));
		}

		if (Request::Get('PriceFrom')) {
			$Where .= $this->DB->Parse(' AND t.price >= ?s', Request::Get('PriceFrom'));
		}

		if (Request::Get('PriceTo')) {
			$Where .= $this->DB->Parse(' AND t.price <= ?s', Request::Get('PriceTo'));
		}

		if (Request::Get('PeriodFrom')) {
			$Where .= $this->DB->Parse(' AND t.period_from >= ?s', $this->MySqlDate(Request::Get('PeriodFrom')));
		}

		if (Request::Get('PeriodTo')) {
			$Where .= $this->DB->Parse(' AND t.period_to <= ?s', $this->MySqlDate(Request::Get('PeriodTo')));
		}

		if (! empty(Request::Get('Keyword'))) {
			$Where .= $this->DB->Parse(' AND (d.title LIKE "%?p%" OR d.descr LIKE "%?p%")', Request::Get('Keyword'), Request::Get('Keyword'));
		}

		//echo $Where;

		if (Request::Get('Types') && is_array(Request::Get('Types'))) {
			$Values = [];
			foreach (Request::Get('Types') as $Val) {
				if (intval($Val)) {
					$Values[] = $Val;
				}
			}
			if (count($Values)) {
				$Where = $this->DB->Parse(' AND t.type_id IN (?a)', $Values);
			}
		}


		$Resp['Data'] =  $this->DB->GetAll('SELECT t.tour_id, t.photos_cnt, t.photo_ver, t.cat_id, t.old_price, t.price, t.currency_id, t.services, t.period_from, t.period_to,
										 		   d.title, d.short_descr
										      FROM tours t
									    INNER JOIN tour_descr d ON d.tour_id = t.tour_id AND d.lang_id = ?i
										     WHERE t.status_id = ?i AND t.photos_cnt > 0 ?p
										  ORDER BY t.tour_id DESC
										     LIMIT ?i, ?i',
										           Lang::GetLangID(), ACTIVE_STATUS_ID, $Where, $Page['StartRow'], ADMIN_NEWS_NUM);

		$Resp['Page'] = $Page['Page'];
		$Resp['Cnt']  = $this->DB->GetOne('SELECT COUNT(0)
											 FROM tours t
									   INNER JOIN tour_descr d ON d.tour_id = t.tour_id AND d.lang_id = ?i
											WHERE t.status_id = ?i AND t.photos_cnt > 0 ?p', Lang::GetLangID(), ACTIVE_STATUS_ID, $Where);

		return $Resp;
	}

	public function GetTourData($ID)
	{
		$Data = $this->DB->GetRow('SELECT t.*, d.*
			 					     FROM tours t
			 				   INNER JOIN tour_descr d ON d.tour_id = t.tour_id AND d.lang_id = ?i
			 				        WHERE t.tour_id = ?i AND t.status_id = ?i AND t.photos_cnt > 0', Lang::GetLangID(), $ID, ACTIVE_STATUS_ID);

		if (! empty($Data)) {
			$this->DB->Query('UPDATE tours SET views = views + 1 WHERE tour_id = ?i', $ID);
		}

		return $Data;
	}

	public function GetTourPopupData()
	{
		$Data = $this->DB->GetRow('SELECT t.tour_id, t.photos_cnt, t.photo_ver, t.period_from, t.period_to, t.old_price, t.price, t.currency_id, t.services,
										  t.type_id, d.title
			 					     FROM tours t
			 				   INNER JOIN tour_descr d ON d.tour_id = t.tour_id AND d.lang_id = ?i
			 				        WHERE t.tour_id = ?i AND t.status_id = ?i AND t.photos_cnt > 0', Lang::GetLangID(), Request::Get('TourID'), ACTIVE_STATUS_ID);

		return $Data;
	}

	public function GetSimilarTours($ID, $CatID)
	{
		return $this->DB->GetAll('SELECT t.tour_id, t.photo_ver, t.cat_id, t.old_price, t.price, t.currency_id, t.services, d.title
								    FROM tours t
							   LEFT JOIN tour_descr d ON d.tour_id = t.tour_id AND d.lang_id = ?i
								   WHERE t.status_id = ?i AND t.photos_cnt > 0 AND t.tour_id != ?i AND t.cat_id = ?i
								ORDER BY RAND()
								   LIMIT ?i', Lang::GetLangID(), ACTIVE_STATUS_ID, $ID, $CatID, 12);
	}
}