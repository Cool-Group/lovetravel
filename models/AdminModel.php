<?php

/**
* Admin Model
*/
class AdminModel extends Model
{
	public $PostTables;

	function __construct()
	{
		parent::__construct();

		$this->PostTables = [
			'news' 			=> [
				'id' 			=> 'news_id',
				'descr_table' 	=> 'news_descr',
				'upl_dir' 		=> UPL_NEWS,
				'file_type' 	=> IMG_TYPE,
				'photos_list'	=> true,
				'fields' 			=> [
					'status_id' 	=> ['name' => 'status_id'],
					'has_file' 		=> ['name' => 'photos_cnt'],
					'file_ver' 		=> ['name' => 'photo_ver'],
				],
			],
			'blog' 			=> [
				'id' 			=> 'blog_id',
				'descr_table' 	=> 'blog_descr',
				'upl_dir' 		=> UPL_BLOG,
				'file_type' 	=> IMG_TYPE,
				'photos_list'	=> true,
				'fields' 		=> [
					'status_id' 	=> ['name' => 'status_id'],
					'has_file' 		=> ['name' => 'photos_cnt'],
					'file_ver' 		=> ['name' => 'photo_ver'],
				],
			],
			'tours' 		=> [
				'id' 			=> 'tour_id',
				'descr_table' 	=> 'tour_descr',
				'upl_dir' 		=> UPL_TOURS,
				'file_type' 	=> IMG_TYPE,
				'photos_list'	=> true,
				'fields' 			=> [
					'status_id' 	=> ['name' => 'status_id'],
					'has_file' 		=> ['name' => 'photos_cnt'],
					'file_ver' 		=> ['name' => 'photo_ver'],
				],
			],
			'slider' 		=> [
				'id' 			=> 'slider_id',
				'descr_table' 	=> 'slider_descr',
				'upl_dir' 		=> UPL_SLIDER,
				'file_type' 	=> IMG_TYPE,
				'fields' 			=> [
					'status_id' 	=> ['name' => 'status_id'],
					'sort_order' 	=> ['name' => 'sort_order'],
					'has_file' 		=> ['name' => 'has_photo'],
					'file_ver' 		=> ['name' => 'photo_ver'],
				],
			],
			'team' 			=> [
				'id' 			=> 'team_id',
				'descr_table' 	=> 'team_descr',
				'upl_dir' 		=> UPL_TEAM,
				'file_type' 	=> IMG_TYPE,
				'fields' 		=> [
					'status_id' 	=> ['name' => 'status_id'],
					'sort_order' 	=> ['name' => 'sort_order'],
					'has_file' 		=> ['name' => 'has_photo'],
					'file_ver' 		=> ['name' => 'photo_ver'],
				],
			],
			'contact' 		=> [
				'id' 			=> 'contact_id',
				'descr_table' 	=> 'contact_descr'
			],
			'services' 		=> [
				'id' 			=> 'service_id',
				'descr_table' 	=> 'service_descr',
				'upl_dir' 		=> UPL_SERVICES,
				'file_type' 	=> IMG_TYPE,
				'fields' 		=> [
					'status_id' 	=> ['name' => 'status_id'],
					'has_file' 		=> ['name' => 'photos_cnt'],
					'file_ver' 		=> ['name' => 'photo_ver'],
				],
			],
			'cats' 			=> [
				'id' 				=> 'cat_id',
				'descr_table' 		=> 'cat_descr',
				'sub_items' 		=> 'sub_cats',
				/*'upl_dir' 			=> UPL_CATS,*/
				'file_type' 		=> '.png',
				'unable_to_delete'	=> ['ids' => [TOURS_IN_CAT_ID, TOURS_OUT_CAT_ID], 'status_message' => 'მთავარი მიმართულების წაშლა შეუძლებელია (შეზღუდულია)'],
				'fields' 			=> [
					'status_id' => ['name' => 'status_id'],
					'parent_id' => ['name' => 'parent_cat_id'],
					'sub_items' => ['name' => 'sub_cats'],
					'has_file' 	=> ['name' => 'has_photo'],
					'file_ver' 	=> ['name' => 'photo_ver'],
				],
				'options' 			=> ['status', 'sort', 'edit', 'delete']
			],
			'blog_cats' 	=> [
				'id'				=> 'cat_id',
				'descr_table' 		=> 'blog_cat_descr',
				'unable_to_delete'	=> ['ids' => [1], 'status_message' => 'ბლოგის მთავარი კატეგორიის წაშლა შეუძლებელია (შეზღუდულია)'],
				'fields' 			=> [
					'status_id' 	=> ['name' => 'status_id'],
					'sort_order' 	=> ['name' => 'sort_order']
				],
				'options' 		=> ['status', 'sort', 'edit', 'delete']
			],
			'countries' 	=> [
				'id'				=> 'country_id',
				'descr_table' 		=> 'country_descr',
				'fields' 			=> [
					'status_id' 	=> ['name' => 'status_id'],
					'sort_order' 	=> ['name' => 'sort_order']
				],
				'options' 		=> ['status', 'sort', 'edit', 'delete']
			],
			'tour_types' 	=> [
				'id'				=> 'type_id',
				'descr_table' 		=> 'tour_type_descr',
				'unable_to_delete'	=> ['ids' => [WINTER_TOURS_TYPE_ID, SEA_TOURS_TYPE_ID], 'status_message' => 'მთავარი კატეგორიის წაშლა შეუძლებელია (შეზღუდულია)'],
				'fields' 			=> [
					'status_id' 	=> ['name' => 'status_id'],
					'sort_order' 	=> ['name' => 'sort_order']
				],
				'options' 		=> ['status', 'sort', 'edit', 'delete']
			],
			'tour_notes' 	=> [
				'id' 			=> 'note_id',
				'descr_table'	=> 'tour_note_descr',
				'fields' 		=> [
					'status_id' 	=> ['name' => 'status_id'],
					'sort_order' 	=> ['name' => 'sort_order']
				],
				'options' 		=> ['status', 'sort', 'edit', 'delete']
			],
			'tour_services' 	=> [
				'id' 			=> 'service_id',
				'descr_table'	=> 'tour_service_descr',
				'fields' 		=> [
					'status_id' 	=> ['name' => 'status_id'],
					'icon' 			=> ['name' => 'icon', 'input' => 'text', 'label' => 'იკონა', 'required' => true, 'unique' => true, 'status_message' => 'ასეთი იკონა უკვე დამატებულია ბაზაში'],
					'sort_order' 	=> ['name' => 'sort_order']
				],
				'options' 		=> ['status', 'sort', 'edit', 'delete']
			],
			'langs' 		=> [
				'id' 				=> 'lang_var_id',
				'descr_table' 		=> 'lang_descr',
				//'title_ckeditor'	=> true,
				'fields' => [
					'lang_var' 		=> ['name' => 'lang_var', 'input' => 'text', 'label' => 'ცვლადი', 'required' => true, 'unique' => true, 'status_message' => 'ასეთი ცვლადი უკვე დამატებულია ბაზაში'],
					'lang_var_js' 	=> ['name' => 'lang_var_js', 'input' => 'checkbox', 'label' => 'javascript (js) ცვლადი'],
				],
				'options' 		=> ['edit', 'delete'],
				'callback'		=> 'UpdateLangs',
			],
		];
	}

	public function Authorize()
	{
		$AdminID = $this->DB->GetOne('SELECT admin_id
										FROM admins
									   WHERE email = ?s AND password = ?s',
								             Request::Post('Email'), Request::Post('Password'));

		if (! empty($AdminID)) {
			Session::Set('AdminID', $AdminID);
			return true;
		}
		return array('StatusCode' => 0, 'StatusMessage' => 'მომხმარებელი არ მოიძებნა');
	}

	/* News start */

	public function AddNewsPost()
	{
		
		if ($this->DB->Query('INSERT INTO news SET status_id = 0')) {
			$NewsID = $this->DB->InsertID();
			foreach (unserialize(LANGS) as $Lang => $LangID) {
				$this->DB->Query('INSERT INTO news_descr
									   	  SET news_id = ?i, lang_id = ?i, title = ?s, short_descr = ?s, descr = ?s',
									   	      $NewsID, $LangID, Request::Post('Title-' . $Lang), Request::Post('ShortDescr-' . $Lang), Request::Post('Descr-' . $Lang));
			}
			$this->UpdateFiles('news', $NewsID);
			$this->Resp['StatusCode'] = 1;
		}
		return $this->Response();
	}

	public function EditNewsPost()
	{
		$NewsID = Request::Post('NewsID');
		foreach (unserialize(LANGS) as $Lang => $LangID) {
			$this->DB->Query('INSERT INTO news_descr
								   	  SET news_id = ?i, lang_id = ?i, title = ?s, short_descr = ?s, descr = ?s
								   	   ON DUPLICATE KEY
								   UPDATE title = ?s, short_descr = ?s, descr = ?s',
								   	      $NewsID, $LangID, Request::Post('Title-' . $Lang), Request::Post('ShortDescr-' . $Lang), Request::Post('Descr-' . $Lang),
								   	      Request::Post('Title-' . $Lang), Request::Post('ShortDescr-' . $Lang), Request::Post('Descr-' . $Lang));
		}
		$this->UpdateFiles('news', $NewsID);
		$this->Resp['StatusCode'] = 1;

		return $this->Response();
	}

	public function GetNewsData($ID)
	{
		$Data = [];

		$Data['News'] 	= $this->DB->GetRow('SELECT * FROM news WHERE news_id = ?i', $ID);
		$Data['Descr'] 	= $this->DB->GetInd('lang_id', 'SELECT * FROM news_descr WHERE news_id = ?i', $ID);

		return $Data;
	}

	public function GetNews()
	{
		$Resp  = array();
		$Where = '';
		$Page  = Functions::GetPage(ADMIN_NEWS_NUM);

		$SearchFields = array(
			'Keyword' => ' AND (d.title LIKE "%?p%" OR d.descr LIKE "%?p%")'
		);

		foreach($SearchFields as $Key => $Val) {
			if(Request::Get($Key)) {
		    	$Where .= $this->DB->Parse($Val, Request::Get($Key), Request::Get($Key));
		    }
		}

		$Resp['Data'] =  $this->DB->GetAll('SELECT n.news_id, n.status_id, n.photos_cnt, n.photo_ver,
												   d.title, d.descr
											  FROM news n
									     LEFT JOIN news_descr d ON d.news_id = n.news_id AND d.lang_id = ?i
										     WHERE n.status_id != ?i ?p
										  ORDER BY n.news_id DESC
										     LIMIT ?i, ?i',
										           Lang::GetLangID(), DELETED_STATUS_ID,
										           $Where, $Page['StartRow'], ADMIN_NEWS_NUM);

		$Resp['Page'] = $Page['Page'];
		$Resp['Cnt']  = $this->DB->GetOne('SELECT COUNT(0)
											 FROM news n
										LEFT JOIN news_descr d ON d.news_id = n.news_id AND d.lang_id = ?i
											WHERE n.status_id != ?i ?p', Lang::GetLangID(), DELETED_STATUS_ID, $Where);

		return $Resp;
	}

	/* News end */

	/* Blog start */

	public function AddBlogPost()
	{
		
		if ($this->DB->Query('INSERT INTO blog SET cat_id = ?i', Request::Post('CatID'))) {
			$BlogID = $this->DB->InsertID();
			foreach (unserialize(LANGS) as $Lang => $LangID) {
				$this->DB->Query('INSERT INTO blog_descr
									   	  SET blog_id = ?i, lang_id = ?i, title = ?s, short_descr = ?s, descr = ?s',
									   	      $BlogID, $LangID, Request::Post('Title-' . $Lang), Request::Post('ShortDescr-' . $Lang), Request::Post('Descr-' . $Lang));
			}
			$this->UpdateFiles('blog', $BlogID);
			$this->Resp['StatusCode'] = 1;
		}
		return $this->Response();
	}

	public function EditBlogPost()
	{
		$BlogID = Request::Post('BlogID');
		$Update = $this->DB->Query('UPDATE blog SET cat_id = ?i WHERE blog_id = ?i', Request::Post('CatID'), $BlogID);

		foreach (unserialize(LANGS) as $Lang => $LangID) {
			$this->DB->Query('INSERT INTO blog_descr
								   	  SET blog_id = ?i, lang_id = ?i, title = ?s, short_descr = ?s, descr = ?s
								   	   ON DUPLICATE KEY
								   UPDATE title = ?s, short_descr = ?s, descr = ?s',
								   	      $BlogID, $LangID, Request::Post('Title-' . $Lang), Request::Post('ShortDescr-' . $Lang), Request::Post('Descr-' . $Lang),
								   	      Request::Post('Title-' . $Lang), Request::Post('ShortDescr-' . $Lang), Request::Post('Descr-' . $Lang));
		}
		$this->UpdateFiles('blog', $BlogID);
		$this->Resp['StatusCode'] = 1;

		return $this->Response();
	}

	public function GetBlogData($ID)
	{
		$Data = [];

		$Data['Blog'] 	= $this->DB->GetRow('SELECT * FROM blog WHERE blog_id = ?i', $ID);
		$Data['Descr'] 	= $this->DB->GetInd('lang_id', 'SELECT * FROM blog_descr WHERE blog_id = ?i', $ID);

		return $Data;
	}

	public function GetBlog()
	{
		$Resp  = array();
		$Where = '';
		$Page  = Functions::GetPage(ADMIN_NEWS_NUM);

		$SearchFields = array(
			'Keyword' => ' AND (d.title LIKE "%?p%" OR d.descr LIKE "%?p%")'
		);

		foreach($SearchFields as $Key => $Val) {
			if(Request::Get($Key)) {
		    	$Where .= $this->DB->Parse($Val, Request::Get($Key), Request::Get($Key));
		    }
		}

		$Resp['Data'] =  $this->DB->GetAll('SELECT n.blog_id, n.status_id, n.photos_cnt, n.photo_ver,
												   d.title, d.descr
											  FROM blog n
									     LEFT JOIN blog_descr d ON d.blog_id = n.blog_id AND d.lang_id = ?i
										     WHERE n.status_id != ?i ?p
										  ORDER BY n.blog_id DESC
										     LIMIT ?i, ?i',
										           Lang::GetLangID(), DELETED_STATUS_ID,
										           $Where, $Page['StartRow'], ADMIN_NEWS_NUM);

		$Resp['Page'] = $Page['Page'];
		$Resp['Cnt']  = $this->DB->GetOne('SELECT COUNT(0)
											 FROM blog n
										LEFT JOIN blog_descr d ON d.blog_id = n.blog_id AND d.lang_id = ?i
											WHERE n.status_id != ?i ?p', Lang::GetLangID(), DELETED_STATUS_ID, $Where);

		return $Resp;
	}

	/* Blog end */

	/* Services start */

	public function AddServicePost()
	{
		
		if ($this->DB->Query('INSERT INTO services SET status_id = 0')) {
			$ID = $this->DB->InsertID();
			foreach (unserialize(LANGS) as $Lang => $LangID) {
				$this->DB->Query('INSERT INTO service_descr
									   	  SET service_id = ?i, lang_id = ?i, title = ?s, short_descr = ?s, descr = ?s',
									   	      $ID, $LangID, Request::Post('Title-' . $Lang), Request::Post('ShortDescr-' . $Lang), Request::Post('Descr-' . $Lang));
			}
			$this->UpdateFiles('services', $ID);
			$this->Resp['StatusCode'] = 1;
		}
		return $this->Response();
	}

	public function EditServicePost()
	{
		$ID = Request::Post('ServiceID');
		foreach (unserialize(LANGS) as $Lang => $LangID) {
			$this->DB->Query('INSERT INTO service_descr
								   	  SET service_id = ?i, lang_id = ?i, title = ?s, short_descr = ?s, descr = ?s
								   	   ON DUPLICATE KEY
								   UPDATE title = ?s, short_descr = ?s, descr = ?s',
								   	      $ID, $LangID, Request::Post('Title-' . $Lang), Request::Post('ShortDescr-' . $Lang), Request::Post('Descr-' . $Lang),
								   	      Request::Post('Title-' . $Lang), Request::Post('ShortDescr-' . $Lang), Request::Post('Descr-' . $Lang));
		}
		$this->UpdateFiles('services', $ID);
		$this->Resp['StatusCode'] = 1;

		return $this->Response();
	}

	public function _GetServices()
	{
		$Resp  = array();
		$Where = '';
		$Page  = Functions::GetPage(ADMIN_NEWS_NUM);

		$SearchFields = array(
			'Keyword' => ' AND (d.title LIKE "%?p%" OR d.short_descr LIKE "%?p%" OR d.descr LIKE "%?p%")'
		);

		foreach($SearchFields as $Key => $Val) {
			if(Request::Get($Key)) {
		    	$Where .= $this->DB->Parse($Val, Request::Get($Key), Request::Get($Key), Request::Get($Key));
		    }
		}

		$Resp['Data'] =  $this->DB->GetAll('SELECT n.service_id, n.status_id, n.photos_cnt, n.photo_ver,
												   d.title, d.descr
											  FROM services n
									     LEFT JOIN service_descr d ON d.service_id = n.service_id AND d.lang_id = ?i
										     WHERE n.status_id != ?i ?p
										  ORDER BY n.service_id DESC
										     LIMIT ?i, ?i',
										           Lang::GetLangID(), DELETED_STATUS_ID,
										           $Where, $Page['StartRow'], ADMIN_NEWS_NUM);

		$Resp['Page'] = $Page['Page'];
		$Resp['Cnt']  = $this->DB->GetOne('SELECT COUNT(0)
											 FROM services n
										LEFT JOIN service_descr d ON d.service_id = n.service_id AND d.lang_id = ?i
											WHERE n.status_id != ?i ?p', Lang::GetLangID(), DELETED_STATUS_ID, $Where);

		return $Resp;
	}

	/* Services end */

	/* Slider start */

	public function AddSliderPost()
	{
		if ($this->DB->Query('INSERT INTO slider SET sort_order = ?i', Request::Post('SortOrder'))) {
			$ID = $this->DB->InsertID();
			foreach (unserialize(LANGS) as $Lang => $LangID) {
				$this->DB->Query('INSERT INTO slider_descr
									   	  SET slider_id = ?i, lang_id = ?i, title_1 = ?s, title_2 = ?s',
									   	      $ID, $LangID, Request::Post('Title-1-' . $Lang), Request::Post('Title-2-' . $Lang));
			}
			$this->UpdateFiles('slider', $this->DB->InsertID());
			$this->Resp['StatusCode'] = 1;
		}
		return $this->Response();
	}

	public function EditSliderPost()
	{
		$ID = Request::Post('SliderID');
		$this->DB->Query('UPDATE slider SET sort_order = ?i WHERE slider_id = ?i', Request::Post('SortOrder'), $ID);
		foreach (unserialize(LANGS) as $Lang => $LangID) {
			$this->DB->Query('INSERT INTO slider_descr
								   	  SET slider_id = ?i, lang_id = ?i, title_1 = ?s, title_2 = ?s
								   	   ON DUPLICATE KEY
								   UPDATE title_1 = ?s, title_2 = ?s',
								   	      $ID, $LangID, Request::Post('Title-1-' . $Lang), Request::Post('Title-2-' . $Lang),
								   	      Request::Post('Title-1-' . $Lang), Request::Post('Title-2-' . $Lang));
		}
		$this->UpdateFiles('slider', $ID);
		$this->Resp['StatusCode'] = 1;

		return $this->Response();
	}

	public function GetSlider()
	{
		$Resp  = array();
		$Where = '';
		$Page  = Functions::GetPage(ADMIN_NEWS_NUM);

		$Resp['Data'] =  $this->DB->GetAll('SELECT s.slider_id, s.status_id, s.sort_order, s.has_photo, s.photo_ver, d.title_1, d.title_2
											  FROM slider s
										 LEFT JOIN slider_descr d ON d.slider_id = s.slider_id AND d.lang_id = ?i
										     WHERE s.status_id != ?i
										  ORDER BY s.sort_order, s.slider_id DESC
										     LIMIT ?i, ?i',
										           Lang::GetLangID(), DELETED_STATUS_ID,
										           $Page['StartRow'], ADMIN_NEWS_NUM);

		$Resp['Page'] = $Page['Page'];
		$Resp['Cnt']  = $this->DB->GetOne('SELECT COUNT(0) FROM slider WHERE status_id != ?i', DELETED_STATUS_ID);

		return $Resp;
	}

	/* Slider end */

	/* Tours start */

	public function AddTourPost()
	{
		$this->SetTourServices();

		$Insert = $this->DB->Query('INSERT INTO tours
										    SET cat_id = ?i, type_id = ?i, old_price = ?s, price = ?s, currency_id = ?i,
										    	period_from = ?s, period_to = ?s, services = ?s, notes = ?s, hotels = ?s, map_coords = ?s',
										        Request::Post('CatID'), Request::Post('TypeID'), Request::Post('OldPrice'), Request::Post('Price'),
										        Request::Post('CurrencyID'), $this->MySqlDate(Request::Post('PeriodFrom')), $this->MySqlDate(Request::Post('PeriodTo')),
										        json_encode(Request::Post('Services')), json_encode(Request::Post('Notes')), Request::Post('Hotels'), Request::Post('MapCoords'));
		if ($Insert) {
			$ID = $this->DB->InsertID();
			foreach (unserialize(LANGS) as $Lang => $LangID) {
				$this->DB->Query('INSERT INTO tour_descr
									   	  SET tour_id = ?i, lang_id = ?i, title = ?s, short_descr = ?s, descr = ?s',
									   	      $ID, $LangID, Request::Post('Title-' . $Lang), Request::Post('ShortDescr-' . $Lang), Request::Post('Descr-' . $Lang));
			}

			$this->EditTourServices($ID, false);
			$this->UpdateFiles('tours', $ID);
			$this->Resp['StatusCode'] = 1;
		}
		return $this->Response();
	}

	public function EditTourPost()
	{
		$this->SetTourServices();

		$ID = Request::Post('TourID');

		$Update = $this->DB->Query('UPDATE tours
									   SET cat_id = ?i, type_id = ?i, old_price = ?s, price = ?s, currency_id = ?i,
									   	   period_from = ?s, period_to = ?s, services = ?s, notes = ?s, hotels = ?s, map_coords = ?s
									 WHERE tour_id = ?i',
										   Request::Post('CatID'), Request::Post('TypeID'), Request::Post('OldPrice'), Request::Post('Price'),
										   Request::Post('CurrencyID'), $this->MySqlDate(Request::Post('PeriodFrom')), $this->MySqlDate(Request::Post('PeriodTo')),
										   json_encode(Request::Post('Services')), json_encode(Request::Post('Notes')), Request::Post('Hotels'), Request::Post('MapCoords'), $ID);

		foreach (unserialize(LANGS) as $Lang => $LangID) {
			$this->DB->Query('INSERT INTO tour_descr
								   	  SET tour_id = ?i, lang_id = ?i, title = ?s, short_descr = ?s, descr = ?s
								   	   ON DUPLICATE KEY
								   UPDATE title = ?s, short_descr = ?s, descr = ?s',
								   	      $ID, $LangID, Request::Post('Title-' . $Lang), Request::Post('ShortDescr-' . $Lang), Request::Post('Descr-' . $Lang),
								   	      Request::Post('Title-' . $Lang), Request::Post('ShortDescr-' . $Lang), Request::Post('Descr-' . $Lang));
		}

		$this->EditTourServices($ID);
		$this->UpdateFiles('tours', $ID);
		$this->Resp['StatusCode'] = 1;

		return $this->Response();
	}

	private function SetTourServices()
	{
		$Values = [];

		foreach (Request::Post('Services') as $Key => $Val) {
			if ($Val != 0) {
				$Values[$Key] = $Val;
			}
		}

		Request::SetPost('Services', $Values);
	}

	private function EditTourServices($ID, $Edit = true)
	{		
		if ($Edit) {
			$this->DB->Query('DELETE FROM tour_checked_services WHERE tour_id = ?i', $ID);
		}
		if (is_array(Request::Post('Services'))) {
			$Values = [];
			foreach (Request::Post('Services') as $Key => $Val) {
				$Values[] = $this->DB->Parse('(?i, ?i, ?i)', $ID, $Key, $Val);
			}
			if (count($Values)) {
				$this->DB->Query('INSERT INTO tour_checked_services (tour_id, service_id, type_id) VALUES ?p', implode(',', $Values));
			}
		}
	}

	public function GetTours()
	{
		$Resp  = array();
		$Where = '';
		$Page  = Functions::GetPage(ADMIN_NEWS_NUM);

		$SearchFields = array(
			'Keyword' => ' AND (d.title LIKE "%?p%" OR d.descr LIKE "%?p%")'
		);

		foreach($SearchFields as $Key => $Val) {
			if(Request::Get($Key)) {
		    	$Where .= $this->DB->Parse($Val, Request::Get($Key), Request::Get($Key));
		    }
		}

		$Resp['Data'] =  $this->DB->GetAll('SELECT n.tour_id, n.status_id, n.is_main, n.photos_cnt, n.photo_ver,
												   d.title, d.descr
											  FROM tours n
									     LEFT JOIN tour_descr d ON d.tour_id = n.tour_id AND d.lang_id = ?i
										     WHERE n.status_id != ?i ?p
										  ORDER BY n.is_main DESC, n.tour_id DESC
										     LIMIT ?i, ?i',
										           Lang::GetLangID(), DELETED_STATUS_ID,
										           $Where, $Page['StartRow'], ADMIN_NEWS_NUM);

		$Resp['Page'] = $Page['Page'];
		$Resp['Cnt']  = $this->DB->GetOne('SELECT COUNT(0)
											 FROM tours n
										LEFT JOIN tour_descr d ON d.tour_id = n.tour_id AND d.lang_id = ?i
											WHERE n.status_id != ?i ?p', Lang::GetLangID(), DELETED_STATUS_ID, $Where);

		return $Resp;
	}

	/* Tours end */

	/* Team start */

	public function AddTeamPost()
	{
		if ($this->DB->Query('INSERT INTO team SET status_id = 0')) {
			$ID = $this->DB->InsertID();
			foreach (unserialize(LANGS) as $Lang => $LangID) {
				$this->DB->Query('INSERT INTO team_descr
									   	  SET team_id = ?i, lang_id = ?i, fullname = ?s, position = ?s',
									   	      $ID, $LangID, Request::Post('Fullname-' . $Lang), Request::Post('Position-' . $Lang));
			}
			$this->UpdateFiles('team', $ID);
			$this->Resp['StatusCode'] = 1;
		}
		return $this->Response();
	}

	public function EditTeamPost()
	{
		$ID = Request::Post('TeamID');
		foreach (unserialize(LANGS) as $Lang => $LangID) {
			$this->DB->Query('INSERT INTO team_descr
								   	  SET team_id = ?i, lang_id = ?i, fullname = ?s, position = ?s
								   	   ON DUPLICATE KEY
								   UPDATE fullname = ?s, position = ?s',
								   	      $ID, $LangID, Request::Post('Fullname-' . $Lang), Request::Post('Position-' . $Lang),
								   	      Request::Post('Fullname-' . $Lang), Request::Post('Position-' . $Lang));
		}
		$this->UpdateFiles('team', $ID);
		$this->Resp['StatusCode'] = 1;

		return $this->Response();
	}

	public function GetTeam()
	{
		$Resp  = array();
		$Where = '';
		$Page  = Functions::GetPage(ADMIN_NEWS_NUM);

		$SearchFields = array(
			'Keyword' => ' AND (d.fullname LIKE "%?p%" OR d.position LIKE "%?p%")'
		);

		foreach($SearchFields as $Key => $Val) {
			if(Request::Get($Key)) {
		    	$Where .= $this->DB->Parse($Val, Request::Get($Key), Request::Get($Key));
		    }
		}

		$Resp['Data'] =  $this->DB->GetAll('SELECT t.team_id, t.status_id, t.is_main, t.has_photo, t.photo_ver,
												   d.fullname, d.position
											  FROM team t
									     LEFT JOIN team_descr d ON d.team_id = t.team_id AND d.lang_id = ?i
										     WHERE t.status_id != ?i ?p
										  ORDER BY t.is_main DESC, t.team_id DESC
										     LIMIT ?i, ?i',
										           Lang::GetLangID(), DELETED_STATUS_ID,
										           $Where, $Page['StartRow'], ADMIN_NEWS_NUM);

		$Resp['Page'] = $Page['Page'];
		$Resp['Cnt']  = $this->DB->GetOne('SELECT COUNT(0)
											 FROM team t
										LEFT JOIN team_descr d ON d.team_id = t.team_id AND d.lang_id = ?i
											WHERE status_id != ?i ?p', Lang::GetLangID(), DELETED_STATUS_ID, $Where);

		return $Resp;
	}

	/* Team end */

	/*List start*/

	public function AddListItem()
	{
		$ID = $this->InsertListItem();

		if (is_array($ID)) {
			return $ID;
		}

		if (isset($this->PostTables[Request::Post('PostTable')]['fields']['sub_items'])) {
			$this->DB->Query('UPDATE ?n
								 SET ?n = ?s
							   WHERE ?n = ?i',
							         Request::Post('PostTable'),
							         $this->PostTables[Request::Post('PostTable')]['fields']['sub_items']['name'], $ID,
							         $this->PostTables[Request::Post('PostTable')]['id'], $ID);
		}

		foreach (unserialize(LANGS) as $Key => $Val) {
			$this->DB->Query('INSERT INTO ?n
									  SET ?n = ?i, lang_id = ?i, title = ?s',
									      $this->PostTables[Request::Post('PostTable')]['descr_table'],
									      $this->PostTables[Request::Post('PostTable')]['id'], $ID, $Val, Request::Post('title-' . $Key));
		}
		
		if (isset($this->PostTables[Request::Post('PostTable')]['upl_dir']) && ! empty(Request::Post('File'))) {
			$this->UpdateFiles(Request::Post('PostTable'), $ID);
		}

		$this->ListItemCallback();
		
		return array(
			'StatusCode' 	=> 1,
			'parent_id' 	=> isset($this->PostTables[Request::Post('PostTable')]['fields']['parent_id']) ? Request::Post($this->PostTables[Request::Post('PostTable')]['fields']['parent_id']['name']) : 0,
			'ID' 			=> $ID,
			'title' 		=> Request::Post('title-' . Lang::GetLang()));
	}

	private function InsertListItem()
	{
		$Query = [];

		foreach ($this->PostTables[Request::Post('PostTable')]['fields'] as $Key => $Val) {
			if (isset($Val['unique']) && $this->DB->GetOne('SELECT COUNT(0) FROM ?n WHERE ?n = ?s LIMIT 1', Request::Post('PostTable'), $Val['name'], Request::Post($Val['name']))) {
				return array('StatusCode' => 0, 'StatusMessage' => $Val['status_message']);
			}
			$Query[] = $this->DB->Parse('?n = ?s', $Val['name'], $Val['name'] == 'sort_order' ? $this->GetNewListItemSortOrder() : Request::Post($Val['name']));
		}

		$this->DB->Query('INSERT INTO ?n SET ?p', Request::Post('PostTable'), implode(', ', $Query));

		return $this->DB->InsertID();
	}

	private function GetNewListItemSortOrder()
	{
		return (int)$this->DB->GetOne('SELECT MAX(sort_order)
										 FROM ?n ?p',
										      Request::Post('PostTable'),
										      isset($this->PostTables[Request::Post('PostTable')]['fields']['parent_id']) ?
										      $this->DB->Parse('WHERE ?n = ?i', $this->PostTables[Request::Post('PostTable')]['fields']['parent_id']['name'],
										      Request::Post($this->PostTables[Request::Post('PostTable')]['fields']['parent_id']['name'])) : '') + 1;
	}

	public function ChangeListItemSortOrder()
	{
		$ItemData 		= $this->DB->GetRow('SELECT sort_order ?p
											   FROM ?n
											  WHERE ?n = ?i',
											   		isset($this->PostTables[Request::Post('PostTable')]['fields']['parent_id']) ?
											  		$this->DB->Parse(', ?n', $this->PostTables[Request::Post('PostTable')]['fields']['parent_id']['name']) : '',
											  		Request::Post('PostTable'),
											        $this->PostTables[Request::Post('PostTable')]['id'], Request::Post('ID'));
		$SecondItemData = $this->DB->GetRow('SELECT sort_order, ?n
											   FROM ?n
											  WHERE sort_order ?p ?i AND status_id < ?i
											  		?p
										   ORDER BY sort_order ?p
											  LIMIT 1',
											  		$this->PostTables[Request::Post('PostTable')]['id'],
											  		Request::Post('PostTable'),
											        Request::Post('SortID') ? '>' : '<', $ItemData['sort_order'], DELETED_STATUS_ID,
											        isset($this->PostTables[Request::Post('PostTable')]['fields']['parent_id']) ?
											        $this->DB->Parse(' AND ?n = ?i', $this->PostTables[Request::Post('PostTable')]['fields']['parent_id']['name'],
											        								 $ItemData[$this->PostTables[Request::Post('PostTable')]['fields']['parent_id']['name']]) : '',
											        Request::Post('SortID') ? '' : 'DESC');

		$this->DB->Query('UPDATE ?n
							 SET sort_order = ?i
						   WHERE ?n = ?i',
						         Request::Post('PostTable'),
						         $SecondItemData['sort_order'],
						         $this->PostTables[Request::Post('PostTable')]['id'], Request::Post('ID'));
		$this->DB->Query('UPDATE ?n
							 SET sort_order = ?i
						   WHERE ?n = ?i',
						     	 Request::Post('PostTable'),
						     	 $ItemData['sort_order'],
						     	 $this->PostTables[Request::Post('PostTable')]['id'], $SecondItemData[$this->PostTables[Request::Post('PostTable')]['id']]);

		return true;
	}

	public function ChangeListItemStatus()
	{
		$ItemData = $this->DB->GetRow('SELECT status_id ?p
										 FROM ?n
										WHERE ?n = ?i',
											  isset($this->PostTables[Request::Post('PostTable')]['fields']['parent_id']) ?
											  $this->DB->Parse(', ?n', $this->PostTables[Request::Post('PostTable')]['fields']['parent_id']['name']) : '',
										      Request::Post('PostTable'),
										      $this->PostTables[Request::Post('PostTable')]['id'], Request::Post('ID'));
		$ItemData['status_id'] = $ItemData['status_id'] ? INACTIVE_STATUS_ID : ACTIVE_STATUS_ID;
		
		if ($this->DB->Query('UPDATE ?n SET status_id = ?i WHERE ?n = ?i', Request::Post('PostTable'), $ItemData['status_id'], $this->PostTables[Request::Post('PostTable')]['id'], Request::Post('ID'))) {
			if (isset($this->PostTables[Request::Post('PostTable')]['fields']['parent_id'])) {
				$this->UpdateSubListItems($ItemData[$this->PostTables[Request::Post('PostTable')]['fields']['parent_id']['name']]);
			}
			return array('StatusCode' => 1, 'ItemStatusID' => $ItemData['status_id']);
		}

		return false;
	}

	private function UpdateSubListItems($ParentID)
	{
		if ($ParentID) {
			$ItemData = $this->DB->GetRow('SELECT ?n, (SELECT GROUP_CONCAT(?n) FROM ?n WHERE ?n = ?i AND status_id = ?i) AS sub_items
										     FROM ?n
										    WHERE ?n = ?i',
										    	  $this->PostTables[Request::Post('PostTable')]['fields']['parent_id']['name'], $this->PostTables[Request::Post('PostTable')]['fields']['sub_items']['name'],
										    	  Request::Post('PostTable'), $this->PostTables[Request::Post('PostTable')]['fields']['parent_id']['name'], $ParentID, ACTIVE_STATUS_ID,
										    	  Request::Post('PostTable'), $this->PostTables[Request::Post('PostTable')]['id'], $ParentID);
			
			$ItemData['sub_items'] = rtrim($ItemData['sub_items'], ',');

			$this->DB->Query('UPDATE ?n
								 SET ?n = ?s
							   WHERE cat_id = ?i',
							   		 Request::Post('PostTable'),
							   		 $this->PostTables[Request::Post('PostTable')]['fields']['sub_items']['name'], $ParentID . ( ! empty($ItemData['sub_items']) ? ',' . $ItemData['sub_items'] : ''),
							   		 $ParentID);

			$this->UpdateSubListItems($ItemData[$this->PostTables[Request::Post('PostTable')]['fields']['parent_id']['name']]);
		}
	}

	public function EditListItem()
	{
		foreach (unserialize(LANGS) as $Lang => $LangID) {
			$this->DB->Query('INSERT INTO ?n
									  SET ?n = ?i, lang_id = ?i, title = ?s
									   ON DUPLICATE KEY
								   UPDATE title = ?s',
							  			  $this->PostTables[Request::Post('PostTable')]['descr_table'],
							  			  $this->PostTables[Request::Post('PostTable')]['id'], Request::Post('ID'), $LangID, Request::Post('title-' . $Lang),
							  			  Request::Post('title-' . $Lang));
		}

		if (isset($this->PostTables[Request::Post('PostTable')]['upl_dir']) && ! empty(Request::Post('File'))) {
			$this->UpdateFiles(Request::Post('PostTable'), Request::Post('ID'));
		}

		$this->ListItemCallback();

		return array('StatusCode' => 1, 'ID' => Request::Post('ID'), 'title' => Request::Post('title-' . Lang::GetLang()));
	}

	public function DeleteListItem()
	{
		if (isset($this->PostTables[Request::Post('PostTable')]['unable_to_delete']) && in_array(Request::Post('ID'), $this->PostTables[Request::Post('PostTable')]['unable_to_delete']['ids'])) {
			return array('StatusCode' => 0, 'StatusMessage' => $this->PostTables[Request::Post('PostTable')]['unable_to_delete']['status_message']);
		}

		if (! isset($this->PostTables[Request::Post('PostTable')]['fields']['status_id'])) {
			$this->DB->Query('DELETE FROM ?n WHERE ?n = ?i', Request::Post('PostTable'), $this->PostTables[Request::Post('PostTable')]['id'], Request::Post('ID'));
			$this->DB->Query('DELETE FROM ?n WHERE ?n = ?i', $this->PostTables[Request::Post('PostTable')]['descr_table'], $this->PostTables[Request::Post('PostTable')]['id'], Request::Post('ID'));

			$this->ListItemCallback();

			return true;
		}

		if ($this->DB->Query('UPDATE ?n SET status_id = ?i WHERE ?n = ?i', Request::Post('PostTable'), DELETED_STATUS_ID, $this->PostTables[Request::Post('PostTable')]['id'], Request::Post('ID'))) {
			if (isset($this->PostTables[Request::Post('PostTable')]['fields']['parent_id'])) {
				$ParentID = $this->DB->GetOne('SELECT ?n
												 FROM ?n
												WHERE ?n = ?i',
												      $this->PostTables[Request::Post('PostTable')]['fields']['parent_id']['name'],
												      Request::Post('PostTable'),
												      $this->PostTables[Request::Post('PostTable')]['id'], Request::Post('ID'));
				$this->UpdateSubListItems($ParentID);
			}

			$this->ListItemCallback();

			return true;
		}

		return false;
	}

	private function ListItemCallback()
	{
		if (isset($this->PostTables[Request::Post('PostTable')]['callback'])) {
			$this->{$this->PostTables[Request::Post('PostTable')]['callback']}();
		}
	}

	/*List end*/

	/*Langs start*/

	public function GetLangs()
    {
        return $this->DB->GetInd('lang_var_id', 'SELECT s.lang_var_id, s.lang_var_id AS id, d.title, s.lang_var AS title_1, IF(s.lang_var_js, "js", "") AS title_2
				                               	   FROM langs s
				                              LEFT JOIN lang_descr d ON d.lang_var_id = s.lang_var_id AND d.lang_id = ?i
				                               ORDER BY s.lang_var_id', Lang::GetLangID());
    }

    public function UpdateLangs()
	{
		new LangsGenerator($this->DB);
	}

	/*Langs end*/

	/*About start*/

	public function EditAbout()
	{
		foreach (unserialize(LANGS) as $Lang => $LangID) {
			$this->DB->Query('INSERT INTO about
								   	  SET about_id = 1, lang_id = ?i, descr = ?s
								   	   ON DUPLICATE KEY
								   UPDATE descr = ?s',
								   	      $LangID, Request::Post('Descr-' . $Lang), Request::Post('Descr-' . $Lang));
		}
		$this->Resp['StatusCode'] = 1;

		return $this->Response();
	}

	public function GetAbout()
	{
		return $this->DB->GetInd('lang_id', 'SELECT * FROM about WHERE about_id = 1');
	}

	/*About end*/

	/*Contact start*/

	public function EditContact ()
	{
		$Values = [];

		foreach (Request::Post('SocialMedia') as $Key => $Val) {
			if (! empty($Val)) {
				$Values[$Key] = $Val;
			}
		}

		$this->DB->Query('UPDATE contact
							 SET mobile = ?s, phone = ?s, email = ?s, noreply_email = ?s, social_media = ?s
						   WHERE contact_id = 1',
						         Request::Post('Mobile'), Request::Post('Phone'), Request::Post('Email'), Request::Post('NoreplyEmail'), json_encode($Values));
		foreach (unserialize(LANGS) as $Lang => $LangID) {
			$this->DB->Query('INSERT INTO contact_descr
									  SET contact_id = 1, lang_id = ?i, address = ?s, short_descr = ?s, open_hours = ?s
									   ON DUPLICATE KEY
								   UPDATE address = ?s, short_descr = ?s, open_hours = ?s',
							 			  $LangID, Request::Post('Address-' . $Lang), Request::Post('ShortDescr-' . $Lang), Request::Post('OpenHours-' . $Lang),
							 			  Request::Post('Address-' . $Lang), Request::Post('ShortDescr-' . $Lang), Request::Post('OpenHours-' . $Lang));
		}
		$this->Resp['StatusCode'] = 1;

		return $this->Response();
	}

	/*Contact end*/

	/*Post start*/

	public function GetPostData($PostTable, $PostID)
	{
		$Data = [];

		$Data['Data']	= $this->DB->GetRow('SELECT * FROM ?n WHERE ?n = ?i', $PostTable, $this->PostTables[$PostTable]['id'], $PostID);
		$Data['Descr'] 	= $this->DB->GetInd('lang_id', 'SELECT * FROM ?n WHERE ?n = ?i', $this->PostTables[$PostTable]['descr_table'], $this->PostTables[$PostTable]['id'], $PostID);

		//$Data['Data']['id'] = $this->PostTables[$PostTable]['id'];

		if (isset($this->PostTables[$PostTable]['upl_dir'])) {
			$Data['Data']['upl_dir'] = $this->PostTables[$PostTable]['upl_dir'];
		}

		return $Data;
	}

	public function ChangePostStatus($PostTable, $PostField, $PostID)
	{
		$this->DB->Query('UPDATE ?n
			                 SET ?n = IF(?n = ?i, ?i, ?i)
			               WHERE ?n = ?i', $PostTable, $PostField, $PostField, INACTIVE_STATUS_ID, ACTIVE_STATUS_ID, INACTIVE_STATUS_ID, $this->PostTables[$PostTable]['id'], $PostID);
		$this->Resp['StatusCode'] 	= 1;
		$this->Resp['StatusID'] 	= $this->DB->GetOne('SELECT ?n FROM ?n WHERE ?n = ?i', $PostField, $PostTable, $this->PostTables[$PostTable]['id'], $PostID);
		
		return $this->Response();
	}

	public function DeletePost($PostTable, $PostID)
	{
		return $this->DB->Query('UPDATE ?n SET status_id = ?i WHERE ?n = ?i', $PostTable, DELETED_STATUS_ID, $this->PostTables[$PostTable]['id'], $PostID);
	}

	/*Post end*/

	/*Files start*/

	private function UpdateProductFile($ID)
	{
		$HasFile 	= 0;
		$File 		= str_replace(URL, '', Request::Post('File'));

		if ($File != '' && file_exists($File)) {
			$HasFile = 1;
			if (strpos($File, 'tmp/') !== false && ! rename($File, str_replace(URL, '', UPL_PRODUCT_DOCS) . $ID . '.pdf')) {
				$HasFile = 0;
			}
		}
		return $this->DB->Query('UPDATE products SET has_file = ?i WHERE tour_id = ?i', $HasFile, $ID);
	}

	private function UpdateFiles($PostTable, $ID)
	{
		if (isset($this->PostTables[$PostTable]['photos_list'])) {
			return $this->UpdatePhotos($PostTable, $ID);
		}

		$HasFile 	= 0;
		$File 		= str_replace(URL, '', Request::Post('File'));

		if ($File != '' && file_exists($File)) {
			$HasFile = 1;
			if (strpos($File, 'tmp/') !== false && ! rename($File, str_replace(URL, '', $this->PostTables[$PostTable]['upl_dir']) . $ID . $this->PostTables[$PostTable]['file_type'])) {
				$HasFile = 0;
			}
		}
		return $this->DB->Query('UPDATE ?n
									SET ?n = ?i, ?n = ?n + 1
								  WHERE ?n = ?i',
								  		$PostTable, $this->PostTables[$PostTable]['fields']['has_file']['name'], $HasFile,
								  		$this->PostTables[$PostTable]['fields']['file_ver']['name'], $this->PostTables[$PostTable]['fields']['file_ver']['name'],
								  		$this->PostTables[$PostTable]['id'], $ID);
	}

	private function UpdatePhotos($PostTable, $ID)
	{
		$i = 0;
		$j = 0;
		if (Request::Post('Photos') != '') {
			$Photos 	= explode(',', Request::Post('Photos'));
			$PhotosDir 	= str_replace(URL, '', $this->PostTables[$PostTable]['upl_dir']);
			foreach ($Photos as $Val) {
				$Val = str_replace(URL, '', $Val);
				if (rename($Val, $PhotosDir . 'thumbs/' . $ID . '_' . ($i + 1) . '_' . $this->PostTables[$PostTable]['file_type']) &&
					rename(str_replace('thumbs', 'large', $Val), $PhotosDir . 'large/' . $ID . '_' . ($i + 1) . '_' . $this->PostTables[$PostTable]['file_type'])) {
					$i++;
				}
			}
			if ($i) {
				for ($k = 1; $k <= $i; $k++) {
					if (rename($PhotosDir . 'thumbs/' . $ID . '_' . $k . '_' . $this->PostTables[$PostTable]['file_type'],
							   $PhotosDir . 'thumbs/' . $ID . '_' . ($j + 1) . $this->PostTables[$PostTable]['file_type']) &&
						rename($PhotosDir . 'large/' . $ID . '_' . $k . '_' . $this->PostTables[$PostTable]['file_type'],
							   $PhotosDir . 'large/' . $ID . '_' . ($j + 1) . $this->PostTables[$PostTable]['file_type'])) {
						$j++;
					}
				}
			}
		}
		return $this->DB->Query('UPDATE ?n
									SET ?n = ?i, ?n = ?n + 1
								  WHERE ?n = ?i',
								  		$PostTable, $this->PostTables[$PostTable]['fields']['has_file']['name'], $j,
								  		$this->PostTables[$PostTable]['fields']['file_ver']['name'], $this->PostTables[$PostTable]['fields']['file_ver']['name'],
								  		$this->PostTables[$PostTable]['id'], $ID);
	}

	/*Files end*/
}