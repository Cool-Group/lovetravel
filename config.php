<?php
//constants

ini_set('allow_url_fopen', 1);
if (function_exists('gethostname')) {
	//echo gethostname(); exit;
	switch (gethostname()) {
		case "WIN-BGHLVJH5EUK":
		case "DESKTOP-OS5942M":
		case "Office-PC":
		case "Home-PC":
		case "DESKTOP-9L66BUG":	
		case "DESKTOP-9LSMT53":
			define('URL', 'http://localhost/lovetravel/');
			define('BRANCH', 'MASTER');
			//database
			define('DB_HOST', 'localhost');
			define('DB_NAME', 'lovetravel');
			define('DB_USER', 'root');
			define('DB_PASS', '');
		break;
		case "cpanel7.srv.co.ge":
			define('URL', 'http://lovetravel.ge/');
			define('BRANCH', 'PRODUCTION');
			//database
			define('DB_HOST', 'localhost');
			define('DB_NAME', 'eurotrad_lovetravel');
			define('DB_USER', 'eurotrad_trade');
			define('DB_PASS', 'dvUprIor3CBw');
		break;

		default: //'www.lovetravel.ge':
			define('URL', 'http://lovetravel.ge/');
			define('BRANCH', 'PRODUCTION');
			//database
			define('DB_HOST', 'localhost');
			define('DB_NAME', 'eurotrad_lovetravel');
			define('DB_USER', 'eurotrad_trade');
			define('DB_PASS', 'dvUprIor3CBw');
		break;
	}
} else {
	define('URL', 'http://lovetravel.ge/');
	define('BRANCH', 'PRODUCTION');
	//database
	define('DB_HOST', 'localhost');
	define('DB_NAME', 'eurotrad_lovetravel');
	define('DB_USER', 'eurotrad_trade');
	define('DB_PASS', 'dvUprIor3CBw');
	
}
define('SITE', 'lovetravel.ge');
define('PHONE', '032 00 00 00');
define('EMAIL', 'info@lovetravel.ge');
define('SITE_YEAR', 2017);
define('ROOT', dirname(__FILE__) . '/');
define('LIBS', ROOT . 'libs/');
define('LIBS_URL', URL . 'libs/');
define('THEME', URL . 'templates/');
define('LOGO', THEME . 'images/logo.png');
define('TPL', 'templates/');
define('UPL', URL . 'uploads/');
define('UPL_NEWS', UPL . 'news/');
define('UPL_BLOG', UPL . 'blog/');
define('UPL_TEAM', UPL . 'team/');
define('UPL_TOURS', UPL . 'tours/');
define('UPL_CATS', UPL . 'cats/');
define('UPL_SERVICES', UPL . 'services/');
define('UPL_SLIDER', UPL . 'slider/');
define('TMP', URL . 'tmp/');
define('NO_PHOTO', UPL_NEWS . '0.jpg');
define('CACHE', 'cache/');
define('NEWS_MAX_PHOTOS', 1);
define('BLOG_MAX_PHOTOS', 1);
define('SERVICES_MAX_PHOTOS', 1);
define('TOUR_MAX_PHOTOS', 12);
define('LANGS', serialize(array('ka' => 1, 'en' => 2, 'ru' => 3)));
define('LANG_TITLES', serialize(array('ka' => 'ქა', 'en' => 'Eng', 'ru' => 'Ру')));
define('LANGS_DIR', URL . 'langs/');
define('DEFAULT_LANG', 'ka');
define('DEFAULT_LANG_ID', 1);
define('SEC_KEY', '54af7585727ba9d2a01b48753571961d');
define('SEND_EMAILS_LIMIT', 5);
define('SEND_EMAIL_MAX_ATTEMPT', 3);
define('IMG_TYPES', serialize(array('image/png', 'image/jpg', 'image/jpeg', 'image/gif')));
define('IMG_TYPE', '.jpg');
define('DOC_TYPES', serialize(array('PDF' => 'application/pdf')));
define('LARI', '<i class="lari"></i>');
define('BLOG_THUMB_PHOTO_WIDTH', 360);//პატარა სურათის სიგანე
define('BLOG_THUMB_PHOTO_HEIGHT', 280);//პატარა სურათის სიმაღლე
define('BLOG_LARGE_PHOTO_WIDTH', 1024);//დიდი სურათის მაქსიმალური სიგანე
define('BLOG_LARGE_PHOTO_HEIGHT', 708);//პატარა სურათის მაქსიმალური სიმაღლე
define('NEWS_THUMB_PHOTO_WIDTH', 360);//პატარა სურათის სიგანე
define('NEWS_THUMB_PHOTO_HEIGHT', 280);//პატარა სურათის სიმაღლე
define('NEWS_LARGE_PHOTO_WIDTH', 1024);//დიდი სურათის მაქსიმალური სიგანე
define('NEWS_LARGE_PHOTO_HEIGHT', 708);//პატარა სურათის მაქსიმალური სიმაღლე
define('TOUR_THUMB_PHOTO_WIDTH', 360);//პატარა სურათის სიგანე
define('TOUR_THUMB_PHOTO_HEIGHT', 280);//პატარა სურათის სიმაღლე
define('TOUR_LARGE_PHOTO_WIDTH', 1024);//დიდი სურათის მაქსიმალური სიგანე
define('TOUR_LARGE_PHOTO_HEIGHT', 708);//პატარა სურათის მაქსიმალური სიმაღლე
define('TEAM_PHOTO_WIDTH', 131);//დიდი სურათის მაქსიმალური სიგანე
define('TEAM_PHOTO_HEIGHT', 221);//პატარა სურათის მაქსიმალური სიმაღლე
define('SERVICES_PHOTO_WIDTH', 460);//დიდი სურათის მაქსიმალური სიგანე
define('SERVICES_PHOTO_HEIGHT', 305);//პატარა სურათის მაქსიმალური სიმაღლე
define('CAT_PHOTO_SIZE', 150);//კატეგორიის სურათის ზომა
define('FB_APP_ID', '');
define('FB_ADMINS', '');
define('INACTIVE_STATUS_ID', 0);
define('ACTIVE_STATUS_ID', 1);
define('DATE_FORMAT', 'd/m/Y');

//cats
define('TOURS_IN_CAT_ID', 1);
define('TOURS_OUT_CAT_ID', 2);
define('MENU_MAX_CATS', 8);

//types
define('WINTER_TOURS_TYPE_ID', 7);
define('SEA_TOURS_TYPE_ID', 1);

//services
define('SERVICES_TYPE_ID', 0);
define('ADDITIONAL_SERVICS_TYPE_ID', 1);

define('NEWS_TYPE_ID', 1);
define('BLOG_TYPE_ID', 2);

define('DELETED_STATUS_ID', 2);
define('ADMIN_NEWS_NUM', 9);
define('MAIN_NEWS_NUM', 9);
define('GALLERY_TOURS_NUM', 14);

//Twig
define('TWIG_CACHE', false);
define('TWIG_CACHE_PATH', 'cache/twig/');
define('TWIG_TPL', ROOT . 'templates/');
define('TWIG_THEME', URL . 'templates/');

//files
define('FILE_VER', rand());

//Cookie
define('COOKIE_SITE_TIME', 30); //in days
define('COOKIE_SITE_DOMAIN', ''); //ex: localhost

//map
define('MAP_JS_API_KEY', '');

//social media
define('SOCIAL_MEDIA', serialize(array(1 => 'facebook', 2 => 'twitter', 3 => 'youtube', 4 => 'instagram'/*, 5 => 'vk'*/)));