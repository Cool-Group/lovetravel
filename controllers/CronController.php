<?php

class Cron extends Controller
{
	public function __construct($View = true)
	{
		parent::__construct($View);

		set_time_limit(0);
	}
	
	public function Index()
	{
		echo 'Index';
	}

	private function Sec()
	{
		if (Request::Req('SecKey') != SEC_KEY) {
			exit('Access denied!!!');
		}
	}

	public function Minutes1()
	{
		$this->SendEmails();

		echo 'Minute1 Cron';
	}

	public function Minutes5()
	{
		//
		
		echo 'Minutes5 Cron';
	}

	public function Hour1()
	{
		//
		
		echo 'Hour1 Cron';
	}

	public function Day1()
	{
		$this->TmpClear();

		echo 'Day1 Cron';
	}

	public function Test()
	{
		//
	}

	public function UpdateLangs()
	{
		$this->Model->UpdateLangs();
	}

	public function SendEmails()
	{
		$this->Model->SendEmails();
	}

	public function SendSMS()
	{
		//$this->Model->SendSMS();
	}

	public function TmpClear()
	{
		foreach (scandir(dirname(__FILE__) . '/../tmp/') as $Tmp) {
			if ($Tmp != '.' && $Tmp != '..') {
				if ((time() - filemtime(dirname(__FILE__) . '/../tmp/' . $Tmp)) > 1800) {
					// delete
					unlink(dirname(__FILE__) . '/../tmp/' . $Tmp);
				} 
			}
		}
	}

	public function MemcacheClear()
	{
		$this->Model->MC->flush();
	}
}