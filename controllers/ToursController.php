<?php

/**
* Tours Controller
*/
class Tours extends Controller
{
	function __construct()
	{
		parent::__construct();
	}

	public function Index($ID = 0)
	{
		$ID = (int)$ID;

    	if ($ID) {
    		
    		$Data = $this->Model->GetTourData($ID);

    		$this->View->Render('tours/view.tpl', array(
				'Cats'  	    => $this->Model->GetCats(),
	    		//'Slider'    	=> $this->Model->GetSlider(),
	            'Contacts'  	=> $this->Model->GetContacts(),
	    		'Services'      => $this->Model->GetServices(),
            	'BlogCats'      => $this->Model->GetBlogCats(),
				'Types' 		=> $this->Model->GetTypes(),
				'Notes' 		=> $this->Model->GetNotes(),
				'TourServices'  => $this->Model->GetTourServices(),
				'Data' 			=> $Data,
				'SimilarTours'  => $this->Model->GetSimilarTours($ID, $Data['cat_id']),
				//'GalleryTours'  => $this->Model->GetGalleryTours(),
				'Favorites'     => $this->Model->GetFavorites(),
			));
    	} else {

	    	$Data = $this->Model->GetTours();

		    $this->View->Render('tours/index.tpl', array(
				'Cats'  	    => $this->Model->GetCats(),
	    		//'Slider'    	=> $this->Model->GetSlider(),
	            'Contacts'  	=> $this->Model->GetContacts(),
	    		'Services'      => $this->Model->GetServices(),
            	'BlogCats'      => $this->Model->GetBlogCats(),
				'Types' 		=> $this->Model->GetTypes(),
				'Notes' 		=> $this->Model->GetNotes(),
				'TourServices'  => $this->Model->GetTourServices(),
				//'GalleryTours'  => $this->Model->GetGalleryTours(),
				'Favorites'     => $this->Model->GetFavorites(),
				'Data' 			=> $Data['Data'],
				'Pagination'	=> array(
					'CurrentPage' 	=> $Data['Page'],
					'PerPage'		=> ADMIN_NEWS_NUM,
					'ContentCount'	=> $Data['Cnt']
				)
			));
		}
	}

	public function Photos()
	{
        $this->View->Render('tours/photos.tpl', array(
        	'Data' 			=> $this->Model->GetTourPopupData(),
        	'Types' 		=> $this->Model->GetTypes(),
			'TourServices'  => $this->Model->GetTourServices(),
        ));
	}
}