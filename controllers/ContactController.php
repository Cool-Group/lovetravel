<?php

/**
* Contact Controller
*/
class Contact extends Controller
{
    function __construct()
    {
    	parent::__construct();
    }

    public function Index()
    {
            $this->View->Render('contact/index.tpl', array(
            	'Captcha'   => Captcha::Get(),
            	'Cats'      => $this->Model->GetCats(),
                'Contacts'  => $this->Model->GetContacts(),
                'Services'  => $this->Model->GetServices(),
                'BlogCats'  => $this->Model->GetBlogCats(),
                'Types'     => $this->Model->GetTypes(),
            ));
    }

    public function Send()
    {
    	Captcha::Check();

    	$Resp = array('StatusCode' => 0, 'StatusMessage' => Lang::Get('MessageSendError'));

        if (! Functions::IsEmail(Request::Post('Email'))) {
        	$Resp['StatusCode'] = 2;
        } elseif (Request::Post('Phone') == '') {
        	$Resp['StatusCode'] = 3;
        } elseif (Request::Post('Message') == '') {
        	$Resp['StatusCode'] = 4;
        } elseif ($this->Model->Send()) {
        	Captcha::_Unset();
        	$Resp = array('StatusCode' => 1, 'StatusMessage' => Lang::Get('MessageSendSuccess'));
        }

        $this->View->RenderJSON($Resp);
    }
}