<?php

/**
* User Controller
*/
class User extends Controller
{
	function __construct()
	{
		parent::__construct();

		if (Session::Get('UserID') === false && $this->IsLoginLogOutType() === false) {
			$this->LoginForm();
		}
	}

	public function Index()
	{
		$this->Account();
	}

	private function Params()
	{
		return array(
			'Cats'  	=> $this->Model->GetCats(),
			'Services'	=> $this->Model->GetServices(),
            'BlogCats'	=> $this->Model->GetBlogCats(),
            'Contacts'	=> $this->Model->GetContacts(),
            'Types'		=> $this->Model->GetTypes()
        );
	}

	/* News start */

	public function News()
	{
	    $Data = $this->Model->GetNews();

	    $this->View->Render('user/news/index.tpl', array(
			'NewsData' 		=> $Data['Data'],
			'Pagination'	=> array(
				'CurrentPage' 	=> $Data['Page'],
				'PerPage'		=> ADMIN_NEWS_NUM,
				'ContentCount'	=> $Data['Cnt']
			)
		));
	}

	public function AddNews()
	{
		$this->View->Render('user/news/add.tpl');
	}

	public function AddNewsPost()
	{
		$this->View->RenderJSON($this->Model->AddNewsPost());
	}

	public function EditNews($ID = 0)
	{
		$this->View->Render('user/news/edit.tpl', array('Data' => $this->Model->GetNewsData($ID)));
	}

	public function EditNewsPost()
	{
		$this->View->RenderJSON($this->Model->EditNewsPost());
	}

	/* News end */

	/* Services start */

	public function Services()
	{
	    $Data = $this->Model->GetServices();

	    $this->View->Render('user/services/index.tpl', array(
			'Data' 			=> $Data['Data'],
			'Pagination'	=> array(
				'CurrentPage' 	=> $Data['Page'],
				'PerPage'		=> ADMIN_NEWS_NUM,
				'ContentCount'	=> $Data['Cnt']
			)
		));
	}

	public function AddService()
	{
		$this->View->Render('user/services/add.tpl');
	}

	public function AddServicePost()
	{
		$this->View->RenderJSON($this->Model->AddServicePost());
	}

	public function EditService($ID = 0)
	{
		$this->View->Render('user/services/edit.tpl', array('Data' => $this->Model->GetPostData('services', $ID)));
	}

	public function EditServicePost()
	{
		$this->View->RenderJSON($this->Model->EditServicePost());
	}

	/* Services end */

	/* Slider start */

	public function Slider()
	{
	    $Data = $this->Model->GetSlider();

	    $this->View->Render('user/slider/index.tpl', array(
			'Data' 			=> $Data['Data'],
			'Pagination'	=> array(
				'CurrentPage' 	=> $Data['Page'],
				'PerPage'		=> ADMIN_NEWS_NUM,
				'ContentCount'	=> $Data['Cnt']
			)
		));
	}

	public function AddSlider()
	{
		$this->View->Render('user/slider/add.tpl');
	}

	public function AddSliderPost()
	{
		$this->View->RenderJSON($this->Model->AddSliderPost());
	}

	public function EditSlider($ID = 0)
	{
		$this->View->Render('user/slider/edit.tpl', array('Data' => $this->Model->GetPostData('slider', $ID)));
	}

	public function EditSliderPost()
	{
		$this->View->RenderJSON($this->Model->EditSliderPost());
	}

	/* Slider end */

	/* Team start */

	public function Team()
	{
	    $Data = $this->Model->GetTeam();

	    $this->View->Render('user/team/index.tpl', array(
			'Data' 			=> $Data['Data'],
			'Pagination'	=> array(
				'CurrentPage' 	=> $Data['Page'],
				'PerPage'		=> ADMIN_NEWS_NUM,
				'ContentCount'	=> $Data['Cnt']
			)
		));
	}

	public function AddTeam()
	{
		$this->View->Render('user/team/add.tpl');
	}

	public function AddTeamPost()
	{
		$this->View->RenderJSON($this->Model->AddTeamPost());
	}

	public function EditTeam($ID = 0)
	{
		$this->View->Render('user/team/edit.tpl', array('Data' => $this->Model->GetPostData('team', $ID)));
	}

	public function EditTeamPost()
	{
		$this->View->RenderJSON($this->Model->EditTeamPost());
	}

	/* Team end */
	
	/* Contact start */

	public function Contact()
	{
		$this->View->Render('user/contact/index.tpl', array('Data' => $this->Model->GetPostData('contact', 1)));
	}

	public function EditContact()
	{
		$this->View->RenderJSON($this->Model->EditContact());
	}

	/* Contact end */
	
	/* Tours start */

	public function Tours()
	{
	    $Data = $this->Model->GetTours();

	    $this->View->Render('user/tours/index.tpl', array(
			'Data' 			=> $Data['Data'],
			'Pagination'	=> array(
				'CurrentPage' 	=> $Data['Page'],
				'PerPage'		=> ADMIN_NEWS_NUM,
				'ContentCount'	=> $Data['Cnt']
			)
		));
	}

	public function AddTour()
	{
		$this->View->Render('user/tours/add.tpl', array(
			'Cats' 			=> $this->Model->GetCats()['Tree'],
			'Services' 		=> $this->Model->GetTourServices(),
			'Types' 		=> $this->Model->GetTypes(),
			'Notes'			=> $this->Model->GetNotes(),
			'Currencies' 	=> $this->Model->GetCurrencies(),
		));
	}

	public function AddTourPost()
	{
		$this->View->RenderJSON($this->Model->AddTourPost());
	}

	public function EditTour($ID = 0)
	{		
		$this->View->Render('user/tours/edit.tpl', array(
			'Data' 			=> $this->Model->GetPostData('tours', $ID),
			'Cats' 			=> $this->Model->GetCats()['Tree'],
			'Services' 		=> $this->Model->GetTourServices(),
			'Types' 		=> $this->Model->GetTypes(),
			'Notes'			=> $this->Model->GetNotes(),
			'Currencies' 	=> $this->Model->GetCurrencies(),
		));
	}

	public function EditTourPost()
	{
		$this->View->RenderJSON($this->Model->EditTourPost());
	}

	/* Tours end */

	/*Cats start*/

	public function Cats()
	{
		$this->View->Render('user/list/list.tpl', array(
			'list' 			=> $this->Model->GetCats(0)['Tree'],
			'list_params' 	=> array(
				'post_table'			=> 'cats',
				'list_title' 			=> 'კატეგორიები',
				'add_item_title' 		=> 'კატეგორიის დამატება',
				'edit_item_title' 		=> 'კატეგორიის რედაქტირება',
				'add_sub_item_title' 	=> 'ქვეკატეგორიის დამატება',
				'options'				=> array('status', 'sort', 'edit', 'delete')
			))
		);
	}

	/*Cats end*/

	/* Types start */

	public function Types()
	{
	    $this->View->Render('user/list/list.tpl', array(
			'list' 			=> $this->Model->GetTypes(0),
			'list_params' 	=> array(
				'post_table'		=> 'tour_types',
				'list_title' 		=> 'ტურის ტიპები',
				'add_item_title' 	=> 'ტურის ტიპის დამატება',
				'edit_item_title' 	=> 'ტურის ტიპის რედაქტირება',
				'options'			=> array('status', 'sort', 'edit', 'delete')
			))
		);
	}

	/*Types end*/

	/* Notes start */

	public function Notes()
	{
	    $this->View->Render('user/list/list.tpl', array(
			'list' 			=> $this->Model->GetNotes(0),
			'list_params' 	=> array(
				'post_table'		=> 'tour_notes',
				'list_title' 		=> 'ტურის შენიშვნები',
				'add_item_title' 	=> 'ტურის შენიშვნის დამატება',
				'edit_item_title' 	=> 'ტურის შენიშვნის რედაქტირება',
				'options'			=> array('status', 'sort', 'edit', 'delete')
			))
		);
	}

	/*Notes end*/

	/*Tour services start*/

	public function TourServices()
	{
		$this->View->Render('user/list/list.tpl', array(
			'list' 			=> $this->Model->GetTourServices(0),
			'list_params' 	=> array(
				'post_table'			=> 'tour_services',
				'list_title' 			=> 'ტურის სერვისები',
				'add_item_title' 		=> 'ტურის სერვისის დამატება',
				'edit_item_title' 		=> 'ტურის სერვისის რედაქტირება',
				'options'				=> array('status', 'sort', 'edit', 'delete')
			))
		);
	}

	/*Tour services end*/

	/* Langs start */

	public function Langs()
	{
	    $this->View->Render('user/list/list.tpl', array(
			'list' 			=> $this->Model->GetLangs(),
			'list_params' 	=> array(
				'post_table'		=> 'langs',
				'list_title' 		=> 'ენები',
				'add_item_title' 	=> 'ცვლადის დამატება',
				'edit_item_title' 	=> 'ცვლადის რედაქტირება',
				'options'			=> array('edit', 'delete')
			))
		);
	}

	/*Langs end*/

	/*List start*/

	public function NewListItem()
	{
		$this->View->Render('user/list/add.tpl', array('post_table' => $this->Model->PostTables[Request::Get('post_table')]));
	}

	public function ViewListItem()
	{
		$this->View->Render('user/list/edit.tpl', ['Data' => $this->Model->GetPostData(Request::Get('post_table'), Request::Get('id'))]);
	}

	public function AddListItem()
	{
		$Data = $this->Model->AddListItem();
		//print_r(array('id' => $Data['ID'], 'title' => $Data['Title'])); exit;
		if (is_array($Data) && $Data['StatusCode'] == 1) {
			$Data['item'] = $this->View->Render('user/list/sub_list.tpl', array(
				'list' 			=> array(0 => array('id' => $Data['ID'], 'title' => $Data['title'])),
				'list_params'	=> array(
					'post_table' 			=> Request::Post('PostTable'),
					'edit_item_title' 		=> Request::Post('EditItemTitle'),
					'add_sub_item_title' 	=> Request::Post('AddSubItemTitle'),
					'options'				=> $this->Model->PostTables[Request::Post('PostTable')]['options']
				)
			), true);
		}
		
		$this->View->RenderJSON($Data);
	}

	public function EditListItem()
	{
		$this->View->RenderJSON($this->Model->EditListItem());
	}

	public function DeleteListItem()
	{
		$this->View->RenderJSON($this->Model->DeleteListItem());
	}

	public function ChangeListItemSortOrder()
	{
		$this->View->RenderJSON($this->Model->ChangeListItemSortOrder());
	}

	public function ChangeListItemStatus()
	{
		$this->View->RenderJSON($this->Model->ChangeListItemStatus());
	}

	/*List end*/

	public function About()
	{
		$this->View->Render('user/about/index.tpl', array('Data' => $this->Model->GetAbout()));
	}

	public function EditAbout()
	{
		$this->View->RenderJSON($this->Model->EditAbout());
	}

	public function DeletePost($PostTable = '', $PostID = 0)
	{
		$this->View->RenderJSON($this->Model->DeletePost($PostTable, $PostID));
	}

	public function ChangePostStatus($PostTable = '', $PostField = '', $PostID = 0)
	{
		$this->View->RenderJSON($this->Model->ChangePostStatus($PostTable, $PostField, $PostID));
	}

	/*Guest start*/

	public function LoginForm()
	{
		header('Location: ' . $this->GetURI() . 'user/login/');
		exit;
	}

	public function IsLoginLogOutType()
	{
		$URL 		= Request::Get('url');
		$Lang 		= defined('LANGS') ? Lang::GetLang() . '/' : '';
		$Actions 	= array(
			'login', 'logout', 'authorize', 'register', 'add', 'recover', 'recoverpassword', 'emailactivation', 'resendactivationemail',
			'resendactemail', 'changepassword', 'changepass', 'newpassword', 'setnewpassword', 'getcaptcha'
		);

		foreach ($Actions as $Val) {
			if (strpos($URL, $Lang . 'user/' . $Val) === 0) {
				return true;
			}
		}
		return false;
	}

	public function LogIn()
	{
		$this->View->Render('user/quest/login.tpl', $this->Params());
	}

	public function Authorize()
	{
		$this->View->RenderJSON($this->Model->Authorize());
	}

	public function Recover()
	{
		$this->View->Render('user/quest/recover.tpl', array_merge($this->Params(), array(
			'Captcha' => Captcha::Get(),
		)));
	}

	public function RecoverPassword()
	{
		Captcha::Check();

		$this->View->RenderJSON($this->Model->RecoverPassword());
	}

	public function Register()
	{
		$this->View->Render('user/quest/register.tpl', array_merge($this->Params(), array(
			'Captcha' 	=> Captcha::Get(),
			'Countries' => $this->Model->GetCountries()
		)));
	}

	public function Add()
	{
		Captcha::Check();

		$this->View->RenderJSON($this->Model->Add());
	}

	public function ReSendActivationEmail()
	{
		$this->View->Render('user/quest/resend_activation_email.tpl', array_merge($this->Params(), array(
			'Captcha' 	=> Captcha::Get(),
		)));
	}

	public function ResendActEmail()
	{
		Captcha::Check();

		$this->View->RenderJSON($this->Model->ResendActEmail());
	}

	public function EmailActivation()
	{
		$this->View->Render('user/quest/email_activation.tpl', array_merge($this->Params(), array(
			'EmailActivation' => $this->Model->EmailActivation()
		)));
	}

	public function NewPassword()
	{
		$this->View->Render('user/quest/new_password.tpl', array_merge($this->Params(), array(
			'Captcha' 	=> Captcha::Get(),
			'EmailHash' => $this->Model->CheckEmailHash(1)
		)));
	}

	public function SetNewPassword()
	{
		Captcha::Check();

		$this->View->RenderJSON($this->Model->ChangePass(true));
	}

	/*Guest end*/

	/*Authorized start*/

	public function Account()
	{
		$this->View->Render('user/authorized/account.tpl', array_merge($this->Params(), array(
			'Countries' => $this->Model->GetCountries(),
			'Data' 		=> $this->Model->GetUserData(),
		)));
	}

	public function EditAccount()
	{
		$this->View->Render('user/authorized/edit_account.tpl', array_merge($this->Params(), array(
			'Countries' => $this->Model->GetCountries(),
			'Data' 		=> $this->Model->GetUserData(),
		)));
	}

	public function Edit()
	{
		$this->View->RenderJSON($this->Model->Edit());
	}

	public function ChangePassword()
	{
		$this->View->Render('user/authorized/change_password.tpl', $this->Params());
	}

	public function ChangePass()
	{
		$this->View->RenderJSON($this->Model->ChangePass());
	}

	public function Favorites()
	{
		$this->View->Render('user/authorized/favorites.tpl', array_merge($this->Params(), array(
			'TourServices'  => $this->Model->GetTourServices(),
			'Data' => $this->Model->GetFavorites()
		)));
	}

	public function LogOut()
	{
		Session::Destroy('UserID');

		$this->LoginForm();
	}

	public function AddToFavorites()
	{
		$this->View->RenderJSON($this->Model->AddToFavorites());
	}
	/*Authorized end*/
}