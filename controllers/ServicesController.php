<?php

/**
* Services Controller
*/
class Services extends Controller
{
	function __construct()
	{
		parent::__construct();
	}

	public function Index()
	{
    	$this->View->Render('services/index.tpl', array(
    		'Cats'      => $this->Model->GetCats(),
    		'Slider'    => $this->Model->GetSlider(),
            'Contacts'  => $this->Model->GetContacts(),
    		'Services'  => $this->Model->GetServices(true),
            'BlogCats'  => $this->Model->GetBlogCats(),
    		'Types'     => $this->Model->GetTypes(),
            //'GalleryTours'  => $this->Model->GetGalleryTours()
    	));
	}
}