<?php

/**
* News Controller
*/
class News extends Controller
{
	function __construct()
	{
		parent::__construct();
	}

	public function Index($NewsID = 0)
	{
        if (empty($NewsID)) {
        	$Data = $this->Model->GetNews();
		    $this->View->Render('news/index.tpl', array(
				'Data' 			=> $Data['Data'],
				'Cats'  	    => $this->Model->GetCats(),
				'Pagination'	=> array(
					'CurrentPage' 	=> $Data['Page'],
					'PerPage'		=> ADMIN_NEWS_NUM,
					'ContentCount'	=> $Data['Cnt']
				)
			));
        } else {
        	$this->View->Render('news/view.tpl', array(
        		'Data'	=> $this->Model->GetNews($NewsID),
        		'Cats'	=> $this->Model->GetCats(),
        	));
        }
	}
}