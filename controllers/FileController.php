<?php

/**
* File Controller
*/
class File extends Controller
{
	private $Resp;
	private $Path;
	
	function __construct()
	{
		parent::__construct();

		$this->Resp = array('StatusCode' => 0);
		$this->Path = str_replace(URL, ROOT, TMP);
	}

	public function UploadNewsPhoto()
	{	
		try  {
			if (in_array($_FILES['Photo']['type'], unserialize(IMG_TYPES))) {
				$Img   = md5($_SERVER['REMOTE_ADDR'] . time());
				$Image = new SimpleImage();
				$Image->load($_FILES['Photo']['tmp_name']);
				$Image->areafill(NEWS_PHOTO_WIDTH, NEWS_PHOTO_HEIGHT, '#FFF');
				$Image->save($this->Path . $Img . IMG_TYPE, 80);

				$this->Resp['FilesList'][] = TMP . $Img . IMG_TYPE;
			}
		} catch(Exception $e) {
			echo $e->getMessage();
		}

		$this->Response();
	}

	public function UploadPhoto()
	{	
		$PhotoWidth 	= CAT_PHOTO_SIZE;
		$PhotoHeight 	= CAT_PHOTO_SIZE;
		$ImgType        = IMG_TYPE;

		if (Request::Post('Type') == 'Team') {
			$PhotoWidth 	= TEAM_PHOTO_WIDTH;
			$PhotoHeight 	= TEAM_PHOTO_HEIGHT;
		} else if(Request::Post('Type') == 'Services') {
			$PhotoWidth 	= SERVICES_PHOTO_WIDTH;
			$PhotoHeight 	= SERVICES_PHOTO_HEIGHT;
		}

		if (Request::Post('Type') == 'List') {
			$ImgType = '.png';
		}
		try  {
			if (in_array($_FILES['Photo']['type'], unserialize(IMG_TYPES))) {
				$Img   = md5($_SERVER['REMOTE_ADDR'] . time());
				$Image = new SimpleImage();
				$Image->load($_FILES['Photo']['tmp_name']);
				if (! in_array(Request::Post('Type'), ['Slider', 'List', 'Services',])) {
					$Image->areafill($PhotoWidth, $PhotoHeight, '#FFF');
				}
				$Image->save($this->Path . $Img . $ImgType);

				$this->Resp['FilesList'][] = TMP . $Img . $ImgType;
			}
		} catch(Exception $e) {
			echo $e->getMessage();
		}

		$this->Response();
	}

	public function UploadAboutPhoto()
	{	
		try  {
			if (in_array($_FILES['Photo']['type'], unserialize(IMG_TYPES))) {
				$Img   = md5($_SERVER['REMOTE_ADDR'] . time());
				$Image = new SimpleImage();
				$Image->load($_FILES['Photo']['tmp_name']);
				$Image->areafill(ABOUT_PHOTO_SIZE, ABOUT_PHOTO_SIZE, '#FFF');
				$Image->save($this->Path . $Img . IMG_TYPE);

				$this->Resp['FilesList'][] = TMP . $Img . IMG_TYPE;
			}
		} catch(Exception $e) {
			//echo $e->getMessage();
		}

		$this->Response();
	}

	public function UploadPartnerLogo()
	{	
		try  {
			if (in_array($_FILES['Logo']['type'], unserialize(IMG_TYPES))) {
				$Img   = md5($_SERVER['REMOTE_ADDR'] . time());
				$Image = new SimpleImage();
				$Image->load($_FILES['Logo']['tmp_name']);
				$Image->areafill(PARTNER_LOGO_SIZE, PARTNER_LOGO_SIZE, '#FFF');
				$Image->save($this->Path . $Img . IMG_TYPE);

				$this->Resp['FilesList'][] = TMP . $Img . IMG_TYPE;
			}
		} catch(Exception $e) {
			//echo $e->getMessage();
		}

		$this->Response();
	}

	public function UploadNewsPhotos()
	{	
		$Num  = min(NEWS_MAX_PHOTOS - Request::Post('UploadedFiles'), count($_FILES['Files']['type']));
		
		for ($i = 0; $i < $Num; $i++) {
			$IMG_TYPES = unserialize(IMG_TYPES);
			try  {
				if (in_array($_FILES['Files']['type'][$i], $IMG_TYPES)) {
					$Img   = md5($_SERVER['REMOTE_ADDR'] . time() . $i);
					$Image = new SimpleImage();
					$Image->load($_FILES['Files']['tmp_name'][$i]);
					$Image->best_fit(NEWS_LARGE_PHOTO_WIDTH, NEWS_LARGE_PHOTO_HEIGHT);
					$Image->save($this->Path . $Img . '_large' . IMG_TYPE);
					$Image->load($_FILES['Files']['tmp_name'][$i]);
					$Image->areafill(NEWS_THUMB_PHOTO_WIDTH, NEWS_THUMB_PHOTO_HEIGHT, '#FFF');
					$Image->save($this->Path . $Img . '_thumbs' . IMG_TYPE, 80);

					$this->Resp['FilesList'][] = TMP . $Img . '_thumbs' . IMG_TYPE;
				}
			} catch(Exception $e) {
				//Some code here
			}
		}

		$this->Response();
	}

	public function UploadPhotos()
	{	
		$PostTables = [
			'tours' => [
				'thumb_width' 	=> TOUR_THUMB_PHOTO_WIDTH,
				'thumb_height' 	=> TOUR_THUMB_PHOTO_HEIGHT,
				'large_width' 	=> TOUR_LARGE_PHOTO_WIDTH,
				'large_height' 	=> TOUR_LARGE_PHOTO_HEIGHT,
				'max_photos'	=> TOUR_MAX_PHOTOS,
			],
			'news' => [
				'thumb_width' 	=> NEWS_THUMB_PHOTO_WIDTH,
				'thumb_height' 	=> NEWS_THUMB_PHOTO_HEIGHT,
				'large_width' 	=> NEWS_LARGE_PHOTO_WIDTH,
				'large_height' 	=> NEWS_LARGE_PHOTO_HEIGHT,
				'max_photos'	=> NEWS_MAX_PHOTOS,
			],
			'blog' => [
				'thumb_width' 	=> BLOG_THUMB_PHOTO_WIDTH,
				'thumb_height' 	=> BLOG_THUMB_PHOTO_HEIGHT,
				'large_width' 	=> BLOG_LARGE_PHOTO_WIDTH,
				'large_height' 	=> BLOG_LARGE_PHOTO_HEIGHT,
				'max_photos'	=> BLOG_MAX_PHOTOS,
			]
		];

		if (! isset($PostTables[Request::Post('PostTable')])) {
			$this->Response();
		}

		$Num  		= min($PostTables[Request::Post('PostTable')]['max_photos'] - Request::Post('UploadedFiles'), count($_FILES['Files']['type']));
		$IMG_TYPES 	= unserialize(IMG_TYPES);

		for ($i = 0; $i < $Num; $i++) {
			try  {
				if (in_array($_FILES['Files']['type'][$i], $IMG_TYPES)) {
					$Img   = md5($_SERVER['REMOTE_ADDR'] . time() . $i);
					
					$Image = new SimpleImage();
					$Image->load($_FILES['Files']['tmp_name'][$i]);
					$Image->auto_orient();
					$Image->best_fit($PostTables[Request::Post('PostTable')]['large_width'], $PostTables[Request::Post('PostTable')]['large_height']);
					$Image->save($this->Path . $Img . '_large' . IMG_TYPE);
					$Image->load($this->Path . $Img . '_large' . IMG_TYPE);

					switch ($Image->get_orientation()) {
						case 'portrait':
							$Image->best_fit($PostTables[Request::Post('PostTable')]['thumb_width'], $Image->get_height());
							break;

						case 'landscape':
							$Image->best_fit($Image->get_width(), $PostTables[Request::Post('PostTable')]['thumb_height']);
							break;

						default: //square
							$Image->best_fit($PostTables[Request::Post('PostTable')]['thumb_width'], $PostTables[Request::Post('PostTable')]['thumb_width']);
							break;	
					}
					//$Image->areafill(TOUR_THUMB_PHOTO_WIDTH, TOUR_THUMB_PHOTO_HEIGHT, '#FFF');
					$Image->save($this->Path . $Img . '_thumbs' . IMG_TYPE, 80);

					/*$Image->load($this->Path . $Img . '_large' . IMG_TYPE);
					$Image->overlay(LOGO, 'bottom right', '0.6', -20, -20);
					$Image->save($this->Path . $Img . '_large' . IMG_TYPE, 80);*/

					//$Image->load($_FILES['Files']['tmp_name'][$i]);
					

					$this->Resp['FilesList'][] = TMP . $Img . '_thumbs' . IMG_TYPE;
				}
			} catch(Exception $e) {
				//Some code here
				echo $e->getMessage();
			}
		}

		$this->Response();
	}

	public function UploadPdfFiles()
	{	
		$Num  = min(1 - Request::Post('UploadedFiles'), count($_FILES['Files']['type']));
		
		for ($i = 0; $i < $Num; $i++) {
			$PDF_TYPE = unserialize(DOC_TYPES);
			$PDF_TYPE = $PDF_TYPE['PDF'];
			try  {
				if ($_FILES['Files']['type'][$i] == $PDF_TYPE) {
					$File = md5($_SERVER['REMOTE_ADDR'] . time() . $i);
					if (move_uploaded_file($_FILES['Files']['tmp_name'][$i], $this->Path . $File . '.pdf')) {
						$this->Resp['FilesList'][] = TMP . $File . '.pdf';
					}
				}
			} catch(Exception $e) {
				//echo $e->getMessage();
			}
		}

		$this->Response();
	}

	private function Response()
	{
		if (isset($this->Resp['FilesList']) && count($this->Resp['FilesList'])) {
			$this->Resp['StatusCode'] = 1;			
		} else {
			$this->Resp['StatusMessage']  = 'ატვირთვის დროს მოხდა შეცდომა. სცადეთ თავიდან.';
		}

		$this->View->RenderJSON($this->Resp);
	}

	public function UploadShopLogo()
	{
		$Resp = array('StatusID' => 1, 'Logo' => '');
		$Path = str_replace(URL, ROOT, TMP);
		
		try
		{
			if(in_array($_FILES['Logo']['type'], unserialize(IMG_TYPES)))
			{
				$Img   = md5($_SERVER['REMOTE_ADDR'] . time());
				
				$Image = new SimpleImage();
				$Image->load($_FILES['Logo']['tmp_name']);
				$Image->best_fit(SHOP_LOGO_MAX_WIDTH, SHOP_LOGO_MAX_HEIGHT);

				$Image->areafill(($Width = $Image->get_width()) > SHOP_LOGO_MAX_HEIGHT ? $Width : SHOP_LOGO_MAX_HEIGHT, SHOP_LOGO_MAX_HEIGHT, '#FFF');
				$Image->save($Path . $Img . '_logo' . IMG_TYPE);

				$Resp['Logo'] = TMP . $Img . '_logo' . IMG_TYPE;
			}
		}
		catch(Exception $e)
		{
			echo $e->getMessage();
			//Some code here
		}

		if($Resp['Logo'] == '')
		{
			$Resp['StatusID'] = 0; 
			$Resp['Message']  = 'ლოგო ვერ აიტვირთა. სცადეთ თავიდან.'; 			
		}

		echo json_encode($Resp);
	}
}