<?php

/**
* About Controller
*/
class About extends Controller
{
	function __construct()
	{
		parent::__construct();
	}

	public function Index()
	{
        $this->View->Render('about/index.tpl', array(
        	'Cats'  	    => $this->Model->GetCats(),
    		'Slider'    	=> $this->Model->GetSlider(),
            'Contacts'  	=> $this->Model->GetContacts(),
    		'Data' 			=> $this->Model->GetAbout(),
    		'Team'          => $this->Model->GetTeam(),
    		'Services'      => $this->Model->GetServices(),
            'Types'         => $this->Model->GetTypes(),
            'GalleryTours'  => $this->Model->GetGalleryTours(),
        ));
	}
}