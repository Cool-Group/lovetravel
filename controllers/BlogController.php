<?php

/**
* Blog Controller
*/
class Blog extends Controller
{
	function __construct()
	{
		parent::__construct();
	}

	public function Index($ID = 0)
	{
        if (empty($ID)) {
        	$Data = $this->Model->GetBlog();
		    $this->View->Render('blog/index.tpl', array(
				'Data' 			=> $Data['Data'],
				'Cats'  	    => $this->Model->GetCats(),
				'Contacts'		=> $this->Model->GetContacts(),
				'Services'      => $this->Model->GetServices(),
				'BlogCats'      => $this->Model->GetBlogCats(ACTIVE_STATUS_ID, true),
				'Pagination'	=> array(
					'CurrentPage' 	=> $Data['Page'],
					'PerPage'		=> ADMIN_NEWS_NUM,
					'ContentCount'	=> $Data['Cnt']
				)
			));
        } else {
        	$this->View->Render('blog/view.tpl', array(
        		'Data'	=> $this->Model->GetBlog($ID),
        		'Cats' 	=> $this->Model->GetCats(),
        	));
        }
	}
}