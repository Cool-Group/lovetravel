<?php

/**
* Admin Controller
*/
class Admin extends Controller
{
	function __construct()
	{
		parent::__construct();

		if (Session::Get('AdminID') === false && $this->IsLoginLogOutType() === false) {
			$this->LoginForm();
		}
	}

	public function Index()
	{
		$this->Tours();
	}

	/* News start */

	public function News()
	{
	    $Data = $this->Model->GetNews();

	    $this->View->Render('admin/news/index.tpl', array(
			'NewsData' 		=> $Data['Data'],
			'Pagination'	=> array(
				'CurrentPage' 	=> $Data['Page'],
				'PerPage'		=> ADMIN_NEWS_NUM,
				'ContentCount'	=> $Data['Cnt']
			)
		));
	}

	public function AddNews()
	{
		$this->View->Render('admin/news/add.tpl');
	}

	public function AddNewsPost()
	{
		$this->View->RenderJSON($this->Model->AddNewsPost());
	}

	public function EditNews($ID = 0)
	{
		$this->View->Render('admin/news/edit.tpl', array('Data' => $this->Model->GetNewsData($ID)));
	}

	public function EditNewsPost()
	{
		$this->View->RenderJSON($this->Model->EditNewsPost());
	}

	/* News end */

	/* Blog start */

	public function Blog()
	{
	    $Data = $this->Model->GetBlog();

	    $this->View->Render('admin/blog/index.tpl', array(
			'NewsData' 		=> $Data['Data'],
			'Pagination'	=> array(
				'CurrentPage' 	=> $Data['Page'],
				'PerPage'		=> ADMIN_NEWS_NUM,
				'ContentCount'	=> $Data['Cnt']
			)
		));
	}

	public function AddBlog()
	{
		$this->View->Render('admin/blog/add.tpl', array(
			'Cats' => $this->Model->GetBlogCats(),
		));
	}

	public function AddBlogPost()
	{
		$this->View->RenderJSON($this->Model->AddBlogPost());
	}

	public function EditBlog($ID = 0)
	{
		$this->View->Render('admin/blog/edit.tpl', array(
			'Cats' => $this->Model->GetBlogCats(),
			'Data' => $this->Model->GetBlogData($ID)
		));
	}

	public function EditBlogPost()
	{
		$this->View->RenderJSON($this->Model->EditBlogPost());
	}

	/* Blog end */

	/* Services start */

	public function Services()
	{
	    $Data = $this->Model->_GetServices();

	    $this->View->Render('admin/services/index.tpl', array(
			'Data' 			=> $Data['Data'],
			'Pagination'	=> array(
				'CurrentPage' 	=> $Data['Page'],
				'PerPage'		=> ADMIN_NEWS_NUM,
				'ContentCount'	=> $Data['Cnt']
			)
		));
	}

	public function AddService()
	{
		$this->View->Render('admin/services/add.tpl');
	}

	public function AddServicePost()
	{
		$this->View->RenderJSON($this->Model->AddServicePost());
	}

	public function EditService($ID = 0)
	{
		$this->View->Render('admin/services/edit.tpl', array('Data' => $this->Model->GetPostData('services', $ID)));
	}

	public function EditServicePost()
	{
		$this->View->RenderJSON($this->Model->EditServicePost());
	}

	/* Services end */

	/* Slider start */

	public function Slider()
	{
	    $Data = $this->Model->GetSlider();

	    $this->View->Render('admin/slider/index.tpl', array(
			'Data' 			=> $Data['Data'],
			'Pagination'	=> array(
				'CurrentPage' 	=> $Data['Page'],
				'PerPage'		=> ADMIN_NEWS_NUM,
				'ContentCount'	=> $Data['Cnt']
			)
		));
	}

	public function AddSlider()
	{
		$this->View->Render('admin/slider/add.tpl');
	}

	public function AddSliderPost()
	{
		$this->View->RenderJSON($this->Model->AddSliderPost());
	}

	public function EditSlider($ID = 0)
	{
		$this->View->Render('admin/slider/edit.tpl', array('Data' => $this->Model->GetPostData('slider', $ID)));
	}

	public function EditSliderPost()
	{
		$this->View->RenderJSON($this->Model->EditSliderPost());
	}

	/* Slider end */

	/* Team start */

	public function Team()
	{
	    $Data = $this->Model->GetTeam();

	    $this->View->Render('admin/team/index.tpl', array(
			'Data' 			=> $Data['Data'],
			'Pagination'	=> array(
				'CurrentPage' 	=> $Data['Page'],
				'PerPage'		=> ADMIN_NEWS_NUM,
				'ContentCount'	=> $Data['Cnt']
			)
		));
	}

	public function AddTeam()
	{
		$this->View->Render('admin/team/add.tpl');
	}

	public function AddTeamPost()
	{
		$this->View->RenderJSON($this->Model->AddTeamPost());
	}

	public function EditTeam($ID = 0)
	{
		$this->View->Render('admin/team/edit.tpl', array('Data' => $this->Model->GetPostData('team', $ID)));
	}

	public function EditTeamPost()
	{
		$this->View->RenderJSON($this->Model->EditTeamPost());
	}

	/* Team end */
	
	/* Contact start */

	public function Contact()
	{
		$this->View->Render('admin/contact/index.tpl', array('Data' => $this->Model->GetPostData('contact', 1)));
	}

	public function EditContact()
	{
		$this->View->RenderJSON($this->Model->EditContact());
	}

	/* Contact end */
	
	/* Tours start */

	public function Tours()
	{
	    $Data = $this->Model->GetTours();
	    
	    $this->View->Render('admin/tours/index.tpl', array(
			'Data' 			=> $Data['Data'],
			'Pagination'	=> array(
				'CurrentPage' 	=> $Data['Page'],
				'PerPage'		=> ADMIN_NEWS_NUM,
				'ContentCount'	=> $Data['Cnt']
			)
		));
	}

	public function AddTour()
	{
		$this->View->Render('admin/tours/add.tpl', array(
			'Cats' 			=> $this->Model->GetCats()['Tree'],
			'Services' 		=> $this->Model->GetTourServices(),
			'Types' 		=> $this->Model->GetTypes(),
			'Notes'			=> $this->Model->GetNotes(),
			'Currencies' 	=> $this->Model->GetCurrencies(),
		));
	}

	public function AddTourPost()
	{
		$this->View->RenderJSON($this->Model->AddTourPost());
	}

	public function EditTour($ID = 0)
	{		
		$this->View->Render('admin/tours/edit.tpl', array(
			'Data' 			=> $this->Model->GetPostData('tours', $ID),
			'Cats' 			=> $this->Model->GetCats()['Tree'],
			'Services' 		=> $this->Model->GetTourServices(),
			'Types' 		=> $this->Model->GetTypes(),
			'Notes'			=> $this->Model->GetNotes(),
			'Currencies' 	=> $this->Model->GetCurrencies(),
		));
	}

	public function EditTourPost()
	{
		$this->View->RenderJSON($this->Model->EditTourPost());
	}

	/* Tours end */

	/*Cats start*/

	public function Cats()
	{
		$this->View->Render('admin/list/list.tpl', array(
			'list' 			=> $this->Model->GetCats(0)['Tree'],
			'list_params' 	=> array(
				'post_table'			=> 'cats',
				'list_title' 			=> 'მიმართულებები',
				'add_item_title' 		=> 'მიმართულების დამატება',
				'edit_item_title' 		=> 'მიმართულების რედაქტირება',
				'add_sub_item_title' 	=> 'ქვემიმართულების დამატება',
				'options'				=> array('status', 'sort', 'edit', 'delete')
			))
		);
	}

	/*Cats end*/

	/*Countries start*/

	public function Countries()
	{
		$this->View->Render('admin/list/list.tpl', array(
			'list' 			=> $this->Model->GetCountries(0),
			'list_params' 	=> array(
				'post_table'			=> 'countries',
				'list_title' 			=> 'ქვეყნები',
				'add_item_title' 		=> 'ქვეყნის დამატება',
				'edit_item_title' 		=> 'ქვეყნის რედაქტირება',
				'options'				=> array('status', 'sort', 'edit', 'delete')
			))
		);
	}

	/*Countries end*/

	/*Cats start*/

	public function BlogCats()
	{
		$this->View->Render('admin/list/list.tpl', array(
			'list' 			=> $this->Model->GetBlogCats(0),
			'list_params' 	=> array(
				'post_table'			=> 'blog_cats',
				'list_title' 			=> 'ბლოგის კატეგორიები',
				'add_item_title' 		=> 'ბლოგის კატეგორიის დამატება',
				'edit_item_title' 		=> 'ბლოგის კატეგორიის რედაქტირება',
				'options'				=> array('status', 'sort', 'edit', 'delete')
			))
		);
	}

	/*Cats end*/

	/* Types start */

	public function Types()
	{
	    $this->View->Render('admin/list/list.tpl', array(
			'list' 			=> $this->Model->GetTypes(0),
			'list_params' 	=> array(
				'post_table'		=> 'tour_types',
				'list_title' 		=> 'კატეგორიები',
				'add_item_title' 	=> 'კატეგორიის დამატება',
				'edit_item_title' 	=> 'კატეგორიის რედაქტირება',
				'options'			=> array('status', 'sort', 'edit', 'delete')
			))
		);
	}

	/*Types end*/

	/* Notes start */

	public function Notes()
	{
	    $this->View->Render('admin/list/list.tpl', array(
			'list' 			=> $this->Model->GetNotes(0),
			'list_params' 	=> array(
				'post_table'		=> 'tour_notes',
				'list_title' 		=> 'ტურის შენიშვნები',
				'add_item_title' 	=> 'ტურის შენიშვნის დამატება',
				'edit_item_title' 	=> 'ტურის შენიშვნის რედაქტირება',
				'options'			=> array('status', 'sort', 'edit', 'delete')
			))
		);
	}

	/*Notes end*/

	/*Tour services start*/

	public function TourServices()
	{
		$this->View->Render('admin/list/list.tpl', array(
			'list' 			=> $this->Model->GetTourServices(0),
			'list_params' 	=> array(
				'post_table'			=> 'tour_services',
				'list_title' 			=> 'ტურის სერვისები',
				'add_item_title' 		=> 'ტურის სერვისის დამატება',
				'edit_item_title' 		=> 'ტურის სერვისის რედაქტირება',
				'options'				=> array('status', 'sort', 'edit', 'delete')
			))
		);
	}

	/*Tour services end*/

	/* Langs start */

	public function Langs()
	{
	    $this->View->Render('admin/list/list.tpl', array(
			'list' 			=> $this->Model->GetLangs(),
			'list_params' 	=> array(
				'post_table'		=> 'langs',
				'list_title' 		=> 'ენები',
				'add_item_title' 	=> 'ცვლადის დამატება',
				'edit_item_title' 	=> 'ცვლადის რედაქტირება',
				'options'			=> array('edit', 'delete')
			))
		);
	}

	/*Langs end*/

	/*List start*/

	public function NewListItem()
	{
		$this->View->Render('admin/list/add.tpl', array('post_table' => $this->Model->PostTables[Request::Get('post_table')]));
	}

	public function ViewListItem()
	{
		$this->View->Render('admin/list/edit.tpl', [
			'Data' 			=> $this->Model->GetPostData(Request::Get('post_table'), Request::Get('id')),
			/*'post_table'	=> $this->Model->PostTables[Request::Post('PostTable')]*/
		]);
	}

	public function AddListItem()
	{
		$Data = $this->Model->AddListItem();
		//print_r(array('id' => $Data['ID'], 'title' => $Data['Title'])); exit;
		if (is_array($Data) && $Data['StatusCode'] == 1) {
			$Data['item'] = $this->View->Render('admin/list/sub_list.tpl', array(
				'list' 			=> array(0 => array('id' => $Data['ID'], 'title' => $Data['title'])),
				'list_params'	=> array(
					'post_table' 			=> Request::Post('PostTable'),
					'edit_item_title' 		=> Request::Post('EditItemTitle'),
					'add_sub_item_title' 	=> Request::Post('AddSubItemTitle'),
					'options'				=> $this->Model->PostTables[Request::Post('PostTable')]['options']
				)
			), true);
		}
		
		$this->View->RenderJSON($Data);
	}

	public function EditListItem()
	{
		$this->View->RenderJSON($this->Model->EditListItem());
	}

	public function DeleteListItem()
	{
		$this->View->RenderJSON($this->Model->DeleteListItem());
	}

	public function ChangeListItemSortOrder()
	{
		$this->View->RenderJSON($this->Model->ChangeListItemSortOrder());
	}

	public function ChangeListItemStatus()
	{
		$this->View->RenderJSON($this->Model->ChangeListItemStatus());
	}

	/*List end*/

	public function About()
	{
		$this->View->Render('admin/about/index.tpl', array('Data' => $this->Model->GetAbout()));
	}

	public function EditAbout()
	{
		$this->View->RenderJSON($this->Model->EditAbout());
	}

	public function DeletePost($PostTable = '', $PostID = 0)
	{
		$this->View->RenderJSON($this->Model->DeletePost($PostTable, $PostID));
	}

	public function ChangePostStatus($PostTable = '', $PostField = '', $PostID = 0)
	{
		$this->View->RenderJSON($this->Model->ChangePostStatus($PostTable, $PostField, $PostID));
	}

	public function LoginForm()
	{
		header('Location: ' . $this->GetURI() . 'admin/login/');
		exit;
	}

	public function LogIn()
	{
		$this->View->Render('admin/login.tpl');
	}

	public function IsLoginLogOutType()
	{
		$URL 	= Request::Get('url');
		$Lang 	= defined('LANGS') ? Lang::GetLang() . '/' : '';

		foreach (array('login', 'logout', 'authorize') as $Val) {
			if (strpos($URL, $Lang . 'admin/' . $Val) === 0) {
				return true;
			}
		}
		return false;
	}

	public function Authorize()
	{
		$this->View->RenderJSON($this->Model->Authorize());
	}

	public function LogOut()
	{
		Session::Destroy('AdminID');

		$this->LoginForm();
	}
}