<?php

/**
* Main Controller
*/
class Main extends Controller
{
    function __construct()
    {
    	parent::__construct();
    }

    public function Index()
    {
        $this->View->Render('main/index.tpl', array(
            'Cats'          => $this->Model->GetCats(),
            'Slider'        => $this->Model->GetSlider(),
            'MainTours'     => $this->Model->GetMainTours(),
            //'GalleryTours'  => $this->Model->GetGalleryTours(),
            //'About'         => $this->Model->GetAbout(),
            //'Team'          => $this->Model->GetTeam(),
            'TypedTours'    => array(WINTER_TOURS_TYPE_ID => $this->Model->GetTypedTours(WINTER_TOURS_TYPE_ID), SEA_TOURS_TYPE_ID => $this->Model->GetTypedTours(SEA_TOURS_TYPE_ID)),
            'Contacts'      => $this->Model->GetContacts(),
            'Services'      => $this->Model->GetServices(),
            'BlogCats'      => $this->Model->GetBlogCats(),
            'Types'         => $this->Model->GetTypes(),
            'TourServices'  => $this->Model->GetTourServices(),
            'Favorites'     => $this->Model->GetFavorites(),
        ));
    }
}