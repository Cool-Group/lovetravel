{Header}
<div class="width">
	<div class="content">
		<div class="error-block">
		</div>
		<p class="main-link">
			<a href="{URL}{LANG}/main/" class="b-16 hover">
				<i class="icon-16 arrow-left-1-16"></i>
				<span>{LGoToMainPage}</span>
			</a>
		</p>
	</div>
</div>
{Footer}