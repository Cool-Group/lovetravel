<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>{SITE}</title>
{HeaderInc}
<meta name="author" content="{SITE}" >
<link href="{URL}favicon.ico" rel="shortcut icon" />
<meta name="copyright" content="©{SITE_YEAR} {SITE}" >
<meta name="robots" content="all" />
<meta name="revisit-after" content="1 days" >
<link href="{TWIG_THEME}css/fonts.css" rel="stylesheet">
<!--[if lt IE 7]>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE7.js"></script>
<![endif]-->
<!--[if lt IE 8]>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
<![endif]-->
<!--[if lt IE 9]>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
<![endif]-->
<link href="{TWIG_THEME}css/default.css?v={FILE_VER}" rel="stylesheet">
<link href="{TWIG_THEME}css/style.css?v={FILE_VER}" rel="stylesheet">
<link href="{TWIG_THEME}error/css/style.css?v={FILE_VER}" rel="stylesheet">
</head>
<body>
<div class="header-block-1">
    <a href="{URL}" class="logo" tabindex="1"></a>
</div>