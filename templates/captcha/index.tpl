<div class="form-group row">
	<p class="captcha-block left">
		<span class="captcha relative" style="background-image:url({{ Captcha.Image }});">
			<i class="update-captcha" title="{{ langs.UpdateCaptcha }}" onclick="captcha.update(this, '{{ server.URI ~ server.Page }}/getcaptcha/');"></i>
		</span>
		<span class="captcha-text">
			<input type="hidden" name="CaptchaKey" class="CaptchaKey" value="{{ Captcha.Key }}">
			<input type="text" id="CaptchaVal" name="CaptchaVal" class="CaptchaVal form-control" maxlength="{{ Captcha.Val|length }}" placeholder="{{ langs.Code }}"
			 data-lengths="[{{ Captcha.Val|length }}]" data-error="{{ langs.WriteCaptcha }}">
		</span>
	</p>
</div>