var map = {
    object: null,
    marker: null,
    center: {},
    zoom: null,
    coords: {
        lat: 41.71905551584262,
        lng: 44.82078552246094,
    },
    zoomFar: 10,
    zoomClose: 17,
    fullScreen: false,
    fields: {
        coords: 'MapCoords',
    },
    tags: {
        mapElement: 'map',
        toggleMapFullScreen: '.map-cont > input',
        mapCont: '.map-cont',
    },
    data: {
        //
    },
    classes: {
        active: 'active',
    },
    setPosition: function() {
        this.object.panTo(this.center);
        this.marker = this.marker || new google.maps.Marker({ map: this.object, draggable: true });
        this.marker.setPosition(this.center);
        document.getElementById(this.fields.coords).value = this.center.lat + ',' + this.center.lng;
    },
    set: function(marker) {
        this.object = new google.maps.Map(document.getElementById(this.tags.mapElement), { zoom: this.zoom });
        marker ? this.setPosition() : this.object.setCenter(this.center);
        var map = this;
        this.object.addListener('click', function(e) {
            map.center = { lat: e.latLng.lat(), lng: e.latLng.lng() }
            map.setPosition();
        });
    },
    resize: function(center) {
        google.maps.event.trigger(this.object, 'resize');
        this.object.setCenter(center);
    },
    validate: function(arg, mode) {
        var range = mode == 'lat' ? [-85, 85] : [-180, 180];
        if (!parseFloat(arg) || parseFloat(arg) < range[0] || parseFloat(arg) > range[1]) {
            return false;
        }
        return true;
    },
    toggleFullScreen: function() {
        this.fullScreen = !this.fullScreen;
        if (this.fullScreen == true) {
            $(this.tags.mapCont).addClass(this.classes.active);
        } else {
            $(this.tags.mapCont).removeClass(this.classes.active);
        }
        this.resize(this.object.getCenter());
    },
    initMap: function() {
        var that = this;
        var coords = document.getElementById(this.fields.coords).value.split(',');
        var marker = (this.validate(coords[0], 'lat') && this.validate(coords[1], 'lng'));
        $.extend(this, {
            center: {
                lat: marker ? parseFloat(coords[0]) : this.coords.lat,
                lng: marker ? parseFloat(coords[1]) : this.coords.lng,
            },
            zoom: marker ? this.zoomClose : this.zoomFar
        });
        this.set(marker);
    },
    init: function() {
        var that = this;
        this.initMap();
        $(that.tags.toggleMapFullScreen).click(function() {
            that.toggleFullScreen();
            this.value = that.fullScreen ? 'რუკის დაპატარავება' : 'რუკის გადიდება';
        });
    },
    initLabel: function() {
        if (typeof MapLabel == "undefined") {
            MapLabel = function(opt_options) {
                // Initialization
                this.setValues(opt_options);

                // MapLabel specific
                var span = this.span_ = document.createElement('span');
                span.style.cssText = 'position: relative; left: 3px; top: -8px; ' +
                    'padding: 2px; color:#98312f; font-weight:bold; ';

                var div = this.div_ = document.createElement('div');
                div.appendChild(span);
                div.style.cssText = 'position: absolute; display: none';
            };
            MapLabel.prototype = new google.maps.OverlayView;

            // Implement onAdd
            MapLabel.prototype.onAdd = function() {
                var pane = this.getPanes().overlayLayer;
                pane.appendChild(this.div_);
            };

            // Implement draw
            MapLabel.prototype.draw = function() {
                var projection = this.getProjection();
                var position = projection.fromLatLngToDivPixel(this.get('position'));

                var div = this.div_;
                div.style.left = position.x + 'px';
                div.style.top = position.y + 'px';
                div.style.display = 'block';

                this.span_.innerHTML = this.get('text').toString();
            };
        }
    },
    show: function(elementID, coords, title) {
        this.initLabel();
        try {
            Coords = JSON.parse('[' + coords + ']');
        } catch (e) {
            return;
        }
        var latlng = { lat: parseFloat(Coords[0]), lng: parseFloat(Coords[1]) }
        var map = new google.maps.Map(document.getElementById(elementID), {
            center: latlng,
            zoom: 17
        });
        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            text: title
        });
        var label = new MapLabel({
            map: map
        });

        label.bindTo('position', marker, 'position');
        label.bindTo('text', marker, 'text');
    }
}