var contact = {
    fields: {
        email: '#Email',
        phone: '#Phone',
        message: '#Message'
    },
    send: function(obj) {
        _form.checkFields(obj, function() {
            jAlert(_form.response.StatusMessage, null, function() {
                if (_form.response.StatusCode == 1) {
                    if (PAGE == 'contact') {
                        location.reload();
                    } else {
                        closePopup();
                    }
                } else if (_form.response.StatusCode == 2) {
                    $(obj).find(contact.fields.email).focus();
                } else if (_form.response.StatusCode == 2) {
                    $(obj).find(contact.fields.phone).focus();
                } else if (_form.response.StatusCode == 3) {
                    $(obj).find(contact.fields.message).focus();
                }
            });
        });
        return false;
    }
}