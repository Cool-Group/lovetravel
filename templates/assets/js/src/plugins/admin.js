$(document).ready(function() {
    //<![CDATA[
    $.each($('textarea[data-ckeditor=true]'), function(key, value) {
        CKEDITOR.replace($(this).attr('name'), {
            fullPage: true,
            language: 'ka',
            height: '400px',
        });
    });
    //]]>
});
var admin = {
    fields: {
        email: '#Email',
        password: '#Password',
        phone: '#Phone',
        message: '#Message'
    },
    logIn: function(obj) {
        _form.checkFields(obj, function() {
            if (_form.response.StatusCode == 1) {
                location.href = $(obj).data('success-url');
            } else if (_form.response.StatusCode == 2) {
                $(obj).find(contact.fields.email).focus();
            } else if (_form.response.StatusCode == 3) {
                $(obj).find(contact.fields.password).focus();
            } else {
                jAlert(_form.response.StatusMessage);
            }
        });
        return false;
    },
    savePost: function(obj, noHistoryBack) {
        _form.checkFields(obj, function() {
            if (_form.response.StatusCode == 1) {
                if (noHistoryBack) {
                    jAlert(_form.response.StatusMessage, null, function() {
                        $('body, html').animate({ scrollTop: 0 }, 500);
                    });
                } else {
                    history.back();
                }
            } else {
                jAlert(_form.response.StatusMessage);
            }
        });
        return false;
    },
    changePostStatus: function(obj, postTable, postField, postID) {
        $(obj).parents('li').addLoader();
        $.get(URI + 'admin/changepoststatus/' + postTable + '/' + postField + '/' + postID + '/', null, function(data) {
            data = _form.parseJSON(data);
            $(obj).children('i').removeClass('green red').addClass(data.StatusCode == 1 && data.Data.StatusID == 1 ? 'green' : 'red');
            $(obj).parents('li').removeLoader();
        });
    },
    deletePost: function(obj, postTable, postID) {
        jConfirm('გსურთ წაშლა?', null, function(e) {
            if (e) {
                $(obj).parents('li').remove();
                $.get(URI + 'admin/deletepost/' + postTable + '/' + postID + '/');
            }
        });
    },
    saveContent: function(obj) {
        _form.checkFields(obj);
        return false;
    }
};
var list = {
    new: function(postTable, parentID, popupTitle, editItemTitle, addSubItemTitle) {
        popup(URI + 'admin/newlistitem/', {
            post_table: postTable,
            parent_id: parentID,
            popup_title: popupTitle,
            edit_item_title: editItemTitle,
            add_sub_item_title: addSubItemTitle
        });
    },
    view: function(postTable, id, popupTitle) {
        popup(URI + 'admin/viewlistitem/', { post_table: postTable, id: id, popup_title: popupTitle })
    },
    add: function(obj) {
        _form.checkFields(obj, function() {
            if (_form.response.StatusCode == 1) {
                $('li[data-id="' + _form.response.Data.parent_id + '"]').addClass('sub');
                $('li[data-id="' + _form.response.Data.parent_id + '"] > div + ul').append(_form.response.Data.item);
                closePopup();
            } else {
                jAlert(_form.response.StatusMessage);
            }
        });
        return false;
    },
    edit: function(obj) {
        _form.checkFields(obj, function() {
            $('li[data-id="' + _form.response.Data.ID + '"] > div > span.title').text(_form.response.Data.title);
            closePopup();
        });
        return false;
    },
    delete: function(obj, postTable, id) {
        console.log({ PostTable: postTable, ID: id });
        jConfirm('გსურთ წაშლა?', null, function(e) {
            if (e) {
                $(obj).parent().addLoader();
                $.post(URI + 'admin/deletelistitem/', { PostTable: postTable, ID: id }, function(data) {
                    $(obj).parent().removeLoader();
                    if ((data = _form.parseJSON(data)) == false) {
                        return false;
                    }
                    if (data.StatusCode == 1) {
                        var li = $(obj).parents('ul').first().parents('li').first();
                        $(obj).parents('li').first().remove();
                        if (li.length && !li.find('li').length) {
                            li.removeClass('sub');
                        }
                    } else {
                        jAlert(data.StatusMessage);
                    }
                });
            }
        });
    },
    changeStatus: function(obj, postTable, id) {
        $(obj).parent().addLoader();
        $.post(URI + 'admin/changelistitemstatus/', { PostTable: postTable, ID: id }, function(data) {
            $(obj).parent().removeLoader();
            if ((data = _form.parseJSON(data)) == false) {
                return false;
            }
            if (data.StatusCode == 1) {
                $(obj).find('i').removeClass('green red').addClass(data.Data.ItemStatusID ? 'green' : 'red');
            } else {
                alert('შეცდომა');
            }
        });
    },
    changeSortOrder: function(obj, postTable, id, sortID) {
        if ($(obj).parents('ul').first().parents('li').first().find('ul:first > li').length <= 1 || (sortID == 0 && !$(obj).parents('li').first().prev().length) ||
            (sortID == 1 && !$(obj).parents('li').first().next().length)) {
            return false;
        }
        $(obj).parent().addLoader();
        $.post(URI + 'admin/changelistitemsortorder/', { PostTable: postTable, ID: id, SortID: sortID }, function(data) {
            $(obj).parent().removeLoader();
            if ((data = _form.parseJSON(data)) == false) {
                return false;
            }
            if (data.StatusCode == 1) {
                li = $(obj).parents('li').first();
                if (sortID) {
                    li.next().after(li.clone()[0].outerHTML);
                } else {
                    li.prev().before(li.clone()[0].outerHTML);
                }
                li.remove();
                $('li[data-id="' + id + '"] .options').removeLoader();
            } else {
                alert('შეცდომა');
            }
        });
    }
};