$(document).ready(function() {
    //<![CDATA[
    $.each($('textarea[data-ckeditor=true]'), function(key, value) {
        CKEDITOR.replace($(this).attr('name'), {
            fullPage: true,
            language: 'ka',
            height: '400px',
        });
    });
    //]]>
});
var user = {
    fields: {
        email: '#Email',
        password: '#Password',
        phone: '#Phone',
        message: '#Message',
        currentPassword: '#CurrentPassword',
    },
    logIn: function(obj) {
        _form.checkFields(obj, function() {
            if (_form.response.StatusCode == 1) {
                location.href = $(obj).data('success-url');
            } else if (_form.response.StatusCode == 4) {
                jDialog(_form.response.StatusMessage, {
                    ok: {
                        text: langs.Close,
                        class: 'btn-default',
                        click: function() {
                            $.alerts._hide();
                        }
                    },
                    other_email: {
                        text: langs.ResendLink,
                        class: 'btn-primary',
                        click: function(){
                            $.alerts._hide();
                            location.href = $(obj).data('resendactivationemail-url') + '?Email=' + $(obj).find(user.fields.email).val();
                        }
                    }
                });
            } else {
                jAlert(_form.response.StatusMessage, null, function() {
                    if (_form.response.StatusCode == 2) {
                        $(obj).find(user.fields.email).focus();
                    } else if (_form.response.StatusCode == 3) {
                        $(obj).find(user.fields.password).focus();
                    }
                });
            }
        });
        return false;
    },
    edit: function(obj){
        _form.checkFields(obj, function() {
            jAlert(_form.response.StatusMessage, null, function(){
                if (_form.response.StatusCode == 2) {
                    $(user.fields.currentPassword).focus();
                }
            });
        });
        return false;
    },
    changePassword: function(obj, newPassword) {
        _form.checkFields(obj, function() {
            jAlert(_form.response.StatusMessage, null, function(){
                if (_form.response.StatusCode == 1) {
                    if (newPassword) {
                        location.href = $(obj).data('success-url');
                    } else {
                        $(obj)[0].reset();
                    }
                } else if(_form.response.StatusCode == 2) {
                    $(user.fields.currentPassword).focus();
                }
            });
        });
        return false;
    },
    recover: function(obj) {
        _form.checkFields(obj, function() {
            jAlert(_form.response.StatusMessage, null, function() {
                if (_form.response.StatusCode == 1) {
                    location.href = $(obj).data('success-url');
                } else if (_form.response.StatusCode == 2) {
                    $(obj).find(user.fields.email).focus();
                }
            });
        });
        return false;
    },
    register: function(obj) {
        _form.checkFields(obj, function(){
            jAlert(_form.response.StatusMessage, null, function(){
                if (_form.response.StatusCode == 1) {
                    $(obj)[0].reset();
                    location.href = $(obj).data('success-url');
                } else if (_form.response.StatusCode == 2) {
                    $(user.tags.email).focus();
                }
            });
        });
        return false;
    },
    resendActEmail: function(obj) {
        _form.checkFields(obj, function() {
            jAlert(_form.response.StatusMessage, null, function() {
                if (_form.response.StatusCode == 1) {
                    location.href = $(obj).data('success-url');
                } else if (_form.response.StatusCode == 2) {
                    $(obj).find(user.fields.email).focus();
                }
            });
        });
        return false;
    },
    savePost: function(obj, noHistoryBack) {
        _form.checkFields(obj, function() {
            if (_form.response.StatusCode == 1) {
                if (noHistoryBack) {
                    jAlert(_form.response.StatusMessage, null, function() {
                        $('body, html').animate({ scrollTop: 0 }, 500);
                    });
                } else {
                    history.back();
                }
            } else {
                jAlert(_form.response.StatusMessage);
            }
        });
        return false;
    },
    changePostStatus: function(obj, postTable, postField, postID) {
        $(obj).parents('li').addLoader();
        $.get(URI + 'user/changepoststatus/' + postTable + '/' + postField + '/' + postID + '/', null, function(data) {
            data = _form.parseJSON(data);
            $(obj).children('i').removeClass('green red').addClass(data.StatusCode == 1 && data.Data.StatusID == 1 ? 'green' : 'red');
            $(obj).parents('li').removeLoader();
        });
    },
    deletePost: function(obj, postTable, postID) {
        jConfirm('გსურთ წაშლა?', null, function(e) {
            if (e) {
                $(obj).parents('li').remove();
                $.get(URI + 'user/deletepost/' + postTable + '/' + postID + '/');
            }
        });
    },
    saveContent: function(obj) {
        _form.checkFields(obj);
        return false;
    }
};
var list = {
    new: function(postTable, parentID, popupTitle, editItemTitle, addSubItemTitle) {
        popup(URI + 'user/newlistitem/', {
            post_table: postTable,
            parent_id: parentID,
            popup_title: popupTitle,
            edit_item_title: editItemTitle,
            add_sub_item_title: addSubItemTitle
        });
    },
    view: function(postTable, id, popupTitle) {
        popup(URI + 'user/viewlistitem/', { post_table: postTable, id: id, popup_title: popupTitle })
    },
    add: function(obj) {
        _form.checkFields(obj, function() {
            if (_form.response.StatusCode == 1) {
                $('li[data-id="' + _form.response.Data.parent_id + '"]').addClass('sub');
                $('li[data-id="' + _form.response.Data.parent_id + '"] > div + ul').append(_form.response.Data.item);
                closePopup();
            } else {
                jAlert(_form.response.StatusMessage);
            }
        });
        return false;
    },
    edit: function(obj) {
        _form.checkFields(obj, function() {
            $('li[data-id="' + _form.response.Data.ID + '"] > div > span.title').text(_form.response.Data.title);
            closePopup();
        });
        return false;
    },
    delete: function(obj, postTable, id) {
        console.log({ PostTable: postTable, ID: id });
        jConfirm('გსურთ წაშლა?', null, function(e) {
            if (e) {
                $(obj).parent().addLoader();
                $.post(URI + 'user/deletelistitem/', { PostTable: postTable, ID: id }, function(data) {
                    $(obj).parent().removeLoader();
                    if ((data = _form.parseJSON(data)) == false) {
                        return false;
                    }
                    if (data.StatusCode == 1) {
                        var li = $(obj).parents('ul').first().parents('li').first();
                        $(obj).parents('li').first().remove();
                        if (li.length && !li.find('li').length) {
                            li.removeClass('sub');
                        }
                    } else {
                        jAlert(data.StatusMessage);
                    }
                });
            }
        });
    },
    changeStatus: function(obj, postTable, id) {
        $(obj).parent().addLoader();
        $.post(URI + 'user/changelistitemstatus/', { PostTable: postTable, ID: id }, function(data) {
            $(obj).parent().removeLoader();
            if ((data = _form.parseJSON(data)) == false) {
                return false;
            }
            if (data.StatusCode == 1) {
                $(obj).find('i').removeClass('green red').addClass(data.Data.ItemStatusID ? 'green' : 'red');
            } else {
                alert('შეცდომა');
            }
        });
    },
    changeSortOrder: function(obj, postTable, id, sortID) {
        if ($(obj).parents('ul').first().parents('li').first().find('ul:first > li').length <= 1 || (sortID == 0 && !$(obj).parents('li').first().prev().length) ||
            (sortID == 1 && !$(obj).parents('li').first().next().length)) {
            return false;
        }
        $(obj).parent().addLoader();
        $.post(URI + 'user/changelistitemsortorder/', { PostTable: postTable, ID: id, SortID: sortID }, function(data) {
            $(obj).parent().removeLoader();
            if ((data = _form.parseJSON(data)) == false) {
                return false;
            }
            if (data.StatusCode == 1) {
                li = $(obj).parents('li').first();
                if (sortID) {
                    li.next().after(li.clone()[0].outerHTML);
                } else {
                    li.prev().before(li.clone()[0].outerHTML);
                }
                li.remove();
                $('li[data-id="' + id + '"] .options').removeLoader();
            } else {
                alert('შეცდომა');
            }
        });
    }
};