function maxZ() {
    var maxZ = Math.max.apply(null, $.map($('body *'), function(e, n) {
        if ($(e).css('position') == 'absolute' || $(e).css('position') == 'relative' || $(e).css('position') == 'fixed')
            return parseInt($(e).css('z-index')) || 1;
    }));
    return maxZ;
}

function isEmail(email) {
    var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
    if (email.search(emailRegEx) == -1) {
        return (false);
    }
    return (true);
}

function isInt(input) {
    return !isNaN(input) && parseInt(input) == input;
}

function isFloat(input) {
    return !isNaN(input) && parseFloat(input) == input;
}

function isPos(input) {
    return input * -1 <= 0;
}
Object.size = function(obj) {
    var size = 0,
        key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

function validate(e, type) {
    switch (type) {
        case 'int':
            return validateDouble(e);
            break;

        case 'double':
            return validateDouble(e, 1);
            break;

        case 'alpha':
            return validateAlpha(e);
            break;
    }
}

function validateDouble(e, _double) {
    if (!validateKeys(e, _double)) {
        return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
}

function validateAlpha(e) {
    if (!validateKeys(e)) {
        return;
    }
    // Ensure that it is a alpha and stop the keypress
    if (!(e.keyCode > 64 && e.keyCode < 91)) {
        e.preventDefault();
    }
}

function validateKeys(e, _double) {
    // Allow: backspace, delete, tab, escape and enter
    var keyCodes = [46, 8, 9, 27, 13];
    if (_double) {
        keyCodes.push(110);
        keyCodes.push(190);
    }
    if ($.inArray(e.keyCode, keyCodes) !== -1 ||
        // Allow: Ctrl+A
        (e.keyCode == 65 && e.ctrlKey === true) ||
        // Allow: Ctrl+C
        (e.keyCode == 67 && e.ctrlKey === true) ||
        // Allow: Ctrl+X
        (e.keyCode == 88 && e.ctrlKey === true) ||
        // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
        // let it happen, don't do anything
        return false;
    }
    return true;
}

function changeLang(lang) {
    $.cookie('Lang', lang, { expires: parseInt(COOKIE_SITE_TIME), path: '/', domain: COOKIE_SITE_DOMAIN, secure: false });
    location.href = location.href.replace('/' + LANG + '/', '/' + lang + '/');
    return false;
}

function populateGet() {
    var obj = {},
        params = location.search.slice(1).split('&');
    for (var i = 0, len = params.length; i < len; i++) {
        var keyVal = params[i].split('=');
        var l = decodeURIComponent(keyVal[0]);
        if (l.substr(l.length - 2) == '[]') {
            var obj_arr = obj[l.substr(0, l.length - 2)];
            if (obj_arr) {
                obj[l.substr(0, l.length - 2)][obj_arr.length] = decodeURIComponent(keyVal[1]);
            } else {
                obj[l.substr(0, l.length - 2)] = [decodeURIComponent(keyVal[1])];
            }
        } else {
            obj[decodeURIComponent(keyVal[0])] = decodeURIComponent(keyVal[1]);
        }
    }
    return obj;
}
var GET = populateGet();

function scrollToSection(section) {
    $('body, html').animate({ scrollTop: $('section[data-name="' + section + '"]').offset().top }, 500);
    return false;
}

$(document).ready(function() {
    var swiper = new Swiper('.main-slider .swiper-container', {
        pagination: '.swiper-pagination',
        paginationClickable: true
    });

    /*if (GET.Section != "" && GET.Section != undefined) {
        scrollToSection(GET.Section);
    }*/
    $('[data-datepicker="true"]').datepicker({ dateFormat: 'dd/mm/yy' });

    //main page datepickers
    $('#datepicker1, #datepicker2').datepicker({
        dateFormat: 'dd/mm/yy',
        buttonText: '<i class="ic-right-arrow"></i>'
    });

    var swiper = new Swiper('.swiper-container.suggestion-slider', {
        slidesPerView: 4,
        nextButton: '.button-next',
        prevButton: '.button-prev',
        spaceBetween: 28,
        breakpoints: {
            // when window width is <= 320px
            320: {
                slidesPerView: 1,
                spaceBetween: 0
            },
            // when window width is <= 480px
            480: {
                slidesPerView: 1,
                spaceBetween: 0
            },
            // when window width is <= 640px
            768: {
                slidesPerView: 2,
                spaceBetween: 28
            }
        }
    });

});