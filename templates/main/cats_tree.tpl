{% if Cats|length %}
    <div class="menu-link btn col-md py-0 d-flex align-items-center">
        <a class="menu-item" href="{{ server.URI }}tours/?CatID={{ Cats.cat_id }}">
            {% if Cats.children %}
                <i class="ic-arrow-down-solid"></i>
            {% endif %}
            <span>{{ Cats.title|replace({' ': "<br>"})|raw }}</span>
        </a>
        {% if Cats.children %}
            <div class="sub-menu">
                <ul class="main-ul list-unstyled my-0">
                    {% if Cats.children|length < constants.MENU_MAX_CATS %}
                        {% include "main/sub_cats.tpl" with {Cats: Cats.children} %}
                    {% else %}
                        {% include "main/sub_cats.tpl" with {Cats: Cats.children|slice(0, (Cats.children|length / 2)|round)} %}
                        {% include "main/sub_cats.tpl" with {Cats: Cats.children|slice((Cats.children|length / 2)|round)} %}
                    {% endif %}
                </ul>
            </div>
        {% endif %}
    </div>
{% endif %}