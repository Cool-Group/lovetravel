<li class="main-li align-top">
    <ul class="menu-ul list-unstyled my-0">
        {% for val in Cats %}
            <li>
                <a href="{{ server.URI }}tours/?CatID={{ val.cat_id }}">{{ val.title }}</a>
            </li>
        {% endfor %}
    </ul>
</li>