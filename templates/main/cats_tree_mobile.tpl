{% if Cats|length %}
    <li class="nav-item {% if Cats.children %}dropdown{% endif %}">
        <a class="nav-link dropdown-toggle" href="{{ server.URI }}tours/?CatID={{ Cats.cat_id }}" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
           {{ Cats.title|replace({' ': " "})|raw }}
        </a>
        {% if Cats.children %}
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                {% if Cats.children|length < constants.MENU_MAX_CATS %}
                    {% include "main/sub_cats_mobile.tpl" with {Cats: Cats.children} %}
                {% else %}
                    {% include "main/sub_cats_mobile.tpl" with {Cats: Cats.children|slice(0, (Cats.children|length / 2)|round)} %}
                    {% include "main/sub_cats_mobile.tpl" with {Cats: Cats.children|slice((Cats.children|length / 2)|round)} %}
                {% endif %}
            </div>
        {% endif %}
    </li>
{% endif %}