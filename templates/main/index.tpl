{% extends "index.tpl" %}

{% set metaProperties = {
    title: constants.SITE ~ ' - ' ~ langs.MetaTitle,
    description: langs.MetaDescr,
    keywords: langs.MetaKeywords,
    image: constants.TWIG_THEME ~ 'images/' ~ constants.SITE ~ constants.IMG_TYPE,
    url: '/'
} %}

{% block content %}
    <div class="container-fluid py-5">
	        <div class="container">
				<div class="row align-items-center text-center">
					<div class="col-md-7 mb-lg-0 mb-4">
						<img src="{{ constants.TWIG_THEME }}assets/img/logo-1.png" alt="">
					</div>
					<form class="col-md-5" action="{{ server.URI }}tours/">
						<div class="row d-block">
							<div class="form-group form-group-search">
								<i class="ic-magnifying-glass"></i>
								<input type="text" class="form-control main-search-input text-center" placeholder="{{ langs.SearchKeyword }}"
                                name="Keyword" value="{{ get.Keyword }}">
							</div>
						</div>
						<div class="row d-block main-filter text-left">
							<div class="row align-items-center ">
								<div class="col-5">
									<i class="ic ic-placeholder"></i><i>{{ langs.Direction }}</i>
								</div>
								<div class="col-7">
									<div class="select-container">
										<select class="form-control" name="CatID">
											<option value="0">{{ langs.Select }}</option>
                                            {% include "admin/list/select.tpl" with {list: Cats.Tree, id: get.CatID} only %}
										</select>
										<i class="icon ic-arrow-down"></i>
									</div>
								</div>
							</div>
							<div class="row align-items-center">
								<div class="col-5">
									<i class="ic ic-calendar-1"></i><i>{{ langs.Begin }}</i>
								</div>
								<div class="col-7">
									<div class="select-container">
										<input type="text" id="datepicker1" class="form-control" placeholder="__/__/____" name="PeriodFrom" value="{{ get.PeriodFrom }}">
										<i class="icon ic-arrow-down"></i>
									</div>
								</div>
							</div>
							<div class="row align-items-center">
								<div class="col-5">
									<i class="ic ic-calendar-1"></i><i>{{ langs.End }}</i>
								</div>
								<div class="col-7">
									<div class="select-container">
										<input type="text" id="datepicker2" class="form-control" placeholder="__/__/____" name="PeriodTo" value="{{ get.PeriodTo }}">
										<i class="icon ic-arrow-down"></i>
									</div>
								</div>
							</div>
							<div class="row align-items-center">
								<div class="col-5">
									<i class="ic ic-piggy-bank"></i><i>{{ langs.Budget }}</i>
								</div>
								<div class="col-7">
									<div class="row">
										<div class="col">
											<input type="text" class="form-control" placeholder="{{ langs.From }}" name="PriceFrom" value="{{ get.PriceFrom }}">
										</div>
										<div class="col">
											<input type="text" class="form-control" placeholder="{{ langs.To }}" name="PriceTo" value="{{ get.PriceTo }}">
										</div>
									</div>
								</div>
							</div>
							<div class="row align-items-start category">
								<div class="col-5">
									<i class="ic ic-suitcase"></i><i>{{ langs.Category }}</i>
								</div>
								<div class="col-7">
									{% for val in Types %}
                                        <div class="form-group">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" name="Types[]" value="{{ val.type_id }}" {{ val.type_id in get.Types ? 'checked' }}> {{ val.title }}
                                            </label>
                                        </div>
                                    {% endfor %}
								</div>
							</div>
							<div class="row no-gutters">
								<button class="btn btn-primary btn-block border-0 mt-4">{{ langs.Search }} <i class="ic-magnifying-glass float-right"></i></button>
							</div>
						</div>
					</form>
				</div>
			</div>
	</div>
	{% if MainTours|length %}
        <div class="container-fluid pb-5">
            <div class="container">
                <div class="row d-block section-row no-gutters">
                    <span class="line"></span>
                    <h1><i class="title-main-icon ic-medal-on-a-ribbon-for-number-one"></i> {{ langs.Bestsellers }} <i class="ic-right-arrow"></i></h1>
                </div>
                <div class="row py-4">
                    {% for val in MainTours %}
                        <div class="col-md-4 mb-4">
                            <div class="card card-type">
                                <div class="image" style="background-image: url('{{ constants.UPL_TOURS ~ 'thumbs/' ~ val.tour_id ~ '_1' ~ constants.IMG_TYPE ~ '?v=' ~ val.photo_ver }}')">
                                    <a href="{{ server.URI }}tours/{{ val.tour_id }}/" class="full-view"><i class="ic-right-arrow float-left"></i> {{ langs.ReadMore }}</a>
                                    <a href="#" class="add-to-fav {{ val.tour_id in Favorites ? 'active' }}" onclick="return favorites.post(this, {{ val.tour_id }});">
                                        <i class="ic-favorite float-left"></i> {{ langs.Favorite }}
                                    </a>
                                </div>
                                <div class="card-block">
                                    <h4 class="card-title">{{ Cats.List[val.cat_id].title }} <span class="price float-right">{{ langs.CurrencyTitles[val.currency_id] ~ ' ' ~ val.price }}</span></h4>
                                    <p class="card-text"><span class="category">{{ val.title }}</span><span class="duration float-right text-right">{{ Period(val.period_from, val.period_to) }}</span></p>
                                    {% set tour_services, serv, add_serv = json_decode(val.services, true), "", "" %}
                                    {% if tour_services|length %}
                                        {% for service_id, type_id in tour_services %}
                                            {% if type_id == 1 %}
                                                {% set serv = serv ~ '<i class="' ~ TourServices[service_id].icon ~ ' active" title="' ~ TourServices[service_id].title ~ '"></i>' %}
                                            {% else %}
                                                {% set add_serv = add_serv ~ '<i class="' ~ TourServices[service_id].icon ~ '" title="' ~ TourServices[service_id].title ~ '"></i>' %}
                                            {% endif %}
                                        {% endfor %}
                                        <div class="icons">
                                            {{ (serv ~ add_serv)|raw }}
                                        </div>
                                    {% endif %}
                                </div>
                                {% if val.old_price > 0 %}
                                    <i class="ic-sale"></i>
                                {% endif %}
                            </div>
                        </div>
                    {% endfor %}
                </div>
            </div>
        </div>
    {% endif %}
    {% set typed_tours = {
        (constants.SEA_TOURS_TYPE_ID): 'ic-nautilus-shell-of-sea-life-to-hear-the-sea-sound-for-relaxation',
        (constants.WINTER_TOURS_TYPE_ID): 'ic-nautilus-shell-of-sea-life-to-hear-the-sea-sound-for-relaxation'
    } %}
    {% for tour_type_id, icon in typed_tours %}
        {% if TypedTours[tour_type_id]|length %}
        	<div class="container-fluid">
                <div class="container">
                    <div class="row d-block section-row no-gutters mb-0 mb-lg-5 mt-0">
                        <span class="line"></span>
                        <h1><i class="title-main-icon {{ icon }}"></i> {{ Types[tour_type_id].title }} <i class="ic-right-arrow"></i></h1>
                    </div>
                    {% for val in TypedTours[tour_type_id] %}
                        <div class="row list-view-row align-items-center">
                            <div class="box col-md-4 p-0">
                                <div class="image" style="background-image: url('{{ constants.UPL_TOURS ~ 'thumbs/' ~ val.tour_id ~ '_1' ~ constants.IMG_TYPE ~ '?v=' ~ val.photo_ver }}')">
                                    <a href="{{ server.URI }}tours/{{ val.tour_id }}/" class="full-view"><i class="ic-right-arrow float-left"></i> {{ langs.ReadMore }}</a>
                                    <a href="#" class="add-to-fav {{ val.tour_id in Favorites ? 'active' }}" onclick="return favorites.post(this, {{ val.tour_id }});">
                                        <i class="ic-favorite float-left"></i> {{ langs.Favorite }}
                                    </a>
                                </div>
                                {% if val.old_price > 0 %}
                                    <div class="sale-badge">SALE -{{ langs.CurrencyTitles[val.currency_id] ~ ' ' ~ (val.price - val.old_price) }}</div>
                                {% endif %}
                            </div>
                            <div class="box col-md-3 py-3">
                                <h3 class="mb-4">{{ Cats.List[val.cat_id].title }} <span class="float-right">{{ Period(val.period_from, val.period_to) }}</span></h3>
                                <div class="row">
                                    <div class="col-12 destination mb-3">{{ val.title }}</div>
                                    {% set tour_services, serv, add_serv = json_decode(val.services, true), "", "" %}
                                    {% if tour_services|length %}
                                        {% for service_id, type_id in tour_services %}
                                            {% if type_id == 1 %}
                                                {% set serv = serv ~ '<i class="ic ' ~ TourServices[service_id].icon ~ ' active" title="' ~ TourServices[service_id].title ~ '"></i>' %}
                                            {% else %}
                                                {% set add_serv = add_serv ~ '<i class="ic ' ~ TourServices[service_id].icon ~ '" title="' ~ TourServices[service_id].title ~ '"></i>' %}
                                            {% endif %}
                                        {% endfor %}
                                        {% if serv != "" %}
                                            <div class="col-4 service">{{ langs.Services }}:</div>
                                            <div class="col-8">{{ serv|raw }}</div>
                                        {% endif %}
                                        {% if add_serv != "" %}
                                            <div class="col-4 service mt-3">{{ langs.AddServices }}:</div>
                                            <div class="col-8 mt-3">{{ add_serv|raw }}</div>
                                        {% endif %}
                                    {% endif %}
                                </div>
                            </div>
                            <div class="box col-md-3 py-3">
                                <div class="row align-items-start tour-description no-gutters">
                                    {{ val.short_descr|striptags|raw }}rg
                                </div>
                            </div>
                            <div class="box col-md-2 price">
                                <div class="row align-items-center text-center">
                                    <div class="mx-auto">
                                        {% if val.old_price > 0 %}
                                            <span class="d-block"><del>{{ langs.CurrencyTitles[val.currency_id] ~ ' ' ~ val.old_price }}</del></span>
                                        {% endif %}
                                        <span class="d-block">{{ langs.CurrencyTitles[val.currency_id] ~ ' ' ~ val.price }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    {% endfor %}
                </div>
            </div>
        {% endif %}
    {% endfor %}
	<div class="container-fluid subscription-container d-flex align-items-center mt-5" style="background-image: url({{ constants.TWIG_THEME }}assets/img/stock-photo-132271505.jpg)">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 text-center">
                    <img class="mx-auto mb-5" src="{{ constants.TWIG_THEME }}assets/img/logo-2.png" alt="">
                </div>
                <div class="col-2"></div>
                <div class="col-8 border"></div>
                <div class="col-2"></div>
            </div>
            <div class="row mt-5">
                <div class="form-group mx-auto">
                    <label class="text-center d-block" for="subscripe">{{ langs.SubscribeNews }}</label>
                    <input type="text" class="form-control border-0 text-center" id="subscripe" placeholder="{{ langs.Email }}">
                    <button class="btn btn-default"><i class="ic-right-arrow"></i></button>
                </div>
            </div>
        </div>
    </div>
{% endblock %}