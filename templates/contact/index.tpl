{% extends "index.tpl" %}

{% set metaProperties = {
    title: constants.SITE ~ ' - ' ~ langs.MetaTitle,
    description: langs.MetaDescr,
    keywords: langs.MetaKeywords,
    image: constants.TWIG_THEME ~ 'images/' ~ constants.SITE ~ constants.IMG_TYPE,
    url: '/'
} %}

{% block content %}
    <div class="container contact-container">
        <div class="container">
            <div class="row d-block section-row no-gutters">
                <span class="line"></span>
                <h1>{{ langs.ContactUs }}</h1>
            </div>
            <div class="row mt-4 mb-4">
                <div class="col-md-4">{{ Contacts.short_descr|raw }}</div>
                <div class="col-md-4">
                    <ul class="list-unstyled contact-ul">
                        <li><i class="ic-placeholder"></i> <span class="d-inline-block">{{ Contacts.address }}</span></li>
                        <li><i class="ic-phone-call"></i> <span class="d-inline-block">{{ Contacts.phone }}</span></li>
                        <li><i class="ic-smartphone"></i> <span class="d-inline-block">{{ Contacts.mobile }}</span></li>
                        <li><a href="mailto:{{ Contacts.email }}"><i class="ic-envelope2"></i> <span class="d-inline-block">{{ Contacts.email }}</span></a></li>
                        <li><a href="#"><i class="ic-messenger2"></i> <span class="d-inline-block">@Lovetraveltbilisi</span></a></li>
                        <li><i class="ic-calendar"></i> <span class="d-inline-block">{{ Contacts.open_hours|raw }}</span></li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <form class="contact-form" action="{{ server.URI }}contact/send/" onsubmit="return contact.send(this);">
                        <div class="form-group">
                            <input type="text" class="form-control" name="Name" placeholder="{{ langs.Name }}" data-error="{{ langs.WriteName }}">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="Phone" placeholder="{{ langs.ContactPhone }}" data-error="{{ langs.WriteContactPhone }}">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="Email" placeholder="{{ langs.YourEmail }}" data-type="email" data-error="{{ langs.WriteYourEmail }}">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="Message" cols="30" rows="10" placeholder="{{ langs.MessageText }}" data-error="{{ langs.WriteMessageText }}"></textarea>
                        </div>
                        {% include "captcha/index.tpl" %}
                        <div class="form-group">
                            <button class="btn btn-primary text-center btn-block border-0">{{ langs.Send }} <i class="ic-right-arrow"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div id="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2975.069533946242!2d44.764997015611414!3d41.783715079294005!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40446de641bd7f29%3A0x8a845ab80ac4e36a!2zNiDhg6Thg5Dhg6Dhg6Hhg5Dhg5Phg5Dhg5zhg5jhg6Eg4YOl4YOj4YOp4YOQLCDhg5fhg5Hhg5jhg5rhg5jhg6Hhg5g!5e0!3m2!1ska!2sge!4v1493140487305" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    <div class="container-fluid pb-5 contact-container">
        <div class="container">
            <div class="row d-block section-row no-gutters"><span class="line"></span><h1>{{ langs.Social }}</h1></div>
            <div class="social-icons mt-4">
                <!-- <a href="" class="facebook"><i class="ic-fbfooter"></i></a>
                <a href="" class="youtube"><i class="ic-youtube"></i></a>
                <a href="" class="twitter"><i class="ic-twitter"></i></a> -->
                {% for id, url in contactcs_social_media %}
                    <a href="{{ url }}" class="{{ social_media[id] }}" target="_blank">
                        <i class="ic-{{ social_media[id] }}-circle"></i>
                    </a>
                {% endfor %}
            </div>
        </div>
    </div>
    <div class="container-fluid subscription-container d-flex align-items-center mt-5" style="background-image: url({{ constants.TWIG_THEME }}assets/img/stock-photo-132271505.jpg)">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 text-center">
                    <img class="mx-auto mb-5" src="{{ constants.TWIG_THEME }}assets/img/logo-2.png" alt="">
                </div>
                <div class="col-2"></div>
                <div class="col-8 border"></div>
                <div class="col-2"></div>
            </div>
            <div class="row mt-5">
                <div class="form-group mx-auto">
                    <label class="text-center d-block" for="subscripe">გამოიწერეთ ჩვენი სიახლეები</label>
                    <input type="text" class="form-control border-0 text-center" id="subscripe" placeholder="ელ-ფოსტა">
                    <button class="btn btn-default"><i class="ic-right-arrow"></i></button>
                </div>
            </div>
        </div>
    </div>
{% endblock %}

{% block footer %}
    <script type="text/javascript" src="{{ constants.TWIG_THEME }}assets/js/build/plugins/contact.min.js?v={{ constants.FILE_VER }}"></script>
{% endblock %}