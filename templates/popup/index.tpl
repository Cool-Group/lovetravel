{% spaceless %}
    {% if popupProperties is defined %}
        <div class="popup-header">
            <div class="popup-header-title">
                <!-- <span class="popup-header-icon">
                    <i class="icon-16 {{ popupProperties.icon }}-16 white"></i>
                </span> -->
                <p>{{ popupProperties.title }}</p>
            </div>
        </div>
    {% endif %}
    <div class="popup-content clearfix">
        {% block content %}
        {% endblock %}
        {% block section %}
        {% endblock %}
    </div>
{% endspaceless %}