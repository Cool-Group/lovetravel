<div class="popup-container">
        <div class="popup-wrapper">
            <div class="close-popup"><i class="ic-times"></i></div>
                <div class="container-fluid">
                    <div class="row d-block no-gutters gallery-popup">
                        <div class="image-area">
                            <div class="row d-block align-items-center">
                                <div class="slider-content">
                                    <div class="pr-photos-view-block clearfix" style="width: {{ constants.TOUR_LARGE_PHOTO_WIDTH }}px;">
                                        <div class="pr-photos-view" data-width="{{ constants.TOUR_LARGE_PHOTO_WIDTH }}" data-height="{{ constants.TOUR_LARGE_PHOTO_HEIGHT }}"
	 style="width: {{ constants.TOUR_LARGE_PHOTO_WIDTH }}px; height: {{ constants.TOUR_LARGE_PHOTO_HEIGHT }}px;">
                                            <div class="pr-photos-img">
                                                <div style="width: {{ constants.TOUR_LARGE_PHOTO_WIDTH }}px; height: {{ constants.TOUR_LARGE_PHOTO_HEIGHT }}px;">
                                                    <img src="{{ constants.UPL_TOURS }}large/{{ get.Photo ~ '_' ~ get.PhotoNum ~ get.PhotoType }}" />
                                                </div>
                                            </div>
                                            <div class="pr-photos-overlay"></div>
                                            {% if get.PhotosCnt > 1 %}
                                                <div class="unselect">
                                                    <span class="prev"><i></i></span>
                                                    <span class="next"><i></i></span>
                                                </div>
                                            {% endif %}
                                        </div>
                                    </div>
                                    <div class="image-label">
                                        {% if Data.photos_cnt > 1 %}
                                            <div class="date px-2"><i>{{ get.PhotoNum }}</i><span> - {{ Data.photos_cnt }}</span> </div>
                                        {% endif %}
                                        <div class="text px-3">{{ Data.title }}</div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="thumb-area px-4 py-5">
                            <p class="id-number text-right"># {{ Data.tour_id }}</p>
                            <h2 class="mb-4">{{ Data.title }}</h2>
                            <p class="period">{{ langs.Period }}: <strong>{{ Data.period_from|date('d.m.Y') ~ ' - ' ~ Data.period_to|date('d.m.Y') }}</strong></p>
                            <!-- <p class="period">ხანგრძლივობა: <strong>11დღე -10 ღამე</strong></p> -->
                            {% if Data.price_from > 0 %}
                                <div class="price my-4">{{ Data.price_from ~ ' ' ~ langs.CurrencyTitles[Data.currency_id] }}</div>
                            {% endif %}
                            {% set tour_services, serv, add_serv = json_decode(Data.services, true), "", "" %}
                            {% if tour_services|length %}
                                {% for service_id, type_id in tour_services %}
                                    {% if type_id == 1 %}
                                        {% set serv = serv ~ '<i class="ic ' ~ TourServices[service_id].icon ~ ' active" title="' ~ TourServices[service_id].title ~ '"></i> ' %}
                                    {% else %}
                                        {% set add_serv = add_serv ~ '<i class="ic ' ~ TourServices[service_id].icon ~ '" title="' ~ TourServices[service_id].title ~ '"></i> ' %}
                                    {% endif %}
                                {% endfor %}
                                <div class="icons mb-5">
                                    {{ (serv ~ add_serv)|raw }}
                                </div>
                            {% endif %}
                            <!-- <div class="row d-block no-gutters my-4">
                                <button class="btn btn-primary btn-block">ტურის დაჯავშნა</button>
                                <button class="btn btn-default btn-block">რჩეულებში <i class="ic-favorite"></i></button>
                            </div> -->
                            <div class="row thumbs">
                                {% if get.PhotosCnt > 1 %}
                                    <div class="pr-photos-thumbs">
                                        <ul class="d-flex row no-gutters">
                                            {% for i in 1..get.PhotosCnt %}
                                                <li class="col-md-4 mb-2" class="img{{ i == get.PhotoNum ? ' active' }}"
                                                data-photo="{{ constants.UPL_TOURS }}large/{{ get.Photo ~ '_' ~ i ~ get.PhotoType }}">
                                                    <div class="thumbnail" style="background-image:url({{ constants.UPL_TOURS }}thumbs/{{ get.Photo ~ '_' ~ i ~ get.PhotoType }});"></div>
                                                </li>		
                                            {% endfor %}
                                        </ul>
                                    </div>
                                {% endif %}
                            </div>
                            <div class="row no-gutters d-block mt-4">
                                <p class="category">
                                    {{ langs.Category }}: <strong>{{ Types[Data.type_id].title }}</strong>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        <div class="clearfix"></div>
    </div>