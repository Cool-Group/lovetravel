{% extends "index.tpl" %}

{% set metaProperties = {
    title: constants.SITE ~ ' - ' ~ langs.MetaTitle,
    description: langs.MetaDescr,
    keywords: langs.MetaKeywords,
    image: constants.TWIG_THEME ~ 'images/' ~ constants.SITE ~ constants.IMG_TYPE,
    url: '/'
} %}

{% block content %}
<div class="tours-list">
	<div class="container tours-aboard py-5">
        <!-- {% if get.CatID and Cats.List[get.CatID] != "" %}
            {% set titles, cat, photo = [], Cats.List[get.CatID], "" %}
                    {% for i in 0..Cats.List|length if cat.parent_cat_id != "" %}
                        {% set titles, cat, photo = [cat.title]|merge(titles), Cats.List[cat.parent_cat_id], photo == "" and cat.has_photo ? constants.UPL_CATS ~ cat.cat_id ~ '.png?v=' ~ cat.photo_ver : photo %}
            {% endfor %}
                    <div class="row d-block no-gutters">
                        <h1>{{ titles[0] }}</h1>
                        <h2>
                            {{ titles|length > 1 ? titles|slice(1)|join(', ') }}
                            <img src="{{ photo }}" alt="{{ Cats.List[get.CatID].title }}">
                        </h2>
                    </div>
                    <div class="row no-gutters mt-3 mb-4">
                        <div class="col-md-1 h-line"></div>
                    </div>
                {% endif %} -->
        <div class="container-fluid">
            <div class="row">
                <form class="col-md-3" action="{{ server.URI }}tours/">
                    <div class="form-group form-group-search mb-5">
                        <i class="ic-magnifying-glass"></i>
                        <input type="text" class="form-control main-search-input text-center" placeholder="{{ langs.SearchKeyword }}"
                        name="Keyword" value="{{ get.Keyword }}">
                    </div>
                    <div class="main-filter">
                        <div class="row mb-4 align-items-center">
                            <div class="col-12">
                                <i class="ic ic-placeholder"></i><i><b>{{ langs.Direction }}</b></i>
                            </div>
                            <div class="col-12">
                                <div class="select-container">
                                    <select name="CatID" class="form-control">
                                        <option value="0">{{ langs.Select }}</option>
                                        {% include "admin/list/select.tpl" with {list: Cats.Tree, id: get.CatID} only %}
                                    </select>
                                    <i class="icon ic-arrow-down"></i>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-4 align-items-center">
                            <div class="col-12">
                                <i class="ic ic-calendar-1"></i><i><b>{{ langs.Begin }}</b></i>
                            </div>
                            <div class="col-12">
                                <div class="select-container">
                                    <input type="text" id="datepicker1" class="form-control" placeholder="__/__/____" name="PeriodFrom" value="{{ get.PeriodFrom }}">
                                    <i class="icon ic-arrow-down"></i>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-4 align-items-center">
                            <div class="col-12">
                                <i class="ic ic-calendar-1"></i><i><b>{{ langs.End }}</b></i>
                            </div>
                            <div class="col-12">
                                <div class="select-container">
                                    <input type="text" id="datepicker2" class="form-control" placeholder="__/__/____" name="PeriodTo" value="{{ get.PeriodTo }}">
                                    <i class="icon ic-arrow-down"></i>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-4 align-items-center">
                            <div class="col-12">
                                <i class="ic ic-piggy-bank"></i><i><b>{{ langs.Budget }}</b></i>
                            </div>
                            <div class="col-12">
                                <div class="row">
                                    <div class="col">
                                        <input type="text" class="form-control" placeholder="{{ langs.From }}" name="PriceFrom" value="{{ get.PriceFrom }}">
                                    </div>
                                    <div class="col">
                                        <input type="text" class="form-control" placeholder="{{ langs.To }}"name="PriceTo" value="{{ get.PriceTo }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-4 align-items-start category">
                            <div class="col-12">
                                <i class="ic ic-suitcase"></i><i><b>{{ langs.Category }}</b></i>
                            </div>
                            <div class="col-12 col-12 pl-4 pt-3">
                                {% for val in Types %}
                                    <div class="form-group mb-0">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="checkbox" name="Types[]" value="{{ val.type_id }}" {{ val.type_id in get.Types ? 'checked' }}> {{ val.title }}
                                        </label>
                                    </div>
                                {% endfor %}
                            </div>
                        </div>
                        <div class="row no-gutters">
                            <button class="btn btn-primary btn-block border-0">{{ langs.Search }} <i class="ic-magnifying-glass float-right"></i></button>
                        </div>
                    </div>
                </form>
                <div class="col-md-9">
                    {% if Data|length %}
                        {% for val in Data %}
                            <div class="row list-view-row align-items-center justify-content-between">
                                <div class="box col-sm-4 p-0">
                                    <div class="image" style="background-image: url('{{ constants.UPL_TOURS ~ 'thumbs/' ~ val.tour_id ~ '_1' ~ constants.IMG_TYPE ~ '?v=' ~ val.photo_ver }}')">
                                        <a href="{{ server.URI }}tours/{{ val.tour_id }}/" class="full-view"><i class="ic-right-arrow float-left"></i> {{ langs.ReadMore }}</a>
                                        <a href="#" class="add-to-fav {{ val.tour_id in Favorites ? 'active' }}" onclick="return favorites.post(this, {{ val.tour_id }});">
                                            <i class="ic-favorite float-left"></i> {{ langs.Favorite }}
                                        </a>
                                    </div>
                                    {% if val.old_price > 0 %}
                                        <div class="sale-badge">SALE -{{ langs.CurrencyTitles[val.currency_id] ~ ' ' ~ (val.price - val.old_price) }}</div>
                                    {% endif %}
                                </div>
                                <div class="box col-sm-5 py-3">
                                    <h3 class="mb-4">{{ Cats.List[val.cat_id].title }} <span class="float-right">{{ Period(val.period_from, val.period_to) }}</span></h3>
                                    <div class="row">
                                        <div class="col-md-12 destination mb-3">{{ val.title }}</div>
                                        {% set tour_services, serv, add_serv = json_decode(val.services, true), "", "" %}
                                        {% if tour_services|length %}
                                            {% for service_id, type_id in tour_services %}
                                                {% if type_id == 1 %}
                                                    {% set serv = serv ~ '<i class="ic ' ~ TourServices[service_id].icon ~ ' active" title="' ~ TourServices[service_id].title ~ '"></i>' %}
                                                {% else %}
                                                    {% set add_serv = add_serv ~ '<i class="ic ' ~ TourServices[service_id].icon ~ '" title="' ~ TourServices[service_id].title ~ '"></i>' %}
                                                {% endif %}
                                            {% endfor %}
                                            {% if serv != "" %}
                                                <div class="col-4 service">{{ langs.Services }}:</div>
                                                <div class="col-8">{{ serv|raw }}</div>
                                            {% endif %}
                                            {% if add_serv != "" %}
                                                <div class="col-4 service mt-3">{{ langs.AddServices }}:</div>
                                                <div class="col-8 mt-3">{{ add_serv|raw }}</div>
                                            {% endif %}
                                        {% endif %}
                                    </div>
                                </div>
                                <div class="box col-sm-3 price">
                                    <div class="row align-items-center text-center">
                                        <div class="mx-auto">
                                            {% if val.old_price > 0 %}
                                                <span class="d-block"><del>{{ langs.CurrencyTitles[val.currency_id] ~ ' ' ~ val.old_price }}</del></span>
                                            {% endif %}
                                            <span class="d-block">{{ langs.CurrencyTitles[val.currency_id] ~ ' ' ~ val.price }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        {% endfor %}
                        <div class="container-fluid">
                            {% include "pagination/index.tpl" %}
                        </div>
                    {% endif %}
                </div>
            </div>
        </div>
    </div>    
</div>
{% endblock %}