{% extends "index.tpl" %}

{% set metaProperties = {
    title: constants.SITE ~ ' - ' ~ langs.MetaTitle,
    description: langs.MetaDescr,
    keywords: langs.MetaKeywords,
    image: constants.TWIG_THEME ~ 'images/' ~ constants.SITE ~ constants.IMG_TYPE,
    url: '/'
} %}

{% block content %}
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=1694066887488973";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
	<div class="container tour-detail py-5">
        <div class="row">
            <ol class="breadcrumb">
                <li class="breadcrumb-item category">{{ langs.Category }}:</li>
                <li class="breadcrumb-item"><a href="{{ server.URI }}/tours/?Types[]={{ Data.type_id }}">{{ Types[Data.type_id].title }}</a></li>
                <li class="breadcrumb-item category">{{ langs.Direction }}:</li>
                {% set directions, cat = [], Cats.List[Data.cat_id] %}
                {% for i in 0..Cats.List|length if cat.parent_cat_id != "" %}
                    {% set directions, cat = [cat.cat_id]|merge(directions), Cats.List[cat.parent_cat_id] %}
                {% endfor %}
                {% for cat_id in directions %}
                    <li class="breadcrumb-item {{ loop.last ? 'active' }}"><a href="{{ server.URI }}/tours/?CatID={{ cat_id }}">{{ Cats.List[cat_id].title }}</a></li>
                {% endfor %}
            </ol>
        </div>
        <div class="row  section-row no-gutters">
            <div class="col-sm-6">
                <span class="line"></span>
                <h1><b>{{ Data.title }}</b></h1>
                <h2><b>{{ langs.TourBuyNow }}</b></h2>
            </div>
            <div class="col-sm-6 text-right tour-id-col">
                <div class="">social buttons</div>
                <span class="small-line"></span>
                <h3 class="tour-id">{{ langs.Tour }} # {{ Data.tour_id }}</h3>
            </div>
        </div>
        <div class="row no-gutters">
            <div class="col-md image-col">
                <div class="row d-block no-gutters tour-slider-thumb">
                    <div class="pr-view-img-block">
                        <div class="pr-view-img img img-border-silver" data-tour-id="{{ Data.tour_id }}" data-photo="{{ Data.tour_id }}" data-photo-num="1" data-photos-cnt="{{ Data.photos_cnt }}"
                             data-photo-type="{{ constants.IMG_TYPE }}?v={{ Data.photo_ver }}">
                            <div class="pr-view-img-large">
                                <img src="{{ constants.UPL_TOURS ~ 'large/' ~ Data.tour_id ~ '_1' ~ constants.IMG_TYPE ~ '?v=' ~ Data.photo_ver }}" alt="{{ Data.title }}">
                            </div>
                        </div>
                    </div>
                    <div class="pr-view-imgs row no-gutters d-block">
                        <ul data-left="0">
                            {% for i in 1..Data.photos_cnt %}
                                <li data-img="{{ constants.UPL_TOURS ~ 'large/' ~ Data.tour_id ~ '_' ~ i ~ constants.IMG_TYPE ~ '?v=' ~ Data.photo_ver }});" data-photo-num="{{ i }}"
                                 style="background-image: url({{ constants.UPL_TOURS ~ 'thumbs/' ~ Data.tour_id ~ '_' ~ i ~ constants.IMG_TYPE ~ '?v=' ~ Data.photo_ver }});">
                                </li>
                            {% endfor %}
                        </ul>
                        {% if Data.photos_cnt > 1 %}
                            <span class="arrow left-arrow dn"><i class="ic-left-arrow"></i></span>
                            <span class="arrow right-arrow"><i class="ic-right-arrow"></i></span>
                        {% endif %}
                    </div>
                </div>
                {% if Data.map_coords|length %}
                    <div class="container-fluid thin-line my-4">
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-10"></div>
                        </div>
                    </div>
                    <div id="tour-map"></div>
                {% endif %}
            </div>
            <div class="col-md pl-lg-5 pl-0">
                <div class="period">
                    <div class="">
                        {{ langs.Period }}: <strong>{{ Data.period_from|date('d.m.Y') ~ ' - ' ~ Data.period_to|date('d.m.Y') }}</strong>
                    </div>
                    <div class="">
                        {{ langs.Duration }}: <strong>{{ Period(Data.period_from, Data.period_to) }}</strong>
                    </div>
                </div>
                <!-- <div class="period">
                    ხანგრძლივობა: <strong>11დღე -10 ღამე</strong>
                </div> -->
                {% if Data.price > 0 %}
                    <div class="price"><b>{{ Data.price ~ ' ' ~ langs.CurrencyTitles[Data.currency_id] }}</b></div>
                {% endif %}
                <div class="publish">
                    <div class="">
                        {{ langs.Published }}: {{ Data.insert_date|date('d.m.Y') }}
                    </div>
                    <div>
                        {{ langs.Views }}: 144
                    </div>
                </div>
                <div class="btn-container mb-5">
                    <a href="#" class="btn btn-primary d-block">ტურის დაჯავშნა</a>
                    <a href="#" class="btn btn-default d-block {{ Data.tour_id in Favorites ? 'active' }}" onclick="return favorites.post(this, {{ Data.tour_id }});">
                        <i class="ic-favorite"></i> <span class="d-inline-block">{{ langs.Favorite }}</span>
                    </a>
                </div>
                <div class="container-fluid thin-line my-4">
                    <div class="row">
                        <div class="col-md-1"></div><div class="col-md-11"></div>
                    </div>
                </div>
                <div class="tour-description-container">
                    {{ Data.descr|raw }}
                </div>
                <div class="btn-container">
                    <button class="btn btn-default btn-block collapse-button btn-lg mt-4" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                        <i class="ic-arrow-top-solid d-lg-inline-block d-none"></i> <!--{{ langs.ShowLess }}--> აჩვენე ნაკლები
                    </button>
                </div>
                <div class="container-fluid thin-line mt-4">
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-11"></div>
                    </div>
                </div>
                <div class="collapse show" id="collapseExample">
                    {% if Data.hotels|striptags|trim != "" %}
                        <div class="row d-block no-gutters">
                            <h5 class="my-3"><i class="ic-servhotelpass"></i> {{ langs.Hotels }}</h5>
                            {{ Data.hotels|replace({'ul': "ul class='list-unstyled mb-4'", 'ol': "ol class='list-unstyled mb-4'"})|raw }}
                        </div>
                    {% endif %}
                    {% set tour_services, serv, add_serv = json_decode(Data.services, true), "", "" %}
                    {% if tour_services|length %}
                        {% for service_id, type_id in tour_services %}
                            {% if type_id == 1 %}
                                {% set serv = serv ~ '<li><i class="ic active ' ~ TourServices[service_id].icon ~ '"></i> ' ~ TourServices[service_id].title ~ '</li>' %}
                            {% else %}
                                {% set add_serv = add_serv ~ '<li><i class="ic inactive ' ~ TourServices[service_id].icon ~ '"></i> ' ~ TourServices[service_id].title ~ '</li>' %}
                            {% endif %}
                        {% endfor %}
                        {% if serv != "" %}
                            <div class="row d-block no-gutters">
                                <h5 class="my-3"><i class="ic-favorites"></i> {{ langs.TourServices }}</h5>
                                <ul class="list-unstyled mb-4">
                                   {{ serv|raw }}
                                </ul>
                            </div>
                        {% endif %}
                        {% if serv != "" %}
                            <div class="row d-block no-gutters">
                                <h5 class="my-3"><i class="ic-favorites"></i> {{ langs.TourAddServices }}</h5>
                                <ul class="list-unstyled mb-4">
                                   {{ add_serv|raw }}
                                </ul>
                            </div>
                        {% endif %}    
                    {% endif %}
                    {% set tour_notes = json_decode(Data.notes, true) %}
                    {% if tour_notes|length %}
                        <div class="row d-block no-gutters">
                            <h5 class="my-3"><i class="ic-favorites"></i> {{ langs.Note }}</h5>
                            <ul class="list-unstyled mb-4">
                                {% for val in tour_notes %}
                                    <li><i class="ic inactive ic-down-right-arrow"></i> <i>{{ Notes[val].title }}</i></li>
                                {% endfor %}
                            </ul>
                        </div>
                    {% endif %}
                    <div class="container-fluid thin-line mt-4">
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-11"></div>
                        </div>
                    </div>
                    <div class="fb-comments py-5">
                        <div class="fb-comments" data-href="{{ server.URI }}tours/{{ Data.tour_id }}/" data-numposts="5" data-width="100%"></div>
                    </div>
                </div>
            </div>
        </div>
        {% if SimilarTours|length %}
            <div class="row d-block section-row no-gutters mb-5">
                <span class="line"></span>
                <h1>
                    <i class="heading-icon ic-history-symbol-of-antique-building"></i> {{ langs.SimilarTours }} - {{ Cats.List[Data.cat_id].title }} 
                    <a href="{{ server.URI }}tours/?CatID={{ Data.cat_id }}" class="btn btn-primary">ყველა ტური <i class="ic-right-arrow"></i></a>
                </h1>
            </div>
            <div class="row d-block">
                <!-- Swiper -->
                <div class="suggestion-slider-container p-3 p-lg-0">
                    <div class="swiper-container suggestion-slider">
                        <div class="swiper-wrapper p-0 p-lg-2">
                            {% for val in SimilarTours %}
                                <div class="swiper-slide">
                                    <div class="card card-type">
                                        <div class="image" style="background-image: url('{{ constants.UPL_TOURS ~ 'thumbs/' ~ val.tour_id ~ '_1' ~ constants.IMG_TYPE ~ '?v=' ~ val.photo_ver }}')">
                                            <a href="{{ server.URI }}tours/{{ val.tour_id }}/" class="full-view"><i class="ic-right-arrow float-left"></i> {{ langs.ReadMore }}</a>
                                            <a href="#" class="add-to-fav {{ val.tour_id in Favorites ? 'active' }}" onclick="return favorites.post(this, {{ val.tour_id }});">
                                                <i class="ic-favorite float-left"></i> {{ langs.Favorite }}
                                            </a>
                                            <div class="price">{{ langs.CurrencyTitles[val.currency_id] ~ ' ' ~ val.price }}</div>
                                        </div>
                                        <div class="card-block">
                                            <h4 class="card-title">{{ Cats.List[val.cat_id].title }}</h4>
                                            <p class="card-text"><span class="category">{{ val.title }}</span><!-- <span class="duration float-right text-right">{{ Period(val.period_from, val.period_to) }}</span> --></p>
                                            {% set tour_services, serv, add_serv = json_decode(val.services, true), "", "" %}
                                            {% if tour_services|length %}
                                                {% for service_id, type_id in tour_services %}
                                                    {% if type_id == 1 %}
                                                        {% set serv = serv ~ '<i class="' ~ TourServices[service_id].icon ~ ' active" title="' ~ TourServices[service_id].title ~ '"></i>' %}
                                                    {% else %}
                                                        {% set add_serv = add_serv ~ '<i class="' ~ TourServices[service_id].icon ~ '" title="' ~ TourServices[service_id].title ~ '"></i>' %}
                                                    {% endif %}
                                                {% endfor %}
                                                <div class="icons">
                                                    {{ (serv ~ add_serv)|raw }}
                                                </div>
                                            {% endif %}
                                        </div>
                                        {% if val.old_price > 0 %}
                                            <i class="ic-sale"></i>
                                        {% endif %}
                                    </div>
                                </div>
                            {% endfor %}
                        </div>
                    </div>
                    <div class="slider-buttons d-inline-block button-next"><i class="ic-left-arrow"></i></div>
                    <div class="slider-buttons d-inline-block button-prev"><i class="ic-right-arrow"></i></div>
                </div>
            </div>
        {% endif %}
    </div>
{% endblock %}

{% block footer %}
    <script src="{{ constants.TWIG_THEME }}assets/js/build/plugins/swiper.min.js?v={{ constants.FILE_VER }}"></script>
    <script src="{{ constants.TWIG_THEME }}assets/js/build/plugins/lightgallery.min.js?v={{ constants.FILE_VER }}"></script>
    <script src="{{ constants.TWIG_THEME }}assets/js/build/plugins/lg-thumbnail.min.js?v={{ constants.FILE_VER }}"></script>
    <script src="{{ constants.TWIG_THEME }}assets/js/build/plugins/lightslider.min.js?v={{ constants.FILE_VER }}"></script>
    {% if Data.map_coords|length %}
        <script src="https://maps.googleapis.com/maps/api/js?language={{ globals.Lang.Code }}"></script>
        <script src="{{ constants.TWIG_THEME }}assets/js/build/map.min.js?v={{ constants.FILE_VER }}" type="text/javascript" ></script>
        <script type="text/javascript">
            map.show('tour-map', [{{ Data.map_coords }}], "{{ Data.title }}");
        </script>
    {% endif %}
{% endblock %}