{% extends "index.tpl" %}

{% set metaProperties = {
    title: constants.SITE ~ ' - ' ~ langs.MetaTitle,
    description: langs.MetaDescr,
    keywords: langs.MetaKeywords,
    image: constants.TWIG_THEME ~ 'images/' ~ constants.SITE ~ constants.IMG_TYPE,
    url: '/'
} %}

{% block content %}
    <div class="section-top-50 section-md-top-80 section-bottom-80">
    	<div class="shell">
    		<div class="range">{{ Content|raw }}</div>
    	</div>
    </div>
{% endblock %}