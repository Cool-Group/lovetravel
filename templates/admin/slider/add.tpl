{% extends "admin/index.tpl" %}

{% block section %}
	<form action="{{ server.URI }}admin/addsliderpost/" class="admin-news-form" onsubmit="return admin.savePost(this);">
		<h1>სლაიდის დამატება</h1>
		{% for lang in unserialize(constants.LANGS)|keys %}
			<p class="m-top-20">სათაური 1 - {{ lang }}</p>
			<p class="m-top-5">
				<input type="text" name="Title-1-{{ lang }}" data-error="მიუთუთეთ სათაური 1 - {{ lang }}">
			</p>
			<p class="m-top-20">სათაური 2 - {{ lang }}</p>
			<p class="m-top-5">
				<input type="text" name="Title-2-{{ lang }}">
			</p>
		{% endfor %}
		<p class="m-top-20">რიგითობა</p>
		<p class="m-top-5">
			<input type="text" name="SortOrder" data-error="მიუთუთეთ რიგითობა" data-type="int" value="0">
		</p>
		<p class="m-top-20">სურათი</p>
		<div class="clearfix m-top-5">
            <ul class="clearfix files-list files-list-slider" data-types="[jpg, png, jpeg, gif]" data-max-files="1"
             data-name="File" data-index="0">
                <li class="files-list-new-file shadow-inset" onclick="_files.selectFiles(this);">
                    <span></span>
                    <p>ატვირთვა</p>
                </li>
            </ul>
        </div>
		<p class="m-top-20">
			<input type="submit" class="btn" value="დამატება">
		</p>
	</form>
	<form class="j-form files-list-form" name="files-list-form" data-index="0" method="post" enctype="multipart/form-data"
		 action="{{ server.URI }}file/uploadphoto/">
	    <input type="hidden" name="UploadedFiles" class="files-list-uploaded-files" value="0">
	    <input type="hidden" name="Type" value="Slider">
	    <input type="file" name="Photo" class="files-list-file" onchange="_files.uploadFiles(this);">
	</form>
{% endblock %}

{% block mypage_footer %}
    <script type="text/javascript" src="{{ constants.LIBS_URL }}ckeditor/ckeditor.js?v={{ constants.FILE_VER }}"></script>
{% endblock %}