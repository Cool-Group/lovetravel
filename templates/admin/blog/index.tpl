{% extends "admin/index.tpl" %}

{% block section %}
	<form class="admin-news-search clearfix" action="{{ server.URI }}admin/blog/">
		<div class="left">
			<a href="{{ server.URI }}admin/addblog/" class="btn btn-blue dib">დამატება</a>
		</div>
		<div class="right">
			<input type="text" placeholder="საძიებო სიტყვა" class="left m-left-5" name="Keyword" value="{{ get.Keyword }}">
			<input type="submit" class="btn btn-red left m-left-5" value="ძებნა">
		</div>
	</form>
	<ul class="admin-news clearfix">
		{% if NewsData|length %}
			{% for blog in NewsData %}
				<li>
					<a href="{{ server.URI }}blog/{{ blog.blog_id }}/" class="img col-3" target="_blank">
						<img src="{{ blog.photos_cnt ? constants.UPL_BLOG ~ 'thumbs/' ~ blog.blog_id ~ '_1' ~ constants.IMG_TYPE ~ '?v=' ~ blog.photo_ver : constants.NO_PHOTO }}">
					</a>
					<div class="descr col-7">
						<a class="h2" href="{{ server.URI }}blog/{{ blog.blog_id }}/">{{ blog.title }}</a>
						<p class="h2 m-top-5">{{ blog.short_descr }}</p>
					</div>
					<div class="options col-2">
						<div class="clearfix right">
							<span title="სტატუსი" class="status" onclick="admin.changePostStatus(this, 'blog', 'status_id', {{ blog.blog_id }});">
								<i class="icon-16 star-16 {{ blog.status_id ? 'green' : 'red' }}"></i>
							</span>
							<span title="წაშლა" class="delete" onclick="admin.deletePost(this, 'blog', {{ blog.blog_id }});">
								<i class="icon-16 del-16"></i>
							</span>
							<a href="{{ server.URI }}admin/editblog/{{ blog.blog_id }}/" title="რედაქტირება" class="edit">
								<i class="icon-16 edit-16"></i>
							</a>
						</div>
					</div>
				</li>
			{% endfor %}
		{% else %}
			<p class="data-not-found">ბლოგი არ მოიძებნა</p>
		{% endif %}
	</ul>
{% endblock %}