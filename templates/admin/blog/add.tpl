{% extends "admin/index.tpl" %}

{% block section %}
	<form action="{{ server.URI }}admin/addblogpost/" class="admin-news-form" onsubmit="return admin.savePost(this);">
		<h1>ბლოგის დამატება</h1>
		<p class="m-top-20">კატეგორია</p>
		<div class="clearfix m-top-5">
            <select data-error="მიუთითეთ კატეგორია" name="CatID">
            	<option value="">აირჩიეთ</option>
            	{% include "admin/list/select.tpl" with {list: Cats} only %}
            </select>
        </div>
		{% for lang in unserialize(constants.LANGS)|keys %}
			<p class="m-top-20">დასახელება - {{ lang }}</p>
			<p class="m-top-5">
				<input type="text" name="Title-{{ lang }}" data-error="მიუთუთეთ დასახელება - {{ lang }}">
			</p>
			<p class="m-top-20">მოკლე აღწერა - {{ lang }}</p>
			<div class="m-top-5">
				<textarea name="ShortDescr-{{ lang }}" class="short"></textarea>
			</div>
			<p class="m-top-20">აღწერა - {{ lang }}</p>
			<div class="m-top-5">
				<textarea name="Descr-{{ lang }}" data-ckeditor="true"></textarea>
			</div>
		{% endfor %}
		<p class="m-top-20">სურათი ({{ constants.BLOG_LARGE_PHOTO_WIDTH ~ 'X' ~ constants.BLOG_LARGE_PHOTO_HEIGHT }})</p>
		<div class="clearfix m-top-5">
            <ul class="clearfix files-list files-list-news" data-types="[jpg, png, jpeg, gif]" data-max-files="{{ constants.BLOG_MAX_PHOTOS }}"
             data-name="Photos" data-index="0">
                <li class="files-list-new-file shadow-inset" onclick="_files.selectFiles(this);">
                    <span></span>
                    <p>ატვირთვა</p>
                </li>
            </ul>
        </div>
		<p class="m-top-20">
			<input type="submit" class="btn" value="დამატება">
		</p>
	</form>
	<form class="j-form files-list-form" name="files-list-form" data-index="0" method="post" enctype="multipart/form-data"
		 action="{{ server.URI }}file/uploadphotos/">
	    <input type="hidden" name="UploadedFiles" class="files-list-uploaded-files" value="0">
	    <input type="hidden" name="PostTable" value="blog">
	    <input type="file" name="Files[]" class="files-list-file" onchange="_files.uploadFiles(this);">
	</form>
{% endblock %}

{% block mypage_footer %}
    <script type="text/javascript" src="{{ constants.LIBS_URL }}ckeditor/ckeditor.js?v={{ constants.FILE_VER }}"></script>
{% endblock %}