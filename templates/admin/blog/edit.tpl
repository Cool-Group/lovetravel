{% extends "admin/index.tpl" %}

{% block section %}
	<form action="{{ server.URI }}admin/editblogpost/" class="admin-news-form" onsubmit="return admin.savePost(this);">
		<h1>ბლოგის რედაქტირება</h1>
		<p class="m-top-20">კატეგორია</p>
		<div class="clearfix m-top-5">
            <select data-error="მიუთითეთ კატეგორია" name="CatID">
            	<option value="">აირჩიეთ</option>
            	{% include "admin/list/select.tpl" with {list: Cats, id: Data.Blog.cat_id} only %}
            </select>
        </div>
		{% for lang, lang_id in unserialize(constants.LANGS) %}
			<p class="m-top-20">დასახელება - {{ lang }}</p>
			<p class="m-top-5">
				<input type="text" name="Title-{{ lang }}" value="{{ Data.Descr[lang_id].title }}" data-error="მიუთუთეთ დასახელება - {{ lang }}">
			</p>
			<p class="m-top-20">მოკლე აღწერა - {{ lang }}</p>
			<div class="m-top-5">
				<textarea name="ShortDescr-{{ lang }}" class="short">{{ Data.Descr[lang_id].short_descr }}</textarea>
			</div>
			<p class="m-top-20">აღწერა - {{ lang }}</p>
			<div class="m-top-5">
				<textarea name="Descr-{{ lang }}" data-ckeditor="true">{{ Data.Descr[lang_id].descr }}</textarea>
			</div>
		{% endfor %}
		<p class="m-top-20">სურათი ({{ constants.BLOG_LARGE_PHOTO_WIDTH ~ 'X' ~ constants.BLOG_LARGE_PHOTO_HEIGHT }})</p>
		<div class="clearfix m-top-5">
            <ul class="clearfix files-list files-list-news" data-types="[jpg, png, jpeg, gif]" data-max-files="{{ constants.BLOG_MAX_PHOTOS }}"
            data-name="Photos" data-index="0">
                {% if Data.Blog.photos_cnt %}
	                {% for i in 1..Data.Blog.photos_cnt %}
						<li data-file="{{ constants.UPL_BLOG ~ 'thumbs/' ~ Data.Blog.blog_id ~ '_' ~ i ~ constants.IMG_TYPE }}">
							<img src="{{ constants.UPL_BLOG ~ 'thumbs/' ~ Data.Blog.blog_id ~ '_' ~ i ~ constants.IMG_TYPE ~ '?v=' ~ Data.Blog.photo_ver }}">
							<i onclick="_files.mainFile(this);" class="home" title="მთავარ სურათად დაყენება"></i>
							<i onclick="_files.removeFile(this);" title="წაშლა"></i>
						</li>
	                {% endfor %}
				{% endif %}
                <li class="files-list-new-file shadow-inset" onclick="_files.selectFiles(this);" {{ Data.Blog.photos_cnt == constants.BLOG_MAX_PHOTOS ? 'style="display:none;"' }}>
                    <span></span>
                    <p>ატვირთვა</p>
                </li>
            </ul>
        </div>
		<p class="m-top-20">
			<input type="hidden" id="BlogID" name="BlogID" value="{{ Data.Blog.blog_id }}">
			<input type="submit" class="btn" value="რედაქტირება">
		</p>
	</form>
	<form class="j-form files-list-form" name="files-list-form" data-index="0" method="post" enctype="multipart/form-data"
		 action="{{ server.URI }}file/uploadphotos/">
	    <input type="hidden" name="UploadedFiles" class="files-list-uploaded-files" value="{{ Data.Blog.photos_cnt }}">
	    <input type="hidden" name="PostTable" value="blog">
	    <input type="file" name="Files[]" class="files-list-file" onchange="_files.uploadFiles(this);">
	</form>
{% endblock %}

{% block mypage_footer %}
    <script type="text/javascript" src="{{ constants.LIBS_URL }}ckeditor/ckeditor.js?v={{ constants.FILE_VER }}"></script>
{% endblock %}