{% extends "admin/index.tpl" %}

{% block section %}
	<form action="{{ server.URI }}admin/editservicepost/" class="admin-news-form" onsubmit="return admin.savePost(this);">
		<h1>სერვისის რედაქტირება</h1>
		{% for lang, lang_id in unserialize(constants.LANGS) %}
			<p class="m-top-20">დასახელება - {{ lang }}</p>
			<p class="m-top-5">
				<input type="text" name="Title-{{ lang }}" value="{{ Data.Descr[lang_id].title }}" data-error="მიუთუთეთ დასახელება - {{ lang }}">
			</p>
			<p class="m-top-20">მოკლე აღწერა - {{ lang }}</p>
			<p class="m-top-5">
				<textarea name="ShortDescr-{{ lang }}" data-error="მიუთუთეთ მოკლე აღწერა - {{ lang }}">{{ Data.Descr[lang_id].short_descr }}</textarea>
			</p>
			<p class="m-top-20">აღწერა - {{ lang }}</p>
			<div class="m-top-5">
				<textarea name="Descr-{{ lang }}" data-ckeditor="true">{{ Data.Descr[lang_id].descr }}</textarea>
			</div>
		{% endfor %}
		<p class="m-top-20">სურათები ({{ constants.SERVICES_PHOTO_WIDTH ~ 'X' ~ constants.SERVICES_PHOTO_HEIGHT }})</p>
		<div class="clearfix m-top-5">
            <ul class="clearfix files-list files-list-tours" data-types="[jpg, png, jpeg, gif]" data-max-files="{{ constants.SERVICES_MAX_PHOTOS }}"
             data-name="File" data-index="0">
                {% if Data.Data.photos_cnt %}
					<li data-file="{{ constants.UPL_SERVICES ~ Data.Data.service_id ~ constants.IMG_TYPE }}">
						<img src="{{ constants.UPL_SERVICES ~ Data.Data.service_id ~ constants.IMG_TYPE ~ '?v=' ~ Data.Data.photo_ver }}">
						<i onclick="_files.mainFile(this);" class="home" title="მთავარ სურათად დაყენება"></i>
						<i onclick="_files.removeFile(this);" title="წაშლა"></i>
					</li>
                {% endif %}
                <li class="files-list-new-file shadow-inset" onclick="_files.selectFiles(this);" {{ Data.Data.photos_cnt == constants.SERVICES_MAX_PHOTOS ? 'style="display:none;"' }}>
                    <span></span>
                    <p>ატვირთვა</p>
                </li>
            </ul>
        </div>
		<p class="m-top-20">
			<input type="hidden" name="ServiceID" value="{{ Data.Data.service_id }}">
			<input type="submit" class="btn" value="რედაქტირება">
		</p>
	</form>
	<form class="j-form files-list-form" name="files-list-form" data-index="0" method="post" action="{{ server.URI }}file/uploadphoto/">
	    <input type="hidden" name="UploadedFiles" class="files-list-uploaded-files" value="{{ Data.Data.photos_cnt }}">
	    <input type="hidden" name="Type" value="Services">
	    <input type="file" name="Photo" class="files-list-file" onchange="_files.uploadFiles(this);">
	</form>
{% endblock %}

{% block mypage_footer %}
    <script type="text/javascript" src="{{ constants.LIBS_URL }}ckeditor/ckeditor.js?v={{ constants.FILE_VER }}"></script>
{% endblock %}