{% extends "admin/index.tpl" %}

{% block section %}
	<form class="admin-news-search clearfix" action="{{ server.URI }}admin/services/">
		<div class="left">
			<a href="{{ server.URI }}admin/addservice/" class="btn btn-blue dib">დამატება</a>
		</div>
		<div class="right">
			<input type="text" placeholder="საძიებო სიტყვა" class="left m-left-5" name="Keyword" value="{{ get.Keyword }}">
			<input type="submit" class="btn btn-red left m-left-5" value="ძებნა">
		</div>
	</form>
	<ul class="admin-news clearfix">
		{% if Data|length %}
			{% for val in Data %}
				<li>
					<a class="img col-3">
						<img src="{{ val.photos_cnt ? constants.UPL_SERVICES ~ val.service_id ~ constants.IMG_TYPE ~ '?v=' ~ val.photo_ver : constants.NO_PHOTO }}">
					</a>
					<div class="descr col-6">
						<a class="h2" href="{{ server.URI }}services/{{ val.service_id }}/">{{ val.title }}</a>
						<p class="h2 m-top-5">{{ val.short_descr }}</p>
					</div>
					<div class="options col-3">
						<div class="clearfix right">
							<span title="სტატუსი" class="status" onclick="admin.changePostStatus(this, 'services', 'status_id', {{ val.service_id }});">
								<i class="icon-16 star-16 {{ val.status_id ? 'green' : 'red' }}"></i>
							</span>
							<span title="წაშლა" class="delete" onclick="admin.deletePost(this, 'services', {{ val.service_id }});">
								<i class="icon-16 del-16"></i>
							</span>
							<a href="{{ server.URI }}admin/editservice/{{ val.service_id }}/" title="რედაქტირება" class="edit">
								<i class="icon-16 edit-16"></i>
							</a>
						</div>
					</div>
				</li>
			{% endfor %}
		{% else %}
			<p class="data-not-found">სერვისი არ მოიძებნა</p>
		{% endif %}
	</ul>
{% endblock %}