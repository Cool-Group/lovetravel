{% extends "admin/index.tpl" %}

{% block section %}
	<form class="admin-news-search clearfix" action="{{ server.URI }}admin/team/">
		<div class="left">
			<a href="{{ server.URI }}admin/addteam/" class="btn btn-blue dib">დამატება</a>
		</div>
		<div class="right">
			<input type="text" placeholder="საძიებო სიტყვა" class="left m-left-5" name="Keyword" value="{{ get.Keyword }}">
			<input type="submit" class="btn btn-red left m-left-5" value="ძებნა">
		</div>
	</form>
	<ul class="admin-news clearfix">
		{% if Data|length %}
			{% for val in Data %}
				<li>
					<a href="{{ server.URI }}team/{{ val.team_id }}/" class="img col-2" target="_blank">
						<img src="{{ val.has_photo ? constants.UPL_TEAM ~ val.team_id ~ constants.IMG_TYPE ~ '?v=' ~ val.photo_ver : constants.NO_PHOTO }}">
					</a>
					<div class="descr col-7">
						<a class="h2" href="{{ server.URI }}team/{{ val.team_id }}/">{{ val.fullname }}</a>
						<p class="h2 m-top-5">{{ val.position }}</p>
					</div>
					<div class="options col-3" data-cat="news" data-cat-id="team_id" data-id="126">
						<div class="clearfix right">
							<span title="სტატუსი" class="status" onclick="admin.changePostStatus(this, 'team', 'status_id', {{ val.team_id }});">
								<i class="icon-16 star-16 {{ val.status_id ? 'green' : 'red' }}"></i>
							</span>
							<span title="მთავარ გვერდზე გამოჩენა" class="status" onclick="admin.changePostStatus(this, 'team', 'is_main', {{ val.team_id }});">
								<i class="icon-16 slider-1-16 {{ val.is_main ? 'green' : 'red' }}"></i>
							</span>
							<span title="წაშლა" class="delete" onclick="admin.deletePost(this, 'team', {{ val.team_id }});">
								<i class="icon-16 del-16"></i>
							</span>
							<a href="{{ server.URI }}admin/editteam/{{ val.team_id }}/" title="რედაქტირება" class="edit">
								<i class="icon-16 edit-16"></i>
							</a>
						</div>
					</div>
				</li>
			{% endfor %}
		{% else %}
			<p class="data-not-found">გუნდის წევრები არ მოიძებნა</p>
		{% endif %}
{% endblock %}