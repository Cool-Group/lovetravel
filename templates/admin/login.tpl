{% extends "index.tpl" %}

{% block content %}
	<div class="container">
		<form class="form-signin" action="{{ server.URI }}admin/authorize/"
		 data-success-url="{{ server.URI }}admin/" onsubmit="return admin.logIn(this);">
	        <h2 class="form-signin-heading">Admin panel</h2>
	        <label for="inputEmail" class="sr-only">Email address</label>
	        <input type="email" id="Email" name="Email" class="form-control" placeholder="Email address" required="" autofocus="">
	        <label for="inputPassword" class="sr-only">Password</label>
	        <input type="password" id="Password" name="Password" class="form-control" placeholder="Password" required="">
	        <div class="form-group">
	        	<button class="btn btn-primary btn-block" type="submit">Sign in</button>
	        </div>
	        <a href="{{ server.URI }}">{{ constants.URL }}</a>
      </form>
	</div>
{% endblock %}

{% block footer %}
	<script type="text/javascript" src="{{ constants.TWIG_THEME }}assets/js/build/plugins/admin.min.js?v={{ constants.FILE_VER }}"></script>
{% endblock %}