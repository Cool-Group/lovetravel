{% extends "admin/index.tpl" %}

{% block section %}
	<form action="{{ server.URI }}admin/addtourpost/" class="admin-news-form" onsubmit="return admin.savePost(this);">
		<h1>ტურის დამატება</h1>
		<p class="m-top-20">კატეგორია</p>
		<div class="clearfix m-top-5">
            <select data-error="მიუთითეთ კატეგორია" name="CatID">
            	<option value="">აირჩიეთ</option>
            	{% include "admin/list/select.tpl" with {list: Cats, id: Data.Tour.cat_id, disabled: true} only %}
            </select>
        </div>
        <p class="m-top-20">ტურის ტიპი</p>
		<div class="clearfix m-top-5">
            <select data-error="მიუთითეთ ტურის ტიპი" name="TypeID">
            	<option value="">აირჩიეთ</option>
            	{% for val in Types %}
					<option value="{{ val.type_id }}">{{ val.title }}</option>
            	{% endfor %}
            </select>
        </div>
        <p class="m-top-20">სერვისები</p>
		<div class="m-top-5 clearfix">
			{% for val in Services %}
				<div class="col-4">
					<p><b>{{ val.title }}</b></p>
					<p class="clearfix">
						<input type="radio" name="Services[{{ val.service_id }}]" id="services-1-{{ val.service_id }}" value="1">
						<label for="services-1-{{ val.service_id }}">კი</label>
					</p>
					<p class="clearfix">
						<input type="radio" name="Services[{{ val.service_id }}]" id="services-2-{{ val.service_id }}" value="0" checked>
						<label for="services-2-{{ val.service_id }}">არა</label>
					</p>
					<p class="clearfix">
						<input type="radio" name="Services[{{ val.service_id }}]" id="services-3-{{ val.service_id }}" value="2">
						<label for="services-3-{{ val.service_id }}">დამატებითი</label>
					</p>
				</div>
			{% endfor %}
		</div>
		<p class="m-top-20">შენიშვნები</p>
		<div class="m-top-5 clearfix">
			{% for val in Notes %}
				<p class="clearfix">
					<input type="checkbox" name="Notes[]" id="note-{{ val.note_id }}" value="{{ val.note_id }}">
					<label for="note-{{ val.note_id }}">{{ val.title }}</label>
				</p>
			{% endfor %}
		</div>
		<p class="m-top-20">სასტუმროები</p>
		<div class="m-top-5">
			<textarea name="Hotels" data-ckeditor="true"></textarea>
		</div>
		<div class="m-top-20 clearfix">
			<div class="col-4">
				<p>ძველი ფასი</p>
				<p class="m-top-5">
					<input type="text" name="OldPrice">
				</p>
			</div>
			<div class="col-4">
				<p>ფასი</p>
				<p class="m-top-5">
					<input type="text" name="Price" data-type="double" data-error="მიუთუთეთ ფასი">
				</p>
			</div>
			<div class="col-4">
				<p>ვალუტა</p>
				<p class="m-top-5">
					<select name="CurrencyID">
						{% for val in Currencies %}
							<option value="{{ val.currency_id }}">{{ val.title }}</option>
						{% endfor %}
					</select>
				</p>
			</div>
		</div>
		<div class="m-top-20 clearfix">
			<div class="col-6">
				<p>პერიოდი - დან</p>
				<p class="m-top-5">
					<input type="text" name="PeriodFrom" data-datepicker="true" data-error="მიუთუთეთ პერიოდი - დან">
				</p>
			</div>
			<div class="col-6">
				<p>პერიოდი - მდე</p>
				<p class="m-top-5">
					<input type="text" name="PeriodTo" data-datepicker="true" data-error="მიუთუთეთ პერიოდი - მდე">
				</p>
			</div>
		</div>
		{% for lang in unserialize(constants.LANGS)|keys %}
			<p class="m-top-20">დასახელება - {{ lang }}</p>
			<p class="m-top-5">
				<input type="text" name="Title-{{ lang }}" data-error="მიუთუთეთ დასახელება - {{ lang }}">
			</p>
			<p class="m-top-20">მოკლე აღწერა - {{ lang }}</p>
			<div class="m-top-5">
				<textarea name="ShortDescr-{{ lang }}" data-ckeditor="true"></textarea>
			</div>
			<p class="m-top-20">აღწერა - {{ lang }}</p>
			<div class="m-top-5">
				<textarea name="Descr-{{ lang }}" data-ckeditor="true"></textarea>
			</div>
		{% endfor %}
		<p class="m-top-20">მონიშნეთ რუკაზე</p>
		<div class="m-top-5 clearfix">
			<input id="MapCoords" type="hidden" name="MapCoords" value="" />
            <div class="map-cont relative">
                <div id="map"></div>
                <input type="button" class="btn btn-yellow" value="რუკის გადიდება">
            </div>
		</div>
		<p class="m-top-20">სურათები ({{ constants.TOUR_LARGE_PHOTO_WIDTH ~ 'X' ~ constants.TOUR_LARGE_PHOTO_HEIGHT }})</p>
		<div class="clearfix m-top-5">
            <ul class="clearfix files-list files-list-tours" data-types="[jpg, png, jpeg, gif]" data-max-files="{{ constants.TOUR_MAX_PHOTOS }}"
             data-name="Photos" data-index="0">
                <li class="files-list-new-file shadow-inset" onclick="_files.selectFiles(this);">
                    <span></span>
                    <p>ატვირთვა</p>
                </li>
            </ul>
        </div>
		<p class="m-top-20">
			<input type="submit" class="btn" value="დამატება">
		</p>
	</form>
	<form class="j-form files-list-form" name="files-list-form" data-index="0" method="post" enctype="multipart/form-data"
		 action="{{ server.URI }}file/uploadphotos/">
	    <input type="hidden" name="UploadedFiles" class="files-list-uploaded-files" value="0">
	    <input type="hidden" name="PostTable" value="tours">
	    <input type="file" name="Files[]" class="files-list-file" multiple onchange="_files.uploadFiles(this);">
	</form>
{% endblock %}

{% block mypage_footer %}
	<script src="https://maps.googleapis.com/maps/api/js?language={{ globals.Lang.Code }}"></script>
    <script type="text/javascript" src="{{ constants.LIBS_URL }}ckeditor/ckeditor.js"></script>
    <script src="{{ constants.TWIG_THEME }}assets/js/build/plugins/map.min.js?v={{ constants.FILE_VER }}" type="text/javascript" ></script>
    <script type="text/javascript">map.init();</script>
{% endblock %}