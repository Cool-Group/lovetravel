{% extends "index.tpl" %}

{% block header %}
	<link href="{{ constants.TWIG_THEME }}assets/css/build/admin.min.css?v={{ constants.FILE_VER }}" rel="stylesheet">
{% endblock %}

{% block content %}
	<div class="wrapper">
		<div class="clearfix admin-top-menu">
			<ul>
				<li>
					<a href="{{ server.URI }}admin/"><b>სამართავი პანელი</b></a>
				</li>
				<li class="right">
					<a href="{{ server.URI }}admin/logout/">გასვლა</a>
				</li>
				<li class="right admin-langs">
					{% for lang in unserialize(constants.LANGS)|keys %}
						<a href="{{ constants.URL ~ lang ~ '/' ~ server.Page }}/" class="{{ lang == cookie.Lang ? 'active' }}" onclick="return changeLang('{{ lang }}');">
							<img src="{{ constants.TWIG_THEME }}assets/img/icons/{{ lang }}.svg" alt=""><span>{{ lang_titles[lang] }}</span></a>
						</a>
					{% endfor %}
				</li>
			</ul>
		</div>
		<div class="clearfix">
			<div class="col-3">
				<div class="box admin-left-menu">
					<!-- <a href="{{ server.URI }}admin/news/" {{ server.Action in ['index', 'news', 'addnews', 'editnews'] ? 'class="active"' }}>სიახლეები</a> -->
					<a href="{{ server.URI }}admin/tours/" {{ server.Action in ['', 'index', 'tours', 'addtour', 'edittour'] ? 'class="active"' }}>ტურები</a>
					<a href="{{ server.URI }}admin/cats/" {{ server.Action == 'cats' ? 'class="active"' }}>მიმართულებები</a>
					<a href="{{ server.URI }}admin/types/" {{ server.Action == 'types' ? 'class="active"' }}>კატეგორიები</a>
					<a href="{{ server.URI }}admin/notes/" {{ server.Action == 'notes' ? 'class="active"' }}>შენიშვნები</a>
					<a href="{{ server.URI }}admin/tourservices/" {{ server.Action == 'tourservices' ? 'class="active"' }}>ტურის სერვისები</a>
					<a href="{{ server.URI }}admin/langs/" {{ server.Action == 'langs' ? 'class="active"' }}>ენები</a>
					<a href="{{ server.URI }}admin/countries/" {{ server.Action == 'countries' ? 'class="active"' }}>ქვეყნები</a>
					<a href="{{ server.URI }}admin/services/" {{ server.Action in ['services', 'addservice', 'editservice'] ? 'class="active"' }}>სერვისები</a>
					<a href="{{ server.URI }}admin/blog/" {{ server.Action in ['blog', 'addblog', 'editblog'] ? 'class="active"' }}>ბლოგი</a>
					<a href="{{ server.URI }}admin/blogcats/" {{ server.Action == 'blogcats' ? 'class="active"' }}>ბლოგის კატეგორიები</a>
					<a href="{{ server.URI }}admin/slider/" {{ server.Action in ['slider', 'addslider', 'editslider'] ? 'class="active"' }}>სლაიდერი</a>
					<!-- <a href="{{ server.URI }}admin/about/" {{ server.Action == 'about' ? 'class="active"' }}>ჩვენ შესახებ</a>
					<a href="{{ server.URI }}admin/team/" {{ server.Action in ['team', 'addteam', 'editteam'] ? 'class="active"' }}>ჩვენი გუნდი</a> -->
					<a href="{{ server.URI }}admin/contact/" {{ server.Action == 'contact' ? 'class="active"' }}>კონტაქტები</a>
				</div>
			</div>
			<div class="col-9">
				<div class="m-left-20 clearfix box{{ server.Action == 'donations' ? ' p-20' }}">
					{% block section %}
					{% endblock %}
				</div>
			</div>
		</div>
	</div>
{% endblock %}

{% block footer %}
    <script type="text/javascript" src="{{ constants.LIBS_URL }}ckeditor/ckeditor.js?v={{ constants.FILE_VER }}"></script>
	<script type="text/javascript" src="{{ constants.TWIG_THEME }}assets/js/build/plugins/jform.min.js?v={{ constants.FILE_VER }}"></script>
	{% block mypage_footer %}
    {% endblock %}
	<script type="text/javascript" src="{{ constants.TWIG_THEME }}assets/js/build/plugins/admin.min.js?v={{ constants.FILE_VER }}"></script>
	<script type="text/javascript" src="{{ constants.TWIG_THEME }}assets/js/build/plugins/files.min.js?v={{ constants.FILE_VER }}"></script>
{% endblock %}