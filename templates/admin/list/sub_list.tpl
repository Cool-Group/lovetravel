{% for val in list %}
	<li class="clearfix {{ val.children ? 'sub' }}" data-id="{{ val.id }}">
		<div class="clearfix">
			{% if val.title_2 is defined %}
				<span class="col-3">{{ (val.title_1 ? val.title_1 : "&nbsp;")|raw }}</span>
				<span class="col-2">{{ (val.title_2 ? val.title_2 : "&nbsp;")|raw }}</span>
				<span class="col-4 title">{{ (val.title ? val.title : "&nbsp;")|raw }}</span>
			{% elseif val.title_1 is defined %}
				<span class="col-3">{{ (val.title_1 ? val.title_1 : "&nbsp;")|raw }}</span>
				<span class="col-6 title">{{ (val.title ? val.title : "&nbsp;")|raw }}</span>
			{% else %}
				<span class="title">{{ (val.title ? val.title : "&nbsp;")|raw }}</span>
			{% endif %}
			<div class="options clearfix right">
				{% if 'status' in list_params.options %}
					<span title="სტატუსი" class="shadow-inset" onclick="list.changeStatus(this, '{{ list_params.post_table }}', {{ val.id }});">
						<i class="icon-16 star-16 {{ val.status_id ? 'green' : 'red' }}"></i>
					</span>
				{% endif %}
				{% if 'sort' in list_params.options %}
					<span title="ზემოთ" class="shadow-inset" onclick="list.changeSortOrder(this, '{{ list_params.post_table }}', {{ val.id }}, 0);"><i class="icon-16 arrow-top-1-16"></i></span>
					<span title="ქვემოთ" class="shadow-inset" onclick="list.changeSortOrder(this, '{{ list_params.post_table }}', {{ val.id }}, 1);"><i class="icon-16 arrow-bottom-1-16"></i></span>
				{% endif %}
				{% if list_params.add_sub_item_title %}
					<span title="{{ list_params.add_sub_item_title }}" class="shadow-inset"
					 onclick="list.new('{{ list_params.post_table }}', {{ val.id }}, '{{ list_params.add_sub_item_title }}', '{{ list_params.edit_item_title }}', '{{ list_params.add_sub_item_title }}');">
						<i class="icon-16 add-16"></i>
					</span>
				{% endif %}
				{% if 'edit' in list_params.options %}
					<span title="რედაქტირება" class="shadow-inset" onclick="list.view('{{ list_params.post_table }}', {{ val.id }}, '{{ list_params.edit_item_title }}');">
						<i class="icon-16 edit-16"></i>
					</span>
				{% endif %}
				{% if 'delete' in list_params.options %}
					<span title="წაშლა" class="delete shadow-inset" onclick="list.delete(this, '{{ list_params.post_table }}', {{ val.id }});"><i class="icon-16 del-16"></i></span>
				{% endif %}
			</div>
		</div>
		{% if val.children %}
			<ul>
				{% include "admin/list/sub_list.tpl" with {list: val.children, list_params: list_params} %}
			</ul>
		{% else %}
			<ul></ul>
		{% endif %}
	</li>
{% endfor %}