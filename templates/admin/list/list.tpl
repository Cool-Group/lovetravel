{% extends "admin/index.tpl" %}

{% block section %}
	<ul class="list">
		<li class="sub-list" data-id="0">
			<div>
				<b>{{ list_params.list_title }}</b>
				{% if list_params.add_item_title %}
					<div class="options clearfix right">
						<span title="{{ list_params.add_item_title }}" class="delete shadow-inset"
						 onclick="list.new('{{ list_params.post_table }}', 0, '{{ list_params.add_item_title }}', '{{ list_params.edit_item_title }}', '{{ list_params.add_sub_item_title }}');">
							<i class="icon-16 add-16"></i>
						</span>
					</div>
				{% endif %}
			</div>
			<ul>
				{% include "admin/list/sub_list.tpl" with {list: list, list_params: list_params} only %}
			</ul>
		</li>
	</ul>
{% endblock %}