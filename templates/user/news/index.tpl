{% extends "admin/index.tpl" %}

{% block section %}
	<form class="admin-news-search clearfix" action="{{ server.URI }}admin/news/">
		<div class="left">
			<a href="{{ server.URI }}admin/addnews/" class="btn btn-blue dib">დამატება</a>
		</div>
		<div class="right">
			<input type="text" placeholder="საძიებო სიტყვა" class="left m-left-5" name="Keyword" value="{{ get.Keyword }}">
			<input type="submit" class="btn btn-red left m-left-5" value="ძებნა">
		</div>
	</form>
	<ul class="admin-news clearfix">
		{% if NewsData|length %}
			{% for news in NewsData %}
				<li>
					<a href="{{ server.URI }}news/{{ news.news_id }}/" class="img col-3" target="_blank">
						<img src="{{ news.photos_cnt ? constants.UPL_NEWS ~ 'thumbs/' ~ news.news_id ~ '_1' ~ constants.IMG_TYPE ~ '?v=' ~ news.photo_ver : constants.NO_PHOTO }}">
					</a>
					<div class="descr col-7">
						<a class="h2" href="{{ server.URI }}news/{{ news.news_id }}/">{{ news.title }}</a>
						<p class="h2 m-top-5">{{ news.short_descr }}</p>
					</div>
					<div class="options col-2" data-cat="news" data-cat-id="news_id" data-id="126">
						<div class="clearfix right">
							<span title="სტატუსი" class="status" onclick="admin.changePostStatus(this, 'news', 'status_id', {{ news.news_id }});">
								<i class="icon-16 star-16 {{ news.status_id ? 'green' : 'red' }}"></i>
							</span>
							<span title="წაშლა" class="delete" onclick="admin.deletePost(this, 'news', {{ news.news_id }});">
								<i class="icon-16 del-16"></i>
							</span>
							<a href="{{ server.URI }}admin/editnews/{{ news.news_id }}/" title="რედაქტირება" class="edit">
								<i class="icon-16 edit-16"></i>
							</a>
							{% if news.news_type == 2 %}
							<a href="#" title="ბლოგი" class="blog">
								<i class="icon-16">B</i>
							</a>
							{% endif %}
						</div>
					</div>
				</li>
			{% endfor %}
		{% else %}
			<p class="data-not-found">სიახლე არ მოიძებნა</p>
		{% endif %}
	</ul>
{% endblock %}