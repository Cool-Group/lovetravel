{% extends "admin/index.tpl" %}

{% block section %}
	<form action="{{ server.URI }}admin/addnewspost/" class="admin-news-form" onsubmit="return admin.savePost(this);">
		<h1>სიახლის დამატება</h1>
		{% for lang in unserialize(constants.LANGS)|keys %}
			<p class="m-top-20">დასახელება - {{ lang }}</p>
			<p class="m-top-5">
				<input type="text" name="Title-{{ lang }}" data-error="მიუთუთეთ დასახელება - {{ lang }}">
			</p>
			<p class="m-top-20">მოკლე აღწერა - {{ lang }}</p>
			<div class="m-top-5">
				<textarea name="ShortDescr-{{ lang }}" class="short"></textarea>
			</div>
			<p class="m-top-20">აღწერა - {{ lang }}</p>
			<div class="m-top-5">
				<textarea name="Descr-{{ lang }}" data-ckeditor="true"></textarea>
			</div>
		{% endfor %}
		<p class="m-top-20">სურათი ({{ constants.NEWS_LARGE_PHOTO_WIDTH ~ 'X' ~ constants.NEWS_LARGE_PHOTO_HEIGHT }})</p>
		<div class="clearfix m-top-5">
            <ul class="clearfix files-list files-list-news" data-types="[jpg, png, jpeg, gif]" data-max-files="{{ constants.NEWS_MAX_PHOTOS }}"
             data-name="Photos" data-index="0">
                <li class="files-list-new-file shadow-inset" onclick="_files.selectFiles(this);">
                    <span></span>
                    <p>ატვირთვა</p>
                </li>
            </ul>
        </div>
		<p class="m-top-20">
			<input type="submit" class="btn" value="დამატება">
		</p>
	</form>
	<form class="j-form files-list-form" name="files-list-form" data-index="0" method="post" enctype="multipart/form-data"
		 action="{{ server.URI }}file/uploadnewsphotos/">
	    <input type="hidden" name="UploadedFiles" class="files-list-uploaded-files" value="0">
	    <input type="file" name="Files[]" class="files-list-file" onchange="_files.uploadFiles(this);">
	</form>
{% endblock %}

{% block mypage_footer %}
    <script type="text/javascript" src="{{ constants.LIBS_URL }}ckeditor/ckeditor.js?v={{ constants.FILE_VER }}"></script>
{% endblock %}