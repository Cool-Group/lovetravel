{% extends "user/index.tpl" %}

{% set metaProperties = {
    title: constants.SITE ~ ' - ' ~ langs.MetaTitle,
    description: langs.MetaDescr,
    keywords: langs.MetaKeywords,
    image: constants.TWIG_THEME ~ 'images/' ~ constants.SITE ~ constants.IMG_TYPE,
    url: '/'
} %}

{% block quest_section %}
	<div class="mt-5 mb-5">
		{% if EmailActivation %}
			{{ Message(langs.EmailActivationSuccess, false, 1)|raw }}
		{% else %}
			{{ Message(langs.EmailActivationError)|raw }}
		{% endif %}
	</div>
{% endblock %}