{% extends "user/index.tpl" %}

{% set metaProperties = {
    title: constants.SITE ~ ' - ' ~ langs.MetaTitle,
    description: langs.MetaDescr,
    keywords: langs.MetaKeywords,
    image: constants.TWIG_THEME ~ 'images/' ~ constants.SITE ~ constants.IMG_TYPE,
    url: '/'
} %}

{% block quest_section %}
	{% if EmailHash %}
		<div class="row d-block section-row no-gutters">
			<span class="line"></span>
			<h1><i class="ic-user"></i> {{ langs.ChangePassword }}</h1>
		</div>
		<div class="row user-forms-row">
			<div class="col-md-4">
				<form action="{{ server.URI }}user/setnewpassword/" data-success-url="{{ server.URI }}user/login/" onsubmit="return user.changePassword(this, 1);">
					<div class="form-group mb-3">
						<label for="formGroupExampleInput2">{{ langs.NewPassword }}</label>
						<input type="password" name="Password" class="form-control" id="formGroupExampleInput2" minlength="6" data-minlength="6" data-error="{{ langs.WriteNewPassword }}">
					</div>
					<div class="form-group mb-3">
						<label for="formGroupExampleInput2">{{ langs.RetypeNewPassword }}</label>
						<input type="password" class="form-control" id="formGroupExampleInput2" minlength="6" data-equals="Password" data-error="{{ langs.PasswordsDontMatch }}">
					</div>
					{% include "captcha/index.tpl" %}
					<div class="form-group mt-5">
						<input type="hidden" name="Hash" value="{{ get.Hash }}">
						<button class="btn btn-primary btn-block">{{ langs.Save }}</button>
					</div>
				</form>
			</div>
		</div>
	{% else %}
		<div class="mt-5 mb-5">
			{{ Message(langs.NewPasswordError)|raw }}
		</div>
	{% endif %}
{% endblock %}