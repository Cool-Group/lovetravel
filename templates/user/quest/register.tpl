{% extends "user/index.tpl" %}

{% set metaProperties = {
    title: constants.SITE ~ ' - ' ~ langs.MetaTitle,
    description: langs.MetaDescr,
    keywords: langs.MetaKeywords,
    image: constants.TWIG_THEME ~ 'images/' ~ constants.SITE ~ constants.IMG_TYPE,
    url: '/'
} %}

{% block quest_section %}
	<div class="row d-block section-row no-gutters">
		<span class="line"></span>
		<h1><i class="ic-user"></i> {{ langs.Register }}</h1>
	</div>
	<div class="row user-forms-row contact-form">
		<div class="col-md-4">
			<form action="{{ server.URI }}user/add/" data-success-url="{{ server.URI }}user/login/" class="user-forms" onsubmit="return user.register(this);">
				<div class="form-group mb-3">
					<label for="1"><i>{{ langs.Name }}</i></label>
					<input type="text" name="Name" class="form-control" data-error="{{ langs.WriteName }}">
				</div>
				<div class="form-group mb-3">
					<label for="1"><i>{{ langs.SurName }}</i></label>
					<input type="text" name="SurName" class="form-control" data-error="{{ langs.WriteSurName }}">
				</div>
				<div class="form-group mb-3 ">
					<label for="1"><i>{{ langs.Gender }}</i></label>
					<div class="row pl-2">
						<div class="col-4">
							<label class="radio-label" for="11">{{ langs.Male }}</label>
							<input id="11" name="GenderID" type="radio" value="1" class="form-control" checked="">
						</div>
						<div class="col">
							<label class="radio-label" for="12">{{ langs.Female }}</label>
							<input id="12" name="GenderID" type="radio" value="2" class="form-control">
						</div>
					</div>
				</div>
				<div class="form-group mb-3">
					<label for="1"><i>{{ langs.BirthDate }}</i></label>
					<input id="datepicker1" type="text" name="BirthDate" class="form-control" data-error="{{ langs.SelectBirthDate }}">
					<i class="ic-arrow-down select-arrow"></i>
				</div>
				<div class="form-group mb-3">
					<label for="1"><i>{{ langs.Country }}</i></label>
					<select name="CountryID" id="" class="form-control" data-error="{{ langs.SelectCountry }}">
						<option value="0">{{ langs.Select }}</option>
						{% for key, val in Countries %}
							<option value="{{ key }}">{{ val.title }}</option>
						{% endfor %}
					</select>
					<i class="ic-arrow-down select-arrow"></i>
				</div>
				<div class="form-group mb-3">
					<label for="1"><i>{{ langs.Address }}</i></label>
					<input type="text" name="Address" class="form-control" data-error="{{ langs.WriteAddress }}">
				</div>
				<div class="form-group mb-3">
					<label for="1"><i>{{ langs.ContactPhone }}</i></label>
					<!-- <div class="d-flex">
						<input type="text" class="form-control col-3 border-right-0 rounded-0 rounded-left">
						<input type="text" class="form-control col-9 rounded-0 rounded-right">
					</div> -->
					<input type="text" name="Phone" class="form-control" data-error="{{ langs.WriteContactPhone }}">
				</div>
				<div class="form-group mb-3">
					<label for="1"><i>{{ langs.Email }}</i></label>
					<input type="text" name="Email" id="Email" class="form-control" data-type="email" data-error="{{ langs.WriteEmail }}">
				</div>
				<div class="form-group mb-3">
					<label for="1"><i>{{ langs.Password }}</i></label>
					<input type="password" name="Password" class="form-control" data-minlength="6" data-error="{{ langs.WritePassword }}">
				</div>
				<div class="form-group mb-3">
					<label for="1"><i>{{ langs.RetypePassword }}</i></label>
					<input type="password" class="form-control" data-equals="Password" data-error="{{ langs.PasswordsDontMatch }}">
				</div>
				<div class="register-captcha">
					{% include "captcha/index.tpl" %}
				</div>
				<div class="form-group mt-5">
					<button class="btn btn-primary btn-block">{{ langs.Register }}</button>
				</div>
			</form>
		</div>
	</div>
{% endblock %}