{% extends "user/index.tpl" %}

{% set metaProperties = {
    title: constants.SITE ~ ' - ' ~ langs.MetaTitle,
    description: langs.MetaDescr,
    keywords: langs.MetaKeywords,
    image: constants.TWIG_THEME ~ 'images/' ~ constants.SITE ~ constants.IMG_TYPE,
    url: '/'
} %}

{% block quest_section %}
	<div class="row d-block section-row no-gutters">
		<span class="line"></span>
		<h1><i class="ic-user"></i> {{ langs.RecoverPassword }}</h1>
	</div>
	<div class="row user-forms-row">
		<div class="col-md-4">
			<form action="{{ server.URI }}user/recoverpassword/" class="user-forms" data-success-url="{{ server.URI }}user/login/" onsubmit="return user.recover(this);">
				<div class="form-group mb-3">
					<label for="1"><i>{{ langs.Email }}</i></label>
					<input type="text" name="Email" id="Email" class="form-control" data-type="email" data-error="{{ langs.WriteEmail }}">
				</div>
				<div class="register-captcha">
					{% include "captcha/index.tpl" %}
				</div>
				<div class="form-group mt-5">
					<button class="btn btn-primary btn-block">{{ langs.Continue }}</button>
				</div>
			</form>
		</div>
	</div>
{% endblock %}