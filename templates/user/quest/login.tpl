{% extends "user/index.tpl" %}

{% set metaProperties = {
    title: constants.SITE ~ ' - ' ~ langs.MetaTitle,
    description: langs.MetaDescr,
    keywords: langs.MetaKeywords,
    image: constants.TWIG_THEME ~ 'images/' ~ constants.SITE ~ constants.IMG_TYPE,
    url: '/'
} %}

{% block quest_section %}
	<div class="row d-block section-row no-gutters">
		<span class="line"></span>
		<h1><i class="ic-user"></i> {{ langs.Authorize }}</h1>
	</div>
	<div class="row user-forms-row">
		<div class="col-md-4">
			<form action="{{ server.URI }}user/authorize/" data-success-url="{{ get.Continue ? get.Continue : server.URI ~ 'user/' }}"
			 data-resendactivationemail-url="{{ server.URI }}user/resendactivationemail/" class="user-forms" onsubmit="return user.logIn(this);">
				<div class="form-group mb-3">
					<label for="1"><i>{{ langs.Email }}</i></label>
					<input type="text" name="Email" id="Email" class="form-control" data-type="email" data-error="{{ langs.WriteEmail }}">
				</div>
				<div class="form-group mb-3">
					<label for="1"><i>{{ langs.Password }}</i></label>
					<input type="password" name="Password" id="Password" class="form-control" data-minlength="6" data-error="{{ langs.WritePassword }}">
				</div>
				<div class="form-group mt-5">
					<button class="btn btn-primary btn-block">{{ langs.LogIn }}</button>
				</div>
				<div class="form-group mt-3 mb-5">
					<label for="1"><i>{{ langs.ForgotPassword }}</i></label>
					<a href="{{ server.URI }}user/recover/" class="btn btn-default btn-block">{{ langs.RecoverPassword }}</a>
				</div>
			</form>
		</div>
	</div>
{% endblock %}