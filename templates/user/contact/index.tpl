{% extends "admin/index.tpl" %}

{% block section %}
	<form action="{{ server.URI }}admin/editcontact/" class="admin-news-form" onsubmit="return admin.savePost(this, true);">
		<h1>საკონტაქტო მონაცემების შეცვლა</h1>
		<p class="m-top-20">მობილური</p>
		<p class="m-top-5">
			<input type="text" name="Mobile" value="{{ Data.Data.mobile }}" data-error="მიუთუთეთ მობილური">
		</p>
		<p class="m-top-20">ტელეფონის ნომერი</p>
		<p class="m-top-5">
			<input type="text" name="Phone" value="{{ Data.Data.phone }}" data-error="მიუთუთეთ ტელეფონის ნომერი">
		</p>
		<p class="m-top-20">E-mail</p>
		<p class="m-top-5">
			<input type="text" name="Email" value="{{ Data.Data.email }}" data-error="მიუთუთეთ E-mail" data-type="email">
		</p>
		{% for lang, lang_id in unserialize(constants.LANGS) %}
			<p class="m-top-20">მისამართი - {{ lang }}</p>
			<p class="m-top-5">
				<input type="text" name="Address-{{ lang }}" value="{{ Data.Descr[lang_id].address }}" data-error="მიუთუთეთ მისამართი - {{ lang }}">
			</p>
			<p class="m-top-20">მოკლე აღწერა - {{ lang }}</p>
			<p class="m-top-5">
				<textarea name="ShortDescr-{{ lang }}" data-error="მიუთუთეთ მოკლე აღწერა - {{ lang }}">{{ Data.Descr[lang_id].short_descr }}</textarea>
			</p>
			<p class="m-top-20">სამუშაო საათები - {{ lang }}</p>
			<p class="m-top-5">
				<input type="text" name="OpenHours-{{ lang }}" value="{{ Data.Descr[lang_id].open_hours }}" data-error="სამუშაო საათები - {{ lang }}">
			</p>
		{% endfor %}
		{% set social_media = json_decode(Data.Data.social_media, true) %}
		{% for id, title in unserialize(constants.SOCIAL_MEDIA) %}
			<p class="m-top-20">{{ title|capitalize }} page</p>
			<p class="m-top-5">
				<input type="text" name="SocialMedia[{{ id }}]" value="{{ social_media[id] }}">
			</p>
		{% endfor %}
		<p class="m-top-20">
			<input type="submit" class="btn" value="რედაქტირება">
		</p>
	</form>
{% endblock %}

{% block mypage_footer %}
    <script type="text/javascript" src="{{ constants.LIBS_URL }}ckeditor/ckeditor.js?v={{ constants.FILE_VER }}"></script>
{% endblock %}