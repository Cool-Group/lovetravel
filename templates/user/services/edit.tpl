{% extends "admin/index.tpl" %}

{% block section %}
	<form action="{{ server.URI }}admin/editservicepost/" class="admin-news-form" onsubmit="return admin.savePost(this);">
		<h1>სერვისის რედაქტირება</h1>
		{% for lang, lang_id in unserialize(constants.LANGS) %}
			<p class="m-top-20">დასახელება - {{ lang }}</p>
			<p class="m-top-5">
				<input type="text" name="Title-{{ lang }}" value="{{ Data.Descr[lang_id].title }}" data-error="მიუთუთეთ დასახელება - {{ lang }}">
			</p>
			<p class="m-top-20">აღწერა - {{ lang }}</p>
			<div class="m-top-5">
				<textarea name="Descr-{{ lang }}" data-ckeditor="true">{{ Data.Descr[lang_id].descr }}</textarea>
			</div>
		{% endfor %}
		<p class="m-top-20">
			<input type="hidden" name="ServiceID" value="{{ Data.Data.service_id }}">
			<input type="submit" class="btn" value="რედაქტირება">
		</p>
	</form>
{% endblock %}

{% block mypage_footer %}
    <script type="text/javascript" src="{{ constants.LIBS_URL }}ckeditor/ckeditor.js?v={{ constants.FILE_VER }}"></script>
{% endblock %}