{% extends "admin/index.tpl" %}

{% block section %}
	<form action="{{ server.URI }}admin/addservicepost/" class="admin-news-form" onsubmit="return admin.savePost(this);">
		<h1>სერვისის დამატება</h1>
		{% for lang in unserialize(constants.LANGS)|keys %}
			<p class="m-top-20">დასახელება - {{ lang }}</p>
			<p class="m-top-5">
				<input type="text" name="Title-{{ lang }}" data-error="მიუთუთეთ დასახელება - {{ lang }}">
			</p>
			<p class="m-top-20">აღწერა - {{ lang }}</p>
			<div class="m-top-5">
				<textarea name="Descr-{{ lang }}" data-ckeditor="true"></textarea>
			</div>
		{% endfor %}
		<p class="m-top-20">
			<input type="submit" class="btn" value="დამატება">
		</p>
	</form>
{% endblock %}

{% block mypage_footer %}
    <script type="text/javascript" src="{{ constants.LIBS_URL }}ckeditor/ckeditor.js?v={{ constants.FILE_VER }}"></script>
{% endblock %}