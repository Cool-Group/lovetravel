{% extends "index.tpl" %}

{% block content %}
	<!-- user page-->
	<div class="container">
		{% if session.UserID %}
			<div class="row d-block section-row no-gutters">
				<span class="line"></span>
				<h1>{{ langs.MyPage }}</h1>
			</div>
			<div class="row user-title py-4">
				<div class="col-md-3"></div>
				<div class="col-md-9">{{ sectionProperties.title }}</div>
			</div>
			<div class="row user-nav-row mb-5">
				<div class="col-md-3 ">
					<div class="border-top pt-3">
						{% set menu = {
							account: langs.PersonalData,
							editaccount: langs.EditAccount,
							changepassword: langs.ChangePassword,
							favorites: langs.MyFavorites,
							logout: langs.LogOut,
						} %}
						{% for key, val in menu %}
							<a class="d-block {{ key == server.Action or (loop.first and server.Action == 'index') ? 'active' }}" href="{{ server.URI }}user/{{ key }}/">{{ val }}</a>
						{% endfor %}
					</div>
				</div>
				<div class="col-md-9 border-top pt-3">
					{% block section %}
					{% endblock %}
				</div>
			</div>
		{% else %}
			{% block quest_section %}
			{% endblock %}
		{% endif %}
	</div>
	<!--  user page -->
{% endblock %}

{% block footer %}
	<!-- <script type="text/javascript" src="{{ constants.TWIG_THEME }}assets/js/build/plugins/jform.min.js?v={{ constants.FILE_VER }}"></script> -->
	{% block mypage_footer %}
    {% endblock %}
	<script type="text/javascript" src="{{ constants.TWIG_THEME }}assets/js/src/plugins/user.js?v={{ constants.FILE_VER }}"></script>
	<!-- <script type="text/javascript" src="{{ constants.TWIG_THEME }}assets/js/build/plugins/files.min.js?v={{ constants.FILE_VER }}"></script> -->
{% endblock %}