{% extends "admin/index.tpl" %}

{% block section %}
	<form class="admin-news-search clearfix" action="{{ server.URI }}admin/slider/">
		<div class="left">
			<a href="{{ server.URI }}admin/addslider/" class="btn btn-blue dib">დამატება</a>
		</div>
	</form>
	<ul class="admin-news clearfix">
		{% if Data|length %}
			{% for slider in Data %}
				<li>
					<a class="img col-3">
						<img src="{{ slider.has_photo ? constants.UPL_SLIDER ~ slider.slider_id ~ constants.IMG_TYPE ~ '?v=' ~ slider.photo_ver : constants.NO_PHOTO }}">
					</a>
					<div class="descr col-7">
						<p class="h2">{{ slider.title_1 }}</p>
						<p class="h2">{{ slider.title_2 }}</p>
						<p class="h2">რიგითობა: {{ slider.sort_order }}</p>
					</div>
					<div class="options col-2">
						<div class="clearfix right">
							<span title="სტატუსი" class="status" onclick="admin.changePostStatus(this, 'slider', 'status_id', {{ slider.slider_id }});">
								<i class="icon-16 star-16 {{ slider.status_id ? 'green' : 'red' }}"></i>
							</span>
							<span title="წაშლა" class="delete" onclick="admin.deletePost(this, 'slider', {{ slider.slider_id }});">
								<i class="icon-16 del-16"></i>
							</span>
							<a href="{{ server.URI }}admin/editslider/{{ slider.slider_id }}/" title="რედაქტირება" class="edit">
								<i class="icon-16 edit-16"></i>
							</a>
						</div>
					</div>
				</li>
			{% endfor %}
		{% else %}
			<p class="data-not-found">სლაიდერი არ მოიძებნა</p>
		{% endif %}
	</ul>
{% endblock %}