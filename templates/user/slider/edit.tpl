{% extends "admin/index.tpl" %}

{% block section %}
	<form action="{{ server.URI }}admin/editsliderpost/" class="admin-news-form" onsubmit="return admin.savePost(this);">
		<h1>სლაიდის რედაქტირება</h1>
		{% for lang, lang_id in unserialize(constants.LANGS) %}
			<p class="m-top-20">სათაური 1 - {{ lang }}</p>
			<p class="m-top-5">
				<input type="text" name="Title-1-{{ lang }}" value="{{ Data.Descr[lang_id].title_1 }}" data-error="მიუთუთეთ სათაური 1 - {{ lang }}">
			</p>
			<p class="m-top-20">სათაური 2 - {{ lang }}</p>
			<p class="m-top-5">
				<input type="text" name="Title-2-{{ lang }}" value="{{ Data.Descr[lang_id].title_2 }}">
			</p>
		{% endfor %}
		<p class="m-top-20">რიგითობა</p>
		<p class="m-top-5">
			<input type="text" name="SortOrder" data-error="მიუთუთეთ რიგითობა" data-type="int" value="{{ Data.Data.sort_order }}">
		</p>
		<p class="m-top-20">სურათი</p>
		<div class="clearfix m-top-5">
            <ul class="clearfix files-list files-list-slider" data-types="[jpg, png, jpeg, gif]" data-max-files="1"
            data-name="File" data-index="0">
                {% if Data.Data.has_photo %}
					<li data-file="{{ constants.UPL_SLIDER ~ Data.Data.slider_id ~ constants.IMG_TYPE }}">
						<img src="{{ constants.UPL_SLIDER ~ Data.Data.slider_id ~ constants.IMG_TYPE ~ '?v=' ~ Data.Data.photo_ver }}">
						<i onclick="_files.mainFile(this);" class="home" title="მთავარ სურათად დაყენება"></i>
						<i onclick="_files.removeFile(this);" title="წაშლა"></i>
					</li>
				{% endif %}
                <li class="files-list-new-file shadow-inset" onclick="_files.selectFiles(this);" {{ Data.Data.has_photo ? 'style="display:none;"' }}>
                    <span></span>
                    <p>ატვირთვა</p>
                </li>
            </ul>
        </div>
		<p class="m-top-20">
			<input type="hidden" id="SliderID" name="SliderID" value="{{ Data.Data.slider_id }}">
			<input type="submit" class="btn" value="რედაქტირება">
		</p>
	</form>
	<form class="j-form files-list-form" name="files-list-form" data-index="0" method="post" enctype="multipart/form-data"
		 action="{{ server.URI }}file/uploadphoto/">
	    <input type="hidden" name="UploadedFiles" class="files-list-uploaded-files" value="{{ Data.Data.has_photo }}">
	    <input type="hidden" name="Type" value="Slider">
	    <input type="file" name="Photo" class="files-list-file" onchange="_files.uploadFiles(this);">
	</form>
{% endblock %}

{% block mypage_footer %}
    <script type="text/javascript" src="{{ constants.LIBS_URL }}ckeditor/ckeditor.js?v={{ constants.FILE_VER }}"></script>
{% endblock %}