{% extends "popup/index.tpl" %}

{% set popupProperties = {
    title : get.popup_title,
} %}

{% block content %}
	<form class="clearfix p-20 block-md" action="{{ server.URI }}admin/editlistitem/" onsubmit="return list.edit(this);">
		{% for lang, lang_id in unserialize(constants.LANGS) %}
			<p{{ loop.first == false ? " class='m-top-20'" : "" }} >დასახელება - {{ lang }}</p>
			<div class="clearfix m-top-5">
				<input type="text" name="title-{{ lang }}" data-error="მიუთითეთ დასახელება - {{ lang }}" value="{{ Data.Descr[lang_id].title }}">
			</div>
		{% endfor %}
		{% if Data.Data.has_photo is defined %}
			<p class="m-top-20">სურათი ({{ constants.CAT_PHOTO_SIZE ~ 'X' ~ constants.CAT_PHOTO_SIZE }})</p>
			<div class="clearfix m-top-5">
	            <ul class="clearfix files-list files-list-cats" data-types="[jpg, png, jpeg, gif]" data-max-files="1"
	             data-name="File" data-index="0">
	                {% if Data.Data.has_photo %}
						<li data-file="{{ Data.Data.upl_dir ~ get.id }}.png">
							<img src="{{ Data.Data.upl_dir ~ get.id ~ '.png?v=' ~ Data.Data.photo_ver }}">
							<i onclick="_files.mainFile(this);" class="home" title="მთავარ სურათად დაყენება"></i>
							<i onclick="_files.removeFile(this);" title="წაშლა"></i>
						</li>
					{% endif %}
	                <li class="files-list-new-file shadow-inset" onclick="_files.selectFiles(this);" {{ Data.Data.has_photo ? 'style="display:none;"' }}>
	                    <span></span>
	                    <p>ატვირთვა</p>
	                </li>
	            </ul>
	        </div>
		{% endif %}
		<input type="hidden" name="PostTable" value="{{ get.post_table }}">
		<input type="hidden" name="ID" value="{{ get.id }}">
		<input type="submit" value="შენახვა" class="btn btn-green m-top-20">
	</form>
	{% if Data.Data.has_photo is defined %}
		<form class="j-form files-list-form" name="files-list-form" data-index="0" method="post" enctype="multipart/form-data"
			 action="{{ server.URI }}file/uploadphoto/">
			<input type="hidden" name="Type" value="List">
		    <input type="hidden" name="UploadedFiles" class="files-list-uploaded-files" value="{{ Data.Data.has_photo }}">
		    <input type="file" name="Photo" class="files-list-file" onchange="_files.uploadFiles(this);">
		</form>
	{% endif %}
	<script type="text/javascript">_form.focus();</script>
{% endblock %}