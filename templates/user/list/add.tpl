{% extends "popup/index.tpl" %}

{% set popupProperties = {
    title : get.popup_title,
} %}

{% block content %}
	<form class="p-20 block-md" action="{{ server.URI }}admin/addlistitem/" onsubmit="return list.add(this);">
		{% for val in post_table.fields %}
			{% if val.input is defined %}
				<div class="form-group">
					{% if val.input == 'text' %}
						<p>{{ val.label }}</p>
						<div class="m-top-5">
							<input type="text" name="{{ val.name }}" {% if val.required %}data-error="მიუთითეთ - {{ val.label }}"{% endif %}>
						</div>
					{% elseif val.input == 'checkbox' %}
						<div class="m-top-5">
							<input type="checkbox" name="{{ val.name }}" id="{{ val.name }}" value="1" {{ val.checked ? 'checked' }}
							 {% if val.required %}data-error="მიუთითეთ - {{ val.label }}"{% endif %}>
							<label for="{{ val.name }}">{{ val.label }}</label>
						</div>
					{% endif %}
				</div>
			{% endif %}
		{% endfor %}
		{% for key, val in unserialize(constants.LANGS) %}
			<div class="form-group">
				<p>დასახელება - {{ key }}</p>
				<div class="m-top-5">
					<input type="text" name="title-{{ key }}" data-error="მიუთითეთ დასახელება - {{ key }}">
				</div>
			</div>
		{% endfor %}
		{% if post_table.upl_dir is defined %}
			<p class="m-top-20">სურათი ({{ constants.CAT_PHOTO_SIZE ~ 'X' ~ constants.CAT_PHOTO_SIZE }})</p>
			<div class="clearfix m-top-5">
	            <ul class="clearfix files-list files-list-cats" data-types="[jpg, png, jpeg, gif]" data-max-files="1"
	             data-name="File" data-index="0">
	                <li class="files-list-new-file shadow-inset" onclick="_files.selectFiles(this);">
	                    <span></span>
	                    <p>ატვირთვა</p>
	                </li>
	            </ul>
	        </div>
		{% endif %}
		<input type="hidden" name="PostTable" value="{{ get.post_table }}">
		<input type="hidden" name="EditItemTitle" value="{{ get.edit_item_title }}">
		<input type="hidden" name="AddSubItemTitle" value="{{ get.add_sub_item_title }}">
		{% if post_table.fields.parent_id is defined %}
			<input type="hidden" name="{{ post_table.fields.parent_id.name }}" value="{{ get.parent_id }}">
		{% endif %}
		<input type="submit" value="დამატება" class="btn btn-green m-top-20">
	</form>
	{% if post_table.upl_dir is defined %}
		<form class="j-form files-list-form" name="files-list-form" data-index="0" method="post" enctype="multipart/form-data"
			 action="{{ server.URI }}file/uploadphoto/">
			<input type="hidden" name="Type" value="List">
		    <input type="hidden" name="UploadedFiles" class="files-list-uploaded-files" value="0">
		    <input type="file" name="Photo" class="files-list-file" onchange="_files.uploadFiles(this);">
		</form>
	{% endif %}
	<script type="text/javascript">_form.focus();</script>
{% endblock %}