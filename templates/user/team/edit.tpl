{% extends "admin/index.tpl" %}

{% block section %}
	<form action="{{ server.URI }}admin/editteampost/" class="admin-news-form" onsubmit="return admin.savePost(this);">
		<h1>გუნდის წევრის რედაქტირება</h1>
		{% for lang, lang_id in unserialize(constants.LANGS) %}
			<p class="m-top-20">სახელი, გვარი - {{ lang }}</p>
			<p class="m-top-5">
				<input type="text" name="Fullname-{{ lang }}" value="{{ Data.Descr[lang_id].fullname }}" data-error="მიუთუთეთ დასახელება - {{ lang }}">
			</p>
			<p class="m-top-20">თანამდებობა - {{ lang }}</p>
			<p class="m-top-5">
				<input type="text" name="Position-{{ lang }}" value="{{ Data.Descr[lang_id].position }}" data-error="მიუთუთეთ თანამდებობა - {{ lang }}">
			</p>
		{% endfor %}
		<p class="m-top-20">სურათი ({{ constants.TEAM_PHOTO_SIZE ~ 'X' ~ constants.TEAM_PHOTO_SIZE }})</p>
		<div class="clearfix m-top-5">
            <ul class="clearfix files-list files-list-team" data-types="[jpg, png, jpeg, gif]" data-max-files="1" data-name="File" data-index="0">
                {% if Data.Data.has_photo %}
					<li data-file="{{ constants.UPL_TEAM ~ Data.Data.team_id ~ constants.IMG_TYPE }}">
						<img src="{{ constants.UPL_TEAM ~ Data.Data.team_id ~ constants.IMG_TYPE ~ '?v=' ~ Data.Data.photo_ver }}">
						<i onclick="_files.mainFile(this);" class="home" title="მთავარ ფაილად დაყენება"></i>
						<i onclick="_files.removeFile(this);" title="წაშლა"></i>
					</li>
				{% endif %}
                <li class="files-list-new-file shadow-inset" onclick="_files.selectFiles(this);"
                 {{ Data.Data.has_photo ? 'style="display:none;"' }}>
                    <span></span>
                    <p>ატვირთვა</p>
                </li>
            </ul>
        </div>
		<p class="m-top-20">
			<input type="hidden" id="TeamID" name="TeamID" value="{{ Data.Data.team_id }}">
			<input type="submit" class="btn" value="რედაქტირება">
		</p>
	</form>
	<form class="j-form files-list-form" name="files-list-form" data-index="0" method="post" enctype="multipart/form-data"
		 action="{{ server.URI }}file/uploadphoto/">
	    <input type="hidden" name="UploadedFiles" class="files-list-uploaded-files" value="{{ Data.Data.has_photo }}">
	    <input type="hidden" name="Type" value="Team">
	    <input type="file" name="Photo" class="files-list-file" onchange="_files.uploadFiles(this);">
	</form>
{% endblock %}

{% block mypage_footer %}
    <script type="text/javascript" src="{{ constants.LIBS_URL }}ckeditor/ckeditor.js?v={{ constants.FILE_VER }}"></script>
{% endblock %}