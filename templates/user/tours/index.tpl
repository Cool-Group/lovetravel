{% extends "admin/index.tpl" %}

{% block section %}
	<form class="admin-news-search clearfix" action="{{ server.URI }}admin/tours/">
		<div class="left">
			<a href="{{ server.URI }}admin/addtour/" class="btn btn-blue dib">დამატება</a>
		</div>
		<div class="right">
			<input type="text" placeholder="საძიებო სიტყვა" class="left m-left-5" name="Keyword" value="{{ get.Keyword }}">
			<input type="submit" class="btn btn-red left m-left-5" value="ძებნა">
		</div>
	</form>
	<ul class="admin-news clearfix">
		{% if Data|length %}
			{% for val in Data %}
				<li>
					<a href="{{ server.URI }}tours/{{ val.tour_id }}/" class="img col-3" target="_blank">
						<img src="{{ val.photos_cnt ? constants.UPL_TOURS ~ 'thumbs/' ~ val.tour_id ~ '_1' ~ constants.IMG_TYPE ~ '?v=' ~ val.photo_ver : constants.NO_PHOTO }}">
					</a>
					<div class="descr col-6">
						<a class="h2" href="{{ server.URI }}tours/{{ val.tour_id }}/">{{ val.title }}</a>
						<p class="h2 m-top-5">{{ val.short_descr }}</p>
					</div>
					<div class="options col-3">
						<div class="clearfix right">
							<span title="სტატუსი" class="status" onclick="admin.changePostStatus(this, 'tours', 'status_id', {{ val.tour_id }});">
								<i class="icon-16 star-16 {{ val.status_id ? 'green' : 'red' }}"></i>
							</span>
							<span title="მთავარ გვერდზე გამოჩენა" class="status" onclick="admin.changePostStatus(this, 'tours', 'is_main', {{ val.tour_id }});">
								<i class="icon-16 slider-1-16 {{ val.is_main ? 'green' : 'red' }}"></i>
							</span>
							<span title="წაშლა" class="delete" onclick="admin.deletePost(this, 'tours', {{ val.tour_id }});">
								<i class="icon-16 del-16"></i>
							</span>
							<a href="{{ server.URI }}admin/edittour/{{ val.tour_id }}/" title="რედაქტირება" class="edit">
								<i class="icon-16 edit-16"></i>
							</a>
						</div>
					</div>
				</li>
			{% endfor %}
		{% else %}
			<p class="data-not-found">ტურები არ მოიძებნა</p>
		{% endif %}
	</ul>
{% endblock %}