{% extends "admin/index.tpl" %}

{% block section %}
	<form action="{{ server.URI }}admin/edittourpost/" class="admin-news-form" onsubmit="return admin.savePost(this);">
		<h1>ტურის რედაქტირება</h1>
		<p class="m-top-20">კატეგორია</p>
		<div class="clearfix m-top-5">
            <select data-error="მიუთითეთ კატეგორია" name="CatID">
            	<option value="">აირჩიეთ</option>
            	{% include "admin/list/select.tpl" with {list: Cats, id: Data.Data.cat_id, disabled: true} only %}
            </select>
        </div>
        <p class="m-top-20">ტურის ტიპი</p>
		<div class="clearfix m-top-5">
            <select data-error="მიუთითეთ ტურის ტიპი" name="TypeID">
            	<option value="">აირჩიეთ</option>
            	{% for val in Types %}
					<option value="{{ val.type_id }}" {{ val.type_id == Data.Data.type_id ? 'selected' }}>{{ val.title }}</option>
            	{% endfor %}
            </select>
        </div>
        {% set tour_services = json_decode(Data.Data.services, true) %}
		<p class="m-top-20">სერვისები</p>
		<div class="m-top-5 clearfix">
			{% for val in Services %}
				<div class="col-4">
					<p><b>{{ val.title }}</b></p>
					<p class="clearfix">
						<input type="radio" name="Services[{{ val.service_id }}]" id="services-1-{{ val.service_id }}" value="1" {{ tour_services[val.service_id] == 1 ? 'checked' }}>
						<label for="services-1-{{ val.service_id }}">კი</label>
					</p>
					<p class="clearfix">
						<input type="radio" name="Services[{{ val.service_id }}]" id="services-2-{{ val.service_id }}" value="0" {{ tour_services[val.service_id]|length == 0 ? 'checked' }}>
						<label for="services-2-{{ val.service_id }}">არა</label>
					</p>
					<p class="clearfix">
						<input type="radio" name="Services[{{ val.service_id }}]" id="services-3-{{ val.service_id }}" value="2" {{ tour_services[val.service_id] == 2 ? 'checked' }}>
						<label for="services-3-{{ val.service_id }}">დამატებითი</label>
					</p>
				</div>
			{% endfor %}
		</div>
		{% set tour_notes = json_decode(Data.Data.notes, true) %}
		<p class="m-top-20">შენიშვნები</p>
		<div class="m-top-5 clearfix">
			{% for val in Notes %}
				<p class="clearfix">
					<input type="checkbox" name="Notes[]" id="note-{{ val.note_id }}" value="{{ val.note_id }}" {{ val.note_id in tour_notes ? 'checked' }}>
					<label for="note-{{ val.note_id }}">{{ val.title }}</label>
				</p>
			{% endfor %}
		</div>
		<p class="m-top-20">სასტუმროები</p>
		<div class="m-top-5">
			<textarea name="Hotels" data-ckeditor="true">{{ Data.Data.hotels }}</textarea>
		</div>
		<div class="m-top-20 clearfix">
			<div class="col-4">
				<p>ფასი - დან</p>
				<p class="m-top-5">
					<input type="text" name="PriceFrom" value="{{ Data.Data.price_from }}" data-type="double" data-error="მიუთუთეთ ფასი - დან">
				</p>
			</div>
			<div class="col-4">
				<p>ფასი - მდე</p>
				<p class="m-top-5">
					<input type="text" name="PriceTo" value="{{ Data.Data.price_to }}" data-type="double" data-error="მიუთუთეთ ფასი - მდე">
				</p>
			</div>
			<div class="col-4">
				<p>ვალუტა</p>
				<p class="m-top-5">
					<select name="CurrencyID">
						{% for val in Currencies %}
							<option value="{{ val.currency_id }}" {{ val.currency_id == Data.Data.currency_id ? 'selected' }}>{{ val.title }}</option>
						{% endfor %}
					</select>
				</p>
			</div>
		</div>
		<div class="m-top-20 clearfix">
			<div class="col-6">
				<p>პერიოდი - დან</p>
				<p class="m-top-5">
					<input type="text" name="PeriodFrom" value="{{ Data.Data.period_from|date('d.m.Y') }}" data-datepicker="true" data-error="მიუთუთეთ პერიოდი - დან">
				</p>
			</div>
			<div class="col-6">
				<p>პერიოდი - მდე</p>
				<p class="m-top-5">
					<input type="text" name="PeriodTo" value="{{ Data.Data.period_to|date('d.m.Y') }}" data-datepicker="true" data-error="მიუთუთეთ პერიოდი - მდე">
				</p>
			</div>
		</div>
		{% for lang, lang_id in unserialize(constants.LANGS) %}
			<p class="m-top-20">დასახელება - {{ lang }}</p>
			<p class="m-top-5">
				<input type="text" name="Title-{{ lang }}" value="{{ Data.Descr[lang_id].title }}" data-error="მიუთუთეთ დასახელება - {{ lang }}">
			</p>
			<p class="m-top-20">მოკლე აღწერა - {{ lang }}</p>
			<div class="m-top-5">
				<textarea name="ShortDescr-{{ lang }}" data-ckeditor="true">{{ Data.Descr[lang_id].short_descr }}</textarea>
			</div>
			<p class="m-top-20">აღწერა - {{ lang }}</p>
			<div class="m-top-5">
				<textarea name="Descr-{{ lang }}" data-ckeditor="true">{{ Data.Descr[lang_id].descr }}</textarea>
			</div>
		{% endfor %}
		<p class="m-top-20">მონიშნეთ რუკაზე</p>
		<div class="m-top-5 clearfix">
			<input id="MapCoords" type="hidden" name="MapCoords" value="{{ Data.Data.map_coords }}" />
            <div class="map-cont relative">
                <div id="map"></div>
                <input type="button" class="btn btn-yellow" value="რუკის გადიდება">
            </div>
		</div>
		<p class="m-top-20">სურათები ({{ constants.TOUR_LARGE_PHOTO_WIDTH ~ 'X' ~ constants.TOUR_LARGE_PHOTO_HEIGHT }})</p>
		<div class="clearfix m-top-5">
            <ul class="clearfix files-list files-list-tours" data-types="[jpg, png, jpeg, gif]" data-max-files="{{ constants.TOUR_MAX_PHOTOS }}"
             data-name="Photos" data-index="0">
                {% if Data.Data.photos_cnt %}
					{% for i in 1..Data.Data.photos_cnt %}
						<li data-file="{{ constants.UPL_TOURS ~ 'thumbs/' ~ Data.Data.tour_id ~ '_' ~ i ~ constants.IMG_TYPE }}">
							<img src="{{ constants.UPL_TOURS ~ 'thumbs/' ~ Data.Data.tour_id ~ '_' ~ i ~ constants.IMG_TYPE ~ '?v=' ~ Data.Data.photo_ver }}">
							<i onclick="_files.mainFile(this);" class="home" title="მთავარ სურათად დაყენება"></i>
							<i onclick="_files.removeFile(this);" title="წაშლა"></i>
						</li>
	                {% endfor %}
                {% endif %}
                <li class="files-list-new-file shadow-inset" onclick="_files.selectFiles(this);" {{ Data.Data.photos_cnt == constants.TOUR_MAX_PHOTOS ? 'style="display:none;"' }}>
                    <span></span>
                    <p>ატვირთვა</p>
                </li>
            </ul>
        </div>
		<p class="m-top-20">
			<input type="hidden" name="TourID" value="{{ Data.Data.tour_id }}">
			<input type="submit" class="btn" value="რედაქტირება">
		</p>
	</form>
	<form class="j-form files-list-form" name="files-list-form" data-index="0" method="post" enctype="multipart/form-data"
		 action="{{ server.URI }}file/uploadtourphotos/">
	    <input type="hidden" name="UploadedFiles" class="files-list-uploaded-files" value="0">
	    <input type="file" name="Files[]" class="files-list-file" multiple onchange="_files.uploadFiles(this);">
	</form>
{% endblock %}

{% block mypage_footer %}
	<script src="https://maps.googleapis.com/maps/api/js?language={{ globals.Lang.Code }}"></script>
    <script type="text/javascript" src="{{ constants.LIBS_URL }}ckeditor/ckeditor.js"></script>
    <script src="{{ constants.TWIG_THEME }}assets/js/build/map.min.js?v={{ constants.FILE_VER }}" type="text/javascript" ></script>
    <script type="text/javascript">map.init();</script>
{% endblock %}