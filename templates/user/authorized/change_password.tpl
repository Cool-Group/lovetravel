{% extends "user/index.tpl" %}

{% set sectionProperties = {
	title: langs.ChangePassword
} %}

{% block section %}
	<div class="row  user-forms-row align-items-start">
		<div class="col-5 ">
			<form class="user-forms" action="{{ server.URI }}user/changepass/" onsubmit="return user.changePassword(this);">
				<div class="form-group mb-3">
					<label for="CurrentPassword"><i>{{ langs.CurrentPassword }}:</i></label>
					<input type="password" name="CurrentPassword" class="form-control" id="CurrentPassword" minlength="6" data-minlength="6" data-error="{{ langs.WriteCurrentPassword }}">
				</div>
				<div class="form-group mb-3">
					<label for="formGroupExampleInput2">{{ langs.NewPassword }}:</label>
					<input type="password" name="Password" class="form-control" id="formGroupExampleInput2" minlength="6" data-minlength="6" data-error="{{ langs.WriteNewPassword }}">
				</div>
				<div class="form-group mb-3"> <!-- has-danger -->
					<label for="formGroupExampleInput2">{{ langs.RetypeNewPassword }}:</label>
					<input type="password" class="form-control" id="formGroupExampleInput2" minlength="6" data-equals="Password" data-error="{{ langs.PasswordsDontMatch }}">
				</div>
				<div class="form-group">
					<button class="btn btn-primary btn-block mt-5">{{ langs.Save }}</button>
				</div>
			</form>
		</div>
		<!-- <div class="col validate-col">
			<div class="form-group">
				<span class="validate">პაროლი არასწორია, გთხოვთ შეიყვანოთ სწორი პაროლი!</span>
			</div>
			<div class="form-group"></div>
			<div class="form-group">
				<span class="validate">პაროლები არ ემთხვევა</span>
			</div>
		</div> -->
	</div>
{% endblock %}