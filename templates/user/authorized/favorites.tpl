{% extends "user/index.tpl" %}

{% set sectionProperties = {
	title: langs.MyFavorites
} %}

{% block section %}
	{% if Data|length %}
        {% for val in Data %}
            <div class="row list-view-row align-items-center justify-content-between">
                <div class="box col-sm-4 p-0">
                    <div class="image" style="background-image: url('{{ constants.UPL_TOURS ~ 'thumbs/' ~ val.tour_id ~ '_1' ~ constants.IMG_TYPE ~ '?v=' ~ val.photo_ver }}')">
                        <a href="{{ server.URI }}tours/{{ val.tour_id }}/" class="full-view"><i class="ic-right-arrow float-left"></i> {{ langs.ReadMore }}</a>
                        <a href="#" class="add-to-fav active" onclick="return favorites.post(this, {{ val.tour_id }});">
                            <i class="ic-favorite float-left"></i> {{ langs.Favorite }}
                        </a>
                    </div>
                    {% if val.old_price > 0 %}
                        <div class="sale-badge">SALE -{{ langs.CurrencyTitles[val.currency_id] ~ ' ' ~ (val.price - val.old_price) }}</div>
                    {% endif %}
                </div>
                <div class="box col-sm-5 py-3">
                    <h3 class="mb-4">{{ Cats.List[val.cat_id].title }} <span class="float-right">{{ Period(val.period_from, val.period_to) }}</span></h3>
                    <div class="row">
                        <div class="col-md-12 destination mb-3">{{ val.title }}</div>
                        {% set tour_services, serv, add_serv = json_decode(val.services, true), "", "" %}
                        {% if tour_services|length %}
                            {% for service_id, type_id in tour_services %}
                                {% if type_id == 1 %}
                                    {% set serv = serv ~ '<i class="ic ' ~ TourServices[service_id].icon ~ ' active" title="' ~ TourServices[service_id].title ~ '"></i>' %}
                                {% else %}
                                    {% set add_serv = add_serv ~ '<i class="ic ' ~ TourServices[service_id].icon ~ '" title="' ~ TourServices[service_id].title ~ '"></i>' %}
                                {% endif %}
                            {% endfor %}
                            {% if serv != "" %}
                                <div class="col-4 service">{{ langs.Services }}:</div>
                                <div class="col-8">{{ serv|raw }}</div>
                            {% endif %}
                            {% if add_serv != "" %}
                                <div class="col-4 service mt-3">{{ langs.AddServices }}:</div>
                                <div class="col-8 mt-3">{{ add_serv|raw }}</div>
                            {% endif %}
                        {% endif %}
                    </div>
                </div>
                <div class="box col-sm-3 price">
                    <div class="row align-items-center text-center">
                        <div class="mx-auto">
                            {% if val.old_price > 0 %}
                                <span class="d-block"><del>{{ langs.CurrencyTitles[val.currency_id] ~ ' ' ~ val.old_price }}</del></span>
                            {% endif %}
                            <span class="d-block">{{ langs.CurrencyTitles[val.currency_id] ~ ' ' ~ val.price }}</span>
                        </div>
                    </div>
                </div>
            </div>
        {% endfor %}
        <div class="container-fluid">
            {% include "pagination/index.tpl" %}
        </div>
    {% endif %}
{% endblock %}