{% extends "user/index.tpl" %}

{% set sectionProperties = {
	title: langs.PersonalData
} %}

{% block section %}
	<div class="personal-info-table mb-5">
		<div class="row">
			<div class="col-md-4 text-right">{{ langs.UserID }}:</div>
			<div class="col-md-8"><b>{{ Data.user_id }}</b></div>
		</div>
		<div class="row">
			<div class="col-md-4 text-right">{{ langs.Name }}:</div>
			<div class="col-md-8"><b>{{ Data.name }}</b></div>
		</div>
		<div class="row">
			<div class="col-md-4 text-right">{{ langs.SurName }}:</div>
			<div class="col-md-8"><b>{{ Data.surname }}</b></div>
		</div>
		<div class="row">
			<div class="col-md-4 text-right">{{ langs.Gender }}:</div>
			<div class="col-md-8"><b>{{ Data.gender_id == 1 ? langs.Male : (Data.gender_id == 2 ? langs.FeMale) }}</b></div>
		</div>
		<div class="row">
			<div class="col-md-4 text-right">{{ langs.Country }}:</div>
			<div class="col-md-8"><b>{{ Countries[Data.country_id].title }}</b></div>
		</div>
		<div class="row">
			<div class="col-md-4 text-right">{{ langs.BirthDate }}:</div>
			<div class="col-md-8"><b>{{ Data.birth_date|date('d/m/Y') }}</b></div>
		</div>
		<div class="row">
			<div class="col-md-4 text-right">{{ langs.Address }}:</div>
			<div class="col-md-8"><b>{{ Data.address }}</b></div>
		</div>
		<div class="row">
			<div class="col-md-4 text-right">{{ langs.Email }}:</div>
			<div class="col-md-8"><b>{{ Data.email }}</b></div>
		</div>
		<div class="row">
			<div class="col-md-4 text-right">{{ langs.ContactPhone }}:</div>
			<div class="col-md-8"><b>{{ Data.phone }}</b></div>
		</div>
	</div>
{% endblock %}