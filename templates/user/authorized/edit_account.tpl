{% extends "user/index.tpl" %}

{% set sectionProperties = {
	title: langs.EditAccount
} %}

{% block section %}
	<div class="row user-forms-row align-items-start">
		<div class="col-5">
			<form action="{{ server.URI }}user/edit/" class="user-forms" onsubmit="return user.edit(this);">
				<div class="form-group mb-3">
					<label for="1"><i>{{ langs.UserID }}</i></label>
					<input type="text" class="form-control" value="{{ Data.user_id }}" readonly="" disabled="">
				</div>
				<div class="form-group mb-3">
					<label for="1"><i>{{ langs.Name }}</i></label>
					<input type="text" name="Name" class="form-control" value="{{ Data.name }}" data-error="{{ langs.WriteName }}">
				</div>
				<div class="form-group mb-3">
					<label for="1"><i>{{ langs.SurName }}</i></label>
					<input type="text" name="SurName" class="form-control" value="{{ Data.surname }}" data-error="{{ langs.WriteSurName }}">
				</div>
				<div class="form-group mb-3 ">
					<label for="1"><i>{{ langs.Gender }}</i></label>
					<div class="row pl-2">
						<div class="col-4">
							<label class="radio-label" for="11">{{ langs.Male }}</label>
							<input id="11" name="GenderID" type="radio" value="1" class="form-control" {{ Data.gender_id == 1 ? 'checked' }}>
						</div>
						<div class="col">
							<label class="radio-label" for="12">{{ langs.Female }}</label>
							<input id="12" name="GenderID" type="radio" value="2" class="form-control" {{ Data.gender_id == 2 ? 'checked' }}>
						</div>
					</div>
				</div>
				<div class="form-group mb-3">
					<label for="1"><i>{{ langs.BirthDate }}</i></label>
					<input id="datepicker1" type="text" name="BirthDate" class="form-control" value="{{ Data.birth_date|date('d/m/Y') }}" data-error="{{ langs.SelectBirthDate }}">
					<i class="ic-arrow-down select-arrow"></i>
				</div>
				<div class="form-group mb-3">
					<label for="1"><i>{{ langs.Country }}</i></label>
					<select name="CountryID" id="" class="form-control" data-error="{{ langs.SelectCountry }}">
						<option value="0">{{ langs.Select }}</option>
						{% for key, val in Countries %}
							<option value="{{ key }}" {{ key == Data.country_id ? 'selected' }}>{{ val.title }}</option>
						{% endfor %}
					</select>
					<i class="ic-arrow-down select-arrow"></i>
				</div>
				<div class="form-group mb-3">
					<label for="1"><i>{{ langs.Address }}</i></label>
					<input type="text" name="Address" class="form-control" value="{{ Data.address }}" data-error="{{ langs.WriteAddress }}">
				</div>
				<div class="form-group mb-3">
					<label for="1"><i>{{ langs.ContactPhone }}</i></label>
					<!-- <div class="d-flex">
						<input type="text" class="form-control col-3 border-right-0 rounded-0 rounded-left">
						<input type="text" class="form-control col-9 rounded-0 rounded-right">
					</div> -->
					<input type="text" name="Phone" class="form-control" value="{{ Data.phone }}" data-error="{{ langs.WriteContactPhone }}">
				</div>
				<div class="form-group mb-3">
					<label for="1"><i>{{ langs.Email }}</i></label>
					<input type="text" class="form-control" readonly="" disabled="" value="{{ Data.email }}">
				</div>
				<div class="form-group mb-3">
					<label for="1"><i>{{ langs.CurrentPassword }}</i></label>
					<input type="password" name="CurrentPassword" id="CurrentPassword" class="form-control" data-minlength="6" data-error="{{ langs.WriteCurrentPassword }}">
				</div>
				<div class="form-group mt-5">
					<button class="btn btn-primary btn-block">{{ langs.Save }}</button>
				</div>
			</form>
		</div>
	</div>
{% endblock %}