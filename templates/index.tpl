{% spaceless %}
<!DOCTYPE html>
<html lang="{{ globals.Lang.Code }}">

<head> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta charset="utf-8">
    <title>{{ metaProperties.title }} {{ server.Page }}</title>
    <meta name="description" lang="{{ globals.Lang.Code }}" content="{{ metaProperties.description }}" />
    <meta name="keywords" lang="{{ globals.Lang.Code }}" content="{{ metaProperties.keywords }}" />
    <meta property="og:image" content="{{ metaProperties.image }}" />
    <meta property="og:title" content="{{ metaProperties.title }}" />
    <meta property="og:description" content="{{ metaProperties.description }}" />
    <meta property="og:url" content="{{ server.URI }}{{ metaProperties.url }}" />
    <meta property="og:site_name" content="{{ constants.SITE }}" />
    <meta property="og:type" content="{{ metaProperties.type != '' ? metaProperties.type : 'website' }}" />
    <meta property="fb:app_id" content="{{ constants.FB_APP_ID }}">
    <meta property="fb:admins" content="{{ constants.FB_ADMINS }}" />
    <meta content="{{ globals.Lang.Code }}" http-equiv="Content-Language">
    <!-- <meta name="author" content="{{ constants.SITE }}" > -->
    <link href="{{ constants.URL }}favicon.ico?v=0.1" rel="shortcut icon" />
    <meta name="copyright" content="©{{ constants.SITE_YEAR }} {{ constants.SITE }}">
    <meta name="robots" content="all" />
    <meta name="revisit-after" content="1 days">
    <meta name="referrer" content="unsafe-url">
    <link href="{{ constants.TWIG_THEME }}assets/css/src/style.css?v={{ constants.FILE_VER }}" rel="stylesheet">
    {% block header %} {% endblock %} {% if GetBrowser().name == 'Apple Safari' %}
    <link href="{{ constants.TWIG_THEME }}assets/css/build/safari.min.css?v={{ constants.FILE_VER }}" rel="stylesheet"> {% endif %}
    <script type="text/javascript">
        var LANG = '{{ globals.Lang.Code }}'; 
        var THEME = '{{ constants.THEME }}';
        var URI = '{{ server.URI }}';
        var IMG_TYPE = '{{ constants.IMG_TYPE }}';
        var COOKIE_SITE_TIME = '{{ constants.COOKIE_SITE_TIME }}';
        var COOKIE_SITE_DOMAIN = '{{ constants.COOKIE_SITE_DOMAIN }}';
        var CACHE = '{{ constants.URL }}{{ constants.CACHE }}';
        var PAGE = '{{ server.Page }}';
        var ACTION = '{{ server.Action }}';
        var LOGGED_IN = {{ session.UserID ? 1 : 0 }};
    </script>
</head>

<body>
    {% set lang_titles = unserialize(constants.LANG_TITLES) %} {% if server.Page != 'admin' %}
    {% set social_media, contactcs_social_media = unserialize(constants.SOCIAL_MEDIA), json_decode(Contacts.social_media, true) %}
    <!-- As a heading -->
    <header>
        <nav class="navbar fixed-top navbar-light bg-faded {{ server.Page == 'main' ? ' main' }}">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="row align-items-center main-logo">
                            <a href="{{ server.URI }}"><img src="{{ constants.TWIG_THEME }}assets/img/logoLovetravel.svg" alt=""></a>
                        </div>
                    </div>
                    <div class="col-md-8 menu-col">
                        <div class="row justify-content-end">
                            <div class="col-10">
                                <div class="row justify-content-end text-right top-nav align-items-center">
                                    <div class="authentication d-inline-block">
                                        {% if session.UserID is defined %}
                                            <a href="{{ server.URI }}user/" class="d-inline-block">
                                                <i class="ic-user"></i>{{ session.Email }}</span>
                                            </a>
                                            <a href="{{ server.URI }}user/logout/" class="d-inline-block ml-1" title="{{ langs.LogOut }}">
                                                <i class="ic-sign-out-option"></i>
                                            </a>
                                        {% else %}
                                            <a href="{{ server.URI }}user/login/" class="d-inline-block"><i class="ic-user"></i>{{ langs.Authorize }}</a> / 
                                            <a href="{{ server.URI }}user/register/">{{ langs.Register }}</a>
                                        {% endif %}
                                    </div>
                                    <div class="langs d-inline-block">
                                        {% for lang in unserialize(constants.LANGS)|keys %}
                                            <a href="{{ constants.URL ~ lang ~ '/' ~ server.Page }}/" class="{{ lang == cookie.Lang ? 'active' }}" onclick="return changeLang('{{ lang }}');">
                                                <img src="{{ constants.TWIG_THEME }}assets/img/icons/{{ lang }}.svg" alt="">{{ lang_titles[lang] }}</a>
                                            </a>
                                        {% endfor %}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-end align-items-center bottom-nav">
                            <div class="d-inline-block menu-link">
                                <a href="{{ server.URI }}tours/">{{ langs.Tours }}</a>
                                <i class="ic-arrow-down"></i>
                                <div class="drop-down">
                                    <div class="d-flex">
                                        <div class="dropdown-groups">
                                            <div class="sub-title d-block">{{ langs.Directions }}</div>
                                            <div class="sub-menu-links">
                                                {% for key, val in Cats.Tree %}
                                                    <a class="sub-link d-block" href="{{ server.URI }}tours/?CatID={{ key }}">{{ val.title }}</a>
                                                {% endfor %}
                                            </div>
                                        </div>
                                        <div class="dropdown-groups">
                                            <div class="sub-title d-block">{{ langs.TourTypes }}</div>
                                            <div class="sub-menu-links">
                                                {% for key, val in Types %}
                                                    <a class="sub-link d-block" href="{{ server.URI }}tours/?Types[]={{ key }}">{{ val.title }}</a>
                                                {% endfor %}
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                            <div class="d-inline-block menu-link">
                                <a href="{{ server.URI }}services/">{{ langs.Services }}</a>
                                <i class="ic-arrow-down"></i>
                                <div class="drop-down">
                                    <div class="sub-menu-links">
                                        {% for val in Services %}
                                            <a class="sub-link d-block" href="{{ server.URI }}services/#content-{{ val.service_id }}">{{ val.title }}</a>
                                        {% endfor %}
                                    </div>
                                </div>
                            </div>
                            <a href="{{ server.URI }}airlinetickets/" class="d-inline-block menu-link">{{ langs.AirlineTickets }}</a>
                            <div class="d-inline-block menu-link">
                                <a href="{{ server.URI }}blog/">{{ langs.Blog }}</a>
                                <i class="ic-arrow-down"></i>
                                <div class="drop-down">
                                    <div class="sub-title d-block">{{ langs.Theme }}</div>
                                    <div class="sub-menu-links">
                                        {% for key, val in BlogCats %}
                                            <a class="sub-link d-block" href="{{ server.URI }}blog/?CatID={{ key }}">{{ val.title }}</a>
                                        {% endfor %}
                                    </div> 
                                </div>
                            </div>
                            <a href="{{ server.URI }}contact/" class="d-inline-block menu-link">{{ langs.Contact }}</a>
                        </div>
                    </div>
                </div>
            </div>
            <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse mobile-navbar navbar-toggleable" id="navbarNavAltMarkup">
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="{{ server.URI }}" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">ტურები</a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="{{ server.URI }}">ინდოეთი</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="{{ server.URI }}" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">სერვისი</a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="{{ server.URI }}">ინდოეთი</a>
                            <a class="dropdown-item" href="{{ server.URI }}">ინდოეთი</a>
                            <a class="dropdown-item" href="{{ server.URI }}">ინდოეთი</a>
                            <a class="dropdown-item" href="{{ server.URI }}">ინდოეთი</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="{{ server.URI }}" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">ავიაბილეთები</a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="{{ server.URI }}">ავიაბილეთები</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="{{ server.URI }}" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">ბლოგი</a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="{{ server.URI }}">ავიაბილეთები</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="{{ server.URI }}">კონტაქტი</a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <div class="header-mirror {{ server.Page == 'main' ? ' hidden' }}"></div>
    {% endif %}
    {% if server.Page == 'main' and Slider|length %}
        <!-- მთავარი სლაიდერი -->
        <div class="container-fluid">
            <!-- Swiper -->
            <div class="row d-block main-slider">
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        {% for val in Slider %}
                            <div class="swiper-slide" style="background-image:url('{{ constants.UPL_SLIDER ~ val.slider_id ~ constants.IMG_TYPE ~ '?v=' ~ val.photo_ver }}');">
                                <div class="container">
                                    <div class="row align-items-center text-center text-lg-left">
                                        <h1 class="mx-auto mx-lg-0">{{ val.title_1|raw }}<!-- <br>{{ val.title_2|raw }} --></h1>
                                    </div>
                                </div>
                            </div>
                        {% endfor %}
                    </div>
                    <!-- Add Pagination -->
                    <div class="swiper-pagination"></div>
                </div>
            </div>
        </div>
    {% endif %}
    {% block content %}
    {% endblock %}
    {% if server.Page != 'admin' %}
    <footer>
        <div class="container py-5">
            <div class="row py-5">
                <div class="col-md-4">
                    <ul class="contact-ul">
                        <li><div class="d-flex"><i class="ic-placeholder"></i> {{ Contacts.address }}</div></li>
                        <li><i class="ic-smartphone"></i> {{ Contacts.mobile }}</li>
                        <li><i class="ic-telephone"></i> {{ Contacts.phone }}</li>
                        <li><i class="ic-envelope"></i> {{ Contacts.email }}</li>
                        <li><i class="ic-messenger"></i> @Lovetraveltbilisi</li>
                    </ul>
                    <span class="lines"></span>
                    <ul class="work-hour-ul">
                        <li><i class="ic-event-date-and-time-symbol"></i>{{ Contacts.open_hours|raw }}</li>
                    </ul>
                    <span class="lines"></span>
                    <ul class="socials-ul">
                        <li>
                            {% for id, url in contactcs_social_media %}
                                <a href="{{ url }}" target="_blank"><i class="ic-{{ social_media[id] }}-square"></i></a>
                            {% endfor %}
                        </li>
                    </ul>
                    <span class="lines"></span>

                    <!-- <form class="send-form">
                        <h4>მოგვწერეთ</h4>
                        <div class="form-group">
                            <input type="text" class="form-control border-0" placeholder="თქვენი სახელი">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control border-0" placeholder="თქვენი ელ-ფოსტა">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control border-0" name="" id="" placeholder="გასაგზავნი ტექსტი"></textarea>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary mx-auto d-block border-0">გაგზავნა <i class="ic-right-arrow float-right"></i></button>
                        </div>
                    </form> -->

                </div>
                <div class="col-md-1"></div>
                <div class="col-md-6">{{ Contacts.short_descr|raw }}</div>
                <div class="col-md-1"></div>
            </div>
        </div>
        <div class="copyright d-flex align-items-center">
            <div class="container">
                © <a href="http://www.cgroup.ge/" target="_blank">CGroup</a> 2017
            </div>
        </div>
    </footer>
    {% endif %}
    </div>
    <script type="text/javascript" src="{{ constants.LANGS_DIR }}ka.js?v={{ constants.FILE_VER }}"></script>
    <script type="text/javascript" src="{{ constants.TWIG_THEME }}assets/js/build/libs/jquery.min.js?v={{ constants.FILE_VER }}"></script>
    <script type="text/javascript" src="{{ constants.TWIG_THEME }}assets/js/build/libs/jquery-ui.min.js?v={{ constants.FILE_VER }}"></script>
    <script type="text/javascript" src="{{ constants.TWIG_THEME }}assets/js/build/plugins/tether.min.js?v={{ constants.FILE_VER }}"></script>
    <script type="text/javascript" src="{{ constants.TWIG_THEME }}assets/js/build/libs/bootstrap.min.js?v={{ constants.FILE_VER }}"></script>
    <script type="text/javascript" src="{{ constants.TWIG_THEME }}assets/js/build/plugins/swiper.min.js?v={{ constants.FILE_VER }}"></script>
    <!--<script type="text/javascript" src="{{ constants.TWIG_THEME }}assets/js/build/plugins/slider.min.js?v={{ constants.FILE_VER }}"></script>-->
    <!--<script type="text/javascript" src="{{ constants.TWIG_THEME }}assets/js/build/plugins/parallax.min.js?v={{ constants.FILE_VER }}"></script>-->
    <script type="text/javascript" src="{{ constants.TWIG_THEME }}assets/js/build/scripts.min.js?v={{ constants.FILE_VER }}"></script>
    <script type="text/javascript" src="{{ constants.TWIG_THEME }}assets/js/build/plugins/plugins.min.js?v={{ constants.FILE_VER }}"></script>
    <script type="text/javascript" src="{{ constants.TWIG_THEME }}assets/js/build/plugins/jalerts.min.js?v={{ constants.FILE_VER }}"></script>
    <script type="text/javascript" src="{{ constants.TWIG_THEME }}assets/js/build/plugins/popup.min.js?v={{ constants.FILE_VER }}"></script>
    <script type="text/javascript" src="{{ constants.TWIG_THEME }}assets/js/build/plugins/cookie.min.js?v={{ constants.FILE_VER }}"></script>
    <script type="text/javascript">
        //
    </script>
    {% block footer %} {% endblock %}
</body>

</html>
{% endspaceless %}