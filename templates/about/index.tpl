{% extends "index.tpl" %}

{% set metaProperties = {
    title: constants.SITE ~ ' - ' ~ langs.MetaTitle,
    description: langs.MetaDescr,
    keywords: langs.MetaKeywords,
    image: constants.TWIG_THEME ~ 'images/' ~ constants.SITE ~ constants.IMG_TYPE,
    url: '/'
} %}

{% block content %}
    <div class="container">
        <h3>{{ langs.About }}</h3>
        <div class="mb-5">
            {{ Data|raw }}
            {% if Team|length %}
                <h3 class="mt-5">{{ langs.OurTeam }}</h3>
                <div class="row">
                    {% for val in Team %}
                        <div class="col-md">
                            <img src="{{ constants.UPL_TEAM ~ val.team_id ~ constants.IMG_TYPE ~ '?v=' ~ val.photo_ver }}" alt="{{ val.fullname ~ ' - ' ~ val.position }}">
                            <h5 class="mt-2 mb-0">{{ val.fullname }}</h6>
                            <h6 class="mt-2 mb-0">{{ val.position }}</h6>
                        </div>
                    {% endfor %}
                </div>
            {% endif %}
        </div>
    </div>
{% endblock %}