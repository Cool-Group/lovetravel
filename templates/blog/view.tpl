{% extends "index.tpl" %}

{% set metaProperties = {
    title: constants.SITE ~ ' - ' ~ langs.MetaTitle,
    description: langs.MetaDescr,
    keywords: langs.MetaKeywords,
    image: constants.TWIG_THEME ~ 'images/' ~ constants.SITE ~ constants.IMG_TYPE,
    url: '/'
} %}

{% block content %}
	<section class="section section-inset-2 padding-40">
        <div class="shell">
            <div class="range range-xs-center offset-top-25 offset-md-top-35">
                <div class="cell-sm-12 cell-md-12 offset-top-5">
                    <div class="post post-full">
                        <h3 class="text-bold">{{ Data.title }}</h3>
                        <ul class="list-inline list-inline-dashed text-italic offset-top-10">
                            <li>
                                <time datetime="{{ Data.insert_date }}" class="post-meta-time text-gray-lighter">{{ Data.insert_date|date('d-m-Y') }}</time>
                            </li>
                        </ul>
                        <h4 class="offset-top-10 text-gray-light">{{ Data.short_descr }}</h4>
                        {% if Data.photos_cnt %}
                            <img src="{{ constants.UPL_NEWS ~ 'large/' ~ Data.news_id ~ '_1' ~ constants.IMG_TYPE ~ '?v=' ~ Data.photo_ver }}" alt="{{ Data.title }}" class="img-responsive">
                        {% endif %}
                        <div>{{ Data.descr|raw }}</div>
                        <!-- <div class="offset-top-40 section-bottom-80">
                            <ul class="list-inline list-inline-md">
                                <li><a href="#" class="icon fa-facebook icon-primary"></a></li>
                                <li><a href="#" class="icon fa-twitter icon-primary"></a></li>
                            </ul>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </section>
{% endblock %}