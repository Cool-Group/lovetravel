{% extends "index.tpl" %}

{% set metaProperties = {
    title: constants.SITE ~ ' - ' ~ langs.MetaTitle,
    description: langs.MetaDescr,
    keywords: langs.MetaKeywords,
    image: constants.TWIG_THEME ~ 'images/' ~ constants.SITE ~ constants.IMG_TYPE,
    url: '/'
} %}

{% block content %}
	<div class="container blog-container py-5">
		<div class="row d-block section-row no-gutters">
			<span class="line"></span>
			<h1>{{ BlogCats[get.CatID] is defined ? BlogCats[get.CatID].title : langs.Blog }}</h1>
		</div>
		<div class="row">
			<div class="col-md-9">
				{% for val in Data %}
					<div class="row mb-4 blog-row">
						<div class="col-md-4">
							{% if val.photos_cnt > 0 %}
								<div class="image" style="background-image: url('{{ constants.UPL_BLOG ~ 'thumbs/' ~ val.blog_id ~ '_1' ~ constants.IMG_TYPE ~ '?v=' ~ val.photo_ver }}')"></div>
							{% else %}
								<div class="image"></div>
							{% endif %}
						</div>
						<div class="col-md-8">
							<div class="info-container">
								<h4>{{ val.title }}</h4>
								<ul class="list-unstyled list-inlin">
									<!-- <li class="list-inline-item"><i class="ic ic-user"></i> <i>ნინო</i></li> -->
									<li class="list-inline-item"><i class="ic ic-clock-o"></i> <i>{{ val.insert_date|date('d.m.Y') }}</i></li>
									<!-- <li class="list-inline-item"><i class="ic ic-hashtag"></i> <i>დასვეება ზღვაზე</i></li> -->
								</ul>
								<p>{{ val.short_descr|striptags|slice(0, 255)|raw }}</p>
								<a href="" class="btn btn-primary px-5">{{ langs.Read }}</a>
							</div>
						</div>
					</div>
				{% endfor %}
				<div class="container-fluid">
                    {% include "pagination/index.tpl" %}
                </div>
			</div>
			<div class="col-md-3">
				<h3 class="blog-list-title">{{ langs.SelectBlogTheme }}</h3>
				<div class="blog-nav">
					<h5>{{ langs.Theme }}</h5>
					<div class="list-group">
						{% for key, val in BlogCats %}
                            <a href="{{ server.URI }}blog/?CatID={{ key }}" class="list-group-item list-group-item-action {{ key == get.CatID ? 'active' }}">
                            	<i class="ic-right-arrow"></i> {{ val.title }}</span> <span class="count">({{ val.cnt }})</span>
                            </a>
                        {% endfor %}
					</div>
				</div>
			</div>
		</div>
	</div>
{% endblock %}