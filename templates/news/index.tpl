{% extends "index.tpl" %}

{% set metaProperties = {
    title: constants.SITE ~ ' - ' ~ langs.MetaTitle,
    description: langs.MetaDescr,
    keywords: langs.MetaKeywords,
    image: constants.TWIG_THEME ~ 'images/' ~ constants.SITE ~ constants.IMG_TYPE,
    url: '/'
} %}

{% block content %}
	<div class="container blog-container py-5">
		<div class="row d-block section-row no-gutters">
			<span class="line"></span>
			<h1>ბესტსელერები</h1>
		</div>
		<div class="row">
			<div class="col-md-9">
				<div class="row mb-4 blog-row">
					<div class="col-md-4">
						<div class="image" style="background-image: url('{{ constants.TWIG_THEME }}assets/img/background-08.jpg')"></div>
					</div>
					<div class="col-md-8">
						<div class="info-container">
							<h4>ფაქტი შავი ზღვის შესახებ. რატომ ჰქვია შავზღვას შავი</h4>
							<ul class="list-unstyled list-inlin">
								<li class="list-inline-item"><i class="ic ic-user"></i> <i>ნინო</i></li>
								<li class="list-inline-item"><i class="ic ic-clock-o"></i> <i>24 ივნისი, 2017</i></li>
								<li class="list-inline-item"><i class="ic ic-hashtag"></i> <i>დასვეება ზღვაზე</i></li>
							</ul>
							<p>
								მიუხედავად იმისა, რომ შავ ზღვაში წყალი შავი არ არის, ძველთაგანვე, სხვადასხვა ქვეყნების წარმომადგენლები, მას “შავს" ეძახდნენ. ზუსტად ცნობილი არ არის ვინ, როდის და ... რატომ დაარქვა, ან ასე რატომ აიტაცეს ეს სახელი. ამ საკითხთან დაკავშირებით, რამდენიმე ვერსია არსებობს. მათ შორის, ყველაშე გავრცელებული შემდეგი ვერსიებია:
								ერთი ვერსიის 
							</p>
							<a href="" class="btn btn-primary px-5">წაკითხვა</a>
						</div>
					</div>
				</div>
				<div class="row mb-4 blog-row">
					<div class="col-md-4">
						<div class="image" style="background-image: url('{{ constants.TWIG_THEME }}assets/img/background-08.jpg')"></div>
					</div>
					<div class="col-md-8">
						<div class="info-container">
							<h4>ფაქტი შავი ზღვის შესახებ. რატომ ჰქვია შავზღვას შავი</h4>
							<ul class="list-unstyled list-inlin">
								<li class="list-inline-item"><i class="ic ic-user"></i> <i>ნინო</i></li>
								<li class="list-inline-item"><i class="ic ic-clock-o"></i> <i>24 ივნისი, 2017</i></li>
								<li class="list-inline-item"><i class="ic ic-hashtag"></i> <i>დასვეება ზღვაზე</i></li>
							</ul>
							<p>
								მიუხედავად იმისა, რომ შავ ზღვაში წყალი შავი არ არის, ძველთაგანვე, სხვადასხვა ქვეყნების წარმომადგენლები, მას “შავს" ეძახდნენ. ზუსტად ცნობილი არ არის ვინ, როდის და ... რატომ დაარქვა, ან ასე რატომ აიტაცეს ეს სახელი. ამ საკითხთან დაკავშირებით, რამდენიმე ვერსია არსებობს. მათ შორის, ყველაშე გავრცელებული შემდეგი ვერსიებია:
								ერთი ვერსიის 
							</p>
							<a href="" class="btn btn-primary px-5">წაკითხვა</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<h3 class="blog-list-title">აირჩიეთ ბლოგის თემა</h3>
				<div class="blog-nav">
					<h5>თემა</h5>
					<div class="list-group"> 
						<a href="#" class="list-group-item list-group-item-action active"><i class="ic-right-arrow"></i> უახლესი ბლოგები</span> <span class="count">(12)</span></a>
						<a href="#" class="list-group-item list-group-item-action"><span><i class="ic-right-arrow"></i> დასვენება ზღვაზე</span><span class="count">(12)</span></a>
						<a href="#" class="list-group-item list-group-item-action"><span><i class="ic-right-arrow"></i> სამოგზაურო ხარჯების შემცირების კრეატიული გზები</span><span class="count">(12)</span></a>
						<a href="#" class="list-group-item list-group-item-action"><span><i class="ic-right-arrow"></i>საუკეთესო სეზონი მოგზაურობისათივის</span><span class="count">(12)</span></a>
						<a href="#" class="list-group-item list-group-item-action"><span><i class="ic-right-arrow"></i> დასვენება ზღვაზე</span><span class="count">(12)</span></a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- tickets -->
	<div class="container">
		<div class="row d-block section-row no-gutters"><span class="line"></span><h1><i class="ic-plane"></i> შერჩეული რეისები</h1></div>
		<div class="row user-nav-row">
			<div class="col-md-3 ">
				<div class="border-top pt-3">
					<a class="d-block" href="#">პირადი მონაცემები</a>
					<a class="d-block active" href="#">შერჩეული რეისები (8)</a>
					<a class="d-block" href="#">ანგარიშის რედაქტირება</a>
					<a class="d-block" href="#">პაროლის შეცვლა</a>
					<a class="d-block" href="#">ჩემი რჩეულები</a>
				</div>
			</div>
			<div class="col-md-9">
				<div class="ticket mb-5">
					<div class="ticket-header d-flex align-items-center">
						<div class="row full-width no-gutters">
							<div class="col text-uppercase">
								<div class="d-flex align-items-center">
									<img src="{{ constants.TWIG_THEME }}assets/img/logo-3.png" alt="">
									<span class="ml-2 name">turkish airlines pc 505</span>
								</div>
							</div>
							<div class="col text-right">
								<div class="d-flex full-height align-items-center justify-content-end">
									პირდაპირი ფრენა
								</div>
							</div>
						</div>
					</div>
					<div class="">
						<div class="flight-header d-flex align-items-center">
							<div class="row full-width">
								<div class="col-md"> 
									<span class="grey">გაფრენა: </span><span>თბილისი - სტამბული</span>
								</div>
								<div class="col-md">
									<span class="grey">ფრენის ხანგრძლივობა: </span><span>1სთ. 12წთ.</span>
								</div>
							</div>
						</div>
						<div class="flight-detail d-flex align-items-center">
							<div class="row">
								<div class="col-md">
									<div class="row">
										<div class="col-md-4">
											<div class="full-height d-flex align-items-center">
												<i class="out ic ic-down-right-arrow"></i>
												<span class="flight-time d-inline-block text-center ml-3">
													<span class="time d-block">22:00</span>
													<span class="date">23/07/2017</span>
												</span>
											</div>
										</div>
										<div class="col-md-8">
											<div class="full-height d-flex align-items-center">
												<div>
													<span class="city">
														<span class="text-capitalize">tbilisi </span>
														<span class="text-uppercase">(tbs)</span>
													</span>
													<span class="airport text-capitalize d-block">tbilisi international airport, georgia</span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md">
									<div class="row">
										<div class="col-md-4">
											<div class="full-height d-flex align-items-center">
												<i class="ic ic-down-right-arrow"></i>
												<span class="flight-time d-inline-block text-center ml-3">
													<span class="time grey d-block">23:12</span>
													<span class="date black">23/07/2017</span>
												</span>
											</div>
										</div>
										<div class="col-md-8">
											<div class="full-height d-flex align-items-center">
												<div>
													<span class="city">
														<span class="text-capitalize">tbilisi </span>
														<span class="text-uppercase">(tbs)</span>
													</span>
													<span class="airport text-capitalize d-block">tbilisi international airport, georgia</span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="flight-info">
							<div class="row mx-0">
								<div class="col-md-8 text-container">
									<div class="d-flex align-items-center py-4">
										<i class="ic-info-sign mx-3"></i>
										<div class="d-inline-block text">
											<div class="d-block">
												<div class="title">მგზავრი</div>
												<div class="description">ზრდასრული - 2</div>
											</div>
											<div class="d-block">
												<div class="title">ბილეთის ფასი</div>
												<div class="description">მასაკრებლის გარეშე: <span class="ml-5 float-right">270.00 GEL</span></div>
												<div class="description">გადასახადები:<span class="ml-5 float-right">0.00 GEL</span></div>
												<div class="description">მომსახურება:<span class="ml-5 float-right">110.09 GEL</span></div>
											</div>
											<div class="d-block">
												<div class="title">ბარგი</div>
												<div class="description">ბარგის დამატებითი საკომისიო</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 price-block">
									<div class="d-flex full-height align-items-center py-5">
										<div>
											<span class="d-block">ბილეთის ფასი</span>
											<span class="d-block price">82.00 <span class="text-uppercase currency ml-3">gel</span></span>
											<span class="d-block">2 მგზავრი: 164.00 <span class="text-uppercase">gel</span></span>
										</div>
									</div>
								</div>
							</div>
						</div> 
					</div>
				</div>
				<div class="ticket mb-5">
					<div class="ticket-header d-flex align-items-center">
						<div class="row full-width no-gutters">
							<div class="col text-uppercase">
								<div class="d-flex align-items-center">
									<img src="{{ constants.TWIG_THEME }}assets/img/logo-3.png" alt="">
									<span class="ml-2 name">turkish airlines pc 505</span>
								</div>
							</div>
							<div class="col text-right">
								<div class="d-flex full-height align-items-center justify-content-end">
									პირდაპირი ფრენა
								</div>
							</div>
						</div>
					</div>
					<div class="">
						<div class="flight-header d-flex align-items-center">
							<div class="row full-width">
								<div class="col-md"> 
									<span class="grey">გაფრენა: </span><span>თბილისი - სტამბული</span>
								</div>
								<div class="col-md">
									<span class="grey">ფრენის ხანგრძლივობა: </span><span>1სთ. 12წთ.</span>
								</div>
							</div>
						</div>
						<div class="flight-detail d-flex align-items-center">
							<div class="row">
								<div class="col-md">
									<div class="row">
										<div class="col-md-4">
											<div class="full-height d-flex align-items-center">
												<i class="out ic ic-down-right-arrow"></i>
												<span class="flight-time d-inline-block text-center ml-3">
													<span class="time d-block">22:00</span>
													<span class="date">23/07/2017</span>
												</span>
											</div>
										</div>
										<div class="col-md-8">
											<div class="full-height d-flex align-items-center">
												<div>
													<span class="city">
														<span class="text-capitalize">Frankfurt</span>
														<span class="text-uppercase"> (FRA) </span>
													</span>
													<span class="airport text-capitalize d-block">tbilisi international airport, georgia</span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md">
									<div class="row">
										<div class="col-md-4">
											<div class="full-height d-flex align-items-center">
												<i class="ic ic-down-right-arrow"></i>
												<span class="flight-time d-inline-block text-center ml-3">
													<span class="time grey d-block">23:12</span>
													<span class="date black">23/07/2017</span>
												</span>
											</div>
										</div>
										<div class="col-md-8">
											<div class="full-height d-flex align-items-center">
												<div>
													<span class="city">
														<span class="text-capitalize">tbilisi </span>
														<span class="text-uppercase">(tbs)</span>
													</span>
													<span class="airport text-capitalize d-block">tbilisi international airport, georgia</span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="flight-detail d-flex align-items-center">
							<div class="row">
								<div class="col-md">
									<div class="row">
										<div class="col-md-4">
											<div class="full-height d-flex align-items-center">
												<i class="out ic ic-down-right-arrow"></i>
												<span class="flight-time d-inline-block text-center ml-3">
													<span class="time d-block">22:00</span>
													<span class="date">23/07/2017</span>
												</span>
											</div>
										</div>
										<div class="col-md-8">
											<div class="full-height d-flex align-items-center">
												<div>
													<span class="city">
														<span class="text-capitalize">Frankfurt</span>
														<span class="text-uppercase"> (FRA) </span>
													</span>
													<span class="airport text-capitalize d-block">tbilisi international airport, georgia</span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md">
									<div class="row">
										<div class="col-md-4">
											<div class="full-height d-flex align-items-center">
												<i class="ic ic-down-right-arrow"></i>
												<span class="flight-time d-inline-block text-center ml-3">
													<span class="time grey d-block">23:12</span>
													<span class="date black">23/07/2017</span>
												</span>
											</div>
										</div>
										<div class="col-md-8">
											<div class="full-height d-flex align-items-center">
												<div>
													<span class="city">
														<span class="text-capitalize">tbilisi </span>
														<span class="text-uppercase">(tbs)</span>
													</span>
													<span class="airport text-capitalize d-block">tbilisi international airport, georgia</span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="flight-detail d-flex align-items-center">
							<div class="row">
								<div class="col-md">
									<div class="row">
										<div class="col-md-4">
											<div class="full-height d-flex align-items-center">
												<i class="out ic ic-down-right-arrow"></i>
												<span class="flight-time d-inline-block text-center ml-3">
													<span class="time d-block">22:00</span>
													<span class="date">23/07/2017</span>
												</span>
											</div>
										</div>
										<div class="col-md-8">
											<div class="full-height d-flex align-items-center">
												<div>
													<span class="city">
														<span class="text-capitalize">Frankfurt</span>
														<span class="text-uppercase"> (FRA) </span>
													</span>
													<span class="airport text-capitalize d-block">tbilisi international airport, georgia</span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md">
									<div class="row">
										<div class="col-md-4">
											<div class="full-height d-flex align-items-center">
												<i class="ic ic-down-right-arrow"></i>
												<span class="flight-time d-inline-block text-center ml-3">
													<span class="time grey d-block">23:12</span>
													<span class="date black">23/07/2017</span>
												</span>
											</div>
										</div>
										<div class="col-md-8">
											<div class="full-height d-flex align-items-center">
												<div>
													<span class="city">
														<span class="text-capitalize">tbilisi </span>
														<span class="text-uppercase">(tbs)</span>
													</span>
													<span class="airport text-capitalize d-block">tbilisi international airport, georgia</span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="flight-detail d-flex align-items-center">
							<div class="row">
								<div class="col-md">
									<div class="row">
										<div class="col-md-4">
											<div class="full-height d-flex align-items-center">
												<i class="out ic ic-down-right-arrow"></i>
												<span class="flight-time d-inline-block text-center ml-3">
													<span class="time d-block">22:00</span>
													<span class="date">23/07/2017</span>
												</span>
											</div>
										</div>
										<div class="col-md-8">
											<div class="full-height d-flex align-items-center">
												<div>
													<span class="city">
														<span class="text-capitalize">Frankfurt</span>
														<span class="text-uppercase"> (FRA) </span>
													</span>
													<span class="airport text-capitalize d-block">tbilisi international airport, georgia</span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md">
									<div class="row">
										<div class="col-md-4">
											<div class="full-height d-flex align-items-center">
												<i class="ic ic-down-right-arrow"></i>
												<span class="flight-time d-inline-block text-center ml-3">
													<span class="time grey d-block">23:12</span>
													<span class="date black">23/07/2017</span>
												</span>
											</div>
										</div>
										<div class="col-md-8">
											<div class="full-height d-flex align-items-center">
												<div>
													<span class="city">
														<span class="text-capitalize">tbilisi </span>
														<span class="text-uppercase">(tbs)</span>
													</span>
													<span class="airport text-capitalize d-block">tbilisi international airport, georgia</span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- user page password-->
	<div class="container">
		<div class="row d-block section-row no-gutters">
			<span class="line"></span>
			<h1>ბესტსელერები</h1>
		</div>
		<div class="row user-title py-4">
			<div class="col-md-3"></div>
			<div class="col-md-9">ანგარიშის რედაქტირება</div>
		</div>
		<div class="row user-nav-row">
			<div class="col-md-3 ">
				<div class="border-top pt-3">
					<a class="d-block" href="#">პირადი მონაცემები</a>
					<a class="d-block" href="#">ანგარიშის რედაქტირება</a>
					<a class="d-block active" href="#">პაროლის შეცვლა</a>
					<a class="d-block" href="#">ჩემი რჩეულები</a>
				</div>
			</div>
			<div class="col-md-9 border-top pt-3">
				<div class="row  user-forms-row align-items-start">
					<div class="col-5 ">
						<form class="user-forms ">
							<div class="form-group has-danger">
								<label for="formGroupExampleInput"><i>შეიყვანეთ არსებული პაროლი:</i></label>
								<input type="password" class="form-control" id="formGroupExampleInput">
								
							</div>
							<div class="form-group">
								<label for="formGroupExampleInput2">შეიყვანეთ ახალი პაროლი:</label>
								<input type="password" class="form-control" id="formGroupExampleInput2">
							</div>
							<div class="form-group has-danger">
								<label for="formGroupExampleInput2">გაიმეორეთ ახალი პაროლი:</label>
								<input type="password" class="form-control" id="formGroupExampleInput2">
							</div>
							<div class="form-group">
								<button class="btn btn-primary btn-block mt-5">შენახვა</button>
							</div>
						</form>
					</div>
					<div class="col validate-col">
						<div class="form-group">
							<span class="validate">პაროლი არასწორია, გთხოვთ შეიყვანოთ სწორი პაროლი!</span>
						</div>
						<div class="form-group"></div>
						<div class="form-group">
							<span class="validate">პაროლები არ ემთხვევა</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--  personal info -->
	<div class="container">
		<div class="row d-block section-row no-gutters">
			<span class="line"></span>
			<h1><i class="ic-user"></i> ჩემი გვერდი</h1>
		</div>
		<div class="row user-title py-4">
			<div class="col-md-3"></div>
			<div class="col-md-9">პირადი მონაცემები</div>
		</div>
		<div class="row user-nav-row">
			<div class="col-md-3 ">
				<div class="border-top pt-3">
					<a class="d-block active" href="#">პირადი მონაცემები</a>
					<a class="d-block" href="#">ანგარიშის რედაქტირება</a>
					<a class="d-block" href="#">პაროლის შეცვლა</a>
					<a class="d-block" href="#">ჩემი რჩეულები</a>
				</div>
			</div>
			<div class="col-md-9 border-top pt-3">
				<div class="personal-info-table">
					<div class="row">
						<div class="col-md-4 text-right">მომხმარებლის ID:</div>
						<div class="col-md-8"><b>0011</b></div>
					</div>
					<div class="row">
						<div class="col-md-4 text-right">სახელი:</div>
						<div class="col-md-8"><b>ადრიანა</b></div>
					</div>
					<div class="row">
						<div class="col-md-4 text-right">გვარი:</div>
						<div class="col-md-8"><b>ლიმა</b></div>
					</div>
					<div class="row">
						<div class="col-md-4 text-right">სქესი:</div>
						<div class="col-md-8"><b>მდედრობითი</b></div>
					</div>
					<div class="row">
						<div class="col-md-4 text-right">ქვეყანა:</div>
						<div class="col-md-8"><b>ბრაზილია</b></div>
					</div>
					<div class="row">
						<div class="col-md-4 text-right">დაბადების თარიღი:</div>
						<div class="col-md-8"><b>02/03/1980</b></div>
					</div>
					<div class="row">
						<div class="col-md-4 text-right">მისამართი:</div>
						<div class="col-md-8"><b>თბილისი, ფანასკერტელის 15ა</b></div>
					</div>
					<div class="row">
						<div class="col-md-4 text-right">ელ-ფოსტა:</div>
						<div class="col-md-8"><b>adrianalima@yahoo.com</b></div>
					</div>
					<div class="row">
						<div class="col-md-4 text-right">საკონტაქტო ნომერი:</div>
						<div class="col-md-8"><b>+995 597 23 45 23</b></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- user registration -->
	<div class="container">
		<div class="row d-block section-row no-gutters">
			<span class="line"></span>
			<h1><i class="ic-user"></i> ჩემი გვერდი</h1>
		</div>
		<div class="row user-forms-row">
			<div class="col-md-4">
				<form action="" class="user-forms">
					<div class="form-group mb-3">
						<label for="1"><i>სახელი</i></label>
						<input type="text" class="form-control">
					</div>
					<div class="form-group mb-3">
						<label for="1"><i>გვარი</i></label>
						<input type="text" class="form-control">
					</div>
					<div class="form-group mb-3 ">
						<label for="1"><i>სქესი</i></label>
						<div class="row pl-2">
							<div class="col-4">
								<label class="radio-label" for="11">მამრ.</label>
								<input id="11" name="gander" type="radio" class="form-control">
							</div>
							<div class="col">
								<label class="radio-label" for="12">მდედრ.</label>
								<input id="12" name="gander" type="radio" class="form-control">
							</div>
						</div>
					</div>
					<div class="form-group mb-3">
						<label for="1"><i>ქვეყანა</i></label>
						<select name="" id="" class="form-control"></select>
						<i class="ic-arrow-down select-arrow"></i>
					</div>
					<div class="form-group mb-3">
						<label for="1"><i>დაბადების თარიღი</i></label>
						<input id="datepicker1" type="text" class="form-control">
						<i class="ic-arrow-down select-arrow"></i>
					</div>
					<div class="form-group mb-3">
						<label for="1"><i>მისამართი</i></label>
						<input type="text" class="form-control">
					</div>
					<div class="form-group mb-3">
						<label for="1"><i>ელ-ფოსტა:</i></label>
						<input type="email" class="form-control">
					</div>
					<div class="form-group mb-3">
						<label for="1"><i>საკონტაქტო ნომერი:</i></label>
						<div class="d-flex">
						<input type="text" class="form-control col-3 border-right-0 rounded-0 rounded-left">
						<input type="text" class="form-control col-9 rounded-0 rounded-right">
						</div>
					</div>
					<div class="form-group mb-3">
						<label for="1"><i>ელ-ფოსტა:</i></label>
						<input type="email" class="form-control">
					</div>
					<div class="form-group mt-5">
						<button class="btn btn-primary btn-block">რეგისტრაცია</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	

{% endblock %}