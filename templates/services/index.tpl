{% extends "index.tpl" %}

{% set metaProperties = {
    title: constants.SITE ~ ' - ' ~ langs.MetaTitle,
    description: langs.MetaDescr,
    keywords: langs.MetaKeywords,
    image: constants.TWIG_THEME ~ 'images/' ~ constants.SITE ~ constants.IMG_TYPE,
    url: '/'
} %}

{% block content %}
    <div class="services">
    	{% for val in Services %}
            <div class="container" id="content-{{ val.service_id }}">
                <div class="row d-block section-row no-gutters mb-5">
                    <span class="line"></span>
                    <h1><i class="{{ val.icon }}"></i> {{ val.title }}</h1>
                </div>
                <div class="row">
                    <div class="col-md-5">
                        {% if val.photos_cnt > 0 %}
                            <div class="image" style="background-image: url('{{ constants.UPL_SERVICES ~ val.service_id ~ constants.IMG_TYPE ~ '?v=' ~ val.photo_ver }}')"></div>
                        {% else %}
                            <div class="image"></div>
                        {% endif %}
                    </div>
                    <div class="col-md-7">
                        <h3>{{ val.short_descr }}</h3>
                        <span class="small-line"></span>
                        <div>{{ val.descr|raw }}</div>
                    </div>
                </div>
        	</div>
        {% endfor %}
    </div>
    <div class="container-fluid subscription-container d-flex align-items-center mt-5" style="background-image: url({{ constants.TWIG_THEME }}assets/img/stock-photo-132271505.jpg)">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 text-center">
                    <img class="mx-auto mb-5" src="{{ constants.TWIG_THEME }}assets/img/logo-2.png" alt="">
                </div>
                <div class="col-2"></div>
                <div class="col-8 border"></div>
                <div class="col-2"></div>
            </div>
            <div class="row mt-5">
                <div class="form-group mx-auto">
                    <label class="text-center d-block" for="subscripe">გამოიწერეთ ჩვენი სიახლეები</label>
                    <input type="text" class="form-control border-0 text-center" id="subscripe" placeholder="ელ-ფოსტა">
                    <button class="btn btn-default"><i class="ic-right-arrow"></i></button>
                </div>
            </div>
        </div>
    </div>
{% endblock %}