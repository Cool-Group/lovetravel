{#
	Parameters:
	* PageVar (string) : get page var
	* Pagination.CurrentPage (int) : the current page you are in
	* Pagination.PerPage (int) : Content per page
	* ContentCount (int) : Content count
#}

{% if Pagination != false %}
	{% set Pages = Pagination.ContentCount / Pagination.PerPage|number_format %}
	{% if Pages > 1 %}
		<ul class="pagination row justify-content-end">
			{% if Pagination.PageVar == false %}
				{% set Pagination = Pagination|merge({PageVar: "Page"}) %}
			{% endif %}
			{% if Pagination.CurrentPage > 1 %}
				<li class="page-item">
					<a class="page-link arrow" href="{{ PopulateGet(Pagination.PageVar, Pagination.CurrentPage - 1) }}">
						<i class="ic-left-arrow"></i>
					</a>
				</li>
			{% endif %}
			{% if Pagination.CurrentPage > 5 %}
				<li class="page-item">
					<a class="page-link" href="{{ PopulateGet(Pagination.PageVar, 1) }}">1</a><span>...</span>
				</li>
			{% endif %}
			{% for i in 1..(Pages + 1) %}
				{% if (i - Pagination.CurrentPage)|abs < 5 %}
					{% if i == Pagination.CurrentPage %}
						<li class="page-item">
							<a class="page-link active" href="{{ PopulateGet(Pagination.PageVar, i) }}">{{ i }}</a>
						</li>
					{% else %}
						<li class="page-item">
							<a class="page-link" href="{{ PopulateGet(Pagination.PageVar, i) }}">{{ i }}</a>
						</li>
					{% endif %}
				{% endif %}
			{% endfor %}
			{% set LastPage = Pages|round(0, "ceil") %}
			{% if Pagination.CurrentPage + 4 < Pages %}
				<li class="page-item">
					<span>...</span><a class="page-link" href="{{ PopulateGet(Pagination.PageVar, LastPage) }}">{{ LastPage }}</a>
				</li>
			{% endif %}
			{% if Pagination.CurrentPage < Pages %}
				<li class="page-item">
					<a class="page-link arrow" href="{{ PopulateGet(Pagination.PageVar, Pagination.CurrentPage + 1) }}">
						<i class="ic-right-arrow"></i>
					</a>
				</li>
			{% endif %}
			{% if Pages >= 7 and false %}
				<li class="page-item">
					<input type="text" data-last-page="{{ LastPage }}" placeholder="{{ langs.Page }}">
				</li>
			{% endif %}
		</ul>
	{% endif %}
{% endif %}