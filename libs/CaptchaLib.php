<?php

class CaptchaLib
{
    private $BgPath;
    private $FonthPath;
    private $Config;

    public function __construct($Config = array())
    {
        // Check for GD library
        if(!function_exists('gd_info'))
        {
            throw new Exception('Required GD library is missing');
        }

        $this->BgPath       = ROOT . TPL . 'assets/img/captcha/';
        $this->FonthPath    = ROOT . TPL . 'assets/fonts/captcha/';
        $this->Config       = array(
            'Code'             => '',
            'MinLength'        => 5,
            'MaxLength'        => 5,
            'Backgrounds'      => array(
                $this->BgPath . '45-degree-fabric.png',
                $this->BgPath . 'cloth-alike.png',
                $this->BgPath . 'grey-sandbag.png',
                $this->BgPath . 'kinda-jean.png',
                $this->BgPath . 'polyester-lite.png',
                $this->BgPath . 'stitched-wool.png',
                $this->BgPath . 'white-carbon.png',
                $this->BgPath . 'white-wave.png'
            ),
            'Fonts' => array(
                $this->FonthPath . 'bpg_glaho.ttf'
            ),
            'Characters'      => 'ABCDEFGHJKLMNPRSTUVWXYZabcdefghjkmnprstuvwxyz23456789',
            'MinFontSize'     => 30,
            'MaxFontSize'     => 30,
            'Color'           => '#666',
            'AngleMin'        => 0,
            'AngleMax'        => 10,
            'Shadow'          => true,
            'ShadowColor'     => '#FFF',
            'ShadowOffsetX'   => -1,
            'ShadowOffsetY'   => 1
        );
        $this->EditConfig($Config);
    }

    private function EditConfig($Config)
    {
        // Overwrite defaults with custom Config values
        if(is_array($Config) && !empty($Config))
        {
            foreach($Config as $key => $value)
            {
                $this->Config[$key] = $value;
            }
        }

        // Restrict certain values
        if($this->Config['MinLength'] < 1) $this->Config['MinLength'] = 1;
        if($this->Config['AngleMin'] < 0) $this->Config['AngleMin'] = 0;
        if($this->Config['AngleMax'] > 10) $this->Config['AngleMax'] = 10;
        if($this->Config['AngleMax'] < $this->Config['AngleMin'] ) $this->Config['AngleMax'] = $this->Config['AngleMin'];
        if($this->Config['MinFontSize'] < 10) $this->Config['MinFontSize'] = 10;
        if($this->Config['MaxFontSize'] < $this->Config['MinFontSize'] ) $this->Config['MaxFontSize'] = $this->Config['MinFontSize'];
    }

    public function Get($Config = array())
    {
        // Generate CAPTCHA code if not set by user
        if(empty($this->Config['Code']))
        {
            $this->Config['Code']   = '';
            $Length                 = mt_rand($this->Config['MinLength'], $this->Config['MaxLength']);
            
            while(strlen($this->Config['Code']) < $Length)
            {
                $this->Config['Code'] .= substr($this->Config['Characters'], mt_rand() % (strlen($this->Config['Characters'])), 1);
            }
        }

        return array(
            'Key'   => md5($this->Config['Code'] . time()),
            'Val'   => $this->Config['Code'],
            'Image' => $this->Output()
        );
    }

    private function Hex2rgb($HexStr, $ReturnStr = false, $Seperator = ',')
    {
        $HexStr     = preg_replace("/[^0-9A-Fa-f]/", '', $HexStr); // Gets a proper hex string
        $RGBArray   = array();

        if(strlen($HexStr) == 6)
        {
            $ColorVal       = hexdec($HexStr);
            $RGBArray['r']  = 0xFF & ($ColorVal >> 0x10);
            $RGBArray['g']  = 0xFF & ($ColorVal >> 0x8);
            $RGBArray['b']  = 0xFF & $ColorVal;
        }
        elseif(strlen($HexStr) == 3)
        {
            $RGBArray['r'] = hexdec(str_repeat(substr($HexStr, 0, 1), 2));
            $RGBArray['g'] = hexdec(str_repeat(substr($HexStr, 1, 1), 2));
            $RGBArray['b'] = hexdec(str_repeat(substr($HexStr, 2, 1), 2));
        }
        else
        {
            return false;
        }
        
        return $ReturnStr ? implode($Seperator, $RGBArray) : $RGBArray;
    }

    public function Output()
    {
        // Pick random background, get info, and start captcha
        $Background                                 = $this->Config['Backgrounds'][mt_rand(0, count($this->Config['Backgrounds']) -1)];
        list($BgWidth, $BgHeight, $BgType, $BgAttr) = getimagesize($Background);

        $Captcha    = imagecreatefrompng($Background);
        $Color      = $this->Hex2rgb($this->Config['Color']);
        $Color      = imagecolorallocate($Captcha, $Color['r'], $Color['g'], $Color['b']);

        // Determine text angle
        $Angle = mt_rand($this->Config['AngleMin'], $this->Config['AngleMax'] ) * (mt_rand(0, 1) == 1 ? -1 : 1);

        // Select font randomly
        $Font = $this->Config['Fonts'][mt_rand(0, count($this->Config['Fonts']) - 1)];

        // Verify font file exists
        if(!file_exists($Font)) throw new Exception('Font file not found: ' . $Font);

        //Set the font size.
        $FontSize       = mt_rand($this->Config['MinFontSize'], $this->Config['MaxFontSize']);
        $TextBoxSize    = imagettfbbox($FontSize, $Angle, $Font, $this->Config['Code']);

        // Determine text position
        $BoxWidth       = abs($TextBoxSize[6] - $TextBoxSize[2]);
        $BoxHeight      = abs($TextBoxSize[5] - $TextBoxSize[1]);
        $TextPosXMin    = 0;
        $TextPosXMax    = ($BgWidth) - ($BoxWidth);
        $TextPosX       = mt_rand($TextPosXMin, $TextPosXMax);
        $TextPosYMin    = $BoxHeight;
        $TextPosYMax    = ($BgHeight) - ($BoxHeight / 2);
        
        if($TextPosYMin > $TextPosYMax)
        {
            $TempTextPosY   = $TextPosYMin;
            $TextPosYMin    = $TextPosYMax;
            $TextPosYMax    = $TempTextPosY;
        }

        $TextPosY = mt_rand($TextPosYMin, $TextPosYMax);

        // Draw shadow
        if($this->Config['Shadow'])
        {
            $ShadowColor = $this->Hex2rgb($this->Config['ShadowColor']);
            $ShadowColor = imagecolorallocate($Captcha, $ShadowColor['r'], $ShadowColor['g'], $ShadowColor['b']);
            
            imagettftext($Captcha, $FontSize, $Angle, $TextPosX + $this->Config['ShadowOffsetX'], $TextPosY + $this->Config['ShadowOffsetY'], $ShadowColor, $Font, $this->Config['Code']);
        }

        // Draw text
        imagettftext($Captcha, $FontSize, $Angle, $TextPosX, $TextPosY, $Color, $Font, $this->Config['Code']);

        ob_start();
        // Output image
        //header("Content-type: image/png");
        imagepng($Captcha);
        $ImageData = ob_get_contents();
        ob_end_clean(); 

        return 'data:image/png;base64,' . base64_encode($ImageData);
    }
}