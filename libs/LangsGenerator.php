<?php

class LangsGenerator 
{	
	public $DB          = NULL;
	public $Langs       = array();
	public $LangsFolder = 'langs/';
	
	public function __construct($DB, $Print = false)
	{
		$this->DB = $DB;
		
		foreach (unserialize(LANGS) as $Lang => $LangID) {
			//echo $Lang . 'pele<br>';
			$this->Langs[$Lang]['js']   = array();
			$this->Langs[$Lang]['json'] = array();
			$this->FillLangs($Lang, $LangID);
			$this->SaveLangFiles($Lang, $Print);
		}
	}
	
	public function FillLangs($Lang, $LangID)
	{	
		$Data = $this->DB->GetAll('SELECT l.lang_var, l.lang_var_js, d.title
									 FROM langs l
								LEFT JOIN lang_descr d ON d.lang_var_id = l.lang_var_id AND d.lang_id = ?i', $LangID);

		if (count($Data)) {
			foreach ($Data as $Val) {
				$this->FillLang($Lang, $Val['lang_var'], $Val['title'], $Val['lang_var_js']);
			}
		}
		
		$Data = $this->DB->GetAll('SELECT currency_id, title, symbol FROM currencies');

		foreach ($Data as $Val) {
			$this->FillLang($Lang, 'Currencies[' . $Val['currency_id'] . ']', $Val['title'], 1);
			$this->FillLang($Lang, 'CurrencyTitles[' . $Val['currency_id'] . ']', $Val['symbol'], 1);
		}
	}
	
	public function FillLang($Lang, $Var, $Val, $JS)
	{
		$Var = explode('[', $Var);
		
		switch (count($Var)) {	
			case 2:
				$this->Langs[$Lang]['json'][$Var[0]][substr($Var[1], 0, -1)] = $Val;
				if ($JS == 1) {
					$this->Langs[$Lang]['js'][$Var[0]][substr($Var[1], 0, -1)] = $Val;
				}
				break;
			
			case 3:
				$this->Langs[$Lang]['json'][$Var[0]][substr($Var[1], 0, -1)][substr($Var[2], 0, -1)] = $Val;
				if ($JS == 1) {
					$this->Langs[$Lang]['js'][$Var[0]][substr($Var[1], 0, -1)][substr($Var[2], 0, -1)] = $Val;
				}
				break;
			
			default:
				$this->Langs[$Lang]['json'][$Var[0]] = $Val;
				if ($JS == 1) {
					$this->Langs[$Lang]['js'][$Var[0]] = $Val;
				}
				break;
		}
	}
	
	public function SaveLangFiles($Lang, $Print)
	{	
		if ($Print) {
			echo '<pre>';
			print_r($this->Langs);
			echo json_encode($this->Langs[$Lang]['json'], true);
		}
		
		file_put_contents($this->LangsFolder . $Lang . '.json', json_encode($this->Langs[$Lang]['json'], true));
		file_put_contents($this->LangsFolder . $Lang . '.js', 'var langs = ' . json_encode($this->Langs[$Lang]['js'], true));
	}
}
