<?php

class Functions 
{
	public static function IsEmail($Email)
	{
		if(preg_match("/[-a-zA-Z0-9_.+]+@[a-zA-Z0-9-]+.[a-zA-Z]+/", $Email) > 0)
		{
			return true;
		}
		
		return false;
	}
	
	public static function PageInation($CurrentPageNumber, $ListNumberInPage, $ListCount, $PageName = 'Page')
	{
		$PageStr = '';
		$Pages   = $ListCount / $ListNumberInPage;
		
		if($Pages <= 1)
		{
			return $PageStr;
		}
		
		$CurrentPageNumber = (int)$CurrentPageNumber;
		
		if($CurrentPageNumber == 1)
		{
			$PageStr = '';
		}
		else
		{
			$PageStr = '<a href="' . (self::PopulateGet($PageName, $CurrentPageNumber - 1)) . '">&laquo;</a>';
		}
		
		if($CurrentPageNumber > 5){
			$PageStr .= '<a href="' . (self::PopulateGet($PageName, 1)) . '">1</a><span>...</span>';
		}
		
		for($i = 1; $i < $Pages + 1; $i++)
		{
			if(abs($i - $CurrentPageNumber) < 5)
			{
				if($i == $CurrentPageNumber)
				{
					$PageStr .= '<a class="active" href="' . (self::PopulateGet($PageName, $i)) . '">' . $i . '</a>'; 
				}
				else
				{
					$PageStr .= '<a href="' . (self::PopulateGet($PageName, $i)) . '">' . $i . '</a>';
				}
			}
		}
		
		$LastPage    = ceil($Pages);
		$LastPageBtn = '<span>...</span><a href="' . (self::PopulateGet($PageName, $LastPage)) . '">'.$LastPage.'</a>';
		
		if($CurrentPageNumber + 4 < $Pages)
		{
			$PageStr .= $LastPageBtn;
		}
		
		if($CurrentPageNumber < $Pages)
		{
			$PageStr .= '<a href="' . (self::PopulateGet($PageName, $CurrentPageNumber + 1)) . '">&raquo;</a>';
		}
		
		if($Pages >= 7)
		{
			//PageStr += '<input type="text" last_page="'+LastPage+'" placeholder="'+lang_data.page+'">';	
		}
		
		return '<div class="pageination row"><div class="unselect">' . $PageStr . '</div></div>';
	}
	
	public static function PopulateGet($Req = false, $Par = false)
	{
		$Gets = $Res = array();
		
		if($_SERVER['QUERY_STRING'] != '')
		{
			$Quer = explode('&', rtrim($_SERVER['QUERY_STRING'], '&'));
			
			for($i = 1; $i < count($Quer); $i++)
			{
				$Gets[substr($Quer[$i], 0, strpos($Quer[$i], '='))] = substr($Quer[$i], strpos($Quer[$i], '=') + 1);
			}
		}
		
		if ($Req !== false && $Req != '') {
			if (is_array($Req)) {
				foreach ($Req as $Key => $Val) {
					$Gets[$Key] = $Val;
				}
			} elseif($Par !== false) {
				$Gets[$Req] = $Par;
			}
		}
		
		foreach($Gets as $Key => $Val)
		{
			if(self::NotSpace($Key))
			{
				$Res[] = $Key.'='.$Val;
			}
		}
		
		return str_replace('url=', URL, $Quer[0]) . (count($Res) ? '?' . implode('&', $Res) : '');
	}
	
	public static function NotSpace($Var)
	{
		return strlen(str_replace(' ', '', trim($Var))) > 0;
	}
	
	public static function ObjectToArray($Object)
	{
		if(is_object($Object))
		{
			$Object = get_object_vars($Object);
		}
		
		return $Object;
 	}

 	public static function EmailToStars($Email)
	{
		//myemail@email.com -> my*****@ma***.com
		$Index = strpos($Email, '@');

		for($i = 2; $i < $Index; $i++)
		{
			$Email[$i] = '*';
		}

		for($i = $Index + 3; $i < strrpos($Email, '.'); $i++)
		{
			$Email[$i] = '*';
		}

		return $Email;
	}

	public static function GeneratePassword($Length = 9, $Strength = 0)
	{
		$Vowels     = 'aeuy';
		$Consonants = 'bdghjmnpqrstvz';

		if($Strength & 1)
		{
			$Consonants .= 'BDGHJLMNPQRSTVWXZ';
		}

		if($Strength & 2)
		{
			$Vowels .= 'AEUY';
		}

		if ($Strength & 4)
		{
			$Consonants .= '23456789';
		}

		if ($Strength & 8)
		{
			$Consonants .= '@#$%';
		}

		$Password = '';
		$Alt      = time() % 2;

		for($i = 0; $i < $Length; $i++)
		{
			if($Alt == 1)
			{
				$Password .= $Consonants[(rand() % strlen($Consonants))];
				$Alt       = 0;
			}
			else
			{
				$Password .= $Vowels[(rand() % strlen($Vowels))];
				$Alt 	   = 1;
			}
		}

		return $Password;
	}

	public static function GetPage($RowsNum, $PageName = 'Page')
	{
		$Resp             = array();
		$Resp[$PageName]  = Request::Req($PageName) > 1 ? Request::Req($PageName) : 1;
		$Resp['StartRow'] = ($Resp[$PageName] - 1) * $RowsNum;

		return $Resp;
	}

    /**
     * 
     * @param string $Message message to show
     * @param int $MessageID optional
     *   <pre>Can be 
     *          -1      : show message with close button;
     *          false   : show mwssage without close button;
     *          > 0     : show with close button, once closed won't show again on reload</pre>
     * @return string
     */
	public static function Message($Message, $MessageID = false, $TypeID = 0, $Classes = '')
	{
        $Types 		= array(0 => 'warning', 1 => 'success', 2 => 'error');
        $IDs 		= isset($_COOKIE['siteMessage']) ? explode(',', $_COOKIE['siteMessage']) : array();
        $Classes 	= (isset($Types[$TypeID]) ? $Types[$TypeID] : '') . (empty($Classes) ? '' : ' ' . $Classes);

        
        if($MessageID > 0 && in_array($MessageID, $IDs))
        {
            return '';
        }
        
        $CloseBtn = $MessageID !== false ? ('<i class="notification-close" title="' . Lang::Get('Close') . '" onClick="siteMessage.close(this, ' . $MessageID . ');"></i>') : '';
        
		return '<div class="notification pre-line ' . $Classes . '"><i class="notification-icon"></i>' . $CloseBtn . '<div class="row pr-descr">' . $Message . '</div></div>';
	}

	public static function Price($Price, $CurrencyID)
	{
		$Price = ($Price == (int)$Price) ? (int)$Price : $Price;
		$CurrencySymbols = Lang::Get('CurrencySymbols');
		return $Price ? $Price . ' ' . $CurrencySymbols[$CurrencyID] : '<span class="price-agr">' . Lang::Get('PriceWithAgreement') . '</span>';
	}

	public static function YuotubeEmbed($URL, $Width = 250, $Height = 141)
	{
		$URL = str_replace(array('http://', 'https://'), array('', ''), $URL);
		$VID = '';

		if(substr($URL, 0, 8) == 'youtu.be')
		{
			$VID = substr($URL, strpos($URL, '/') + 1);		
		}
		else
		{
			$VID = explode('v=', $URL);
			$VID = explode('&', $VID[1]);
			$VID = $VID[0];
		}

		$URL = 'https://www.youtube.com/embed/' . $VID . '?enablejsapi=1';

		return '<iframe width="' . $Width . '" height="' . $Height . '" src="' . $URL . '" frameborder="0" allowfullscreen></iframe>';
	}

	public static function ThumbPhoto($PhotoDir = NULL, $Photo = 0, $PhotoVer = 0, $Classes = '')
	{
		return ' class="img' . ($Classes != '' ? ' ' . $Classes : '') . ($PhotoDir == NULL ? ' no-photo' : '') . '"' . 
				($PhotoDir != NULL ? ' style="background-image: url(' . PHOTOS . 'thumbs/' . $PhotoDir . '/' . $Photo . IMG_TYPE . '?v=' . $PhotoVer . ');"': '');
	}

	public static function PrURL($PrID, $Title)
	{
		return URL . Lang::GetLang() . '/product/view/' . str_replace(' ', '-', $Title) . '-' . $PrID . '/';
	}

	public static function Date($Date, $Hour = true, $Type = 0)
	{
		$Date    = explode(' ', $Date);
		$Date[0] = explode('-', $Date[0]);
		$Months  = Lang::Get('Months');
		switch($Type)
		{
			case 1:
				return $Date[0][2] . ' ' . $Months[(int)$Date[0][1]];
			break;
			
			default:
				return $Date[0][2] . '/' . $Date[0][1] . '/' . substr($Date[0][0], 2, 2) . ($Hour ? ' ' . substr($Date[1], 0, 5) : '');
			break;
		}
	}

	public static function ChangeDate($Period)
	{
		$CurrentDate = date('Y-m-d H:i:s');
		$ChangeTime  = strtotime(date('Y-m-d H:i:s', strtotime($CurrentDate)) . $Period);
		
		return date('Y-m-d H:i:s', $ChangeTime);
	}

	public static function EmailForm($Text)
	{
		return
		'<div style="width:100%;display:inline-block;margin:0;padding:0;background-color:#e2e2e2;
		font-family:\'lucida grande\',tahoma,verdana,arial,sans-serif;font-size:13px;line-height:20px;">
			<div style="width:700px;display:block;margin:0 auto;">
				<div style="width:100%;display:inline-block;background-color:#FFF;margin:0 0 20px 0;">
					<p style="margin:0;height:40px;background-color:#FFBC18">
						<a href="' . URL . '" style="height:40px;display:inline-block;color:#FFF;
						margin-left:15px;line-height:38px;font-size:20px;text-decoration:none;">' . SITE . '</a>
					</p>
					<div style="width:100%;margin:20px 0;">
						<div style="margin:0 20px;">' . $Text . '</div>
					</div>
				</div>
			</div>
		</div>';
	}

	public static function PromDays($EndDate)
	{
		$Days = max(0, (strtotime($EndDate) - time()) / 60 / 60 / 24);

		if($Days < 0.1)
		{
			$Days = floor($Days * 24 * 60);

			return $Days < 60 ? $Days . ' ' . Lang::Get('Minute') : 1 . ' ' . Lang::Get('Hour');
		}
		elseif($Days < 1)
		{
			return floor($Days * 24)  . ' ' . Lang::Get('Hour');
		}

		return round($Days) . ' ' . Lang::Get('Day');
	}

	public static function Period($StartDate, $EndDate)
	{
		$Days = max(0, (strtotime($EndDate) - strtotime($StartDate)) / 60 / 60 / 24);

		if($Days < 0.1)
		{
			$Days = floor($Days * 24 * 60);

			return $Days < 60 ? $Days . ' ' . Lang::Get('Minute') : 1 . ' ' . Lang::Get('Hour');
		}
		elseif($Days < 1)
		{
			return floor($Days * 24)  . ' ' . Lang::Get('Hour');	
		}

		return round($Days) . ' ' . Lang::Get('Day');
	}

	public static function FullAddress($Locs, $LocID, $Address = false, &$Data = false, $ReturnArray = false)
	{
		if($LocID == 0)
		{
			if(empty($Data['Locs']))
			{
				$Data['Locs'] = array();
			}
			
			$Data['Locs']		   			= array_reverse($Data['Locs'], true);
			$CurrentLoc 					= $Locs[end(array_keys($Data['Locs']))];
			$Data['ShippingPrice'] 			= $CurrentLoc['sp'];
			$Data['FreeShippingMinAmount'] 	= $CurrentLoc['fsma'];
			$Data['LocsStr'] 				= implode(' / ', $Data['Locs']) . (!empty($Address) ? ' / ' . $Address : '');

			return $ReturnArray ? $Data : $Data['LocsStr'];
		}
		
		$Data['Locs'][$LocID] = $Locs[$LocID]['t'];
		
		return self::FullAddress($Locs, $Locs[$LocID]['p'], $Address, $Data, $ReturnArray);
	}

	private static function CheckHoliday($Date, $HoliDays)
	{
		return ! in_array(date('Y-m-d', strtotime($Date)), $HoliDays) && ! in_array(date('N', strtotime($Date)), array(6, 7));
	}

	public static function ShippingDate($Tbilisi = true, $HoliDays = array())
	{   
		if ($Tbilisi) {
            $Loop 		= TBILISI_SHIPPING_DAYS;
            $Days 		= 0;
            $Deffered 	= true;
            while (true) {
                $ShippingDate = self::ChangeDate('+' . $Days . ' day');
                if (self::CheckHoliday($ShippingDate, $HoliDays)) {
                    if ($Loop <= 0) {
                        return self::Date($ShippingDate, false, 1);
                    }
                    $Loop--;
                } elseif ($Deffered) {
                    $Loop--;
                    $Deffered = false;
                }
                $Days++;
            }
		}

		$Days 	= unserialize(REGION_SHIPPING_DAYS);
        $Diff 	= 0;
        sort($Days);
        $MinDay = $Days[0];
        
        foreach ($Days as &$Day) {
            $Loop = $Day == $MinDay ? $Day : $Day - $MinDay;
            while (true) {
                $ShippingDate = self::ChangeDate('+' . $Diff . ' day');
                if (self::CheckHoliday($ShippingDate, $HoliDays)) {
                    if ($Loop <= 0) {
                        break;
                    }
                    $Loop --;
                }
                $Diff++;
            }
            $Day = $ShippingDate;
        }
		$DateFrom	= explode(' ', $Days[0]);
		$DateFrom 	= explode('-', $DateFrom[0]);
		$DateTo		= explode(' ', $Days[1]);
		$DateTo		= explode('-', $DateTo[0]);
		
		if ($DateFrom[1] == $DateTo[1]) {
			$Months = Lang::Get('Months');
			return $DateFrom[2] . ' - ' . $DateTo[2] . ' ' . $Months[(int)$DateTo[1]];
		}
		$MonthsShort = Lang::Get('MonthsShort');
		return $DateFrom[2] . ' ' . $MonthsShort[(int)$DateFrom[1]] . ' - ' . $DateTo[2] . ' ' . $MonthsShort[(int)$DateTo[1]];
	}


	public static function URL($URL)
	{
		$URL = parse_url($URL);
		
		return '//' . (!is_null($URL['host']) ? $URL['host'] : $URL['path']);
	}

	public static function PhoneFormat($Phone = false, $Type = 0)
	{
		switch($Type)
		{
			case 1:
				$Data 		= array();
				$Data[2] 	= substr($Phone, strlen(PHONE_PREFIX));
				$Data[0] 	= substr($Data[2], 0, 3);
				$Data[1] 	= substr($Data[2], 3);
			break;
			
			default:
				$Data = '(+' . PHONE_PREFIX . ') ' . substr($Phone, strlen(PHONE_PREFIX), 3) . ' ' . 
						substr($Phone, strlen(PHONE_PREFIX) + 3, 3) . ' ' . substr($Phone, strlen(PHONE_PREFIX) + 6);
			break;
		}

		return $Data;
	}

	public static function Convert($Text, $Lang)
	{
		$NewText = $Text;
		
		switch(strtolower($Lang))
		{
			case 'en':
				
				$KaSymbols = array('ა','ბ','გ','დ','ე','ვ','ზ','თ','ი','კ','ლ','მ','ნ','ო','პ','ჟ','რ','ს',
								   'ტ','უ','ფ','ქ','ღ','ყ','შ','ჩ','ც','ძ','წ','ჭ','ხ','ჯ','ჰ');
				$EnSymbols = array('a','b','g','d','e','v','z','T','i','k','l','m','n','o','p','j','r','s',
								   't','u','f','q','R','y','sh','ch','c','Z','w','W','x','j','h');
				
				$i 			= 0;
				$NewText 	= '';
				
				while(mb_substr($Text, $i, 1, 'utf-8') || mb_substr($Text, $i, 1, 'utf-8') == '0')
				{
					$Symbol = mb_substr($Text, $i, 1, 'utf-8');
					
					if(in_array($Symbol, $KaSymbols))
					{
						$NewText .= $EnSymbols[array_search($Symbol, $KaSymbols)];
					}
					else
					{
						$NewText .= $Symbol;
					}

					$i++;
				}
			
			break;
			
			case 'ka':
				
				$KaSymbols = array('ა','ბ','გ','დ','ე','ვ','ზ','თ','ი','კ','ლ','მ','ნ','ო','პ','ჟ','რ','ს',
								   'ტ','უ','ფ','ქ','ღ','ყ','შ','ჩ','ც','ძ','წ','ჭ','ხ','ჯ','ჰ','ხ','ც','ძ');
				$EnSymbols = array('a','b','g','d','e','v','z','T','i','k','l','m','n','o','p','j','r','s',
								   't','u','f','q','R','y','sh','ch','c','Z','w','W','x','j','h','kh','ts','dz');
				
				$i 			= 0;
				$NewText 	= '';

				while(mb_substr($Text, $i, 1, 'utf-8') || mb_substr($Text, $i, 1, 'utf-8') == '0')
				{
					$Symbol = strtolower(mb_substr($Text, $i, 2, 'utf-8'));
					
					if(in_array($Symbol,$EnSymbols))
					{
						$NewText .= $KaSymbols[array_search($Symbol, $EnSymbols)];
						$i       += 2;
					}
					else
					{
						$Symbol = strtolower(mb_substr($Text, $i, 1, 'utf-8'));
						
						if(in_array($Symbol, $EnSymbols))
						{
							$NewText .= $KaSymbols[array_search($Symbol, $EnSymbols)];
						}
						else
						{
							$NewText .= $Symbol;
						}

						$i++;
					}
				}

			break;
			
			case 'ru':
				
				$KaSymbols = array('ა','ბ','გ','დ','ე','ვ','ზ','თ','ი','კ','ლ','მ','ნ','ო','პ','ჟ','რ','ს',
								   'ტ','უ','ფ','ქ','ღ','ყ','შ','ჩ','ც','ძ','წ','ჭ','ხ','ჯ','ჰ','ხ','ც','ძ');
				$RuSymbols = array('а','б','г','д','е','в','з','Т','и','к','л','м','н','о','п','ж','р','с',
									'т','у','ф','k','г','k','щ','ч','ц','дз','тс','ч','х','дж','x','х','ц','дз');
				
				$i 			= 0;
				$NewText 	= '';
				
				while(mb_substr($Text, $i, 1, 'utf-8') || mb_substr($Text, $i, 1, 'utf-8') == '0')
				{
					$Symbol = strtolower(mb_substr($Text, $i, 1, 'utf-8'));
					
					if(in_array($Symbol,$KaSymbols))
					{
						$NewText .= $RuSymbols[array_search($Symbol, $KaSymbols)];
					}
					else
					{
						$NewText.= $Symbol;
					}

					$i += 1;
				}

			break;
		}

		return $NewText;
	}

	public static function FullTextFilter($Str)
	{
		return str_replace(array('+', '-', '@', '>', '<', '(', ')', '~', '*', '"', '?'), array(' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '), $Str);
	}

	public static function GetBrowser()
	{ 
	    $UserAgent 	= $_SERVER['HTTP_USER_AGENT']; 
	    $BrName 	= 'Unknown';
	    $Platform 	= 'Unknown';
	    $Version 	= "";

	    //First get the platform?
	    if (preg_match('/linux/i', $UserAgent))
	    {
	        $Platform = 'linux';
	    }
	    elseif (preg_match('/macintosh|mac os x/i', $UserAgent))
	    {
	        $Platform = 'mac';
	    }
	    elseif (preg_match('/windows|win32/i', $UserAgent))
	    {
	        $Platform = 'windows';
	    }
	    
	    // Next get the name of the useragent yes seperately and for good reason
	    if(preg_match('/MSIE/i',$UserAgent) && !preg_match('/Opera/i',$UserAgent)) 
	    { 
	        $BrName = 'Internet Explorer'; 
	        $UB 	= "MSIE"; 
	    } 
	    elseif(preg_match('/Firefox/i',$UserAgent)) 
	    { 
	        $BrName = 'Mozilla Firefox'; 
	        $UB 	= "Firefox"; 
	    } 
	    elseif(preg_match('/Chrome/i',$UserAgent)) 
	    { 
	        $BrName = 'Google Chrome'; 
	        $UB 	= "Chrome"; 
	    } 
	    elseif(preg_match('/Safari/i',$UserAgent)) 
	    { 
	        $BrName = 'Apple Safari'; 
	        $UB 	= "Safari"; 
	    } 
	    elseif(preg_match('/Opera/i',$UserAgent)) 
	    { 
	        $BrName = 'Opera'; 
	        $UB 	= "Opera"; 
	    } 
	    elseif(preg_match('/Netscape/i',$UserAgent)) 
	    { 
	        $BrName = 'Netscape'; 
	        $UB 	= "Netscape"; 
	    } 
	    
	    // finally get the correct version number
	    $Knownn = array('Version', $UB, 'other');
	    $Pattern = '#(?<browser>' . join('|', $Knownn) . ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
	    
	    if (!preg_match_all($Pattern, $UserAgent, $matches))
	    {
	        // we have no matching number just continue
	    }
	    
	    // see how many we have
	    $i = count($matches['browser']);
	    if ($i != 1)
	    {
	        //we will have two since we are not using 'other' argument yet
	        //see if version is before or after the name
	        if (strripos($UserAgent,"Version") < strripos($UserAgent,$UB))
	        {
	            $Version = $matches['version'][0];
	        }
	        else {
	            $Version = $matches['version'][1];
	        }
	    }
	    else
	    {
	        $Version = $matches['version'][0];
	    }
	    
	    // check if we have a number
	    if ($Version == null || $Version == "")
	    {
	    	$Version = "?";
		}
	    
	    return array(
	        'userAgent' => $UserAgent,
	        'name'      => $BrName,
	        'version'   => $Version,
	        'platform'  => $Platform,
	        'pattern'   => $Pattern
	    );
	}

	public static function LogFile($Data)
	{
		if($h = fopen('log.txt', 'a'))
		{ 
			fwrite($h, $Data . "\n");
			fclose($h);
		}
	}

	public static function ShuffleArray($Array)
	{
		shuffle($Array);

		return $Array;
	}

	public static Function SetRequestPar($Par)
	{
		$Data = array();
		$Keys = array('Attrs');

		if(Request::Get($Par))
		{
			if(in_array($Par, $Keys))
			{
				foreach(explode(PAR_SPLITTER, Request::Get($Par)) as $Key => $Val)
				{
					$Data[substr($Val, 0, strpos($Val, PAR_VALUES_SPLITTER))] = explode(PAR_VALUES_SPLITTER, substr($Val, strpos($Val, PAR_VALUES_SPLITTER) + 1));
				}
			}
			else
			{
				$Data = explode(PAR_VALUES_SPLITTER, Request::Get($Par));
			}
			
			Request::SetGet($Par, $Data);
		}

	}

	public static function SearchFormParams($Key = false, $Val = false)
	{
		$Params = array('UserID', 'LocID', 'Keyword', 'PriceFrom', 'PriceTo', 'CondTypeID', 'Prom', 'SetTypeID');
		$Resp 	= array();

		if($Key !== false)
		{
			unset($Params[$Key]);
		}

		foreach($Params as $Val_1)
		{
			if(Request::Get($Val_1) !== false)
			{
				$Resp[] = $Val_1 . '=' . ($Val_1 == 'Prom' ? implode(PAR_VALUES_SPLITTER, Request::Get($Val_1)) : Request::Get($Val_1));
			}
		}

        if($Key !== false)
		{
            $Resp[] = $Key . '=' . $Val;
        }
		
		return '?' . implode('&', $Resp);
	}
}