<?php

class Email
{
	public static function Send($To, $From, $Subject, $Message)
	{
		if (Functions::IsEmail($To) == false || Functions::IsEmail($From) == false) {
			return false;
		}

		$Headers  = "From: " . strip_tags($From) . "\r\n";
		$Headers .= "Reply-To: ". strip_tags($From) . "\r\n";
		$Headers .= "MIME-Version: 1.0\r\n";
		$Headers .= "Content-Type: text/html; charset=UTF-8\r\n";

		return mail($To, $Subject, str_replace('\n', '<br/>', $Message), $Headers);
	}
}

?>