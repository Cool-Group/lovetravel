<?php

Class SMS
{
	public function Send($Phone, $Message)
	{
		return true;

		$Response = file_get_contents(str_replace(
						array(
							'{Phone}',
							'{Message}'
						),
						array(
							$this->CorrectPhone($Phone),
							urlencode($Message)
						),
						SMS_SEND_URL
					));
		$Response = explode(' ', $Response);
		
		if($Response[0] == 'OK')
		{
			return true; 
		}

		return false;
	}

	public function CorrectPhone($Phone)
	{
		if(substr($Phone, 0, 3) != '995')
		{
			$Phone = '995' . $Phone;
		}

		return $Phone;
	}

	public function RandCode()
	{
		return rand(10000, 99999);
	}
}

?>