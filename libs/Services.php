<?php

/**
* Services Client
*/
class Services
{
	private static $URL = SERVICES_URL;

	public static function Post($Func, $ShowType = 'JSON', $Params = null)
	{
		$Params 	= is_array($Params) ? $Params : array();
		$PostData 	= array_merge(
			Request::Req(),
			$Params
		);

		$Data = Curl::Post(self::$URL . $Func, $PostData);

		return strtoupper($ShowType) == 'ARRAY' ? json_decode($Data, true) : $Data;
	}
}