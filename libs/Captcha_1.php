<?php

class Captcha
{
	public static function Get()
	{
		$Captcha = Services::Post('GetCaptcha', 'ARRAY')['Data'];

		self::Set($Captcha['Key'], $Captcha['Val']);
		
		return $Captcha;
	}

	public static function Set($Key, $Val)
	{
		Session::Push('Captcha', $Key, $Val);
	}

	public static function Check()
	{
		return !empty(Session::Get('Captcha')[Request::Post('CaptchaKey')]) && strtolower(Session::Get('Captcha')[Request::Post('CaptchaKey')]) == strtolower(Request::Post('CaptchaVal'));
	}

	public static function _Unset()
	{
		return Session::_Unset('Captcha', Request::Post('CaptchaKey'));
	}

	public function SubmitService($Func, $Params = null, $Type = 'JSON')
	{
		if(!self::Check())
		{
			$Data = array('StatusCode' => -1, 'StatusMessage' => Lang::Get('IncorrectCaptcha'));
		}
		else
		{
			$Data = Services::Post($Func, 'ARRAY', $Params);

			if($Data['StatusCode'] == 1)
			{
				self::_Unset();
			}
		}

		if(strtoupper($Type) == 'ARRAY')
		{
			return $Data;
		}

		echo json_encode($Data);
	}	
}