<?php

class Captcha
{
	public static function Get()
	{
		$CaptchaLib = new CaptchaLib();
		$Captcha 	= $CaptchaLib->Get();

		self::Set($Captcha['Key'], $Captcha['Val']);
		
		return $Captcha;
	}

	public static function Set($Key, $Val)
	{
		Session::Push('Captcha', $Key, $Val);
	}

	public static function Check($JSON = true)
	{
		$Captchas 	= Session::Get('Captcha');
		$Check 		= ! empty($Captchas[Request::Post('CaptchaKey')]) && strtolower($Captchas[Request::Post('CaptchaKey')]) == strtolower(Request::Post('CaptchaVal'));

		if (! $Check && $JSON) {
			echo json_encode(array('StatusCode' => -1, 'StatusMessage' => Lang::Get('IncorrectCaptcha')));
			exit;
		}

		return $Check;
	}

	public static function _Unset()
	{
		return Session::_Unset('Captcha', Request::Post('CaptchaKey'));
	}
}