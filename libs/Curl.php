<?php

class Curl 
{
	public static function Post($URL, $Data)
	{
		self::BuildQuery($Data, $Params);

		/*if (BRANCH == 'PRODUCTION') {
			$Curl = curl_init();
			
			curl_setopt($Curl, CURLOPT_URL, $URL);
			curl_setopt($Curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($Curl, CURLOPT_POST, true);
			curl_setopt($Curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($Curl, CURLOPT_REFERER, SITE);
			curl_setopt($Curl, CURLOPT_POSTFIELDS, self::ImplodeQuery($Params));
			
			//echo serialize(Curl::ImplodeQuery($Params));
			
			$Result = curl_exec($Curl);
			
			curl_close($Curl);
		} else {*/
			$Result = file_get_contents($URL . '?' . self::ImplodeQuery($Params));
		//}
		
		return $Result;
	}

	public static function GetPage($URL)
	{
	    $Curl = curl_init();

        curl_setopt($Curl, CURLOPT_URL, $URL);
        curl_setopt($Curl, CURLOPT_AUTOREFERER, true);
        curl_setopt($Curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($Curl, CURLOPT_HEADER, false);
        curl_setopt($Curl, CURLOPT_FOLLOWLOCATION, true);
        
        $Result = curl_exec($Curl);
        
        curl_close($Curl);
        
        return $Result;
	}
	
	public static function BuildQuery($Array, &$NewArray = array(), $Prefix = NULL)
	{
		$Array = is_object($Array) ? get_object_vars($Array) : $Array;
	
		foreach($Array as $Key => $Val)
		{
			$NewKey = isset($Prefix) ? $Prefix . '[' . $Key . ']' : $Key;

			if(is_array($Val) || is_object($Val))
			{
				self::BuildQuery($Val, $NewArray, $NewKey);
			}
			else
			{
				$NewArray[$NewKey] = $Val;
			}
		}
	}
	
	public static function ImplodeQuery($Array)
	{
		$Result = array();
		
		if (count($Array)) {
			foreach ($Array as $Key => $Val) {
				$Result[] = $Key . '=' . urlencode($Val);
			}
		}
		return implode('&', $Result);
	}
}